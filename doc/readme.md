<br>

**CRUD**<br>
version: 1.0<br>
author: Lukas Potthast<br>

<br><br><br><br>

**INSTALLATION**

1. Execute a) or b):

        a) composer global require "laravel/installer"
        b) php composer.phar global require "laravel/installer"
    
2. Execute a) or b): ...

        a) composer create-project --prefer-dist laravel/laravel myprojectname
        b) php composer.phar create-project --prefer-dist laravel/laravel myprojectname
    
3. ... and initialize your new installation (config files!). 
    
4. Copy the "laravel-packages" folder next to your new laravel installation. The folder structure should look like this:

        ../
        |--- laravel-packages/
        |    |--- crud/
        |--- myprojectname/
        |    |--- .env
        |    |--- server.php
        |    |--- ...
    
5. Add the following code to the composer.json file of your new laravel installation:
    
        "repositories": [
            {
                "type": "path",
                "url": "../laravel-packages/crud",
                "options": {
                    "symlink": true
                }
            }
        ],
        
        "require": {
            "lukaspotthast/crud": "dev-master"
        },
        
6. Run a) or b):

        a) composer update
        b) php composer.phar update
        
7. Create a test table in your database:

        CREATE TABLE `test` (
          `id` int(11) NOT NULL,
          `name` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        
8. Add this example route to the routes/web.php file:

        Route::get('test', function () {
            $crud = Crud::get_instance('test');
            $crud->table('test');
        
            return $crud->render();
        });
        
9. Open the "/test" route in your web browser :)

10. If you want to load the assets in your main view, use the following lines of code.

        <?= Crud_Assets::get_if_not_autoloaded('jquery') ?>
        <?= Crud_Assets::get_if_not_autoloaded('jcrop') ?>
        <?= Crud_Assets::get_if_not_autoloaded('jqueryui') ?>
        <?= Crud_Assets::get_if_not_autoloaded('popper.js') ?>
        <?= Crud_Assets::get_if_not_autoloaded('bootstrap4') ?>
        <?= Crud_Assets::get_if_not_autoloaded('fontawesome') ?>
        <?= Crud_Assets::get_if_not_autoloaded('copybtn') ?>
        <?= Crud_Assets::get_if_not_autoloaded('datetimepicker') ?>
        <?= Crud_Assets::get_if_not_autoloaded('tinymce') ?>
        <?= Crud_Assets::get_if_not_autoloaded('select2') ?>
        <?= Crud_Assets::get_if_not_autoloaded('signature-pad') ?>
        <?= Crud_Assets::get_if_not_autoloaded('moment.js') ?>
        <?= Crud_Assets::get_if_not_autoloaded('validate.js') ?>
        <?= Crud_Assets::get_if_not_autoloaded('highlight.js') ?>
        <?= Crud_Assets::get_if_not_autoloaded('spectrum') ?>
        <?= Crud_Assets::get_if_not_autoloaded('crud.css') ?>
        <?= Crud_Assets::get_if_not_autoloaded('crud.js') ?>


<br><br><br><br>

**Field layout:**



<br><br><br><br>

**Validation:**

Field inputs can easily get validated by using the Crud_Validation_Ruleset object which gets provided per field through 
a call to:

    public function get_validation_ruleset(string $field_name) : Crud_Validation_Ruleset
    
For example:

    $ruleset = $crud->get_validation_ruleset('username');
    $ruleset->add_rule('presence', true);
    $ruleset->add_rule('length', ['minimum' => 8, 'maximum' => 64]);

This code defines that the field '*username*'<br>
1. needs to be set/present
1. needs to be at least 8 characters long
2. but can not be longer then 64 characters.

The following rules are available to you:

<br>

***'datetime'***

| option                     | comment |
| :-------                   | --- |
| true                       |  Is date ( e.g. 2013-12-11 10:09:08 ) |
| ['dateOnly' => true]       |  Is date ( e.g. 2013-12-11 )
| ['earliest' => '2013-12-11']     |  Date value cannot be before the specified date.
| ['latest' => '2013-12-11']       |  Date value cannot be after the specified date.
| ['message' => 'error...']  |  Custom error message.<br> You can use the placeholders <br>%{value} and %{date} in the messages.
    
Example:

    $date = new DateTime('now');
    $date = $date->modify('-18 years')->format('d.m.Y');
    $ruleset->add_rule(
        'datetime', [
            'latest' => new DateTime('now')->modify('-18 years')->format('Y-m-d'),
            'message' => 'You need to be at least 18 years old'
        ]
    );

<br>
    
***'email'***

| option                     | comment |
| :-------                   | --- |
| true                       |  Is email ( e.g. test@email.com ) |
| ['message' => 'error...']  |  Custom error message.
    
Example:

    $ruleset->add_rule('email', true);

<br><br><br><br>

**Callbacks:**

Callbacks are used to manipulate the instance when certain events happen.<br>
The available events a callback can be assigned to are listed below:

          Name:                 When does the callback get executed?
          
        - beforerender          Before the rendering of the instance begins.
        - beforeinsert          Before a new entry gets saved.
        - afterinsert           After a new entry got saved to the database.
        - beforeupdate          Before the database gets updated.
        - afterupdate           After the database got updated.
        - befroreremove         Before the entry gets removed.
        - afterremove           After the entry got removed.

To specify a general callback, use the `callback()` function, which is defined as:<br>

    public function callback($for_state, $function, string $filepath = '', array $arguments = array()) : void

The function can be called in the following ways:

    // Using a closure.
    $crud->callback('beforerender', function() {});
    
    // Using an bject member function ( make sure the specified method is public! )
    $crud->callback('beforerender', [$this, 'my_function']);
    
    // Simple function which lies in the specified file.
    $crud->callback('beforerender', 'my_function', __DIR__.'/myFunctions.php');
   
    // For multiple events.
    $crud->callback(['beforeinsert', 'beforeupdate'], 'my_function', 'myFunctions.php');
    
This last line of code will attach a callback to the current instance (`$crud`) which gets executed every
time a new entry gets created (`beforeinsert`) or an entry gets saved (`beforeupdate`), but always before the crud
system stores the changes to the database (`before* instead of after*`)!

A callback function looks like this:
    
    /**
     * @param int $primary_key The primary key of the current entry.
     * @param stdClass $entry The current state of the entry in the database.
     * @param array $postdata The user data entered in the form (create / edit view).
     * @param string $callback_task For which task this function gets executed. E.g. 'afterinsert'.
     * @param Crud $crud The crud instance we are operating on.
     * @param array $custom_arguments Additional (user specified and static) arguments.
     * @return array This function may return different values:<br><br>
     *               return $postdate: Allows to modify the data entered by the user before the entry gets stored
     *                                 (Only does something in 'beforeinsert' and 'beforeupdate')<br><br>
     *               return false: Allows to stop the deletion of an entry.
     *                             (Only does something in 'beforeremove')
     */
    function my_function(?int $primary_key, ?stdClass $entry, ?array $postdata, string $callback_task, Crud $crud, array $custom_arguments)
    {
        Debugbar::info('$primary_key: '.$primary_key);
        Debugbar::info('$entry: '.json_encode($entry));
        Debugbar::info('$postadata: '.json_encode($postadata));
        Debugbar::info('$callback_task: '.$callback_task);
        Debugbar::info('$crud: '.serialize($crud));
        Debugbar::info('$custom_arguments: '.json_encode($custom_arguments));
        Debugbar::info('TASK: '.$crud->get_task());
    
        //...
    }
    
Or simplified:

    function my_function($primary_key, $entry, $postdata, $callback_task, $crud, $custom_arguments)
    {
        //...
    }



<br><br><br><br>

**Column callbacks:**

Column callbacks help you to style the output in the list view.<br>
A column callback gets called for every table-field of the specified column.



<br><br><br><br>

**Field callbacks:**