<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Session;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Session;
use Lukaspotthast\Crud\Frameworks\Laravel\Laravel_Session;

/**
 * Class Session
 * @package Lukaspotthast\Crud\Session
 */
abstract class Session
{

    public static function init(): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Session::session_init();
                break;

            case Frameworks::LARAVEL:
                Laravel_Session::session_init();
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function put(string $key, $value): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Session::session_put($key, $value);
                break;

            case Frameworks::LARAVEL:
                Laravel_Session::session_put($key, $value);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    /**
     * This function returns null if $key is not defined in the session!
     *
     * @param string $key
     * @return mixed|null
     */
    public static function get(string $key)
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                return PHP_Session::session_get($key);
                break;

            case Frameworks::LARAVEL:
                return Laravel_Session::session_get($key);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

}