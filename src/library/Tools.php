<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Support\HTML;

abstract class Tools
{

    /**
     * Generates a valid HTML-id-attribute for the specified field.<br>
     * The generated string must not be altered. It can then later be parsed with the parse_input_id(string) function.
     *
     * @param Crud          $crud
     *      The crud instance to operate with.
     *
     * @param Column_Schema $col
     *      The field (schema) for which the id should be generated.
     *
     * @param string        $extension
     *      OPTIONAL information string. Can be accessed in the array returned by parse_input_id(string) with
     *      the key: 'extension'.
     *
     * @return string
     *      ...
     */
    public static function encode_input_id(Crud $crud, Column_Schema $col, string $extension = ''): string
    {
        $id_array = [
            $crud->get_instance_name(),
            $crud->get_verification_key(),
            !is_null($col->get_origin_table()) ? $col->get_origin_table()->get_name() : '',
            $col->get_name(),
            $extension,
        ];

        $id_string = json_encode($id_array);

        return HTML::encode_id($id_string);
    }

    /**
     * Parses a string which was previously calculated through a call to encode_input_id(stdClass, string).
     *
     * @param string $encoded_id
     *      The string to parse.
     *
     * @return array
     *      Returns an array of the following structure:<br>
     * <pre>
     *      [
     *          'instance_name' => string,
     *          'instance_verification_key' => string,
     *          'table_name' => string,
     *          'field_name' => string,
     *          'extension' => string
     *      ]
     * </pre>
     */
    public static function decode_input_id(string $encoded_id): array
    {
        $id_string = HTML::decode_id($encoded_id);

        $id_array = json_decode($id_string);

        return [
            'instance_name'             => $id_array[0],
            'instance_verification_key' => $id_array[1],
            'table_name'                => $id_array[2],
            'field_name'                => $id_array[3],
            'extension'                 => $id_array[4],
        ];
    }

    /**
     * Generates a valid HTML-id-attribute for the specified tab number.<br>
     * The generated string must not be altered. It can then later be parsed with the parse_tab_id(string) function.
     *
     * @param Crud   $crud       The crud instance to operate with.
     * @param int    $tab_number The tab number, for which this id should be generated.
     * @param string $extension
     *                           OPTIONAL information string. Can be accessed in the array returned by
     *                           parse_tab_id(string) with the key: 'extension'.
     * @return string
     */
    public static function encode_tab_id(Crud $crud, int $tab_number, string $extension = ''): string
    {
        $id_array = [
            $crud->get_instance_name(),
            'tab',
            $tab_number,
            $extension,
        ];

        $id_string = json_encode($id_array);

        return HTML::encode_id($id_string);
    }

    /**
     * Parses a string which was previously calculated through a call to calculate_tab_id(int, string).
     *
     * @param string $id_string The string to parse.
     * @return array Returns an array of the following structure:<br>
     *                          <pre>
     *                          [
     *                          'instance_name' => string,
     *                          'instance_verification_key' => string,
     *                          'tab_number' => int,
     *                          'extension' => string
     *                          ]
     *                          </pre>
     */
    public static function decode_tab_id(string $id_string): array
    {
        $id_string = HTML::decode_id($id_string);

        $id_array = json_decode($id_string);

        return [
            'instance_name' => $id_array[0],
            // $id_array[1] === 'tab'
            'tab_number'    => $id_array[2],
            'extension'     => $id_array[3],
        ];
    }

    /**
     * Check if the "APC" opcode cache is loaded and enabled.
     *
     * @return bool
     *      true / false
     */
    public static function is_apc_enabled(): bool
    {
        return (extension_loaded('apc') and ini_get('apc.enabled'));
    }

}