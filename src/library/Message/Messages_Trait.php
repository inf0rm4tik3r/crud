<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Message;

/**
 * Provides a Message_Controller instance.
 *
 * @package Lukaspotthast\Crud\Message
 */
trait Messages_Trait
{

    /**
     * @var Message_Controller
     */
    private $message_controller;

    /**
     * @return Message_Controller
     */
    public function message_controller(): Message_Controller
    {
        return $this->message_controller;
    }

}