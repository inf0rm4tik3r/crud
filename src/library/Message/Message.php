<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Message;

use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Tools;

class Message
{

    public const SUPPORTED_TYPES = ['info', 'success', 'warning', 'danger'];

    /**
     * All automatically closing messages will close after this delay.
     * @var int The delay in milliseconds.
     */
    public const AUTOMATIC_CLOSE_DELAY = 2500;

    /**
     * Reference to the parent Crud instance.
     * @var Crud
     */
    private $crud;

    /**
     * @var Assert
     */
    private $assert;

    /**
     * The heading for the displayable message.
     * @var string
     */
    private $heading;

    /**
     * The message which is to be displayed.
     * @var string
     */
    private $message;

    /**
     * The type of this message: One of Crud_Message:SUPPORTED_TYPES
     * @var string
     */
    private $type;

    /**
     * Defines if the user is allowed to close / dismiss this message.
     * @var bool
     */
    private $dismissible;

    /**
     * Defines whether this message will be automatically closed after a certain period of time.
     * @var bool
     */
    private $automatically_closing;

    /**
     * Defines whether the message will be kept over requests.
     * Normally all previously added messages will be removed whenever a new request begins.
     * @var bool
     */
    private $keep;

    /**
     * Crud_Message constructor.
     * @param Crud $crud
     * @param string $heading
     * @param string $message
     * @param string $type
     * @param bool $dismissible
     * @param bool $automatically_closing
     * @param bool $keep
     */
    public function __construct(
        Crud &$crud,
        string $heading = '',
        string $message = '',
        string $type = 'success',
        bool $dismissible = true,
        bool $automatically_closing = false,
        bool $keep = false)
    {
        $this->set_crud($crud);
        $this->set_heading($heading);
        $this->set_message($message);
        $this->set_type($type);
        $this->set_dismissible($dismissible);
        $this->set_automatically_closing($automatically_closing);;
        $this->set_keep($keep);;
    }

    public function set_crud(Crud $crud) : void
    {
        $this->crud = $crud;
        $this->assert = $crud->get_assert();
    }

    /**
     * @param string $heading
     */
    public function set_heading(string $heading) : void
    {
        // The heading must be specified as a string.
        if ( !$this->assert->is_string(__FUNCTION__, $heading, 'heading', compact('heading')) )
        {
            $this->heading = 'error';
            return;
        }

        $this->heading = $heading;
    }

    /**
     * @return string
     */
    public function get_heading() : string
    {
        return $this->heading;
    }

    /**
     * @param string $message
     */
    public function set_message(string $message) : void
    {
        // The message must be specified as a string.
        if ( !$this->assert->is_string(__FUNCTION__, $message, 'message', compact('message')) )
        {
            $this->message = 'error';
            return;
        }

        $this->message = $message;
    }

    /**
     * @return string
     */
    public function get_message() : string
    {
        return $this->message;
    }

    public function set_type(string $type) : void
    {
        $args = compact('type');

        // The $type parameter must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $type, 'type', $args) )
        {
            return;
        }

        // The $type parameter must be one of the supported message types.
        if ( !in_array($type, self::SUPPORTED_TYPES) )
        {
            $this->crud->error(
                '<b>'.__FUNCTION__.'(): The $type must be one of the supported message types specified in '
                .'Crud_Message::SUPPORTED_TYPES.</b><br>'
                .'Use one of: '.implode(', ', self::SUPPORTED_TYPES)
                .'Arguments passed:<br>'.Tools::args_as_list($args)
            );
            return;
        }

        $this->type = $type;
    }

    public function get_type() : string
    {
        return $this->type;
    }

    /**
     * @param bool $dismissible
     */
    public function set_dismissible(bool $dismissible) : void
    {
        $this->dismissible = $dismissible;
    }

    /**
     * @return bool
     */
    public function is_dismissible(): bool
    {
        return $this->dismissible;
    }

    /**
     * @param bool $automatically_close
     */
    public function set_automatically_closing(bool $automatically_close) : void
    {
        $this->automatically_closing = $automatically_close;
    }

    /**
     * @return bool
     */
    public function should_automatically_close(): bool
    {
        return $this->automatically_closing;
    }

    /**
     * @param bool $keep
     */
    public function set_keep(bool $keep) : void
    {
        $this->keep = $keep;
    }

    /**
     * @return bool
     */
    public function should_be_kept(): bool
    {
        return $this->keep;
    }

}