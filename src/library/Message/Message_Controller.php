<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Message;

use Lukaspotthast\Crud\Crud;

/**
 * Class Message_Controller
 * @package Lukaspotthast\Crud\Message
 */
class Message_Controller
{

    /**
     * Stores all displayable messages.
     *
     * @var array
     */
    private $messages;

    /**
     * Message_Controller constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * Deletes all currently stored messages.
     */
    public function reset(): void
    {
        $this->messages = [];
    }

    /**
     * Adds the specified message object.
     *
     * @param Message $message
     */
    public function add(Message $message): void
    {
        array_push($this->messages, $message);
    }

    /**
     * @param Crud   $crud
     * @param string $heading
     * @param string $message
     * @param string $type
     * @param bool   $dismissible
     * @param bool   $automatically_closing
     * @param bool   $keep
     */
    public function new(
        Crud &$crud,
        string $heading,
        string $message,
        string $type = 'success',
        bool $dismissible = true,
        bool $automatically_closing = false,
        bool $keep = false): void
    {
        array_push($this->messages, new Message(
                $crud, $heading, $message, $type, $dismissible, $automatically_closing, $keep
            )
        );
    }

    /**
     * Checks if there are any messages set at the moment.
     *
     * @return bool
     */
    public function any(): bool
    {
        return count($this->messages) > 0;
    }

    /**
     * Returns the current messages-array.
     *
     * @return array
     */
    public function get(): array
    {
        return $this->messages;
    }

}