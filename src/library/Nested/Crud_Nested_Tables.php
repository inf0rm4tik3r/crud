<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Nested;

use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Crud;

use stdClass;

trait Crud_Nested_Tables
{
    private $nested_table_instance_names; // array

    /**
     * Stores references to all once loaded nested instances.
     * Represents a cache storage. Needs to be cleared before this instance gets stored.
     * @var array [string => Crud]
     */
    private $nested_instances_cache;

    private $is_nested = false;
    private $parent_instance_name; // string
    private $parent_connect_field; // string
    private $connect_field; // string

    public function reset_nested_tables()
    {
        $this->nested_table_instance_names = array();
        $this->is_nested = false;
        $this->parent_instance_name = null;
        $this->parent_connect_field = null;
        $this->connect_field = null;

        $this->invalidate_nested_table_cache();
    }


    /**
     * Returns the Crud object for the specified nested table or null, if th
     *
     * @param string $instance_name
     * @return Crud|null
     */
    public function &get_nested_table_instance(string $instance_name) : ?Crud
    {
        // Try to find the given instance name in the list of known nested table instance names.
        foreach ($this->nested_table_instance_names as &$nested_instance_name)
        {
            if ( $nested_instance_name === $instance_name )
            {
                // Load the instance from the session if it was not already cached.
                if ( !array_key_exists($instance_name, $this->nested_instances_cache) )
                {
                    $this->nested_instances_cache[$instance_name] = Crud::get_instance($nested_instance_name, false, false);
                }

                // Return the cached instance.
                return $this->nested_instances_cache[$instance_name];
            }
        }

        // If the specified name did not match any of known nested table instance names, null will be returned.
        // The function should only get called with names of existing nested table instances.
        $this->error(__FUNCTION__.'(): There is no nested table instance with the name "'.$instance_name.'".');
        return null;
    }

    /**
     * Returns all nested table instances in an array of the form:<br>
     * [
     *      (string) 'instance_name' => (Crud) instance
     * ]
     * @return array
     */
    public function get_nested_table_instances() : array
    {
        $instances = array();

        // Acquire all known nested table instance and return them.
        foreach ($this->nested_table_instance_names as &$nested_table_instance_name)
        {
            $instances[$nested_table_instance_name] = $this->get_nested_table_instance($nested_table_instance_name);
        }

        return $instances;
    }

    /**
     * @param Crud $instance REFERENCE!
     */
    private function update_nested_table_cache(Crud &$instance) : void
    {
        $this->nested_instances_cache[$instance->get_instance_name()] = $instance;
    }

    /**
     * Clears the cache for nested table instances.<br>
     * Either completely or just the cached instance for the supplied $instance_name.
     *
     * @param null|string $instance_name
     *      Leave this argument at NULL to clear the complete cache, or specify a specific nested table instance name
     *      to only clear the cache from the value that correlates to that name.
     */
    public function invalidate_nested_table_cache(?string $instance_name = null) : void
    {
        if ( !is_null($instance_name) and array_key_exists($instance_name, $this->nested_instances_cache) )
        {
            unset($this->nested_instances_cache[$instance_name]);
        }
        else
        {
            $this->nested_instances_cache = [];
        }
    }

    public function store_nested_table_instances() : void
    {
        foreach ( $this->get_nested_table_instances() as &$instance )
        {
            /** @var $instance Crud */
            $instance->get_storage()->store_instance();
        }
    }



    //                                                            id                                       template_ref
    public function nested_table($nested_instance_name, $connect_field, $nested_table_name, $nested_table_connect_field)
    {
        $args = compact('nested_instance_name', 'connect_field', 'nested_table_name', 'nested_table_connect_field');

        // Argument check.
        if ( !($nested_instance_name and is_string($nested_instance_name) and
               $connect_field and is_string($connect_field) and
               $nested_table_name and is_string($nested_table_name) and
               $nested_table_connect_field and is_string($nested_table_connect_field) )
        )
        {
            $this->error(__FUNCTION__.'(): You have to supply at least the first first 4 parameters as strings.', $args);
            return null;
        }

        if (in_array($nested_instance_name, $this->nested_table_instance_names))
        {
            $this->error(__FUNCTION__.'(): A nested table with this name was already defined.', $args);
            return null;
        }

        //TODO: Will lead to name clashes if next line is not used, but access to nested table needs to be easy!
        //$nested_instance_name = $this->instance_name.'_nested_'.$nested_table_name;

        $nested_instance = Crud::get_instance($nested_instance_name);
        $nested_instance->table($nested_table_name);
        $nested_instance->set_nested($this->get_instance_name(), $connect_field, $nested_table_connect_field);

        array_push($this->nested_table_instance_names, $nested_instance_name);

        //$nested_instance_ref =& $nested_instance; // TODO: remove
        $this->update_nested_table_cache($nested_instance); // TODO: add & to update_nested_table_cache() function

        return $nested_instance;
    }

    private function set_nested(string $parent_instance_name, string $connect_field, string $nested_table_connect_field) : void
    {
        // Notice: We are IN the object from the nested table instance!
        $this->is_nested = true;
        $this->parent_instance_name = $parent_instance_name;

        $this->parent_connect_field = $connect_field; // For example: Link 'id' of the parent table
        $this->connect_field = $nested_table_connect_field; // with '*_ref' of this (nested) table.

        // Nested instances should be asynchronously loaded.
        $asynchronous = Config::get('crud.use_asynchronous_loading_for_nested_instances');
        if ( !$this->assert->is_bool(__FUNCTION__, $asynchronous, 'asynchronous', []) )
        {
            $asynchronous = true;
        }
        $this->asynchronous($asynchronous);
    }

    /**
     * @param null|stdClass $parent_entry
     */
    public function update_nested_table_instances_where_connection(?stdClass $parent_entry): void
    {
        foreach ( $this->get_nested_table_instances() as &$instance)
        {
            /** @var Crud $instance */

            if ( is_null($parent_entry) )
            {
                $parent_connect_value = null;
            }
            else
            {
                $parent_connect_value = $parent_entry->{$instance->parent_connect_field};
            }

            $where_query = $instance->get_where_query();

            if ( is_null($where_query->find('parent_connection')) )
            {
                // Add the where clause if was not jet defined.
                $where_query->where($instance->connect_field, '=', $parent_connect_value, 'parent_connection');
            }
            else
            {
                // Otherwise update it.
                $where_query->update_value('parent_connection', $parent_connect_value);
            }
        }
    }

    public function is_nested() : bool
    {
        return $this->is_nested;
    }

    public function is_nested_table(string $name) : bool
    {
        return in_array($name, $this->nested_table_instance_names);
    }

    public function get_parent_instance_name() : ?string
    {
        return $this->parent_instance_name;
    }

}