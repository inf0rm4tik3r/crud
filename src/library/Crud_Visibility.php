<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Lukaspotthast\Crud\Config\Config;

trait Crud_Visibility
{

    private $element_visibility; //array

    private function reset_visibility() : void
    {
        $this->element_visibility = array();

        // Try to load the visibility modifiers from the configuration file.
        $visibility = Config::get('crud.visibility');

        if ( $this->assert->is_array(__FUNCTION__, $visibility, 'visibility', compact('visibility')) )
        {
            foreach ( $visibility as $key => &$value )
            {
                $this->set_visibility($key, $value);
            }
        }
    }

    private function set_visibility(string $element_name, bool $visibility) : void
    {
        $args = compact('element_name', 'visibility');

        // The element name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $element_name, 'element_name', $args) )
        {
            return;
        }

        // The visibility modifier must be specified as a boolean value.
        if ( !$this->assert->is_bool(__FUNCTION__, $visibility, 'visibility', $args) )
        {
            return;
        }

        // The element name must be one of the supported elements!
        if ( !in_array($element_name, self::VISIBILITY_SUPPORTED_ELEMENTS) )
        {
            $this->error(__FUNCTION__.'(): The visibility change is not supported for "'.$element_name.'".', $args);
            return;
        }

        $this->element_visibility[$element_name] = $visibility;
    }

    private function get_visibility(string $element_name) : bool
    {
        $args = compact('element_name');

        if ( !in_array($element_name, self::VISIBILITY_SUPPORTED_ELEMENTS) )
        {
            $this->error(__FUNCTION__.'(): The visibility state must not be queried for "'.$element_name.'".', $args);
            return false;
        }

        // Every element, for which no visibility was specified, is visible.
        if ( !array_key_exists($element_name, $this->element_visibility) )
        {
            return true;
        }

        return $this->element_visibility[$element_name];
    }


    public function visibility(string $element_name, bool $visibility) : void
    {
        $this->set_visibility($element_name, $visibility);
    }

    public function show(string $element_name) : void
    {
        $this->set_visibility($element_name, true);
    }

    public function hide(string $element_name) : void
    {
        $this->set_visibility($element_name, false);
    }

    public function is_shown(string $element_name) : bool
    {
        return $this->get_visibility($element_name);
    }

    public function is_hidden(string $element_name) : bool
    {
        return !$this->get_visibility($element_name);
    }


}