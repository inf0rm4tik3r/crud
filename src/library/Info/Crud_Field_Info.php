<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Info;

trait Crud_Field_Info
{

    /**
     * Stores info-strings for fields.
     * @var array (field_name => info)
     */
    private $field_info;

    /**
     * Resets the defined field <-> information associations.
     */
    private function reset_field_info() : void
    {
        $this->field_info = array();
    }

    /**
     * Define an field info/description which will be displayed right next/under the fields input.
     *
     * @param string $field_name The field for which the info should be specified.
     * @param string $info The info which gets displayed. Empty strings are allowed.
     */
    public function field_info(string $field_name, string $info) : void
    {
        $args = compact('field_name', 'info');

        // The field name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // The info parameter must be specified as a string.
        if ( !$this->assert->is_string(__FUNCTION__, $info, 'info', $args) )
        {
            return;
        }

        // The column specified by the field name must exist.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // Set the specified info for the specified field.
        $this->field_info[$field_name] = $info;
    }

    /**
     * Check whether an info was defined for the specified field.
     *
     * @param string $field_name The field to check.
     * @return bool
     */
    public function has_field_info(string $field_name) : bool
    {
        return array_key_exists($field_name, $this->field_info);
    }

    /**
     * Returns the field info for the specified field.<br>
     * Make sure that you checked the existence of an info for the field specified here through the
     * has_field_info(string $field_name) function before calling this method!
     *
     * @param string $field_name The field to query.
     * @return string
     */
    public function get_field_info(string $field_name) : string
    {
        return $this->field_info[$field_name];
    }

}