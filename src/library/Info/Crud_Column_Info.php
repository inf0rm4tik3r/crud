<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Info;

trait Crud_Column_Info
{

    private $column_info; // array(column => info)

    private function reset_column_info()
    {
        $this->column_info = array();
    }

    /**
     * Define an column info/description which will be displayed right under the columns name in the table header.
     *
     * @param string $column_name The column for which the info should be specified.
     * @param mixed $info The info which gets displayed. Arrays and objects get converted to strings.
     */
    public function column_info(string $column_name, $info) : void
    {
        $args = compact('column_name', 'info');

        // Check if the column name got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return;
        }

        // Check if the info got properly specified.
        if ( !isset($info) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): The info argument must be specified.</b><br>'
                .'Arguments passed:<br>'.$this->args_as_list($args)
            );
            return;
        }

        // Check if the column really exists.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column_name, $args) )
        {
            return;
        }

        // Convert the $info argument if an array got provided.
        if ( is_array($info) )
        {
            $info = json_encode($info);
        }

        // Convert the $info argument if an object got provided.
        if ( is_object($info) )
        {
            if ( method_exists($info, '__toString') )
            {
                $info = (string) $info;
            }
            else
            {
                $info = serialize($info);
            }
        }

        // The input is valid. We can set the specified info for the specified column.
        $this->column_info[$column_name] = $info;
    }

    public function has_column_info($column_name)
    {
        return array_key_exists($column_name, $this->column_info);
    }

    public function get_column_info($column_name)
    {
        return $this->column_info[$column_name];
    }

}