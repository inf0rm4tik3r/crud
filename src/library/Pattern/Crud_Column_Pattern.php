<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Pattern;

use Lukaspotthast\Crud\Debug\Debug;
use stdClass;

trait Crud_Column_Pattern
{

    private $column_pattern; // array(col => pattern)

    private function reset_column_pattern() : void
    {
        $this->column_pattern = array();
    }

    /**
     * Defines a pattern for the specified column.<br>
     * Examples are:<br>
     * <code>
     * $crud->column_pattern('price', '{price} €');
     * $crud->column_pattern('email', '<a href="mailto:{email}">{email}</a>');
     * </code>
     *
     * @param string $column
     * @param string $pattern
     */
    public function column_pattern(string $column, string $pattern) : void
    {
        $args = compact('col', 'pattern');

        // Check if the column got specified correctly.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column, 'column', $args) )
        {
            return;
        }

        // Check if the column really exists.
        if ( ! $this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column, $args) )
        {
            return;
        }

        // Check if the pattern got specified correctly.
        if ( !$this->assert->is_string(__FUNCTION__, $pattern, 'pattern', $args) )
        {
            return;
        }

        // Display a warning, should the pattern got specified as an empty string.
        if ( empty($pattern) )
        {
            $this->warning(__FUNCTION__.'(): The pattern for "'.$column.'" was specified as an empty string.');
        }

        // Check if a pattern for this column already got defined.
        if ( $this->has_column_pattern($column) )
        {
            $this->warning(__FUNCTION__.'(): A pattern for column "'.$column.'" was already defined. Overwriting...');
        }

        // The input is valid. We can store the column pattern in the instance.
        $this->column_pattern[$column] = $pattern;
    }

    /**
     * Checks whether a column pattern was defined for the given column.<br>
     * Used internally. Arguments only checked in debug mode.
     *
     * @param string $column_name THe column to check.
     * @return bool true / false
     */
    public function has_column_pattern(string $column_name) : bool
    {
        if ( $this->in_debug_mode() )
        {
            if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column_name, compact('column_name')) )
            {
                return false;
            }
        }

        return array_key_exists($column_name, $this->column_pattern);
    }

    /**
     * Returns the final computation of the column pattern for the column specified with $col.<br>
     * If no pattern was ever specified for this column, $output will be returned unmodified.<br>
     * Used internally. Arguments only checked in debug mode.
     *
     * @param string $column_name The column for which the pattern should be computed.
     * @param string $output The regular output for this column.
     * @param stdClass $entry The current entry data.
     * @return string The column pater in which all {field-tags} got replaced with actual data.
     */
    public function apply_column_pattern(string $column_name, string $output, stdClass $entry) : string
    {
        // Return the given $output unmodified should no column pattern has benn defined.
        if ( !$this->has_column_pattern($column_name) )
        {
            Debug::error("NO column pattern to apply! Please check beforehand with has_column_pattern()");
            return $output;
        }

        // Store the generated output in the current entry, so that it can be used for field tag replacement.
        $entry->{$column_name}['data'] = $output;

        // Return the column pattern, in which all {tags} have been replaced.
        return $this->replace_field_tags($this->column_pattern[$column_name], $entry);
    }

}