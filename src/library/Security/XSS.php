<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Security;

use HTMLPurifier;
use HTMLPurifier_Config;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Arr;

class XSS
{

    /**
     * @var HTMLPurifier
     */
    private static $purifier;

    public static function init_htmlpurifier(): void
    {
        // Works with UTF-8 by default.
        $config         = HTMLPurifier_Config::createDefault();
        self::$purifier = new HTMLPurifier($config);
    }

    /**
     * ...
     *
     * @param string|int|float|double|bool|array $data
     *      ...
     * @param array                              $options
     *      ...
     *
     * @return string|array
     *      The value returned is either a string or an array of (arrays of...) strings.
     */
    public static function secure($data, array $options = []) /* type of $data */
    {
        if ( Crud::PERFORM_TYPECHECKS )
        {
            Arg::typecheck($data, ['null', 'string', 'int', 'float', 'double', 'bool', 'array']);
            Arg::typecheck($options, ['bool']);
        }

        if ( is_null($data) )
        {
            return '';
        }

        if ( is_int($data) or is_float($data) or is_double($data) or is_bool($data) )
        {
            return $data;
        }

        if ( is_string($data) )
        {
            return self::_secure($data, $options);
        }

        else if ( is_array($data) )
        {
            foreach ( $data as &$entry )
            {
                $entry = self::secure($entry, $options);
            }
            return $data;
        }
        else
        {
            die(__CLASS__ . ' - ' . __FUNCTION__ . '(): The type of $data: "' . gettype($data) . '" is not supported!');
        }
    }

    /**
     * ...
     *
     * @param string $evil_string
     *      ...
     *
     * @param array  $options
     *      ...
     *
     * @return string
     *      ...
     */
    private static function _secure(string $evil_string, array $options = []): string
    {
        // Nothing needs to be done if the string is empty.
        if ( empty($evil_string) )
        {
            return $evil_string;
        }

        $do_nothing    = Arr::get($options, 'do_nothing', false);
        $escape_string = Arr::get($options, 'escape_string', true);
        $allow_html    = Arr::get($options, 'allow_html', false);

        if ( $do_nothing )
        {
            return $evil_string;
        }
        if ( !$escape_string and $allow_html )
        {
            return self::$purifier->purify($evil_string);
        }
        else
        {
            return htmlentities($evil_string, ENT_QUOTES, 'UTF-8');
        }
    }

}

XSS::init_htmlpurifier();