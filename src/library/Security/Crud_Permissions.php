<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Security;

use Lukaspotthast\Crud\Config\Config;

trait Crud_Permissions
{
    
    private $permissions;

    private function reset_permissions()
    {
        $this->permissions = array();

        $permissions = Config::get('crud.permissions');

        if ( $this->assert->is_array(__FUNCTION__, $permissions, 'permissions', compact('permissions')) )
        {
            foreach ( $permissions as $key => &$value )
            {
                $this->allow($key, $value);
            }
        }
    }

    public function set_allowed(string $permission_name, bool $allowed) : void
    {
        $args = compact('permission_name', 'allowed');

        // The permission name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $permission_name, 'permission_name', $args) )
        {
            return;
        }

        // The allowed modifier must be specified as a boolean value.
        if ( !$this->assert->is_bool(__FUNCTION__, $allowed, 'allowed', $args) )
        {
            return;
        }

        // The permission name must be one of the supported permission elements!
        if ( !in_array($permission_name, self::PERMISSION_SUPPORTED_ELEMENTS) )
        {
            $this->error(__FUNCTION__.'(): The permission change is not supported for "'.$permission_name.'".', $args);
            return;
        }

        $this->permissions[$permission_name] = $allowed;
    }

    private function get_allowed(string $permission_name) : bool
    {
        $args = compact('permission_name');

        // If the specified permission must be one of the supported permission elements!
        if ( !in_array($permission_name, self::PERMISSION_SUPPORTED_ELEMENTS) )
        {
            $this->error(__FUNCTION__.'(): The permission state must not be queried for "'.$permission_name.'".', $args);
            return false;
        }

        // Every permission, for which no state was specified, is allowed.
        if ( !array_key_exists($permission_name, $this->permissions) )
        {
            return true;
        }

        return $this->permissions[$permission_name];
    }

    public function allow(string $action) : void
    {
        $this->set_allowed($action, true);
    }

    public function forbid(string $action) : void
    {
        $this->set_allowed($action, false);
    }

    public function is_allowed(string $permission_name) : bool
    {
        return $this->get_allowed($permission_name);
    }

    public function is_forbidden(string $permission_name) : bool
    {
        return !$this->get_allowed($permission_name);
    }

}