<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Security;

use Lukaspotthast\Crud\Language\Translation\Translator;

trait Crud_Verification_Key
{

    /**
     * Stores the current verification key.
     * @var string
     */
    private $verification_key = '';

    /**
     * Stores whether or not an error occurred while validating the verification key.
     * Gets reset to false on every request.
     * @var bool
     */
    private $verification_error = false;

    /**
     * Resets the $verification_error variable.
     */
    private function reset_verification_error() : void
    {
        $this->set_verification_error(false);
    }

    /**
     * Generates a new verification key.
     */
    public function regenerate_verification_key() : void
    {
        $this->set_verification_key( $this->generate_verification_key() );
    }

    /**
     * Throws the name of this instance, the current time with micro seconds and a random integer into
     * the sha1 algorithm to create a key.
     * @return string A "ready to be used" verification key.
     */
    private function generate_verification_key() : string
    {
        return sha1($this->get_instance_name().microtime().rand());
    }

    /**
     * Checks if the specified key equals the currently active verification key.
     * The verification passed if has_verification_error() returns false afterwards.
     * @param string $key The key to check.
     */
    public function check_verification_key(string $key) : void
    {
        // Perform argument checks if the debug mode is enabled.
        if ( $this->in_debug_mode() )
        {
            $args = compact('key');

            // Check if the key got properly specified.
            if ( !$this->assert->is_non_empty_string(__FUNCTION__, $key, 'key', $args) )
            {
                return;
            }
        }

        // Check if the verification key of the current instance does not match the provided key.
        // If that should be the case, this instance object was not the one that supplied the html to the client.
        // Any next action should be permitted.
        if ( $this->verification_key !== $key )
        {
            $this->verification_error = true;
            $this->error(__FUNCTION__.'(): '.Translator::get('crud.session_expired'), compact('key'));
        }
    }

    /**
     * Set whether or not a verification error occurred.
     * @param bool $error true / false.
     */
    private function set_verification_error(bool $error) : void
    {
        $this->verification_error = $error;
    }

    /**
     * Returns if an error occurred while validating the verification key.
     * @return bool
     */
    public function has_verification_error() : bool
    {
        return $this->verification_error;
    }

    /**
     * Sets the verification key to the provided string value.
     * @param string $key The new key to use.
     */
    private function set_verification_key(string $key) : void
    {
        $this->verification_key = $key;
    }

    /**
     * Returns the currently set verification key.
     * @return string
     */
    public function get_verification_key() : string
    {
        return $this->verification_key;
    }

}