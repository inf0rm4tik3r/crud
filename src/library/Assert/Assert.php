<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Assert;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Connection;
use Lukaspotthast\Crud\Database\DBHelper;
use Lukaspotthast\Crud\Database\Table;
use Lukaspotthast\Crud\Database\Table_Set;
use Lukaspotthast\Support\Error\Throws_Errors_Interface;

class Assert
{
    /**
     * @var Throws_Errors_Interface
     */
    private $err;

    /**
     * @var Connection
     */
    private $db_connection;

    /**
     * Assert constructor.
     *
     * @param Throws_Errors_Interface $err
     * @param Connection|null         $connection
     */
    public function __construct(Throws_Errors_Interface &$err, ?Connection $connection)
    {
        $this->err           = $err;
        $this->db_connection = $connection;
    }

    public function is_set(
        string $function_name, $argument, string $argument_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !isset($argument) )
        {
            if ( $generates_error )
            {
                $msg = sprintf('%s(): "%s" must be set!', $function_name, $argument_name);
                $this->err->error($msg, $args);
            }
            return false;
        }
        return true;
    }

    public function table_column_exists(
        Table &$table,
        string $function_name, string $column_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !$table->get_schema()->has_table_column_schema($column_name) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified table column "' . $column_name . '" does not exist.', $args
                );
            }
            return false;
        }
        return true;
    }

    public function custom_column_exists(
        Table &$table,
        string $function_name, string $column_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !$table->get_schema()->has_custom_column_schema($column_name) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified column "' . $column_name . '" does not exist.', $args
                );
            }
            return false;
        }

        return true;
    }

    public function column_exists(
        Table $table,
        string $function_name, string $column_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !$table->get_schema()->has_column_schema($column_name) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified column "' . $column_name . '" does not exist.', $args
                );
            }
            return false;
        }

        return true;
    }

    public function set_column_exists(
        Table_Set $table_set,
        string $function_name, string $table_name, string $column_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !$table_set->has_column($table_name, $column_name) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified column "' . $column_name . '" does not exist in "' . $table_name . '".',
                    $args
                );
            }
            return false;
        }

        return true;
    }

    public function nested_table_exists(
        Crud &$crud,
        string $function_name, string $instance_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !$crud->get_nested_table_instance($instance_name) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified nested table "' . $instance_name . '" does not exist.', $args
                );
            }
            return false;
        }

        return true;
    }

    public function is_column_or_nested(
        Table &$table,
        Crud &$crud,
        string $function_name, string $name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !self::column_exists($table, $function_name, $name, $args, false) and
             !self::nested_table_exists($crud, $function_name, $name, $args, false) )
        {
            self::column_exists($table, $function_name, $name, $args, $generates_error);
            self::nested_table_exists($crud, $function_name, $name, $args, $generates_error);
            return false;
        }
        return true;
    }

    public function is_bool(
        string $function_name, $value, string $value_param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($value) and is_bool($value)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): You must specify "' . $value_param_name . '"" as a boolean.', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_int(
        string $function_name, $value, string $value_param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($value) and is_int($value)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): You must specify "' . $value_param_name . '"" as an int.', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_numeric(
        string $function_name, $value, string $value_param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($value) and is_numeric($value)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): You must specify "' . $value_param_name . '"" as something numeric.', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_string(
        string $function_name, $param, string $param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($param) and is_string($param)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): "' . $param_name . '" must be specified as a string!', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_non_empty_string(
        string $function_name, $param, string $param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($param) and is_string($param) and !empty($param)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): "' . $param_name . '" must be specified as a non empty string!', $args
                );
            }
            return false;
        }
        return true;
    }

    public function maybe_is_array(
        string $function_name, $param, string $param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !is_array($param) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): "' . $param_name . '" must be specified as null or as an array!', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_array(
        string $function_name, $param, string $param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($param) and is_array($param)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): "' . $param_name . '" must be specified as an array!', $args
                );
            }
            return false;
        }
        return true;
    }

    public function is_non_empty_array(
        string $function_name, $param, string $param_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !(isset($param) and is_array($param) and !empty($param)) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): "' . $param_name . '" must be specified as a non empty array!', $args
                );
            }
            return false;
        }
        return true;
    }

    public function table_exists(
        string $function_name, string $table_name, array $args, bool $generates_error = true
    ): bool
    {
        if ( !DBHelper::table_exists($table_name, $this->db_connection->get()) )
        {
            if ( $generates_error )
            {
                $this->err->error(
                    $function_name . '(): The specified table "' . $table_name . '" does not exist in your database.', $args
                );
            }
            return false;
        }
        return true;
    }

}