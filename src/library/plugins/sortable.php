<?php
declare(strict_types = 1);

function order_col($field_value, $field_name, $primary_key, $entry, $crud, $custom_arguments)
{
    return '
        <span class="sort-start">
            <i id="order-' . $field_value['value'] . '" class="fa fa-sort"></i> <span class="order-value">' . $field_value['value'] . '</span>
        </span>
        
        <script>
            $( document ).ready(function() {
                // i -> span -> td : add
                $("#order-' . $field_value['value'] . '").parent().parent().addClass("sort-start");
            });
        </script>
    ';
}