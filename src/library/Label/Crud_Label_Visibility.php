<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Label;

trait Crud_Label_Visibility
{
    private $label_visibility; // array
    private $supported_label_visibility_modes; // array
    private $default_label_visibility;

    private function reset_label_visibility()
    {
        $this->label_visibility = array();
        $this->supported_label_visibility_modes = array(self::SHOWN, self::HIDDEN, self::NONE);
        $this->default_label_visibility = self::SHOWN;
    }

    /**
     * Specify the rendering of a specific field label. Provides the ability to make the label SHOWN (default),
     * HIDDEN (makes the label invisible, but it will still occupy space) or NONE (this will completely remove the
     * label, making the fields input take up the label area as well).
     * @param string $field_name The field which should be modified.
     * @param int $visibility_mode One of: Crud::SHOWN, Crud::HIDDEN, Crud::NONE.
     */
    public function label_visibility(string $field_name, int $visibility_mode) : void
    {
        $args = compact('field_name', 'visibility_mode');

        // Check if the field name got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // Check if the field specified by the given field name really exists.
        if ( !$this->assert->is_column_or_nested($this->table_set->get_main_table(), $this, __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // Check if the visibility mode got properly specified.
        if ( !$this->assert->is_int(__FUNCTION__, $visibility_mode, 'visibility_mode', $args) )
        {
            return;
        }

        // Check if the visibility mode is supported.
        if ( !in_array($visibility_mode, $this->supported_label_visibility_modes) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): The specified visibility mode is not supported!</b><br>'
                .'Arguments passed:<br>'.$this->args_as_list($args)
            );
            return;
        }

        $this->label_visibility[$field_name] = $visibility_mode;
    }

    /**
     * Pseudo function for label_visibility($field_name, $visibility_mode).
     * @param string $field_name
     * @param int $visibility_mode
     */
    public function label_vis(string $field_name, int $visibility_mode)
    {
        $this->label_visibility($field_name, $visibility_mode);
    }

    /**
     * Returns the visibility mode for the specified field name.
     * @param string $field_name The field to check.
     * @return int The visibility mode.
     */
    public function get_label_visibility(string $field_name) : int
    {
        // Only perform argument checks if the debugging mode is enabled.
        if ( $this->in_debug_mode() )
        {
            $args = compact('field_name');

            // Check if the field name got properly specified.
            if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
            {
                return - 1;
            }

            // Check if the field specified by the given field name really exists.
            if ( !$this->assert->is_column_or_nested($this->table_set->get_main_table(), $this, __FUNCTION__, $field_name, $args) )
            {
                return -1;
            }
        }

        // Return the specified wrapping mode (if set) or the default.
        if ( array_key_exists($field_name, $this->label_visibility) )
        {
            return $this->label_visibility[$field_name];
        }
        else
        {
            return $this->default_label_visibility;
        }
    }

}