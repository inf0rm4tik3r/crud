<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Label;

trait Crud_Label_Wrapping
{
    private $label_wrapping; // array
    private $supported_label_wrapping_modes; // array
    private $default_label_wrapping; // int

    private function reset_label_wrapping()
    {
        $this->label_wrapping = array();
        $this->supported_label_wrapping_modes = array(self::WRAP_DYNAMIC, self::WRAP_ALWAYS, self::WRAP_NEVER);
        $this->default_label_wrapping = self::WRAP_DYNAMIC;
    }

    /**
     * Specify the wrapping behaviour of a specific field label and its corresponding input. Provides the ability to
     * make the label WRAP_DYNAMIC (default, input wraps if the content width gets to small),
     * WRAP_ALWAYS (input will always be rendered in a new line) or WRAP_NEVER (the field label will always be
     * aside its input).
     * @param string $field_name The field which should be modified.
     * @param int $wrapping_mode One of: Crud::WRAP_DYNAMIC, Crud::WRAP_ALWAYS, Crud::WRAP_NEVER.
     */
    public function label_wrapping(string $field_name, int $wrapping_mode) : void
    {
        $args = compact('field_name', 'wrapping_mode');

        // Check if the field name got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // Check if the field specified by the given field name really exists.
        if ( !$this->assert->is_column_or_nested($this->table_set->get_main_table(), $this, __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // Check if the visibility mode got properly specified.
        if ( !$this->assert->is_int(__FUNCTION__, $wrapping_mode, 'wrapping_mode', $args) )
        {
            return;
        }

        // Check if the wrapping mode is supported.
        if ( !in_array($wrapping_mode, $this->supported_label_wrapping_modes) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): The specified wrapping mode is not supported!</b><br>'
                .'Arguments passed:<br>'.$this->args_as_list($args)
            );
            return;
        }

        $this->label_wrapping[$field_name] = $wrapping_mode;
    }

    /**
     * Pseudo function for label_wrapping($field_name, $wrapping_mode).
     * @param string $field_name
     * @param int $wrapping_mode
     */
    public function label_wrap(string $field_name, int $wrapping_mode)
    {
        $this->label_wrapping($field_name, $wrapping_mode);
    }

    /**
     * Returns the visibility mode for the specified field name.
     * @param string $field_name The field to check.
     * @return int The visibility mode.
     */
    public function get_label_wrapping(string $field_name) : int
    {
        // Only perform argument checks if the debugging mode is enabled.
        if ( $this->in_debug_mode() )
        {
            $args = compact('field_name');

            // Check if the field name got properly specified.
            if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
            {
                return -1;
            }

            // Check if the field specified by the given field name really exists.
            if ( !$this->assert->is_column_or_nested($this->table_set->get_main_table(), $this, __FUNCTION__, $field_name, $args) )
            {
                return -1;
            }
        }

        // Return the specified wrapping mode (if set) or the default.
        if ( array_key_exists($field_name, $this->label_wrapping) )
        {
            return $this->label_wrapping[$field_name];
        }
        else
        {
            return $this->default_label_wrapping;
        }
    }

}