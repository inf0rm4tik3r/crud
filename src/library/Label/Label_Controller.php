<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Label;

use Lukaspotthast\Support\Arg;

/**
 * Class Label_Controller
 * @package Lukaspotthast\Crud\Label
 */
class Label_Controller
{

    /**
     * Stores the defined labels.
     *
     * @var array
     */
    private $labels;

    /**
     * Label_Controller constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     *
     */
    public function reset(): void
    {
        $this->labels = [];
    }

    /**
     * @param string $key
     * @param Label  $label
     */
    public function update(string $key, Label $label): void
    {
        Arg::typecheck($key, '!string');
        $this->labels[$key] = $label;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return array_key_exists($key, $this->labels);
    }

    /**
     * @param string $key
     * @return Label
     */
    public function get(string $key): Label
    {
        return $this->labels[$key];
    }

}