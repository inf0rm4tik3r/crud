<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Label;

class Label
{

    /**
     * @var string
     */
    private $name;

    /**
     * Label constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->set($name);
    }

    public function set(string $name): void
    {
        $this->name = $name;
    }

    public function get(): string
    {
        return $this->name;
    }

}