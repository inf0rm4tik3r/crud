<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Label;

/**
 * Trait Label_Trait
 * @package Lukaspotthast\Crud\Label
 */
trait Labels_Trait
{

    /**
     * @var Label_Controller
     */
    private $label_controller;

    /**
     * @return Label_Controller
     */
    public function label_controller(): Label_Controller
    {
        return  $this->label_controller;
    }

    /**
     * Set the label to use for a specific key.<br>
     * Use the labeled() function to retrieve the label for a specific key.
     *
     * @param string $key
     *      The key to set the label for.
     * @param string $label
     *      The label which is to be used in rendering.
     */
    public function label(string $key, string $label): void
    {
        $this->label_controller->update($key, new Label($label));
    }

    /**
     * Returns the label for the given key, or the key itself is no label is defined for it.
     *
     * @param string $key
     *      The key to retrieve the label for.
     * @return string
     *      Either the label for the given key or the key itself.
     */
    public function labeled(string $key): string
    {
        $lc = $this->label_controller;
        return $lc->has($key) ? $lc->get($key)->get() : $key;
    }

}