<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Behaviour;

/**
 * Trait Behaviours
 * @package Lukaspotthast\Crud\Behaviour
 */
trait Behaviours
{

    /**
     * @var Behaviour_Controller
     */
    private $behaviour_controller;


    public function behaviour_controller(): Behaviour_Controller
    {
        return $this->behaviour_controller;
    }

    public function field_behaviour(string $field_name, $condition, int $action): void
    {
        $this->behaviour_controller->add_field_behaviour($field_name, $condition, $action);
    }

}