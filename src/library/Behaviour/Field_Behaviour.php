<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Behaviour;

use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Condition\Condition;
use Lukaspotthast\Crud\Crud;

class Field_Behaviour
{

    /**
     * If the condition is met, reload the field.<br>
     * If the condition is not met, do nothing.<br>
     * INFO: The field renderer will receive the current state of the edit form as the entry data. Which means
     * that, if the field rendering depends on the state of another field, the rendering output may change dynamically.
     */
    public const ACTION_RELOAD = 1;

    /**
     * If the condition is met, make the field invisible.<br>
     * If the condition is not met, do nothing.
     */
    public const ACTION_HIDE = 2;

    /**
     * If the condition is met, make the field visible.<br>
     * If the condition is met met, do nothing.
     */
    public const ACTION_SHOW = 3;

    /**
     * If the condition is met, make the field visible.
     * If the condition is not met, make the field invisible.
     */
    public const ACTION_CHANGE_VISIBILITY = 4;

    /**
     * If the condition is met, disable the field.<br>
     * If the condition is not met, do nothing.
     */
    public const ACTION_DISABLE = 5;

    /**
     * If the condition is met, enable the field.<br>
     * If the condition is not met, do nothing.
     */
    public const ACTION_ENABLE = 6;

    /**
     * If the condition is met, enable the field (make it editable).<br>
     * If the condition is not met, disable the field (make it not editable).
     */
    public const ACTION_CHANGE_EDITABILITY = 7;

    /**
     * Reference to the parent Crud instance.
     * @var Crud
     */
    private $crud;

    /**
     * @var Assert
     */
    private $assert;

    /**
     * The field to operate on.
     * @var string
     */
    private $field_name;

    /**
     * What should be done if the condition is met. One of the defined ACTION_* constants.
     * @var int
     */
    private $action;

    /**
     * The condition, describing when the action needs to be triggered.
     * @var Condition
     */
    private $condition;

    /**
     * Crud_Field_Behaviour constructor.
     * @param Crud $crud
     */
    public function __construct(Crud $crud)
    {
        $this->crud = $crud;
        $this->assert = $crud->get_assert();
        $this->field_name = null;
        $this->action = null;
        $this->condition = null;
    }

    public function set_field_name(string $field_name) : void
    {
        $args = compact('field_name');

        $this->field_name = null;

        // Check if $field_name is set an non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // Check if $field_name exists in the current state of the main table set.
        if ( !$this->crud->get_table_set()->has_column('', $field_name) )
        {
            $this->crud->error(__FUNCTION__.'(): The field "'.$field_name.'" could not be found.', $args);
            return;
        }

        $this->field_name = $field_name;
    }

    public function get_field_name() : string
    {
        return $this->field_name;
    }

    /**
     * Checks if the specified action (as an identifier) is supported.
     * @param int $action The action identifier to check.
     * @return bool true / false
     */
    public static function is_supported_action(int $action) : bool {
        return ($action > 0 and $action <= 7);
    }

    /**
     * @param int $action Must be one of the supported actions, which are defined as constants in this class.
     */
    public function set_action(int $action) : void
    {
        $this->action = null;

        // The specified action must be one of the supported actions.
        if ( !self::is_supported_action($action) )
        {
            return;
        }

        $this->action = $action;
    }

    public function get_action(): int {
        return $this->action;
    }

    public function set_condition(Condition $condition): void
    {
        $this->condition = $condition;
    }

    public function get_condition(): Condition
    {
        return $this->condition;
    }

    /**
     * Checks if this field behaviour object is valid in its current state:
     * It therefore must have a field_name, an action and the condition set (not equal to null).
     * @return bool true / false
     */
    public function is_valid(): bool
    {
        return
            $this->field_name !== null and
            $this->action !== null and
            $this->condition !== null;
    }

    public function to_string(): string
    {
        return
            __CLASS__
            .': ['
            .json_encode([
                'action' => $this->action,
                'condition' => $this->condition->to_string()
            ])
            .']';
    }

    public function to_json(): string
    {
        return json_encode([
            'action' => $this->action,
            'condition' => $this->condition->replaced_field_names()->to_json()
        ]);
    }

}