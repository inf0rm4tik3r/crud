<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Behaviour;

use Closure;
use Lukaspotthast\Crud\Condition\Condition;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Func;

class Behaviour_Controller
{

    private $crud;

    /**
     * Stores the defined field behaviours for each field.
     * @var array
     */
    private $field_behaviours;

    /**
     * @var array
     */
    private $column_behaviours = [];

    public function __construct(Crud &$crud)
    {
        $this->crud = $crud;
    }

    public function reset(): void {
        $this->reset_field_behaviours();
        $this->reset_column_behaviours();
    }

    public function reset_field_behaviours(): void {
        $this->field_behaviours = [];
    }

    public function reset_column_behaviours(): void {
        $this->column_behaviours = [];
    }

    /**
     * The $condition must either be an instance of Crud_Condition, or a closure function. If the latter,
     * a fresh Crud_Condition object will be passed to this function as the only argument. It can then be instantiated
     * and must not be returned.
     *
     * @param string $field_name
     * @param Condition|Closure $condition
     * @param int $action
     */
    public function add_field_behaviour(string $field_name, $condition, int $action): void
    {
        $args = compact('field_name', 'action', 'condition');

        // The $field_name must be a non empty string.
        if ( !$this->crud->get_assert()->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // The $action must be an integer and must be supported.
        if ( !$this->crud->get_assert()->is_int(__FUNCTION__, $action, 'action', $args) )
        {
            return;
        }
        if ( !Field_Behaviour::is_supported_action($action) )
        {
            $this->crud->error(__FUNCTION__.'(): The specified action is not supported!', $args);
            return;
        }

        // The $condition must either be an instance of Crud_Condition or a closure function.
        if ( $condition instanceof Condition ) {
            // All fine. Nothing to do here.
        }
        else if ( Func::is_closure($condition) )
        {
            // Create a fresh Crud_Condition object, let the closure function set up the condition, and then use it
            // as the condition for the Crud_Field_Behaviour.
            $condition_object = new Condition($this->crud);
            $condition->__invoke($condition_object);
            $condition = $condition_object;
        }
        else
        {
            // Not supported argument type.
            $this->crud->error(
                __FUNCTION__.'(): The specified $condition was neither a Crud_Condition object nor a closure',
                $args
            );
            return;
        }

        $field_behaviour = new Field_Behaviour($this->crud);
        $field_behaviour->set_field_name($field_name);
        $field_behaviour->set_action($action);
        $field_behaviour->set_condition($condition);

        // Abort if the constructed field behaviour object is not valid.
        // This would be the case if the specified field name was incorrect.
        if ( !$field_behaviour->is_valid() )
        {
            $args = compact('field_name', 'action', 'condition');
            $this->crud->error(__FUNCTION__.'(): The behaviour object is not valid.', $args);
            return;
        }

        if ( !array_key_exists($field_name, $this->field_behaviours) )
        {
            $this->field_behaviours[$field_name] = array();
        }
        array_push($this->field_behaviours[$field_name], $field_behaviour);
    }

    public function has_field_behaviours(string $field_name) : bool
    {
        return array_key_exists($field_name, $this->field_behaviours) and count($this->field_behaviours[$field_name]) > 0;
    }

    public function get_field_behaviours(string $field_name) : array
    {
        return $this->field_behaviours[$field_name];
    }

    public function get_field_behaviours_as_json(string $field_name) : string
    {
        $behaviours_array = [];

        if ( $this->has_field_behaviours($field_name) )
        {
            foreach ($this->get_field_behaviours($field_name) as &$field_behaviour)
            {
                array_push($behaviours_array, $field_behaviour->to_json());
            }
        }

        return json_encode($behaviours_array);
    }

}