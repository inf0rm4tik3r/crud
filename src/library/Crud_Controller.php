<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Lukaspotthast\Crud\Database\DBHelper;
use Lukaspotthast\Crud\Database\Relation;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\Http\Request;
use Lukaspotthast\Crud\Support\Date;

class Crud_Controller
{

    /**
     * Retrieves the crud instance from the session on which the given request wants to operate.
     * This must be specified through the 'crud_instance_name' input.
     *
     * @param Request $request The request object of the current ajax call.
     * @return Crud The loaded and unserialized ready to use Crud instance.
     */
    private function get_crud_instance(Request $request) : Crud
    {
        $instance = Crud::get_instance($request->input('crud_instance_name'), false, false, true);

        $instance->check_verification_key($request->input('crud_instance_verification_key'));
        $instance->set_requester('ajax');

        return $instance;
    }

    /**
     * Reloads an instance.
     *
     * @param Request $request
     * @return mixed
     */
    public function reload(Request $request): string
    {
        Debug::start_measure('CRUD - working...');
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the reload from the Session..
        $instance = $this->get_crud_instance($request);

        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        // Load the desired task, if necessary.
        if ( $request->has('crud_task_after') )
        {
            $instance->set_task($request->input('crud_task_after'));
        }

        Debug::stop_measure(__FUNCTION__);

        $out = $instance->render();
        Debug::stop_measure('CRUD - working...');
        return $out;
    }

    public function reset(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the reload from the Session..
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_field(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $current_task = $instance->get_task();
        $current_entry_id = $instance->get_current_entry_id();

        // Temporarily set the instance into edit mode.
        $instance->set_task('edit');
        $instance->set_current_entry_id($request->input('crud_entry_id'));

        $output = $instance->get_renderer()->render_field($request->input('crud_field_name'));

        // Restore the previous task.
        $instance->set_task($current_task);
        $instance->set_current_entry_id($current_entry_id);

        // Whe do not call the ordinary render function, so we need to store the instance manually.
        $instance->get_storage()->store_instance();

        Debug::stop_measure(__FUNCTION__);

        return $output;
    }

    public function load_list_view(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $instance->set_current_entry_id(null);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_structure_view(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $instance->set_current_entry_id(null);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_create_view(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $instance->set_current_entry_id(null);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_view_view(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $instance->set_current_entry_id($request->input('crud_entry'));

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_edit_view(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $instance->set_current_entry_id($request->input('crud_entry'));

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_limit(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_result_limit((int)$request->input('crud_limit'));

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function load_page(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the load from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->get_paginator()->set_current_page((int)$request->input('crud_page'));

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function save(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the save from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->save_ajax(
            self::prepare_field_data($instance, $request->all()['crud_field_data']),
            $request->input('crud_task_from'),
            $request->input('crud_task_after'),
            null
        );

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function save_inline(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the save from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->save_ajax(
            self::prepare_field_data($instance, $request->all()['crud_field_data']),
            $request->input('crud_task_from'),
            $request->input('crud_task_after'),
            $request->input('crud_entry_id')
        );

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function delete(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $old_entry_id = $instance->get_current_entry_id();
        $instance->set_current_entry_id($request->input('crud_entry_id'));
        $instance->delete_ajax($request->input('crud_entry_id'));
        $instance->set_current_entry_id($old_entry_id);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function restore(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the restore from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->set_task($request->input('crud_task_after'));
        $old_entry_id = $instance->get_current_entry_id();
        $instance->set_current_entry_id($request->input('crud_entry_id'));
        $instance->restore_entry((int)$request->input('crud_entry_id'));
        $instance->set_current_entry_id($old_entry_id);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function order_by(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        // Retrieve the name of the column which should be changed.
        $column_name = $request->input('crud_column_name');
        // Save the current order_by status of that column, so we can restore that later.
        $old_order_by_status = $instance->get_order_by_status($column_name);

        // Check whether the click should reset the previous order_by selection.
        // This can only happen if the instances order_by mode is not 'multi'!
        if ( $instance->get_order_by_mode() !== 'multi' )
        {
            // In 'single' mode, every click has to reset the previous order_by selection.
            if ( !$request->input('should_be_appended') or $instance->get_order_by_mode() === 'single'  )
            {
                // If not, the click on the current column has to remove all other order_by selections.
                $instance->reset_order_by_selection();
                // The previous state of the current column must then be restored.
                if ( !is_null($old_order_by_status) )
                {
                    $instance->order_by($column_name, $old_order_by_status);
                }
            }
        }

        // Selection the next order_by state.
        switch($old_order_by_status)
        {
            case null:
                $instance->order_by($column_name, 'asc');
                break;
            case 'asc':
                $instance->order_by($column_name, 'desc');
                break;
            case 'desc':
                $instance->remove_order_by($column_name);
        }

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function apply_reorder(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $table_name = $request->input('table_name');
        $order_field = $request->input('order_field');

        $id_dragged = $request->input('id_dragged');
        $old_position = $request->input('old_position');
        $new_position = $request->input('new_position');

        DBHelper::perform_reorder($table_name, $order_field, null, null, $id_dragged, $old_position, $new_position);
        DBHelper::maintain_order_integrity($table_name, $order_field, null, null);

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function toggle_search_tab(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $search_tab = $instance->get_search_tab($request->input('search_tab_internal_name'));
        $search_tab->toggle_active();

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }


    public function load_search(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        // DO SMTH

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function apply_search(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->get_search()->apply($request->input('search_input'));

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }

    public function reset_search(Request $request): string
    {
        Debug::start_measure(__FUNCTION__, 'CRUD - '.__FUNCTION__);

        // Get the instance-object that requested the delete from the Session.
        $instance = $this->get_crud_instance($request);
        if ( $instance->has_verification_error() )
        {
            return $instance->render();
        }

        $instance->get_search()->reset();

        Debug::stop_measure(__FUNCTION__);

        return $instance->render();
    }


    public function async_validator(Request $request)
    {
        return 'nooooo :D';
    }



    /* HELPER FUNCTIONS */


    /**
     * Only the output of this function should be used to perform a database update!
     *
     * @param $crud
     * @param $field_data_input
     * @return array
     */
    private function prepare_field_data(Crud $crud, $field_data_input)
    {
        $full_date_format =
             $crud->get_date_format_internal()
            .$crud->get_date_separator_internal()
            .$crud->get_time_format_internal();

        $date_format = $crud->get_date_format_internal();
        $time_format = $crud->get_time_format_internal();

        $timestamp_default = DBHelper::get_default_for_col_type('timestamp');
        $datetime_default = DBHelper::get_default_for_col_type('datetime');
        $date_default = DBHelper::get_default_for_col_type('date');
        $time_default = DBHelper::get_default_for_col_type('time');

        $field_data = array();

        foreach ( $field_data_input as /** @var array $input */ &$input )
        {
            // Decode the input id.
            $id_decoded = Tools::decode_input_id($input['id']);

            Debug::info($id_decoded);

            // Acquire the column-schema for the current field input (based on the decoded id).
            $col = $crud->get_table_set()->get_column($id_decoded['table_name'], $id_decoded['field_name']);

            // If get_column() returned null, the column does not exist (or at least not inside the specified table).
            if ( is_null($col) )
            {
                // We then must break the current iteration. The given input was unknown!
                // It should not be processed any further.
                continue;
            }

            $field = $col->get_name();
            $type = $col->get_type()->get_name();

            $data = $crud->get_table_set()->get_field_data_info($id_decoded['table_name'], $id_decoded['field_name'], $input['value'], false, false);
            //Debug::info("field data for '".$field."': ", $data);

            $value = $data['value'];

            // If no value was set, we use a fallback to the default value of the database.
            //TODO: allow null
            if ( !isset($value) )
            {
                Debug::info($field.' value was not set');
                $value = DBHelper::get_default_for_col_type($type); // TODO or allow NULL! if specified in $col->real_type !!!
            }

            // If an array got provided, it should be processed so it can get stored in the database.
            if (is_array($value))
            {
                if ( $data['internal_type'] === 'relation' and $data['display_type'] === 'multiselect' )
                {
                    $value = Relation::stringify_keys($value);
                }
                else
                {
                    Debug::warning('Crud: '.__FUNCTION__.'(): The submitted data for "'.$field.'" was an '.
                        'array, but the system does not know how to handle arrays for fields of internal_type "'.
                        $data['internal_type'].'" and display_type "'.$data['display_type'].'"!');
                }
            }

            $field_data[$field] = $value;

            /* TIMESTAMP */
            if ($type === 'timestamp')
            {
                // If the "on_update CURRENT TIMESTAMP" extra got defined in the database,
                // this timestamp should be set to the current time!
                if ( $col->on_update_current_timestamp() )
                {
                    $field_data[$field] = date( $full_date_format, time() );
                }
                else
                {
                    $field_data[$field] = Date::get_formatted($full_date_format, $value, $timestamp_default);
                }
            }

            /* DATETIME */
            else if ($type === 'datetime')
            {
                $field_data[$field] = Date::get_formatted($full_date_format, $value, $datetime_default);
            }

            /* DATE */
            else if ( $type === 'date' )
            {
                $field_data[$field] = Date::get_formatted($date_format, $value, $date_default);
            }

            /* TIME */
            else if ( $type === 'time' )
            {
                $field_data[$field] = Date::get_formatted($time_format, $value, $time_default);
            }

            /* INT */
            else if ( $type === 'tinyint' or $type === 'smallint' or $type === 'int' or $type === 'bigint' )
            {
                if ( empty($value) )
                {
                    $field_data[$field] = DBHelper::get_default_for_col_type($type);
                }
                else
                {
                    $field_data[$field] = intval($value);
                }
            }

            /* IMAGE */
            else if ( $type === 'image' )
            {
                // TODO: sollte geändert werden: aktueller wert in db muss mitgegeben und zurück übergeben werden. leer bedeutet bild löschen.
                // Remove the input if no file was selected, so we do not loose the information stored in the current
                // entry.
                if ( empty($value) or $value === 'undefined' )
                {
                    unset($field_data[$field]);
                }
            }

            /*
             * INTERNAL TYPE
             */

            // Signature
            if ( $data['internal_type'] === 'signature' )
            {
                //...
            }
        }

        return $field_data;
    }

}