<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Search;

use Lukaspotthast\Crud\Condition\Condition;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Tools;

class Crud_Search
{

    /**
     * @var Crud
     */
    private $crud;

    /**
     * @var Condition
     */
    private $condition;

    /**
     * Crud_Search constructor.
     * @param Crud $crud
     */
    public function __construct(&$crud)
    {
        $this->crud      = $crud;
        $this->condition = new Condition($crud, $this);
    }

    /**
     * @param array $data_array
     */
    public function apply(array $data_array): void
    {
        foreach ( $data_array as $encoded_id => &$data )
        {
            $id = Tools::decode_input_id($encoded_id);

            $this->condition->where($id['table_name'] . '.' . $id['field_name'], $data['op'], $data['val']);
        }
    }

    /**
     *
     */
    public function reset(): void
    {
        $this->condition->reset();
    }

    /**
     * @return Condition
     */
    public function get_condition(): Condition
    {
        return $this->condition;
    }

}