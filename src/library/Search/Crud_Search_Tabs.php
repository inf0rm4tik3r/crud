<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Search;

trait Crud_Search_Tabs
{

    private $search_tabs; // array

    private function reset_search_tabs()
    {
        $this->search_tabs = array();
    }

    /**
     * Adds a search tab to this instance.<br>
     * Search tabs will be displayed in the list view and allow the user to quickly apply certain search parameters.<br>
     * The parameters set with a search tab will not affect the users ability to use the search function. Search tab
     * where queries will be applied before the users search input.<br>
     * If a search tab with the specified internal name already exists, a warning will be generated and the old object
     * gets overwritten!
     *
     * @param string $internal_name The internal name with which this search tab get referenced. Needs to be unique.
     * @return Search_Tab The newly generated Crud_Search_Tab object.
     */
    public function search_tab(string $internal_name) : Search_Tab
    {
        $args = compact('internal_name');

        // Check if the internal name got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $internal_name, 'name', $args) )
        {
            return null;
        }

        // Check if a search tab already got defined with the given internal name.
        if ( array_key_exists($internal_name, $this->search_tabs) )
        {
            $this->warning(
                __FUNCTION__.'(): A search tab with the internal name "'.$internal_name
                .' already got defined. Overwriting...');
        }

        // Create a new search tab.
        $search_tab = new Search_Tab($this, $internal_name);

        // Add the search tab to the collection.
        $this->search_tabs[$internal_name] = $search_tab;

        // Return the search tab object to allow easy manipulation by the user.
        return $search_tab;
    }

    /**
     * Acquire the search tab object for the specified name.
     *
     * @param string $internal_name The internal name of the object that should be returned.
     * @return Search_Tab Returns the found search tab or null on an error or if no tab exists for the given name.
     */
    public function get_search_tab(string $internal_name) : ?Search_Tab
    {
        $args = compact('internal_name');

        // Check if the internal name got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $internal_name, 'name', $args) )
        {
            return null;
        }

        // Check if a search tab for the specified name really exists.
        if ( !array_key_exists($internal_name, $this->search_tabs) )
        {
            $this->error(__FUNCTION__.'(): There is no search tab present for: "'.$internal_name.'".', $args);
            return null;
        }

        // The search tab exists, so we can return it to the requester.
        return $this->search_tabs[$internal_name];
    }

    /**
     * Check if search tabs got defined.
     *
     * @return bool True if search tabs got defined. False otherwise.
     */
    public function has_search_tabs() : bool
    {
        return count($this->search_tabs) > 0;
    }

    /**
     * Acquire all defined search tab objects.
     *
     * @return array Returns an array of the form (string internal_name => Crud_Search_Tab object).
     */
    public function get_search_tabs() : array
    {
        return $this->search_tabs;
    }

}