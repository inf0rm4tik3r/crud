<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Search;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Condition\Condition;

class Search_Tab
{

    /**
     * Adds a search tab to this instance.<br>
     * Search tabs will be displayed in the list view and allow the user to quickly apply certain search parameters.<br>
     * The parameters set with a search tab will not affect the users ability to use the search function. Search tab
     * where queries will be applied before the users search input.<br>
     * If a search tab with the specified internal name already exists, a warning will be generated and the old object
     * gets overwritten!
     *
     * @param string $internal_name The internal name with which this search tab get referenced. Needs to be unique.
     * @param string $display_name The name which will be display to the user. May contain HTML code.
     * @param string $icon_classes Optional icon classes. e.g. "fa fa-users"
     * @param string $classes Optional css class names which will be applied to the wrapping div of the search tab.
     * @return Search_Tab
     */

    private $crud = null;
    private $internal_name = null;
    private $display_name = 'no_display_name_specified';
    private $icon_classes = null;
    private $classes = null;
    private $where_query = null;
    private $active = false;

    public static function add_search_tab_queries(&$search_tabs, &$query)
    {
        foreach ( $search_tabs as &$search_tab )
        {
            if ( $search_tab->is_active() )
            {
                $query = $query->where(function ($sub_query) use ($search_tab) {
                    $search_tab->get_where_query()->add_to($sub_query);
                });
            }
        }
    }

    public function __construct(Crud &$crud, string $internal_name = 'search_tab')
    {
        $this->crud = $crud;
        $this->internal_name = $internal_name;

        $this->where_query = new Condition($crud);
    }

    public function get_internal_name() : string
    {
        return $this->internal_name;
    }

    public function set_display_name(string $display_name) : void
    {
        // Check if the display name got properly specified.
        if ( !$this->crud->get_assert()->is_non_empty_string(__FUNCTION__, $display_name, 'display_name', compact('display_name')) )
        {
            return;
        }

        $this->display_name = $display_name;
    }

    public function get_display_name() : string
    {
        return $this->display_name;
    }

    public function set_icon_classes(string $icon_classes) : void
    {
        // Check if the icon classes got properly specified.
        if ( !$this->crud->get_assert()->is_non_empty_string(__FUNCTION__, $icon_classes, 'icon_classes', compact('icon_classes')) )
        {
            return;
        }

        $this->icon_classes = $icon_classes;
    }

    public function has_icon_classes() : bool
    {
        return !empty($this->icon_classes);
    }

    public function get_icon_classes() : string
    {
        return $this->icon_classes;
    }

    public function get_where_query() : Condition
    {
        return $this->where_query;
    }

    public function active(bool $active) : void
    {
        // Check if the active parameter got properly specified.
        if ( !$this->crud->get_assert()->is_bool(__FUNCTION__, $active, 'active', compact('active')) )
        {
            return;
        }

        // Argument is valid. The active state can be set.
        $this->active = $active;
    }

    public function toggle_active()
    {
        $this->active = !$this->active;
    }

    public function is_active() : bool
    {
        return $this->active;
    }
}