<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Debug;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Debugging;
use Lukaspotthast\Crud\Frameworks\Laravel\Laravel_Debugging;

/**
 * Class Debug
 * @package Lukaspotthast\Crud\Debug
 */
abstract class Debug
{

    /**
     * @var bool
     */
    private static $active = true;

    public static function set_active(bool $active): void
    {
        self::$active = $active;
    }

    public static function is_active(): bool
    {
        return self::$active;
    }

    public static function init(): void
    {
        if ( self::$active )
        {
            switch ( Crud::get_framework() )
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_init();
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_init();
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function debug($data): void
    {
        if ( self::$active )
        {
            switch (Crud::get_framework())
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_message('debug', $data);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_message('debug', $data);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function info($data): void
    {
        if ( self::$active )
        {
            switch (Crud::get_framework())
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_message('info', $data);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_message('info', $data);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function warning($data): void
    {
        if ( self::$active )
        {
            switch ( Crud::get_framework() )
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_message('warning', $data);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_message('warning', $data);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function error($data): void
    {
        if ( self::$active )
        {
            switch ( Crud::get_framework() )
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_message('error', $data);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_message('error', $data);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function start_measure(string $identifier, string $name = ''): void
    {
        if ( self::$active )
        {
            switch ( Crud::get_framework() )
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_start_measure($identifier, $name);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_start_measure($identifier, $name);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

    public static function stop_measure(string $identifier): void
    {
        if ( self::$active )
        {
            switch ( Crud::get_framework() )
            {
                case Frameworks::PHP:
                    PHP_Debugging::debug_stop_measure($identifier);
                    break;

                case Frameworks::LARAVEL:
                    Laravel_Debugging::debug_stop_measure($identifier);
                    break;

                default:
                    die('Unsupported framework specified!');
                    break;
            }
        }
    }

}