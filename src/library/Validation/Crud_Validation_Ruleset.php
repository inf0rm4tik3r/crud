<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Validation;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Debug\Debug;

/**
 * Class Crud_Validation_Ruleset
 * @package Lukaspotthast\Crud\Validation
 */
class Crud_Validation_Ruleset
{

    /**
     * @var Crud
     */
    private $crud;

    /**
     * @var array
     */
    private $rules;

    function __construct(Crud $crud)
    {
        $this->crud = $crud;
        $this->rules = array();
    }

    public function reset() : Crud_Validation_Ruleset
    {
        $this->rules = array();
        return $this;
    }

    public function debug() : void
    {
        Debug::debug('Crud_Validation_Ruleset: debug(): ');
        Debug::debug($this->rules);
    }

    public function set_rule(string $name, $option) : Crud_Validation_Ruleset
    {
        $this->rules[$name] = $option;

        return $this;
    }

    public function add_rule(string $name, $option) : Crud_Validation_Ruleset
    {
        // TODO: argument checks
        $args = compact('name', 'option');

        if ( !$this->rule_exists($name) )
        {
            $this->set_rule($name, $option);
        }
        else
        {
            $this->crud->error(__FUNCTION__.'(): The rule "'.$name.'" already got defined.', $args);
        }

        return $this;
    }

    public function maybe_add_rule(string $name, $option) : Crud_Validation_Ruleset
    {
        if ( !$this->rule_exists($name) )
        {
            $this->set_rule($name, $option);
        }

        return $this;
    }

    public function remove_rule(string $name) : Crud_Validation_Ruleset
    {
        $args = compact('name');

        if ( $this->rule_exists($name) )
        {
            unset( $this->rules[$name] );
        }
        else
        {
            $this->crud->error(
                __FUNCTION__.'(): The rule "'.$name.'" is not defined and therefore cannot be removed.', $args
            );
        }

        return $this;
    }

    public function maybe_remove_rule(string $name) : Crud_Validation_Ruleset
    {
        if ( $this->rule_exists($name) )
        {
            unset( $this->rules[$name] );
        }

        return $this;
    }

    public function get_rule(string $name) // string|array|null
    {
        if ( $this->rule_exists($name) )
        {
            return $this->rules[$name];
        }
        else
        {
            return null;
        }
    }

    public function rule_exists($name) : bool
    {
        return array_key_exists($name, $this->rules);
    }

    public function get_rules_array() : array
    {
        return $this->rules;
    }

    public function get_rules_json() : string
    {
        return json_encode($this->rules);
    }

}