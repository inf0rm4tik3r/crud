<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Validation;

trait Crud_Validation
{

    /**
     * These are the validator names, which are always available for use through the validate.js library.
     * @var array
     */
    private $STANDARD_VALIDATORS = [
        'date',
        'datetime',
        'email',
        'equality',
        'exclusion',
        'format',
        'inclusion',
        'length',
        'numericality',
        'presence',
        'url'
    ];

    private $field_validation_rulesets; // array

    private $custom_validators; // array
    private $custom_async_validators; // array



    private function reset_validation() : void
    {
        $this->field_validation_rulesets = array();

        $this->custom_validators = array();
        $this->custom_async_validators = array();
    }

    public function debug_validation_rulesets() : void
    {
        foreach ( $this->field_validation_rulesets as &$ruleset )
        {
            $ruleset->debug();
        }
    }

    public function get_validation_ruleset(string $field_name) : Crud_Validation_Ruleset
    {
        if ( $this->validation_ruleset_exists($field_name) )
        {
            // A validation ruleset is already present. We can simply return it.
            return $this->field_validation_rulesets[$field_name];
        }
        else
        {
            // A validation rules does not jet exists. We need to instantiate and store one.
            $validation_ruleset = new Crud_Validation_Ruleset($this);
            $this->field_validation_rulesets[$field_name] = $validation_ruleset;
            return $validation_ruleset;
        }
    }

    private function validation_ruleset_exists(string $field_name) : bool
    {
        return array_key_exists($field_name, $this->field_validation_rulesets);
    }


    public function define_validator(string $validator_name, array $supported_options, string $js_function_name) : void
    {
        $args = compact('validator_name', 'supported_options', 'js_function_name');

        // The validator name must be specified as a non empty string.
        if ( !Assert::is_non_empty_string($this, __FUNCTION__, $validator_name, 'validator_name', $args) )
        {
            return;
        }

        // The validator name must not be in use.
        if ( $this->validator_already_defined($validator_name) )
        {
            $this->error(__FUNCTION__.'(): A validator with the name "'.$validator_name.'" already exists!', $args);
            return;
        }

        // The supported option must be defined as an array.
        if ( !Assert::is_array($this, __FUNCTION__, $supported_options, 'supported_options', $args) )
        {
            return;
        }

        // The JavaScript function name must be specified as a non empty string.
        if ( !Assert::is_non_empty_string($this, __FUNCTION__, $js_function_name, 'js_function_name', $args) )
        {
            return;
        }

        /*
         * The input is valid:
         */

        $this->custom_validators[$validator_name] = [
            'supported_options' => $supported_options,
            'js_function_name' => $js_function_name
        ];
    }

    public function has_custom_validator(string $validator_name) : bool
    {
        return array_key_exists($validator_name, $this->custom_validators);
    }

    public function get_custom_validator(string $validator_name) : ?array
    {
        if ( $this->has_custom_validator($validator_name) )
        {
            return $this->custom_validators[$validator_name];
        }
        return null;
    }

    public function get_custom_validators() : array
    {
        return $this->custom_validators;
    }


    public function define_async_validator(string $validator_name, array $supported_options, $function) : void
    {
    }

    public function has_custom_async_validator(string $validator_name) : bool
    {
        return array_key_exists($validator_name, $this->custom_async_validators);
    }

    public function get_custom_async_validator(string $validator_name) : ?array
    {
        if ( $this->has_custom_validator($validator_name) )
        {
            return $this->custom_async_validators[$validator_name];
        }
        return null;
    }

    public function get_custom_async_validators() : array
    {
        return $this->custom_async_validators;
    }


    private function validator_already_defined(string $validator_name) : bool
    {
        if ( !Assert::is_non_empty_string($this, __FUNCTION__, $validator_name, 'validator_name', compact('validator_name')) )
        {
            return false;
        }

        if ( in_array($validator_name, $this->STANDARD_VALIDATORS) )
        {
            return true;
        }

        if ( $this->has_custom_validator($validator_name) )
        {
            return true;
        }

        if ( $this->has_custom_async_validator($validator_name) )
        {
            return true;
        }

        return false;
    }



}