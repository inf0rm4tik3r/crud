<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Asset;

use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Crud;

class Asset
{
    /* Where did i put all those files exactly? */
    const PACKAGE_PUBLIC_FOLDER = __DIR__ . '/../../public/';
    const BASE_PATH = '';//'libraries/crud/';
    const BASE_PATH_PLUGINS = 'plugins/';//'libraries/crud/plugins/';
    const REAL_PATH = self::PACKAGE_PUBLIC_FOLDER.self::BASE_PATH;
    const REAL_PATH_PLUGINS = self::PACKAGE_PUBLIC_FOLDER.self::BASE_PATH_PLUGINS;
    const DEPLOY_PATH = 'libraries/crud/';
    const DEPLOY_PATH_PLUGINS = 'libraries/crud/plugins/';

    /**
     * [
     *   asset_name (identifier) => [
     *     'path' => path,
     *     'files' [
     *       file_name => file_type (css / js)
     *     ]
     *   ]
     * ]
     */
    const PACKAGE_ASSETS = [
        'crud' => [
            'path' => self::REAL_PATH,
            'output' => '',
            'files' => [
                'css/crud.css' => 'css',
                'js/dist/crud.js' => 'js'
            ]
        ],

        'crud_minified' => [
            'path' => self::REAL_PATH,
            'output' => '',
            'files' => [
                'css/crud.min.css' => 'css',
                'js/dist/crud.min.js' => 'js'
            ]
        ],
    ];

    const PACKAGE_PLUGIN_ASSETS = [
        'jquery' => [
            'path' => self::REAL_PATH_PLUGINS.'jquery',
            'output' => 'jquery',
            'files' => [
                'jquery/jquery-3.2.1.min.js' => 'js'
            ]
        ],

        'jcrop' => [
            'path' => self::REAL_PATH_PLUGINS.'jcrop',
            'output' => 'jcrop',
            'files' => [
                'jcrop/css/jquery.Jcrop.min.css' => 'css',
                'jcrop/js/jquery.Jcrop.min.js' => 'js'
            ]
        ],

        'jqueryui' => [
            'path' => self::REAL_PATH_PLUGINS.'jqueryui',
            'output' => 'jqueryui',
            'files' => [
                'jqueryui/css/jquery-ui.min.css' => 'css',
                'jqueryui/js/jquery-ui.min.js' => 'js'
            ]
        ],

        'popper.js' => [
            'path' => self::REAL_PATH_PLUGINS.'popper.js',
            'output' => 'popper.js',
            'files' => [
                'popper.js/popper.min.js' => 'js',
                'popper.js/popper-utils.min.js' => 'js',
            ]
        ],

        'tooltip.js' => [
            'path' => self::REAL_PATH_PLUGINS.'tooltip.js',
            'output' => 'popper.js',
            'files' => [
                'tooltip.js/tooltip.min.js' => 'js'
            ]
        ],

        'tooltipster' => [
            'path' => self::REAL_PATH_PLUGINS.'tooltipster',
            'output' => 'tooltipster',
            'files' => [
                'tooltipster/css/tooltipster.bundle.min.css' => 'css',
                'tooltipster/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-borderless.min.css' => 'css',
                'tooltipster/js/tooltipster.bundle.min.js' => 'js',
            ]
        ],

        'fontawesome' => [
            'path' => self::REAL_PATH_PLUGINS.'fontawesome',
            'output' => 'fontawesome',
            'files' => [
                'fontawesome/css/font-awesome.min.css' => 'css'
            ]
        ],

        'highlight.js' => [
            'path' => self::REAL_PATH_PLUGINS.'highlight.js',
            'output' => 'highlight.js',
            'files' => [
                'highlight.js/styles/default.css' => 'css',
                'highlight.js/highlight.pack.js' => 'js'
            ]
        ],

        'copybtn' => [
            'path' => self::REAL_PATH_PLUGINS.'copybtn',
            'output' => 'copybtn',
            'files' => [
                'copybtn/copybtn.css' => 'css',
                'copybtn/copybtn.js' => 'js'
            ]
        ],

        'flatpickr' => [
            'path' => self::REAL_PATH_PLUGINS.'flatpickr',
            'output' => 'flatpickr',
            'files' => [
                'flatpickr/flatpickr.min.css' => 'css',
                'flatpickr/flatpickr.min.js' => 'js',
                'flatpickr/l10n/de.js' => 'js'
                //'flatpickr/l10n/de.js' => 'js'
            ]
        ],

        'tinymce' => [
            'path' => self::REAL_PATH_PLUGINS.'tinymce',
            'output' => 'tinymce',
            'files' => [
                'tinymce/tinymce-4.6.5/tinymce.min.js' => 'js'
            ]
        ],

        'ckeditor' => [
            'path' => self::REAL_PATH_PLUGINS.'ckeditor',
            'output' => 'ckeditor',
            'files' => [
                'ckeditor/ckeditor.js' => 'js'
            ]
        ],

        'select2' => [
            'path' => self::REAL_PATH_PLUGINS.'select2',
            'output' => 'select2',
            'files' => [
                'select2/css/select2.min.css' => 'css',
                'select2/js/select2.full.min.js' => 'js'
            ]
        ],

        'signature-pad' => [
            'path' => self::REAL_PATH_PLUGINS.'signature-pad',
            'output' => 'signature-pad',
            'files' => [
                'signature-pad/signature-pad.min.js' => 'js'
            ]
        ],

        'spectrum' => [
            'path' => self::REAL_PATH_PLUGINS.'spectrum',
            'output' => 'spectrum',
            'files' => [
                'spectrum/spectrum.css' => 'css',
                'spectrum/spectrum.js' => 'js'
            ]
        ],

        'moment.js' => [
            'path' => self::REAL_PATH_PLUGINS.'moment.js',
            'output' => 'moment.js',
            'files' => [
                'moment.js/moment.min.js' => 'js'
            ]
        ],

        'validate.js' => [
            'path' => self::REAL_PATH_PLUGINS.'validate.js',
            'output' => 'validate.js',
            'files' => [
                'validate.js/validate.min.js' => 'js'
            ]
        ],

        'iziModal' => [
            'path' => self::REAL_PATH_PLUGINS.'iziModal',
            'output' => 'iziModal',
            'files' => [
                'iziModal/css/iziModal.min.css' => 'css',
                'iziModal/js/iziModal.min.js' => 'js'
            ]
        ],
    ];

    /**
     * You may query the following assets:<br>
     * <ul>
     * <li> JQuery (3.2.1) - 'jquery' </li>
     * <li> Jcrop (0.9.12) - 'jcrop' </li>
     * <li> JQuery UI (1.12.1) - 'jqueryui' </li>
     * <li> Popper.js (1.12.5) - 'popper.js' </li>
     * <li> Tooltip.js (1.1.5) - 'tooltip.js' </li>
     * <li> Tooltipster (4.2.5) - 'tooltipster' </li>
     * <li> FontAwesome 4 (4.7.0) - 'fontawesome' </li>
     * <li> highlight.js (9.12.0) - 'highlight.js' </li>
     * <li> CopyBTN (1.0) - 'copybtn' </li>
     * <li> Flatpickr (3.0.7) - 'flatpickr' </li>
     * <li> Pickdate.js (3.5.6) - 'pickdate.js' </li>
     * <li> TinyMCE (4.6.5) - 'tinymce' </li>
     * <li> CK-Editor (?) - 'ckeditor' </li>
     * <li> Select2 (4.0.4) - 'select2' </li>
     * <li> Signature-Pad (2.3.2) - 'signature-pad' </li>
     * <li> Spectrum (1.8.0.0) - 'spectrum' </li>
     * <li> Moment.js (2.18.1) - 'moment.js' </li>
     * <li> Validate.js (0.11.1) - 'validate.js' </li>
     * <li> iziModal (1.5.1) - 'iziModal' </li>
     * <br>
     * <li> CRUD - 'crud' </li>
     * <li> CRUD - 'crud_minified' </li>
     * </ul>
     *
     * @param string $identifier
     * @return string <script> and <link> tags which will load the requested asset.
     */
    public static function get(string $identifier) : string
    {
        $in_package_assets = array_key_exists($identifier, self::PACKAGE_ASSETS);
        $in_package_plugin_assets = array_key_exists($identifier, self::PACKAGE_PLUGIN_ASSETS);

        if ( !$in_package_assets and !$in_package_plugin_assets )
        {
            $info = __CLASS__.': '.__FUNCTION__.'(): '.'\"'.$identifier.'\" is not valid. Check method documentation!';
            return '
                <script> 
                    window.alert("'.$info.'");
                    console.error("'.$info.'");
                </script>
            ';
        }

        $include = '';

        if ( $in_package_assets )
        {
            $asset = self::PACKAGE_ASSETS[$identifier];
            $asset_files = $asset['files'];
            $base_path = self::DEPLOY_PATH;
        }
        else
        {
            $asset = self::PACKAGE_PLUGIN_ASSETS[$identifier];
            $asset_files = $asset['files'];
            $base_path = self::DEPLOY_PATH_PLUGINS;
        }

        foreach ( $asset_files as $file_name => &$file_type )
        {
            if ( $file_type === 'css' )
            {
                $include .= '<link href="'.Crud::get_asset_path().$base_path.htmlspecialchars($file_name).'" rel="stylesheet">'.PHP_EOL;
            }
            else if ( $file_type === 'js' )
            {
                $include .= '<script src="'.Crud::get_asset_path().$base_path.htmlspecialchars($file_name).'"></script>'.PHP_EOL;
            }
            else
            {
                $info = __CLASS__.': '.__FUNCTION__.'(): '.'\"'.$file_type.'\" is not a not supported type.';
                $include .= '
                    <script> 
                        window.alert("'.$info.'");
                        console.error("'.$info.'");
                    </script>
                ';
            }
        }

        return $include;
    }

    /**
     * Returns whether the specified resource (JS library / plugin) should be automatically loaded.
     * @param string $identifier The name of the resource. See get() for what is available.
     * @return bool true / false
     */
    public static function should_be_autoloaded(string $identifier) : bool
    {
        // In the configuration file, array keys can not contain dots.
        // If the user passes in a string with dots, they must be replaced!
        // Laravel's config() function would otherwise interpret the dots and search in the wrong path.
        $identifier = str_replace('.', '_', $identifier);

        $result = Config::get('crud.load_asset.'.$identifier, false);

        if ( !is_bool($result) )
        {
            $result = false;
        }

        return $result;
    }

    /**
     * Returns the requested resource's integration lines only if it should be automatically loaded by the crud
     * system.<br> Internally used by the instance wrapper view to provide libraries during the rendering.<br>
     * See get_if_not_autoloaded() if you want to let the crud system integrate resources into your site.
     * @param string $identifier
     * @return string
     */
    public static function get_if_autoloaded(string $identifier) : string
    {
        if ( !self::should_be_autoloaded($identifier) )
        {
            return '';
        }

        return self::get($identifier);
    }

    /**
     * Returns the requested resources's integration lines (CSS and JS) only if that resource should not be
     * automatically loaded by the crud system.<br>
     * You can call this: <br>
     * <pre>
     *  <?= Asset::get_if_not_autoloaded('jquery') ?>
     * </pre>
     * See the get() method for information about which libraries / plugins can be obtained that way.
     * @param string $identifier
     * @return string
     */
    public static function get_if_not_autoloaded(string $identifier) : string
    {
        if ( self::should_be_autoloaded($identifier) )
        {
            return '';
        }

        return self::get($identifier);
    }

}