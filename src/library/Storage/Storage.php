<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Storage;

use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Relation;
use Lukaspotthast\Crud\Debug\Debug;

class Storage extends Session_Storage
{

    private $crud;

    public function __construct(?Crud $crud = null)
    {
        $this->crud = $crud;
    }

    public function store_instance(): void
    {
        if ( $this->crud === null )
        {
            die(__FUNCTION__ . '(): Unsupported if no CRUD instance specified.');
        }

        Debug::start_measure('store_instance', 'CRUD - store_instance');
        parent::store('crud_' . $this->crud->get_instance_name(), $this->crud);
        Debug::stop_measure('store_instance');
    }

    public function restore_instance(?string $identifier)
    {
        if ( $identifier === null )
        {
            $identifier = $this->crud->get_instance_name();
        }

        Debug::start_measure('restore_instance', 'CRUD - restore_instance');
        $instance = parent::restore('crud_' . $identifier);
        Debug::stop_measure('restore_instance');
        return $instance instanceof Crud ? $instance : null;
    }

    function prepare_for_storage(): void
    {
        if ( $this->crud === null )
        {
            die(__FUNCTION__ . '(): Unsupported if no CRUD instance specified.');
        }

        // We do not want to store that big chunk of data into our session.
        $this->crud->get_table_set()->invalidate_entry_cache();

        // Data loaded by relations should also not get stored in the session.
        foreach ( $this->crud->get_relations() as &$relation )
        {
            /** @var $relation Relation */

            // TODO: invalidate table_set cache
            $relation->_invalidate_cache();
        }

        // All nested table instances will be stored separately in the session.
        // We do not need to keep internal copies / references to them.
        $this->crud->invalidate_nested_table_cache();
    }

    function prepare_for_restoration(): void
    {
    }

    function uses_compression(): bool
    {
        $use_compression = Config::get('crud.use_compression', true);
        return (isset($use_compression) and is_bool($use_compression)) ? $use_compression : false;
    }

    function get_compression_level(): int
    {
        $compression_level = Config::get('crud.compression_level', 6);
        return (isset($compression_level) and is_int($compression_level)) ? $compression_level : 6;
    }

}