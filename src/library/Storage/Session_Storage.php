<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Storage;

use Lukaspotthast\Crud\Session\Session;
use Lukaspotthast\Support\Serialization;

/**
 * Class Session_Storage
 * @package Lukaspotthast\Crud\Storage
 */
abstract class Session_Storage implements Stores_Data
{

    /**
     * Stores the value specified by $data under the key $identifier in the session. <br>
     * This function serializes the incoming data, so you can safely provide arrays and objects.
     *
     * @param string $identifier
     *      The key under which the data gets saved in the session.
     * @param mixed  $data
     *      The data to store.
     */
    public function store(string $identifier, $data): void
    {
        $this->prepare_for_storage();

        $serialized = Serialization::maybe_serialize($data);

        if ( $serialized !== null )
        {
            if ( $this->uses_compression() )
            {
                $compressed = gzcompress($serialized, $this->get_compression_level());
            }
        }

        // Put the (compressed or uncompressed) serialized version of the current object in the session.
        Session::put($identifier, isset($compressed) ? $compressed : $serialized);
    }

    /**
     * Restores from the session what is currently stored under $identifier. <br>
     * This might return null!
     *
     * @param string $identifier
     *      The key used for the session lookup.
     *
     * @return mixed|null
     *      The unserialized value from the session or null.
     */
    public function restore(string $identifier)
    {
        $this->prepare_for_restoration();

        // $session_value might be null!
        $session_value = Session::get($identifier);

        if ( $session_value !== null )
        {
            if ( $this->uses_compression() )
            {
                $uncompressed = @gzuncompress($session_value);
            }
        }

        $unserialized = Serialization::maybe_unserialize(isset($uncompressed) ? $uncompressed : $session_value);

        return $unserialized;
    }

}