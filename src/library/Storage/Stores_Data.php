<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Storage;

/**
 * Interface Stores_Data
 * @package Lukaspotthast\Crud\Storage
 */
interface Stores_Data
{

    /**
     * @param string $identifier
     * @param        $data
     */
    function store(string $identifier, $data): void;

    /**
     * @param string $identifier
     * @return mixed
     */
    function restore(string $identifier);

    /**
     *
     */
    function prepare_for_storage(): void;

    /**
     *
     */
    function prepare_for_restoration(): void;

    /**
     * @return bool
     */
    function uses_compression(): bool;

    /**
     * @return int
     */
    function get_compression_level(): int;

}