<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Spreadsheet;

use Lukaspotthast\Crud\Crud;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Exporter
{

    /**
     * @var Crud
     */
    private $crud;

    public function __construct(Crud $crud)
    {
        $this->crud = $crud;
    }

    public function gather_data(): array
    {
        return [
            ['a', 'b', 'c', 'd'],
            [1, '2017', 'foo', true],
            [2, '2018', 'bar-baz', false]
        ];
    }


    /**
     * @param array $data
     *      The data, which should be exported. <br>
     *      The first entry of the array must define the columns. <br>
     *      Each other consecutive entry corresponds to an entry for the spreadsheet.
     * @return Spreadsheet
     */
    public function export(array $data): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $sheet = null;
        try {
            $sheet = $spreadsheet->getActiveSheet();
        } catch (Exception $e) {}

        // Return an empty spreadsheet if no data got defined.
        if ( count($data) === 0 )
        {
            return $spreadsheet;
        }

        try {
            $sheet->fromArray($data);
        } catch (Exception $e) {}

        /*
        // Extract the column information from the array.
        $col_info = $data[0];
        unset($data[0]);

        foreach ( $col_info as &$col )
        {
            $sheet->setCellValue('A1', 'Hello World !');

        }


        // Iterate over the leftover data, each entry in the array corresponds to an entry (row) of/for the spreadsheet.
        foreach ( $data as &$entry )
        {

        }
        $sheet->setCellValue('A1', 'Hello World !');
        */

        return $spreadsheet;
    }

    public function export_as_xlsx(): void
    {

    }

}