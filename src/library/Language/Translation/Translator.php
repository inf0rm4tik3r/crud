<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Language\Translation;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Translator;
use Lukaspotthast\Crud\Frameworks\Laravel\Laravel_Translator;

/**
 * Class Translator
 * @package Lukaspotthast\Crud\Language\Translation
 */
class Translator
{

    public static function init(): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Translator::translator_init();
                break;

            case Frameworks::LARAVEL:
                Laravel_Translator::translator_init();
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function set(string $key, $value): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Translator::translator_set($key, $value);
                break;

            case Frameworks::LARAVEL:
                Laravel_Translator::translator_set($key, $value);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function has(string $key): string
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                return PHP_Translator::translator_has($key);
                break;

            case Frameworks::LARAVEL:
                return Laravel_Translator::translator_has($key);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function get(string $key)
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                return PHP_Translator::translator_get($key);
                break;

            case Frameworks::LARAVEL:
                return Laravel_Translator::translator_get($key);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

}