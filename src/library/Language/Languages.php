<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Language;

abstract class Languages
{

    public const EN = 'en';
    public const DE = 'de';

}