<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Pagination;

use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Tools;

trait Crud_Result_Limits
{

    private $result_limit_options; // array

    private function hard_reset_result_limits() : void
    {
        $this->result_limit_options = array(
            array('limit' => 10, 'text' => '10'),
            array('limit' => 20, 'text' => '20'),
            array('limit' => 50, 'text' => '50'),
            array('limit' => -1, 'text' => Translator::get('crud.limit_all'))
        );

        // Activate the first limit.
        $this->update_result_limit( $this->result_limit_options[0]['limit'] );
    }

    /**
     * Pseudo function.
     * @param $limits
     */
    public function limits($limits)
    {
        $this->set_result_limit_options($limits);
    }

    public function set_result_limit_options($limits)
    {
        if ( !(isset($limits) and is_array($limits)) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): The $limits parameter needs to be an array an can not be null!</b><br>'
                .'Arguments passed:<br>'.Tools::args_as_list(compact('limits'))
            );
            return;
        }

        // Check if the given array represents a basic integer list.
        $all_ints = true;
        foreach ($limits as &$limit)
        {
            if (!is_int($limit))
            {
                $all_ints = false;
                break;
            }
        }
        // If so, check that every value is positive.
        if ($all_ints === true)
        {
            foreach ($limits as &$limit)
            {
                if ($limit < -1)
                {
                    $this->error('limit_option(): Limit options must be positive. Exception: -1 == "All".');
                    $all_ints = false;
                    break;
                }
            }
        }

        // Check if the given array is an array of ('limit','text') arrays.
        // But only if it is not already clear that the array only contains ints.
        $all_limit_text_pairs = true;
        if ($all_ints === false)
        {
            foreach ($limits as &$limit)
            {
                if (!is_array($limit))
                {
                    $all_limit_text_pairs = false;
                    break;
                }
                else
                {
                    if (!(array_key_exists('limit', $limit) and
                        array_key_exists('text', $limit))
                    )
                    {
                        $this->error('limit_options(): Every element in the array needs a "limit" value and a "text" value!');
                        $all_limit_text_pairs = false;
                        break;
                    }
                }
            }
            // Given parameter is an array of arrays with ('limit' and 'text').
            if ($all_limit_text_pairs)
            {
                foreach ($limits as &$limit)
                {
                    if (!(is_int($limit['limit']) and is_string($limit['text'])))
                    {
                        $this->error(
                            'limit_options(): An element of your $limits array contains wrong data.'
                            .' Every elements "limit" option needs to be an integer.'
                            .' Every elements "text" option needs to be a string.');
                        $all_limit_text_pairs = false;
                        break;
                    }
                }
            }
        }
        else
        {
            $all_limit_text_pairs = false;
        }

        if ($all_ints)
        {
            $this->result_limit_options = array();
            foreach ($limits as &$limit)
            {
                array_push($this->result_limit_options, array(
                    'limit' => $limit,
                    'text'  => $limit !== -1 ? (string)$limit : Translator::get('crud.limit_all'),
                ));
            }
            $this->reset_result_limit_if_wrong();
        }
        else
        {
            if ($all_limit_text_pairs)
            {
                $this->result_limit_options = $limits;
                $this->reset_result_limit_if_wrong();
            }
            else
            {
                $this->error(
                    'limit_options(): The specified options were in the wrong format. '
                    .' Unable to set the new options.'
                    .' Either supply an array of integers or an array of ("limit" and "text") pairs.'
                    .' For example: array(10, 20, 50, -1) or array(array("limit" => 25, "text" => "only some!"), array...)');
            }
        }
    }

    private function reset_result_limit_if_wrong()
    {
        // Check if the currently active result limit is an available option. If that is true, we do not need to
        // reset it to the first available option. => The result limit persists through a page reload!
        $current_result_limit_is_option = false;
        foreach ($this->result_limit_options as &$option)
        {
            if ($option['limit'] === $this->get_paginator()->get_pagination()->get_results_per_page())
            {
                $current_result_limit_is_option = true;
            }
        }

        if ($current_result_limit_is_option === false)
        {
            $this->update_result_limit( $this->result_limit_options[0]['limit'] );
        }
    }

    public function get_result_limit_options()
    {
        return $this->result_limit_options;
    }

    public function default_limit(int $result_limit) : void
    {
        $this->set_result_limit($result_limit);
    }

    public function set_result_limit(int $result_limit) : void
    {
        $args = compact('result_limit');

        // Check if the result limit was properly specified.
        if ( !$this->assert->is_int(__FUNCTION__, $result_limit, 'result_limit', $args) )
        {
            return;
        }

        // Checks whether the specified $result_limit really existed as an option.
        $limit_option_exists = false;
        foreach ( $this->result_limit_options as &$limit_option )
        {
            if ($limit_option['limit'] === $result_limit)
            {
                $limit_option_exists = true;
                break;
            }
        }
        if ( !$limit_option_exists )
        {
            $this->error(__FUNCTION__.'(): The specified result limit "'.$result_limit.'" does not exist.', $args);
            return;
        }

        // Update the result limit to the submitted option.
        $this->update_result_limit($result_limit);
    }

    private function update_result_limit(int $result_limit): void
    {
        $this->get_paginator()->set_results_per_page($result_limit);
    }

}