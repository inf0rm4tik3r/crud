<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Pagination;

/**
 * Class Paginator
 * @package Lukaspotthast\Crud\Pagination
 */
class Paginator
{

    /**
     * @var Pagination
     */
    private $pagination;

    /**
     * Paginator constructor.
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * Resets this paginator to its original state.
     */
    public function reset(): void
    {
        $this->pagination = new Pagination();
    }

    /**
     *
     */
    public function build_pagination(): void
    {
        $this->_build_pagination();
    }

    /**
     * @internal
     * @param array $data
     */
    private function _build_pagination(array $data = []): void
    {
        /* Use either the supplied data or the data from the previous pagination state. */
        $total_result_count = array_key_exists('total_result_count', $data) ?
            $data['total_result_count'] :
            $this->pagination->get_total_result_count();

        $current_page = array_key_exists('current_page', $data) ?
            $data['current_page'] :
            $this->pagination->get_current_page();

        $results_per_page = array_key_exists('results_per_page', $data) ?
            $data['results_per_page'] :
            $this->pagination->get_results_per_page();

        /* Compute the new pagination state. */
        $page_count = $this->compute_page_count($total_result_count, $results_per_page);
        $current_page = $this->compute_current_page($results_per_page, $page_count, $current_page);

        $first_page = 1;
        $last_page = $page_count;

        $has_previous_page = $current_page > 1;
        $previous_page = $has_previous_page ? $current_page - 1 : null;
        $has_next_page = $current_page < $page_count;
        $next_page = $has_next_page ? $current_page + 1 : null;

        // Create a new immutable pagination state object.
        $this->pagination = new Pagination(
            $page_count,
            $current_page,
            $first_page,
            $last_page,
            $has_previous_page,
            $previous_page,
            $has_next_page,
            $next_page,
            $total_result_count,
            $results_per_page,
            $this->compute_first_result($total_result_count, $results_per_page, $current_page),
            $this->compute_last_result($total_result_count, $results_per_page, $current_page),
            $this->compute_page_options($page_count, $current_page)
        );
    }

    /**
     * @param int $total_result_count
     * @param int|null $results_per_page
     * @return int
     */
    private function compute_page_count(int $total_result_count, ?int $results_per_page): int
    {
        // Calculate a value representing the amount of available pages.
        // If $results_per_page is set to null, all entries should be shown at once. => Only 1 page.
        $page_count = ($results_per_page === null) ? 1 : (int) ceil($total_result_count / $results_per_page);

        // We mathematically get a page_count of 0 if the $total_result_count was 0. But there must be at least 1 page.
        if ($page_count === 0)
        {
            $page_count = 1;
        }

        return $page_count;
    }

    /**
     * @param int|null $results_per_page
     * @param int $page_count
     * @param int $current_page
     * @return int
     */
    private function compute_current_page(?int $results_per_page, int $page_count, int $current_page): int
    {
        // We may need to update the current page if the amount of results per page changed.
        if ( $results_per_page !== $this->pagination->get_results_per_page() and $results_per_page !== null )
        {
            $current_page = (int) ceil($this->pagination->get_first_result() / $results_per_page);
        }

        // Set the current page to the last page which is available within the current result_limit selection,
        // should the current vale exceed the current page count.
        if ($current_page > $page_count)
        {
            $current_page = $page_count;
        }

        // There are no page numbers below "1".
        if ($current_page < 1)
        {
            $current_page = 1;
        }

        return $current_page;
    }

    /**
     * @param int $total_result_count
     * @param int|null $results_per_page
     * @param int $current_page
     * @return int|null
     */
    private function compute_first_result(int $total_result_count, ?int $results_per_page, int $current_page): ?int
    {
        // There may not be any results at all. We then neither got a first or last result.
        if ( $total_result_count === 0 )
        {
            return null;
        }
        // Its simple when all results are on a single page.
        else if ( $results_per_page === null )
        {
            return 1;
        }
        // Otherwise calculate the first result.
        else
        {
            // The last result of the previous page + 1.
            return ($current_page - 1) * $results_per_page + 1;
        }
    }

    /**
     * @param int $total_result_count
     * @param int|null $results_per_page
     * @param int $current_page
     * @return int|null
     */
    private function compute_last_result(int $total_result_count, ?int $results_per_page, int $current_page): ?int
    {
        // There may not be any results at all. We then neither got a first or last result.
        if ( $total_result_count === 0 )
        {
            return null;
        }
        // Its simple when all results are on a single page.
        else if ( $results_per_page === null )
        {
            return $total_result_count;
        }
        // Otherwise calculate the last result.
        else
        {
            // On the last page, (current * per_page) would result in a too large number!
            return min($current_page * $results_per_page, $total_result_count);
        }
    }

    /**
     * Calculates the page indices which should / could be displayed to the user.
     *
     * @param int $pc The "page_count" (Number of available pages).
     * @param int $p The current "page".
     * @return array Array of page indices. May contain '-1' elements to indicate blanks/placeholders (1,2,3,4,-1,999).
     */
    private function compute_page_options(int $pc, int $p): array
    {
        // Just return all available pages if there are not too many of them.
        if ($pc <= 10)
        {
            return range(1, $pc, 1);
        }

        // Single ... at the right. Start of page spectrum.
        else if ($p <= 5)
        {
            return array(1, 2, 3, 4, 5, 6, 7, -1, $pc - 1, $pc);
        }

        // With ... at the left and right. In the middle of the available pages.
        else if ($p > 5 and $p < $pc - 4)
        {
            return array(1, 2, -1, $p - 2, $p - 1, $p, $p + 1, $p + 2, -1, $pc - 1, $pc);
        }

        // Single ... at the left. End of page spectrum.
        else if ($p >= $pc - 4)
        {
            return array(1, 2, -1, $pc - 6, $pc - 5, $pc - 4, $pc - 3, $pc - 2, $pc - 1, $pc);
        }

        // Error...
        else
        {
            return array(-1);
        }
    }

    /**
     * @param int $total_result_count
     */
    public function set_total_result_count(int $total_result_count): void
    {
        $this->_build_pagination(['total_result_count' => $total_result_count]);
    }

    /**
     * @param int $current_page
     */
    public function set_current_page(int $current_page): void
    {
        $this->_build_pagination(['current_page' => $current_page]);
    }

    /**
     * @param int $results_per_page
     */
    public function set_results_per_page(int $results_per_page): void
    {
        // We allow -1 to be passed in this function. -1 represents the 'See all' state, which is represented through
        // a NULL value in the pagination.
        if ($results_per_page === -1)
            $results_per_page = null;

        $this->_build_pagination(['results_per_page' => $results_per_page]);
    }

    /**
     * @return Pagination
     */
    public function get_pagination(): Pagination
    {
        return $this->pagination;
    }

}