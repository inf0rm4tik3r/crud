<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Pagination;

/**
 * IMMUTABLE Class Pagination
 * @package Lukaspotthast\Crud\Pagination
 */
class Pagination
{

    /** @var int */
    private $page_count;

    /** @var int */
    private $current_page;

    /** @var int */
    private $first_page;

    /** @var int */
    private $last_page;

    /** @var bool */
    private $has_previous_page;

    /** @var int|null */
    private $previous_page;

    /** @var bool */
    private $has_next_page;

    /** @var int|null */
    private $next_page;

    /** @var int */
    private $total_result_count;

    /**
     * How many elements will be displayed per page. May be null if this variable was not set.
     * The paginator will display all items on a single page if $per_page is null.
     * @var int|null 
     */
    private $results_per_page;

    /** @var int|null */
    private $first_result;

    /** @var int|null */
    private $last_result;

    /** @var array */
    private $options;

    /**
     * Pagination constructor.
     * @param int $page_count
     * @param int $current_page
     * @param int $first_page
     * @param int $last_page
     * @param bool $has_previous_page
     * @param int|null $previous_page
     * @param bool $has_next_page
     * @param int|null $next_page
     * @param int $total_result_count
     * @param int|null $results_per_page
     * @param int|null $first_result
     * @param int|null $last_result
     * @param array $options
     */
    public function __construct(
        int $page_count = 1,              // There is at least one page.
        int $current_page = 1,            // And we are on that (first) page.
        int $first_page = 1,              // The first page is always 1.
        int $last_page = 1,               // If only one page exists, it must be the last page.
        bool $has_previous_page = false,  // ...
        ?int $previous_page = null,       // If only one page exists, there can not be a previous page.
        bool $has_next_page = false,      // ...
        ?int $next_page = null,           // If only one page exists, there can not be a next page.
        int $total_result_count = 0,      // We assume that we do not have any results.
        ?int $results_per_page = null,    // NULL => All results are shown on one page.
        ?int $first_result = null,        // Unspecified, because our total_result_count is 0.
        ?int $last_result = null,         // Unspecified, because our total_result_count is 0.
        array $options = []               // Unspecified.
    )
    {
        $this->set_page_count($page_count);
        $this->set_current_page($current_page);
        $this->set_first_page($first_page);
        $this->set_last_page($last_page);
        $this->set_has_previous_page($has_previous_page);
        $this->set_previous_page($previous_page);
        $this->set_has_next_page($has_next_page);
        $this->set_next_page($next_page);
        $this->set_total_result_count($total_result_count);
        $this->set_results_per_page($results_per_page);
        $this->set_first_result($first_result);
        $this->set_last_result($last_result);
        $this->set_options($options);
    }

    /**
     * Generates a json representation of this object.
     * @return string Json encoded array of properties.
     */
    public function to_json(): string
    {
        return json_encode(
            [
                'page_count' => $this->page_count,
                'current_page' => $this->current_page,
                'first_page' => $this->first_page,
                'last_page' => $this->last_page,
                'has_previous_page' => $this->has_previous_page,
                'previous_page' => $this->previous_page,
                'has_next_page' => $this->has_next_page,
                'next_page' => $this->next_page,
                'total_result_count' => $this->total_result_count,
                'results_per_page' => $this->results_per_page,
                'first_result' => $this->first_result,
                'last_result' => $this->last_result,
                'options' => $this->options
            ]
        );
    }

    /**
     * @internal
     * @param int $page_count
     */
    private function set_page_count(int $page_count): void
    {
        $this->page_count = $page_count;
    }

    /**
     * @return int
     */
    public function get_page_count(): int
    {
        return $this->page_count;
    }

    /**
     * @param int $current_page
     */
    private function set_current_page(int $current_page): void
    {
        $this->current_page = $current_page;
    }

    /**
     * @return int
     */
    public function get_current_page(): int
    {
        return $this->current_page;
    }

    /**
     * @internal
     * @param int $first_page
     */
    private function set_first_page(int $first_page): void
    {
        $this->first_page = $first_page;
    }

    /**
     * @return int
     */
    public function get_first_page(): int
    {
        return $this->first_page;
    }

    /**
     * @internal
     * @param int $last_page
     */
    private function set_last_page(int $last_page): void
    {
        $this->last_page = $last_page;
    }

    /**
     * @return int
     */
    public function get_last_page(): int
    {
        return $this->last_page;
    }

    /**
     * @internal
     * @param bool $has_previous_page
     */
    private function set_has_previous_page(bool $has_previous_page): void
    {
        $this->has_previous_page = $has_previous_page;
    }

    /**
     * @return bool
     */
    public function has_previous_page(): bool
    {
        return $this->has_previous_page;
    }

    /**
     * @internal
     * @param int|null $previous_page
     */
    private function set_previous_page(?int $previous_page): void
    {
        $this->previous_page = $previous_page;
    }

    /**
     * @return int|null
     */
    public function get_previous_page(): ?int
    {
        return $this->previous_page;
    }

    /**
     * @internal
     * @param bool $has_next_page
     */
    private function set_has_next_page(bool $has_next_page): void
    {
        $this->has_next_page = $has_next_page;
    }

    /**
     * @return bool
     */
    public function has_next_page(): bool
    {
        return $this->has_next_page;
    }

    /**
     * @internal
     * @param int|null $next_page
     */
    private function set_next_page(?int $next_page): void
    {
        $this->next_page = $next_page;
    }

    /**
     * @return int|null
     */
    public function get_next_page(): ?int
    {
        return $this->next_page;
    }

    /**
     * @internal
     * @param int $total_result_count
     */
    private function set_total_result_count(int $total_result_count): void
    {
        $this->total_result_count = $total_result_count;
    }

    /**
     * @return int
     */
    public function get_total_result_count(): int
    {
        return $this->total_result_count;
    }

    /**
     * @internal
     * @param int|null $results_per_page
     */
    private function set_results_per_page(?int $results_per_page): void
    {
        $this->results_per_page = $results_per_page;
    }

    /**
     * @return int|null
     */
    public function get_results_per_page(): ?int
    {
        return $this->results_per_page;
    }

    /**
     * @internal
     * @param int|null $first_result
     */
    private function set_first_result(?int $first_result): void
    {
        $this->first_result = $first_result;
    }

    /**
     * @return int|null
     */
    public function get_first_result(): ?int
    {
        return $this->first_result;
    }

    /**
     * @internal
     * @param int|null $last_result
     */
    private function set_last_result(?int $last_result): void
    {
        $this->last_result = $last_result;
    }

    /**
     * @return int|null
     */
    public function get_last_result(): ?int
    {
        return $this->last_result;
    }

    /**
     * @internal
     * @param array $options
     */
    private function set_options(array $options): void
    {
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function get_options(): array
    {
        return $this->options;
    }

}