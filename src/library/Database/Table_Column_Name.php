<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Lukaspotthast\Support\Error\Throws_Errors_Interface;
use Lukaspotthast\Support\Str;

class Table_Column_Name
{

    /**
     * @var string
     */
    private $table_name = '';

    /**
     * @var string
     */
    private $column_name = '';

    /**
     * @var Throws_Errors_Interface
     */
    private $throws_errors;

    public function __construct(Throws_Errors_Interface $throws_errors, $table_column_name = null)
    {
        $this->throws_errors = $throws_errors;
        if ( $table_column_name !== null )
        {
            $this->set($table_column_name);
        }
    }

    public function set($table_column_name)
    {
        $parsed           = self::parse($table_column_name, $this->throws_errors);
        $this->table_name = $parsed['table_name'];
        $this->table_name = $parsed['column_name'];
    }

    public function get_table_name(): string
    {
        return $this->table_name;
    }

    public function get_column_name(): string
    {
        return $this->column_name;
    }

    /**
     * This function can be used to parse the user input for a combined "table and column name". <br>
     * The function always returns an array with 2 string elements, which are accessible through the keys
     * 'table_name' and 'column_name'. <br>
     * <br>
     * The function behaves in the following ways: <br>
     * (lets assume we have a table called 'test' and a column called 'name') <br>
     *  - 'name' => ['', 'name']
     *  - 'test.name' => ['test', 'name']
     *  - '"test"."name"' => ['test', 'name']
     *  - 'test."na.me"' => ['test', 'na.me']
     *
     * @param mixed                   $table_column_name
     *          The input which should be parsed. Either as an array of size 2 or a string.
     *
     * @param Throws_Errors_Interface $throws_errors
     *          Object to use for reporting errors.
     *
     * @return array Array: <br>
     *          <pre>
     *          [
     *          'table_name' : string,
     *          'column_name' : string
     *          ]
     *          </pre>
     */
    public static function parse($table_column_name, Throws_Errors_Interface $throws_errors): array
    {
        $array = [];

        // If an array with 2 elements got provided, it can be used as is.
        if ( is_array($table_column_name) and count($table_column_name) === 2 )
        {
            $array['table_name']  = strval($table_column_name[0]);
            $array['column_name'] = strval($table_column_name[1]);
        }

        // If the input was a string..
        else if ( is_string($table_column_name) )
        {
            // The string must be split.
            if ( Str::contains($table_column_name, '.') )
            {
                // The string must be carefully split.
                if ( Str::contains($table_column_name, '"') )
                {
                    $split = Str::split_by_boundary($table_column_name, '.', '"', 2);

                    switch ( count($split) )
                    {
                        case 0:
                            // impossible...
                            break;
                        case 1:
                            $array['table_name']  = '';
                            $array['column_name'] = $split[0];
                            break;
                        default:
                            // cannot be more then 2
                            $array['table_name']  = $split[0];
                            $array['column_name'] = $split[1];
                            break;
                    }
                }
                // The string can be split primitively.
                else
                {
                    $split                = explode('.', $table_column_name, 2);
                    $array['table_name']  = $split[0];
                    $array['column_name'] = $split[1];
                }
            }
            // The string must not be split.
            else
            {
                $table_column_name    = str_replace('"', '', $table_column_name);
                $array['table_name']  = '';
                $array['column_name'] = $table_column_name;
            }
        }

        // If the input was neither an array of size 2 nor a string, we add an error and just return empty names.
        else
        {
            $throws_errors->error(
                __FUNCTION__ . '(): The input could not be parsed. $table_column_name must be specified as an array 
                of size 2 or a string!',
                compact($table_column_name)
            );
            $array['table_name']  = '';
            $array['column_name'] = '';
        }

        return $array;
    }

}