<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Lukaspotthast\Crud\Error\Exceptions\State_Exception;

trait Has_Database_Connection
{

    /**
     * @var Connection|null
     */
    private $db_connection = null;

    /**
     * @param string $name
     * @param array $options
     */
    public function set_db_connection(string $name, array $options): void
    {
        $this->db_connection = new Connection($name, $options);
    }

    /**
     * @return \Illuminate\Database\Connection
     */
    public function db(): \Illuminate\Database\Connection
    {
        if ( $this->db_connection === null )
        {
            throw new State_Exception(
                'Calling db() is not allowed. The database connection was not jet set by calling set_db_connection().'
            );
        }
        return $this->db_connection->get();
    }

}