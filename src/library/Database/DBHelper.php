<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Database\Schema\Column_Type;
use Lukaspotthast\Crud\Database\Schema\Table_Schema;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\Log\Log;
use Lukaspotthast\Support\Str;
use stdClass;

class DBHelper
{

    /**
     * Determines whether the specified table exists in the current database.
     * @param string                          $table_name The table which is to be checked.
     * @param \Illuminate\Database\Connection $db
     * @return bool true / false
     */
    public static function table_exists(string $table_name, Connection $db): bool
    {
        return $db->getSchemaBuilder()->hasTable($table_name);
    }

    /**
     * Loads the schema for the given table.
     *
     * @param Table                           $table
     *      The table for which the schema should be loaded.
     *
     * @param \Illuminate\Database\Connection $db
     *      ...
     *
     * @return Table_Schema
     *      The schema of the specified table.
     */
    public static function load_schema(Table $table, Connection $db): Table_Schema
    {
        // Query the column information from the underlying database.
        $columns = $db->select('
            SELECT * 
            FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE
              TABLE_SCHEMA = "' . $db->getDatabaseName() . '" AND 
              TABLE_NAME = "' . $table->get_name() . '"
        ');
        //dump($columns);
        $column_schemata = [];

        // Extract the 'real' column type.
        foreach ( $columns as &$col /** @var stdClass $col */ )
        {
            $name = $col->COLUMN_NAME;
            $type = self::compute_column_type($col);

            $has_default   = $col->COLUMN_DEFAULT !== null;
            $default_value = $col->COLUMN_DEFAULT;
            $default_type  = gettype($default_value);

            $privileges = explode(',', $col->PRIVILEGES);
            $comment    = $col->COLUMN_COMMENT;

            $is_nullable                 = $col->IS_NULLABLE === 'YES';
            $on_update_current_timestamp = Str::contains($col->EXTRA, 'on update CURRENT_TIMESTAMP');
            $is_auto_incrementing        = Str::contains($col->EXTRA, 'auto_increment');
            $is_primary_key              = Str::contains($col->COLUMN_KEY, 'PRI');
            $is_unique                   = Str::contains($col->COLUMN_KEY, 'UNI');

            $column_schema = new Column_Schema(
                $table, $name, $type, $has_default, $default_type, $default_value,
                $is_nullable, $on_update_current_timestamp, $is_auto_incrementing, $is_primary_key, $is_unique,
                $privileges, $comment
            );
            array_push($column_schemata, $column_schema);
        }

        $table_schema = new Table_Schema();
        $table_schema->set_table_column_schemata($column_schemata);

        //dump($table_schema);
        //dd("ok");
        return $table_schema;
    }

    public static function compute_column_type(stdClass $col): Column_Type
    {
        $name                     = $col->DATA_TYPE;
        $options                  = [];
        $is_unsigned              = false;
        $maximum_character_length = isset($col->CHARACTER_MAXIMUM_LENGTH) ? $col->CHARACTER_MAXIMUM_LENGTH : -1;
        $maximum_byte_length      = isset($col->CHARACTER_OCTET_LENGTH) ? $col->CHARACTER_OCTET_LENGTH : -1;
        $display_length           = -1;
        $character_set            = isset($col->CHARACTER_SET_NAME) ? $col->CHARACTER_SET_NAME : '';
        $collation                = isset($col->COLLATION_NAME) ? $col->COLLATION_NAME : '';

        $opening_brace_pos = strpos($col->COLUMN_TYPE, '(', 0);
        $closing_brace_pos = strrpos($col->COLUMN_TYPE, ')', 0);

        // If a '(' is part of the columns type, calculate the real type name and the extract the length.
        if ( $opening_brace_pos !== false )
        {
            $data = substr($col->COLUMN_TYPE,
                $opening_brace_pos + 1,
                $closing_brace_pos - $opening_brace_pos - 1
            );
            if ( $name === 'enum' )
            {
                $options = str_getcsv($data, ',', "'");;
            }
            else
            {
                $display_length = (int)$data;
            }
        }

        // If 'unsigned' is part of $col->Type set the unsigned bit to true.
        if ( Str::contains($col->COLUMN_TYPE, 'unsigned') )
        {
            $is_unsigned = true;
        }

        return new Column_Type(
            $name, $options, $is_unsigned,
            $maximum_character_length, $maximum_byte_length, $display_length,
            $character_set, $collation
        );
    }

    /**
     * ...
     *
     * @param string $col_real_type_name
     *      ...
     *
     * @return string|int
     *      ...
     */
    public static function get_default_for_col_type(string $col_real_type_name)
    {
        switch ( $col_real_type_name )
        {
            case 'varchar':
            case 'text':
            case 'mediumtext':
            case 'longtext':
            case 'enum':
                return '';
                break;

            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'int':
            case 'bigint':
                return 0;
                break;

            case 'float':
            case 'double':
            case 'decimal':
                return 0;
                break;

            case 'tinyblob':
            case 'mediumblob':
            case 'blob':
            case 'longblob':
                return '';
                break;

            case 'timestamp':
                return '1970-01-01 12:00:00';
                break;

            case 'datetime':
                return '1970-01-01 00:00:00';
                break;

            case 'date':
                return '1970-01-01';
                break;

            case 'time':
                return '00:00:00';
                break;

            case 'subselect':
                return '';
                break;

            default:
                $msg = __FUNCTION__ . '(): default case was used for type: ' . $col_real_type_name . '!';
                Log::warning($msg);
                Debug::warning($msg);
                return null;
                break;
        }
    }

    /**
     * Determines if the given variable represents a DB query.
     *
     * @param mixed $var
     *      The variable which is to be checked.
     *
     * @return bool
     *      True if $var is an object of 'Illuminate\Database\Query\Builder'.
     */
    public static function is_laravel_query_object($var)
    {
        return is_object($var) and get_class($var) === 'Illuminate\Database\Query\Builder';
    }

    /**
     * Retrieves the raw SQL query from a DB query object. <br>
     * All parameters are bound. => There will be no '?' in the result.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *      The query to operate on.
     *
     * @return mixed
     *      Returns the raw SQL query.
     */
    public static function get_raw_laravel_query(Builder $query): string
    {
        $sql      = $query->toSql();
        $bindings = $query->getBindings();

        // Replace all '?' in the string representation of the query with the parameters that were actually used.
        foreach ( $bindings as &$binding )
        {
            $sql = preg_replace('#\?#', $binding, $sql, 1);
        }

        return $sql;
    }

}