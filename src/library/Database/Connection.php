<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use DebugBar\DataCollector\PDO\PDOCollector;
use DebugBar\DataCollector\PDO\TraceablePDO;
use Illuminate\Database\Capsule\Manager as Capsule;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Debugging;
use Lukaspotthast\Support\Arr;
use Serializable;

/**
 * Class Connection
 * @package Lukaspotthast\Crud\Database
 */
class Connection implements Serializable
{

    /**
     * @var string
     */
    private const MAIN_CONNECTION_NAME = 'main';

    /**
     * @var array
     *      The default options used to initialize a connection: <br>
     *      <pre>
     *      [
     *           'driver' => 'mysql',
     *           'host' => 'localhost',
     *           'database' => 'main',
     *           'username' => 'root',
     *           'password' => 'root',
     *           'charset' => 'utf8mb4',
     *           'collation' => 'utf8mb4_unicode_ci',
     *           'prefix' => ''
     *       ]
     *      </pre>
     */
    private const DEFAULT_OPTIONS = [
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'main',
        'username'  => 'root',
        'password'  => 'root',
        'charset'   => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix'    => '',
    ];

    /**
     * The name of this connection.
     * @var string|null
     */
    private $name = null;

    /**
     * @var Capsule
     */
    private $capsule = null;

    /**
     * @var \Illuminate\Database\Connection
     */
    private $connection = null;

    /**
     * @var array
     */
    private $last_connection_options = null;

    /**
     * Connection constructor.
     * @param string $name
     * @param array  $options
     */
    public function __construct(string $name = null, array $options = [])
    {
        $this->name = $name;
        $this->init($options);
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param array $options
     *      Connection details as an array. It may contain the following keys: <br>
     *      'driver', 'host', 'database', 'username', 'password', 'charset', 'collation' and 'prefix'. <br>
     *      If a specific information was not specified the default value will be used from the
     *      {@see self::DEFAULT_OPTIONS} array.
     */
    public function init(array $options = []): void
    {
        // Create the capsule and add a connection to it.
        $this->capsule = new Capsule();
        $this->capsule->addConnection(
            [
                'driver'    => Arr::get($options, 'driver', self::DEFAULT_OPTIONS['driver']),
                'host'      => Arr::get($options, 'host', self::DEFAULT_OPTIONS['host']),
                'database'  => Arr::get($options, 'database', self::DEFAULT_OPTIONS['database']),
                'username'  => Arr::get($options, 'username', self::DEFAULT_OPTIONS['username']),
                'password'  => Arr::get($options, 'password', self::DEFAULT_OPTIONS['password']),
                'charset'   => Arr::get($options, 'charset', self::DEFAULT_OPTIONS['charset']),
                'collation' => Arr::get($options, 'collation', self::DEFAULT_OPTIONS['collation']),
                'prefix'    => Arr::get($options, 'prefix', self::DEFAULT_OPTIONS['prefix']),
            ],
            self::MAIN_CONNECTION_NAME
        );

        // Lets save the connection for easier access.
        $this->connection = $this->capsule->getConnection(self::MAIN_CONNECTION_NAME);

        // This object is serializable, but the connection can not be stored!
        // We therefore have to store the connection information ($options) in order to recreate the connection on
        // the reconstruction of this object.
        $this->last_connection_options = $options;

        if ( Debug::is_active() and PHP_Debugging::has_debugbar() )
        {
            $debugbar = PHP_Debugging::get_debugbar();

            // The PDO object of our connection must get wrapped in a TraceablePDO object for the DebugBar to work.
            $traceable_pdo = new TraceablePDO($this->connection->getPdo());
            $this->connection->setPdo($traceable_pdo);

            // Create and set the PDO collector if it did not jet existed.
            if ( $debugbar->hasCollector('pdo') )
            {
                // We know that the collector exists!
                /** @noinspection PhpUnhandledExceptionInspection */
                $pdo_collector = $debugbar->getCollector('pdo');
            }
            else
            {
                $pdo_collector = new PDOCollector();
                // We know that the collector does not already exist.
                /** @noinspection PhpUnhandledExceptionInspection */
                $debugbar->addCollector($pdo_collector);
            }

            // Add this connections PDO to the collector. Specifying our name is important as the add function will
            // overwrite our last PDO instances at later access (during ajax calls).
            $pdo_collector->addConnection($traceable_pdo, $this->name);
        }
    }

    /**
     * Return the connection. Call your query builder functions on this object.
     *
     * @return \Illuminate\Database\Connection
     */
    public function get()
    {
        return $this->connection;
    }

    /**
     * String representation of object
     * @link  http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            'name'                    => $this->name,
            'last_connection_options' => $this->last_connection_options,
        ]);
    }

    /**
     * Constructs the object
     * @link  http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data                          = unserialize($serialized);
        $this->name                    = $data['name'];
        $this->last_connection_options = $data['last_connection_options'];

        $this->init($this->last_connection_options);
    }
}