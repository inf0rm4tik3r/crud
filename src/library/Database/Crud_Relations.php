<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

trait Crud_Relations
{

    /**
     * Stores all defined relations.
     * @var array
     */
    private $relations;

    /**
     *
     */
    public function reset_relations() : void
    {
        $this->relations = array();
    }


    public function relation(
        $table_column_name,
        string $target_table,
        string $target_table_connect_field,
        string $display_pattern,
        string $type = 'select',
        bool $show_none_option = true
    ) : Relation
    {
        $args = compact('table_column_name', 'target_table', 'target_table_connect_field', 'display_pattern', 'type', 'show_none_option');

        // The $table_column_name must be specified as an array of size 2 or a non empty string.
        if ( ! ((is_array($table_column_name) and count($table_column_name) === 2) or is_string($table_column_name)) )
        {
            $this->error(
                __FUNCTION__.'(): The $table_column_name must be specified as an array of size 2 or a non empty string.',
                $args
            );
            return null;
        }

        $parsed = Table_Column_Name::parse($table_column_name, $this);
        $table_name = $parsed['table_name'];
        $column_name = $parsed['column_name'];

        // The parsed column name must not be empty.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return null;
        }

        // The column must exist in the current table set.
        if ( !$this->assert->set_column_exists($this->get_table_set(), __FUNCTION__, $table_name, $column_name, $args) )
        {
            return null;
        }

        $relation = new Relation($this);

        $relation->set_target_table($target_table);
        $relation->set_target_table_connect_field($target_table_connect_field);
        $relation->set_display_pattern($display_pattern);
        $relation->set_type($type);
        $relation->set_show_none_option($show_none_option);

        // TODO: Check if relation for $field already exists. => Show warning er decline.

        $this->add_relation_internal($table_name, $column_name, $relation);

        // We return the relation so it can get further modified by the user.
        return $relation;
    }

    public function add_relation($table_column_name, Relation $relation) : void
    {
        $parsed = Table_Column_Name::parse($table_column_name, $this);
        $table_name = $parsed['table_name'];
        $column_name = $parsed['column_name'];

        $this->add_relation_internal($table_name, $column_name, $relation);
    }

    private function add_relation_internal($table_name, $column_name, Relation $relation) : void
    {
        $this->relations[$column_name] = $relation; // TODO: store relations with table and column name!
    }

    public function get_relations()
    {
        return $this->relations;
    }

}