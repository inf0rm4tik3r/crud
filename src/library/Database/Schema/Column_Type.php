<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database\Schema;

use Lukaspotthast\Support\Arg;

/**
 * Class Column_Type
 * @package Lukaspotthast\Crud\Database\Schema
 */
class Column_Type
{

    /**
     * @var string
     */
    private $name = '';

    /**
     * Only set if name === 'enum'!
     * @var array
     */
    private $options = [];

    /**
     * Only set and relevant for numeric types like INT...
     * @var bool
     */
    private $is_unsigned = false;

    /**
     * @var int
     */
    private $maximum_character_length = -1;

    /**
     * @var int
     */
    private $maximum_byte_length = -1;

    /**
     * Only set to a positive number if the type definition included a length information.
     * @var int
     */
    private $display_length = -1;


    /** @var string */
    private $character_set;

    /** @var string */
    private $collation;


    /**
     * Column_Type constructor.
     *
     * @param string $name
     * @param array  $options
     * @param bool   $is_unsigned
     * @param int    $maximum_character_length
     * @param int    $maximum_byte_length
     * @param int    $display_length
     * @param string $character_set
     * @param string $collation
     */
    public function __construct(
        string $name, array $options = [], bool $is_unsigned = false,
        int $maximum_character_length = -1, int $maximum_byte_length = -1, int $display_length = -1,
        string $character_set, string $collation
    )
    {
        $this->name        = $name;
        $this->options     = $options;
        $this->is_unsigned = $is_unsigned;

        $this->maximum_character_length = $maximum_character_length;
        $this->maximum_byte_length      = $maximum_byte_length;
        $this->display_length           = $display_length;

        $this->character_set = $character_set;
        $this->collation     = $collation;
    }

    public function __toString(): string
    {
        if ( $this->has_options() )
        {
            return $this->name . '(' . Arg::stringify($this->options) . ')';
        }
        else
        {
            return $this->name . '(' . $this->display_length . ')';
        }
    }

    public function get_name(): string
    {
        return $this->name;
    }

    public function has_options(): bool
    {
        return !empty($this->options);
    }

    public function get_options(): array
    {
        return $this->options;
    }

    public function is_unsigned(): bool
    {
        return $this->is_unsigned;
    }

    public function get_maximum_character_length(): int
    {
        return $this->maximum_character_length;
    }

    public function get_maximum_byte_length(): int
    {
        return $this->maximum_byte_length;
    }

    public function get_display_length(): int
    {
        return $this->display_length;
    }

    /**
     * Also returns the MAXIMUM CHARACTER LENGTH, as it provides the most necessary information.
     * @return int
     */
    public function get_length(): int
    {
        return $this->maximum_character_length;
    }

    public function get_character_set(): string
    {
        return $this->character_set;
    }

    public function get_collation(): string
    {
        return $this->collation;
    }

}