<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database\Schema;

use Lukaspotthast\Crud\Database\Table;

/**
 * Class Column_Schema
 * @package Lukaspotthast\Crud\Database\Schema
 */
class Column_Schema
{

    /**
     * Reference to the parent table in which this column resides in.
     * @var Table|null
     */
    private $origin_table = null;

    /** @var string */
    private $name = null;

    /** @var Column_Type */
    private $type = null;

    /** @var bool */
    private $has_default = false;

    /** @var mixed */
    private $default_value = null;

    /** @var string */
    private $default_type = 'NULL';

    /** @var bool */
    private $has_custom_default = false;

    /** @var mixed */
    private $custom_default_value = null;

    /** @var string */
    private $custom_default_type = 'NULL';

    /** @var bool */
    private $is_nullable = false;

    /** @var bool */
    private $on_update_current_timestamp;

    /** @var bool */
    private $is_auto_incrementing;

    /** @var bool */
    private $is_primary_key;

    /** @var bool */
    private $is_unique;

    /** @var array */
    private $privileges;

    /** @var string */
    private $comment;

    /**
     * Column_Schema constructor.
     *
     * @param Table|null  $origin_table
     * @param string      $name
     * @param Column_Type $type
     * @param bool        $has_default
     * @param string      $default_type
     * @param             $default_value
     * @param bool        $is_nullable
     * @param bool        $on_update_current_timestamp
     * @param bool        $is_auto_incrementing
     * @param bool        $is_primary_key
     * @param bool        $is_unique
     * @param array       $privileges
     * @param string      $comment
     */
    public function __construct(
        ?Table $origin_table, string $name, Column_Type $type,
        bool $has_default, string $default_type, $default_value,
        bool $is_nullable, bool $on_update_current_timestamp,
        bool $is_auto_incrementing, bool $is_primary_key, bool $is_unique,
        array $privileges, string $comment
    )
    {
        $this->origin_table = $origin_table;

        $this->name = $name;
        $this->type = $type;

        $this->has_default   = $has_default;
        $this->default_type  = $default_type;
        $this->default_value = $default_value;

        $this->is_nullable                 = $is_nullable;
        $this->on_update_current_timestamp = $on_update_current_timestamp;
        $this->is_auto_incrementing        = $is_auto_incrementing;
        $this->is_primary_key              = $is_primary_key;
        $this->is_unique                   = $is_unique;

        $this->privileges = $privileges;
        $this->comment    = $comment;
    }

    /*
     * Simple GETTER and SETTER functions.
     */

    public function get_origin_table(): ?Table
    {
        return $this->origin_table;
    }

    public function get_name(): string
    {
        return $this->name;
    }

    public function get_type(): Column_Type
    {
        return $this->type;
    }

    public function has_default(): bool
    {
        return $this->has_default;
    }

    public function get_default_type(): string
    {
        return $this->default_type;
    }

    public function get_default_value()
    {
        return $this->default_value;
    }

    public function set_custom_default(string $type, $value): void
    {
        $this->has_custom_default   = true;
        $this->custom_default_type  = $type;
        $this->custom_default_value = $value;
    }

    public function unset_custom_default(): void
    {
        $this->has_custom_default   = false;
        $this->custom_default_type  = null;
        $this->custom_default_value = null;
    }

    public function has_custom_default(): bool
    {
        return $this->has_custom_default;
    }

    public function get_custom_default_type(): string
    {
        return $this->custom_default_type;
    }

    public function get_custom_default_value()
    {
        return $this->custom_default_value;
    }

    public function is_nullable(): bool
    {
        return $this->is_nullable;
    }

    public function on_update_current_timestamp(): bool
    {
        return $this->on_update_current_timestamp;
    }

    public function is_auto_incrementing(): bool
    {
        return $this->is_auto_incrementing;
    }

    public function is_primary_key(): bool
    {
        return $this->is_primary_key;
    }

    public function is_unique(): bool
    {
        return $this->is_unique;
    }

    public function get_privileges(): array
    {
        return $this->privileges;
    }

    public function get_comment(): string
    {
        return $this->comment;
    }

    /*
    public function is_subselect_column(): bool
    {
        return $this->type->get_name() === 'subselect';
    }
    */

}