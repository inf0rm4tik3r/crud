<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database\Schema;

use Lukaspotthast\Support\Error\Throws_Errors_Interface;

/**
 * Class Table_Schema
 * @package Lukaspotthast\Crud\Database\Schema
 */
class Table_Schema
{

    // Additional information ...

    /**
     * @var array
     */
    private $table_column_schemata = [];

    /**
     * @var array
     */
    private $custom_column_schemata = [];


    /**
     * @param array $column_schemata
     */
    public function set_table_column_schemata(array $column_schemata): void
    {
        $this->table_column_schemata = [];

        // Construct an associative array of the form: 'column_name => column_schema'
        foreach ( $column_schemata as &$col )
        {
            /** @var Column_Schema $col */
            $this->table_column_schemata[$col->get_name()] = $col;
        }
    }

    /**
     * @param Column_Schema $column_schema
     * @param Throws_Errors_Interface $err
     */
    public function add_custom_column(Column_Schema $column_schema, Throws_Errors_Interface $err): void
    {
        // Return if the specified column is already known.
        if ( $this->has_custom_column_schema($column_schema->get_name()) )
        {
            $err->error(
                __CLASS__.' - '.__FUNCTION__.'(): The specified column is already known.',
                compact('column')
            );
            return;
        }

        $this->custom_column_schemata[$column_schema->get_name()] = $column_schema;
    }


    /* Provide access to the schemata of the main columns. */

    public function get_table_column_schemata(): array
    {
        return $this->table_column_schemata;
    }

    public function has_table_column_schema(string $column_name): bool
    {
        return array_key_exists($column_name, $this->table_column_schemata);
    }

    public function get_table_column_schema(string $column_name): Column_Schema
    {
        return $this->table_column_schemata[$column_name];
    }


    /* Provide access to the schemata of custom (dynamically defined) columns. */

    public function get_custom_column_schemata(): array
    {
        return $this->custom_column_schemata;
    }

    public function has_custom_column_schema(string $column_name): bool
    {
        return array_key_exists($column_name, $this->custom_column_schemata);
    }

    public function get_custom_column_schema(string $column_name): Column_Schema
    {
        return $this->custom_column_schemata[$column_name];
    }


    /* Provide access to the schemata of all known columns. */

    public function get_column_schemata(): array
    {
        return array_merge($this->get_table_column_schemata(), $this->get_custom_column_schemata());
    }

    public function has_column_schema(string $column_name): bool
    {
        return $this->has_table_column_schema($column_name) or
               $this->has_custom_column_schema($column_name);
    }

    public function get_column_schema(string $column_name): Column_Schema
    {
        return $this->get_column_schemata()[$column_name];
    }

}