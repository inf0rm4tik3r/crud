<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Condition\Condition;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Sort\Crud_Order_By;
use stdClass;

/**
 * Class Relation
 * @package Lukaspotthast\Crud\Database
 */
class Relation
{
    /**
     * Allows the use of the known order_by function of the main system to order this relations outcome.
     */
    use Crud_Order_By;

    /**
     * Reference to the crud instance. Set in constructor.
     * @var Crud
     */
    private $crud = null;

    /**
     * @var Assert
     */
    private $assert;

    /**
     * The table from which data should be fetched.
     * @var Table_Set
     */
    private $table_set = null;

    /**
     * A column from the defined target_table.<br>
     * The data from that column gets saved in the field which uses this relation to
     * select an entry / entries form the target table.
     * @var string
     */
    private $target_table_connect_field = 'id';

    /**
     * How should entries form the defined target table get rendered?:<br>
     * Use {field_tags} to describe the name with data from the current entry.
     * <pre>
     * e.g. - '{id} - {first_name} {last_name} ({age})' => '1 - Lukas Potthast (22)'
     *                                                  => '2 - Max Mustermann (30)'
     *      - '{price} €'                               => '12,00 €'
     * </pre>
     * @var string
     */
    private $display_pattern = 'no display pattern specified';

    /**
     * Which types are supported by relations.
     * One of these must be chosen for this relation.
     */
    const SUPPORTED_TYPES = [
        'select', 'multiselect',
    ];

    /**
     * How the relation is handled: One of the SUPPORTED_TYPES.
     * @var string
     */
    private $type = 'select';

    /**
     * Determines whether or not a none option should be selectable.
     * @var bool
     */
    private $show_none_option = true;

    /**
     * This where query filters the data from the defined target table, so that only entries we really want to see
     * get rendered for selection.
     * @var Condition
     */
    private $where_query = null;

    /**
     * Holds data from the specified target table once loaded. A drastic drop in DB accesses can be
     * achieved with this variable, because data can be reused across multiple calls to acquire_data().<br>
     * Why?: In 'list' view, every entry needs to know its relation data.
     * @var array
     */
    private $data_cache = null;

    /**
     * Crud_Relation constructor.
     * @param Crud $crud Reference to the crud instance this relation belongs to.
     */
    public function __construct(Crud &$crud)
    {
        $this->crud        = $crud;
        $this->assert      = $crud->get_assert();
        $this->where_query = new Condition($crud);
        $this->table_set   = new Table_Set($crud);

        // TODO: Is this the right place? Should the whole Relation object be resettable?
        $this->reset_order_by();
    }

    /**
     * @param array $keys
     * @return string
     */
    public static function stringify_keys(array $keys): string
    {
        return implode(',', $keys);
    }

    /**
     * @param null|string $keys
     * @return array|null
     */
    public static function parse_keys(?string $keys): array
    {
        if ( $keys === null )
        {
            $keys = '';
        }
        return explode(',', $keys);
    }

    public function set_target_table(string $table_name): void
    {
        $this->table_set->set_main_table($table_name);
    }

    public function get_target_table(): Table
    {
        return $this->table_set->get_main_table();
    }

    /**
     * Specify a column from the previously defined target_table.<br>
     * The data from that column gets saved in the field which uses this relation to
     * select an entry / entries form the target table.
     * @param string $target_table_connect_field
     */
    public function set_target_table_connect_field(string $target_table_connect_field): void
    {
        $args = compact('target_connect_field');

        // Check if the target table name parameter was properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $target_table_connect_field, 'target_connect_field', $args) )
        {
            return;
        }

        // Check if the field is part of the target table.
        if ( !$this->get_target_table()->get_schema()->has_column_schema($target_table_connect_field) )
        {
            $this->crud->error(
                __CLASS__ . ': ' . __FUNCTION__ . '(): The specified connect field does not exist in "'
                . $this->get_target_table()->get_name() . '"!', $args
            );
            return;
        }

        // Argument is valid.
        $this->target_table_connect_field = $target_table_connect_field;
    }

    /**
     * The field name to which this relation connects in the target table.
     * @return string
     */
    public function get_target_table_connect_field(): string
    {
        return $this->target_table_connect_field;
    }

    /**
     * Specify the pattern to be used when rendering entries from the target table as selectables.<br>
     * @see $display_pattern
     * @param string $display_pattern e.g. '{price} €'
     */
    public function set_display_pattern(string $display_pattern): void
    {
        $args = compact('display_pattern');

        // Check if the $display_pattern parameter got correctly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $display_pattern, 'display_pattern', $args) )
        {
            return;
        }

        // MAYBE: Check whether the specified {field_tags} in the pattern all exist!

        // Argument is valid.
        $this->display_pattern = $display_pattern;
    }

    /**
     * The display pattern used for this relation.
     * @return string
     */
    public function get_display_pattern(): string
    {
        return $this->display_pattern;
    }

    /**
     * Specify the type of this relation. You can only choose between SUPPORTED_TYPES.<br>
     * As of now, you can use: 'select' or 'multiselect'
     * @param string $type One of Crud_Relation::SUPPORTED_TYPES.
     */
    public function set_type(string $type): void
    {
        $args = compact('type');

        // Check if the type parameter was correctly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $type, 'type', $args) )
        {
            return;
        }

        // Check that a supported type got submitted.
        if ( !in_array($type, self::SUPPORTED_TYPES) )
        {
            $this->crud->error(
                __CLASS__ . ': ' . __FUNCTION__ . '(): The specified type "' . $type . '" is not supported!<br>'
                . 'Choose one of: ' . json_encode(self::SUPPORTED_TYPES), $args
            );
            return;
        }

        // Argument is valid.
        $this->type = $type;
    }

    /**
     * The type of the this relation.<br>
     * One of the SUPPORTED_TYPES.
     * @return string
     */
    public function get_type(): string
    {
        return $this->type;
    }

    /**
     * Define if a 'none' option should be rendered (and therefore be selectable).
     * @param bool $show_none_option true / false
     */
    public function set_show_none_option(bool $show_none_option): void
    {
        $args = compact('show_none_option');

        // Check if the show_none_option parameter was correctly specified.
        if ( !$this->assert->is_bool(__FUNCTION__, $show_none_option, 'show_none_option', $args) )
        {
            return;
        }

        // Argument is valid.
        $this->show_none_option = $show_none_option;
    }

    /**
     * Whether or not a 'none' option should be selectable in this relation.
     * @return bool true / false
     */
    public function get_show_none_option(): bool
    {
        return $this->show_none_option;
    }

    /**
     * The Crud_Condition object defining which entries from the target table will be selectable.
     * @return Condition
     */
    public function get_where_query(): Condition
    {
        return $this->where_query;
    }

    /**
     * Resets the data cache of this relation. The next call to acquire_data() causes the data to be loaded
     * again from the database.<br>
     * Should be called by the parent crud instance, before storing itself to the session.
     */
    public function _invalidate_cache()
    {
        $this->data_cache = null;
    }

    public function _acquire_data($current_value): array
    {
        // If the main
        if ( $this->data_cache === null )
        {
            $this->load_data();
        }

        return $this->finalize_data($current_value);
    }

    private function &finalize_data($current_value): array
    {
        // Copy the cached data. We do not want to corrupt the cache store.
        $array = $this->data_cache;

        $current_value_exploded = self::parse_keys($current_value);
        $found_current          = false;

        foreach ( $array as $connection => &$option )
        {
            $is_current = (($current_value === $connection) or in_array($connection, $current_value_exploded));
            if ( $is_current )
            {
                $found_current        = true;
                $option['is_current'] = $is_current;
            }
        }

        // What is currently selected is not one of the available options.
        // That could just be the case if the user deleted the entry which got previously selected.
        // But the could also be an entry that did not get loaded in because it did not matched the where clauses.
        // We will try to find the entry that matches the current value and make it an option as well,
        // but defining it as not selectable.
        if ( !$found_current )
        {
            // Retrieve data from the specified target table.
            $query = $this->crud->db()->table($this->get_target_table()->get_name());

            foreach ( $current_value_exploded as &$current )
            {
                $query->where($this->target_table_connect_field, '=', $current);
            }

            $data = $query->get();

            // Add the acquired data.
            foreach ( $data as &$entry )
            {
                $connection = $entry->{$this->target_table_connect_field};
                $is_current = $current_value === $connection or in_array($connection, $current_value_exploded);
                $this->process_option($array, $entry, $is_current, false);
            }
        }

        return $array;
    }

    private function load_data(): void
    {
        // Retrieve data from the specified target table.
        $query = $this->crud->db()->table($this->get_target_table()->get_name());

        // But only where entries match the programmatically defined where clauses.
        $query = $query->where(function ($sub_query) {
            $this->where_query->add_to($sub_query);
        });

        // Add order_by definitions.
        foreach ( $this->get_order_by_columns() as $column => &$order_type )
        {
            $query->orderBy($column, $order_type);
        }

        $data = $query->get();

        // We create an array to store the database entries in, after we brought them in the proper format.
        // This array will later become this objects data_cache.
        $array = [];

        // Add 'none' option if not otherwise specified.
        if ( $this->show_none_option )
        {
            $this->add_option(
                $array, 0, Translator::get('crud.select_option.none'),
                false, true
            );
        }

        // Bring the entry in the correct format:
        $this->table_set->process_collection($data); // TODO: function needs to access the relation table schema....

        // Add the acquired data.
        foreach ( $data as &$entry )
        {
            $this->process_option($array, $entry, false, true);
        }

        $this->data_cache = &$array;
    }

    private function process_option(array &$array, stdClass &$entry, bool $add_as_current, bool $add_as_selectable): void
    {
        // Get the value from the connect_field.
        $connection = $entry->{$this->target_table_connect_field}['value'];

        // Replace {field_tags} with data from the current relation entry.
        $value = $this->crud->replace_field_tags($this->display_pattern, $entry);

        // Add the
        $this->add_option($array, $connection, $value, $add_as_current, $add_as_selectable);
    }

    private function add_option(array &$array, $connection, $value, bool $is_current, bool $is_selectable): void
    {
        if ( empty($value) )
        {
            $value = '- empty -';
        }

        $array[$connection] = [
            'value'         => $value,
            'is_current'    => $is_current,
            'is_selectable' => $is_selectable,
        ];
    }

}