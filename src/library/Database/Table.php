<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Illuminate\Database\Query\Builder;
use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Database\Schema\Column_Type;
use Lukaspotthast\Crud\Database\Schema\Table_Schema;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Error\Throws_Errors_Interface;

class Table
{

    const MYSQL_FIELD_TYPES = [
        'subselect',
        'varchar', 'enum',
        'tinyint', 'smallint', 'mediumint', 'int', 'bigint',
        'tinytext', 'mediumtext', 'text', 'longtext',
        'timestamp', 'datetime', 'date', 'time',
    ];

    const ALLOWED_COLUMN_OPTIONS = [
        'length', 'unsigned', 'on_update_current_timestamp', 'primary', 'unique', 'comment',
    ];

    const DEFAULT_TYPES = [
        'string', 'query',
    ];

    /**
     * Holds a reference to the parent crud instance.
     * @var Crud
     */
    private $crud;
    private $assert;

    /**
     * Stores the name of the database table, which this object is representing.
     * @var string|null This variable will ne set to null, until a table got defined using set_table().
     */
    private $name;

    /**
     * The schema of the specified table.
     * @var Table_Schema
     */
    private $schema;

    /**
     * Holds the state whether or not the table info is currently loaded.
     * @var bool
     */
    private $loaded;

    /**
     * Crud_Table constructor.
     *
     * @param Crud $crud Reference to the parent crud system.
     */
    public function __construct(Crud &$crud)
    {
        $this->crud   = $crud;
        $this->assert = $crud->get_assert();

        $this->reset();
    }

    /**
     * Resets this Crud_Table object to its initial state.<br>
     * This function needs to be called upon instantiation.
     */
    public function reset(): void
    {
        // No table is defined.
        $this->name   = null;
        $this->schema = null;

        // The object is not considered to be fully loaded.
        $this->loaded = false;
    }

    /**
     * @param string $table_name
     */
    public function set_table(string $table_name): void
    {
        $this->set_name($table_name);
        $this->load_schema();
    }

    /**
     * Load the schema for the defined table.
     */
    private function load_schema(): void
    {
        // A table has to be set in order to load the schema.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $this->get_name(), 'name', []) )
        {
            return;
        }

        $this->set_schema(DBHelper::load_schema($this, $this->crud->db()));

        $this->set_loaded(true);
    }

    /**
     * Creates an stdClass object which will represent a database column.<br>
     * This function will return NULL if it encounters an error.
     *
     * @param Throws_Errors_Interface $throws_errors
     * @param string                  $name
     * @param string                  $type
     * @param string|Builder          $default_value
     * @param array                   $options
     * @return null|Column_Schema
     */
    public static function create_column(
        Throws_Errors_Interface $throws_errors,
        string $name, string $type, $default_value = '', array $options
    ): ?Column_Schema
    {
        $assert = new Assert($throws_errors, null);
        $args   = compact('name', 'type', 'may_be_null', 'default_value');

        // The 'name' must be specified as a non empty string.
        if ( !$assert->is_non_empty_string(__FUNCTION__, $name, 'name', $args) )
        {
            return null;
        }

        // The 'type' must be specified as a non empty string.
        if ( !$assert->is_non_empty_string(__FUNCTION__, $type, 'type', $args) )
        {
            return null;
        }

        // The 'type' must be supported.
        if ( !in_array($type, self::MYSQL_FIELD_TYPES) )
        {
            $throws_errors->error(
                __CLASS__ . ' - ' . __FUNCTION__ . '(): The type "' . $type . '" is not supported. Use one of:<br>'
                . json_encode(self::MYSQL_FIELD_TYPES), $args
            );
            return null;
        }

        // The specified default value must either be a string or a laravel query object.
        $default_value_is_string = is_string($default_value);
        $default_value_is_query  = DBHelper::is_laravel_query_object($default_value);
        $default_type            = ($default_value_is_string ? 'string' : ($default_value_is_query ? 'query' : null));

        if ( is_null($default_type) )
        {
            $throws_errors->error(
                __CLASS__ . ' - ' . __FUNCTION__ . '(): The default value must be specified as a string or a laravel query.',
                $args
            );
            return null;
        }

        if ( $default_value_is_query )
        {
            $default_value = DBHelper::get_raw_laravel_query($default_value);
        }

        // The 'options' argument must be specified as an array.
        if ( !$assert->is_array(__FUNCTION__, $options, 'options', $args) )
        {
            return null;
        }

        // The provided options must be supported and must be specified in the correct way / in the correct type.
        foreach ( $options as $key => &$value )
        {
            // The option should be in the ALLOWED_COLUMN_OPTIONS array.
            if ( !in_array($key, self::ALLOWED_COLUMN_OPTIONS) )
            {
                $throws_errors->error(
                    __CLASS__ . ' - ' . __FUNCTION__ . '(): Key "' . Arg::stringify($key) . ' is not allowed!"',
                    $args
                );
                unset($options[$key]);
            }

            switch ( $key )
            {
                case 'length':
                    if ( !is_int($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "length" option must be specified as an int.', $args);
                    }
                    break;

                case 'unsigned':
                    if ( !is_bool($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "unsigned" option must be specified as a boolean.', $args);
                    }
                    break;

                case 'on_update_current_timestamp':
                    if ( !is_bool($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "on_update_current_timestamp" option must be specified as a boolean.', $args);
                    }
                    break;

                case 'primary':
                    if ( !is_bool($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "primary" option must be specified as a boolean.', $args);
                    }
                    break;

                case 'unique':
                    if ( !is_bool($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "unique" option must be specified as a boolean.', $args);
                    }
                    break;

                case 'comment':
                    if ( !is_string($value) )
                    {
                        $throws_errors->error(__FUNCTION__ . '(): The "comment" option must be specified as a string.', $args);
                    }
                    break;
            }
        }

        /*
         * ALL ARGUMENTS ARE CORRECT...
         */

        $column_type = new Column_Type(
            $type,
            [],
            false,
            -1,
            -1,
            -1,
            isset($options['character_set']) ? $options['character_set'] : 'utf8mb4',
            isset($options['collation']) ? $options['collation'] : 'utf8mb4_unicode_ci'
        );

        $column_schema = new Column_Schema(
            null,
            $name,
            $column_type,
            true,
            $default_type,
            $default_value,
            isset($options['is_nullable']) ? $options['is_nullable'] : false,
            isset($options['on_update_current_timestamp']) ? $options['on_update_current_timestamp'] : false,
            isset($options['is_auto_incrementing']) ? $options['is_auto_incrementing'] : false,
            isset($options['is_primary_key']) ? $options['is_primary_key'] : false,
            isset($options['is_unique']) ? $options['is_unique'] : false,
            ['select', 'insert', 'update', 'references'],
            isset($options['comment']) ? $options['comment'] : 'Dynamically inserted column.'
        );

        //dd($column_schema);
        return $column_schema;
    }


    /**
     * @param string $name
     */
    private function set_name(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function get_name(): string
    {
        return $this->name;
    }

    /**
     * @param Table_Schema $schema
     */
    private function set_schema(Table_Schema $schema): void
    {
        $this->schema = $schema;
    }

    /**
     * @return Table_Schema
     */
    public function get_schema(): Table_Schema
    {
        return $this->schema;
    }

    /**
     * @param mixed $loaded
     */
    private function set_loaded(bool $loaded): void
    {
        $this->loaded = $loaded;
    }

    /**
     * @return bool
     */
    public function is_loaded(): bool
    {
        return $this->loaded;
    }

}