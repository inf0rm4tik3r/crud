<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Database;

use Illuminate\Support\Collection;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Debug\Debug;
use stdClass;

class Table_Set
{

    /**
     * Reference to the main crud instance.
     * @var Crud
     */
    private $crud;

    /**
     * The main table to operate on.
     * @var Table
     */
    private $main_table;

    /**
     * Array of joined tables.
     * @var array[string : Crud_Table]
     */
    private $joined_tables;

    //private $join_info; // unused

    /**
     * @var array
     */
    private $entry_cache;
    private $entry_cache_processed;


    /**
     * Crud_Table_Set constructor.
     *
     * @param Crud $crud A reference to the parent crud instance.
     */
    public function __construct(Crud &$crud)
    {
        $this->crud = $crud;
        $this->reset();
    }

    public function reset()
    {
        $this->main_table = null;
        $this->joined_tables = [];
        $this->invalidate_entry_cache();
    }


    /**
     * @param int $id
     * @param bool $process_entry
     * @return mixed
     */
    public function get_entry(?int $id, bool $process_entry = true) : ?stdClass
    {
        // Passing null is permitted, but results in the immediate return of null.
        if ( $id === null )
        {
            return null;
        }

        // The entry needs to be loaded if the cache does not hold a value for the specified id.
        if ( !array_key_exists($id, $this->entry_cache) )
        {
            $this->entry_cache[$id] = $this->load_entry($id);
        }

        // $this->entry_cache[$id] MAY now hold the entry data. load_entry() may returned null!

        // The entry should get loa
        if ( $process_entry )
        {
            // The entry needs to be processed and stored if the cache does not hold a value for the specified id.
            if ( !array_key_exists($id, $this->entry_cache_processed) )
            {
                $entry_copy = $this->entry_cache[$id];
                $this->entry_cache_processed[$id] = $this->process_entry($entry_copy);
            }

            return $this->entry_cache_processed[$id];
        }
        else
        {
            return $this->entry_cache[$id];
        }
    }

    /**
     * Invalidates / resets the cached data stored through calls to <code>get_entry(int)</code>.
     * This function should be called upon storing this instance, so that we do not store a potentially big chunk of
     * data with this instance.
     *
     * @param null|int $id OPTIONAL: If an id gets specified, only the cached entries for this id will be invalidated.
     */
    public function invalidate_entry_cache(?int $id = null) : void
    {
        if ( is_null($id) )
        {
            $this->entry_cache = [];
            $this->entry_cache_processed = [];
        }
        else
        {
            unset($this->entry_cache[$id]);
            unset($this->entry_cache_processed[$id]);
        }
    }

    /**
     * Loads up the entry with the specified id as the value of the define primary auto incrementing column.<br>
     *
     *
     * @param int $id
     * @return null|stdClass
     */
    private function load_entry(int $id) : ?stdClass
    {
        // TODO: Check if the current entry is accessible if the current state needs it to be accessible.

        // TODO: Add defined where clauses and check if the result is null.
        // If true check if the entry itself exists (query without where clauses) and
        // return an error code like: self::ENTRY_NOT_FOUND or self::ENTRY_NOT_ACCESSIBLE;

        // TODO: add parameter: 'with_where_clause'
        // TODO: add joins...

        return $this->crud->db()->table($this->main_table->get_name())
            ->where($this->crud->get_id_column_name(), '=', $id)
            ->first();
    }

    /**
     * Returns an 'entry' which did not actually came from the database.
     * It has the same structure than an entry loaded by the <code>lode_processed_entry()</code> function.
     * BUT: All field values are set to their defaults.
     * This method should be use when constructing a 'create-view'.
     *
     * @return stdClass The dummy entry containing all the fields default values.
     */
    public function create_default_entry() : stdClass
    {
        // Each entry get represented by an stdClass instance.
        $entry = new stdClass();

        // At first, we will add all known columns of this table-set to the entry.
        foreach ($this->get_table_columns() as $table_name => &$table_array)
        {
            foreach ( $table_array as &$col /** @var Column_Schema $col */)
            {
                $entry->{$col->get_name()} = null;
            }
        }

        // The entry represents does not exist. Set the value of the primary auto incrementing column to 0.
        $entry->{$this->crud->get_id_column_name()} = 0;

        // We then need to fill the data in, so that the entry gets the same structure than a normally loaded one.
        $this->process_entry($entry);

        return $entry;
    }

    /**
     * Wrapper, which calls the process_entry(stdClass) function for every entry in the specified collection.
     *
     * @param Collection $collection The data collection to iterate over.
     * @return Collection Returns the manipulated collection.
     */
    public function process_collection(Collection $collection) : Collection
    {
        foreach ( $collection as &$entry )
        {
            $this->process_entry($entry);
        }

        return $collection;
    }


    /**
     * @param stdClass $entry
     * @return stdClass|null
     */
    public function process_entry(?stdClass &$entry) : ?stdClass
    {
        // Passing null is permitted, but results in the immediate return of null.
        if ( is_null($entry) )
        {
            return null;
        }

        // Add custom columns from all known tables to the current entry.
        foreach( $this->get_custom_columns() as $table_name => &$table_array )
        {
            foreach ( $table_array as &$col )
            {
                /** @var Column_Schema $col */
                $entry->{$col->get_name()} = null;
            }
        }

        // Redesign the data storage
        foreach ( $entry as $field_name => &$value )
        {
            $names = Table_Column_Name::parse($field_name, $this->crud);
            if ( empty($names['table_name']) )
            {
                $names['table_name'] = $this->main_table->get_name();
            }

            // Redesign the entries data storage.
            $value = $this->get_field_data_info($names['table_name'], $names['column_name'], $value, true, true); // TODO: extract table from fieldname
        }

        // All loaded default values are did not get processed jet.
        // We have to do that manually.
        foreach ( $entry as $field_name => &$value )
        {
            // Redesign the entries data storage.
            $value['default'] = $this->crud->process_default_value($value['default_type'], $value['default'], $entry);
            $value['default_processed'] = true;
        }

        // Each custom column should get its 'value' part set to its default value.
        foreach( $this->get_custom_columns() as $table_name => &$table_array )
        {
            foreach ( $table_array as &$col )
            {
                /** @var Column_Schema $col */
                $entry->{$col->get_name()}['value'] = $entry->{$col->get_name()}['default'];
            }
        }

        // The system allows callbacks for various fields. We will execute them on the loaded and processed
        // entry and give the user the ability to manipulate / change certain 'value's.
        $this->crud->callback_controller()->field_data_callbacks()->perform_callbacks($entry);

        return $entry;
    }

    /**
     * Returns information for the specified field.<br>
     * Returns ['internal_type', 'display_type', 'value' and (optionally) 'default']
     *
     * @param string $table_name
     * @param string $field_name
     * @param $data_to_use
     * @param bool $with_default
     * @param bool $add_full_relation_data
     * @return array
     */
    public function get_field_data_info(string $table_name, string $field_name, $data_to_use, bool $with_default, bool $add_full_relation_data) : array
    {
        $field_info = [];
        $field_info['internal_type'] = $this->crud->get_internal_type($field_name);
        $field_info['display_type'] = $this->crud->get_display_type($field_name);
        $field_info['value'] = $this->get_value($field_name, $data_to_use, $add_full_relation_data);

        if ( $with_default )
        {
            $default = $this->crud->get_default($table_name, $field_name, $this);
            //dump($default);
            $field_info['default_processed'] = false;
            $field_info['default_type'] = $default['default_type'];
            $field_info['default'] = $default['default_value'];
        }

        return $field_info;
    }

    /**
     * @param string $field_name
     * @param $value_to_use
     * @param bool $add_full_relation_data
     * @return mixed
     */
    public function get_value(string $field_name, $value_to_use, bool $add_full_relation_data)
    {
        if ( $add_full_relation_data and array_key_exists($field_name, $this->crud->get_relations()) )
        {
            return $this->crud->get_relations()[$field_name]->_acquire_data($value_to_use);
        }

        return $value_to_use;
    }

    public function set_main_table(string $table_name): void
    {
        $this->main_table = new Table($this->crud);
        $this->main_table->set_table($table_name);
    }

    /**
     * @return Table|null The main table object, or null if not jet initialized.
     */
    public function &get_main_table(): ?Table
    {
        return $this->main_table;
    }

    public function add_join_table()
    {
    }

    /**
     * Searches through the tables and their columns in this set to find the specified column name.<br>
     * If no table_name gets provided, this function will only search in the main table.
     *
     * @param string $table_name
     * @param string $column_name
     * @return bool
     */
    public function has_column(string $table_name = '', string $column_name): bool
    {
        // TODO: This function should search through all known tables if no table name gets provided!
        if ( empty($table_name) )
        {
            $table_name = $this->main_table->get_name();
        }

        // Search in the main table.
        if ( $table_name === $this->main_table->get_name() )
        {
            if ( $this->main_table->get_schema()->has_column_schema($column_name) )
            {
                return true;
            }
            return false;
        }

        // Search in a joined table.
        foreach ( $this->joined_tables as &$joined_table /** @var Table $joined_table */ )
        {
            if ( $table_name === $joined_table->get_name() )
            {
                if ( $joined_table->get_schema()->has_column_schema($column_name) )
                {
                    return true;
                }
                return false;
            }
        }

        // There is no table matching the table name... :(
        Debug::warning(__FUNCTION__.'(): No table matched the specified table name: '.$table_name);
        return false;
    }

    /**
     * Searches through the tables and their columns in this set to find the specified column name.<br>
     * If no table_name gets provided, this function will only search in the main table.
     *
     * @param string $table_name
     * @param string $column_name
     * @return null|Column_Schema
     */
    public function get_column(string $table_name = '', string $column_name): ?Column_Schema
    {
        if ( empty($table_name) )
        {
            $table_name = $this->main_table->get_name();
        }

        // Search in the main table.
        if ( $table_name === $this->main_table->get_name() )
        {
            if ( $this->main_table->get_schema()->has_column_schema($column_name) )
            {
                return $this->main_table->get_schema()->get_column_schema($column_name);
            }
            return null;
        }

        // Search in the joined table.
        foreach ( $this->joined_tables as &$joined_table /** @var Table $joined_table */ )
        {
            if ( $table_name === $joined_table->get_name() )
            {
                if ( $joined_table->get_schema()->has_column_schema($column_name) )
                {
                    return $joined_table->get_schema()->get_column_schema($column_name);
                }
                return null;
            }
        }

        // There is no table matching the table name... :(
        return null;
    }

    /**
     * Returns the schemata of all defined custom columns in the following array structure: <br>
     * [
     *      table_name_1 : [
     *                          table_column_1,
     *                          table_column_2,
     *                          ...
     *                     ],
     *      table_name_2 : [
     *                          table_column_1,
     *                          table_column_2,
     *                          ...
     *                     ],
     *      ...
     * ]
     * @return array Array of available column names.
     */
    public function get_table_columns(): array
    {
        $columns = [];

        $columns[ $this->main_table->get_name() ] = $this->main_table->get_schema()->get_table_column_schemata();

        foreach ( $this->joined_tables as &$joined_table /** @var Table $joined_table */ )
        {
            $columns[ $joined_table->get_name() ] = $joined_table->get_schema()->get_table_column_schemata();
        }

        return $columns;
    }

    /**
     * Returns the schemata of all defined custom columns in the following array structure: <br>
     * [
     *      table_name_1 : [
     *                          custom_column_1,
     *                          custom_column_2,
     *                          ...
     *                     ],
     *      table_name_2 : [
     *                          custom_column_1,
     *                          custom_column_2,
     *                          ...
     *                     ],
     *      ...
     * ]
     * @return array Array of available column names.
     */
    public function get_custom_columns() : array
    {
        $columns = [];

        $columns[ $this->main_table->get_name() ] = $this->main_table->get_schema()->get_custom_column_schemata();

        foreach ( $this->joined_tables as &$joined_table /** @var Table $joined_table */ )
        {
            $columns[ $joined_table->get_name() ] = $joined_table->get_schema()->get_custom_column_schemata();
        }

        return $columns;
    }

    /**
     * Returns the column schemata for all available table and custom columns in the following array structure: <br>
     * [
     *      table_name_1 : [
     *                          column_1,
     *                          column_2,
     *                          ...
     *                     ],
     *      table_name_2 : [
     *                          column_1,
     *                          column_2,
     *                          ...
     *                     ],
     *      ...
     * ]
     * @return array Array of available column names.
     */
    public function get_columns() : array
    {
        $columns = [];

        $columns[ $this->main_table->get_name() ] = $this->main_table->get_schema()->get_column_schemata();

        foreach ( $this->joined_tables as &$joined_table /** @var Table $joined_table */ )
        {
            $columns[ $joined_table->get_name() ] = $joined_table->get_schema()->get_column_schemata();
        }

        return $columns;
    }

    /**
     * Return the names of all available columns in the following array structure: <br>
     * [
     *      table_name_1 : [
     *                          column_name_1,
     *                          column_name_2,
     *                          ...
     *                     ],
     *      table_name_2 : [
     *                          column_name_1,
     *                          column_name_2,
     *                          ...
     *                     ],
     *      ...
     * ]
     * @return array Array of available column names.
     */
    public function get_column_names() : array
    {
        $columns_multi_level = $this->get_columns();

        foreach ( $columns_multi_level as $table_name => &$columns )
        {
            $columns = array_keys($columns);
        }

        return $columns_multi_level;
    }

    /**
     * Returns the names of all columns in a flat array. <br>
     * Table name and column name will still be separated in little arrays. <br>
     * Structure: <br>
     * [
     *      ['table_name' : string, 'column_name' : string],
     *      ['table_name' : string, 'column_name' : string],
     *      ...
     * ]
     * @return array Array of available column names.
     */
    public function get_column_names_flat() : array
    {
        $column_names_multi_level = $this->get_column_names();

        $column_names_flat = [];

        foreach ( $column_names_multi_level as $table_name => &$column_names )
        {
            foreach ( $column_names as &$column_name )
            {
                array_push($column_names_flat, compact('table_name', 'column_name'));
            }
        }

        return $column_names_flat;
    }

    /**
     * Returns the names of all columns in a flat array. <br>
     * Table name and column name will be concatenated (glued together using the $glue parameter). <br>
     * Structure: <br>
     * [
     *      'table_name'.$glue.'column_name',
     *      'table_name'.$glue.'column_name',
     *      ...
     * ]
     *
     * @param string $glue The string which should be used to glue table name and column name together.
     * @return array Array of available column names.
     */
    public function get_column_names_flat_concatenated(string $glue = '.') : array
    {
        $column_names_multi_level = $this->get_column_names();

        $column_names_flat_concatenated = [];

        foreach ( $column_names_multi_level as $table_name => &$column_names )
        {
            foreach ( $column_names as &$column_name )
            {
                array_push($column_names_flat, $table_name.$glue.$column_name);
            }
        }

        return $column_names_flat_concatenated;
    }

}