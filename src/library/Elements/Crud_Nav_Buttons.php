<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Elements;

use Lukaspotthast\Crud\Support\Functions;

trait Crud_Nav_Buttons
{

    private $custom_header_buttons; // array

    private function reset_nav_buttons()
    {
        $this->custom_header_buttons = array();
    }

    public function header_button($name, $internal_name, $function, $filepath, $arguments = array())
    {
        // Button info.
        $btn_info = 'Name: '.isset($name) ? $name : ''.'<br>'.
        'Internal name: '.isset($internal_name) ? $internal_name : ''.'<br>'.
        'Function: '.isset($function) ? $function : ''.'<br>'.
        'Filepath: '.isset($filepath) ? $filepath : '';

        // Check if parameters are ok.
        if ($name === null /*or $name === ''*/) // Allow empty button names.
        {
            $msg = 'header_button(): You need to specify a name for this button: <br>';
            $this->error($msg.$btn_info);
            return;
        }
        if ($internal_name === null or $internal_name === '')
        {
            $msg = 'header_button(): You need to specify an internal name for this button: <br>';
            $this->error($msg.$btn_info);
            return;
        }
        if ($function === null or $function === '')
        {
            $msg = 'header_button(): You need to specify a function name for this button: <br>';
            $this->error($msg.$btn_info);
            return;
        }
        if ($filepath === null or $filepath === '')
        {
            $msg = 'header_button(): You need to specify a filepath for this button: <br>';
            $this->error($msg.$btn_info);
            return;
        }

        // Check if this button is already defined.
        if (array_key_exists($internal_name, $this->custom_header_buttons))
        {
            $msg = 'header_button(): You already defined this button: <br>';
            $this->error($msg.$btn_info);
            return;
        }

        // Check if the function can be called...
        $callable = Functions::is_callable($function, $filepath, $this);
        if (!$callable)
        {
            $msg = 'header_button(): The provided function "'
                .(is_array($function) ? serialize($function) : $function).'" does not exist or is not callable in "'
                .$filepath.'"!';
            $this->error($msg);
            return;
        }

        // Add the button information to the instance if everything went good.
        $this->custom_header_buttons[$internal_name] = array(
            'name'      => $name,
            'function'  => $function,
            'filepath'  => $filepath,
            'arguments' => $arguments
        );
    }

    public function get_custom_header_buttons()
    {
        return $this->custom_header_buttons;
    }

}