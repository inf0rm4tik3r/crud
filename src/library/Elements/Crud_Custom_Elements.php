<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Elements;

trait Crud_Custom_Elements
{

    private $custom_elements; // array(position, element)


    private function reset_custom_elements() : void
    {
        $this->custom_elements = array();
    }

    public function add_element(string $position, $element) : void
    {
        $args = compact('position', 'element');

        // The position parameter must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $position, 'position', $args) )
        {
            $this->error(
                'Use one of the following supported positions:<br>'
                .json_encode(Crud_Custom_Element::SUPPORTED_POSITIONS)
            );
            return;
        }

        // The element must either be an instance of Crud_Custom_Element, or a string,
        // representing the HTML code of what is to be displayed.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $element, 'element', $args, true) )
        {
            return;
        }

        // Input is valid. We can store the element in the instance.
        if ( ! array_key_exists($position, $this->custom_elements) )
        {
            $this->custom_elements[$position] = array();
        }
        array_push($this->custom_elements[$position], $element);
    }



    public function get_custom_elements_for($position)
    {
        $result = '';

        // Maybe there are no elements specified.
        if ( ! array_key_exists($position, $this->custom_elements ))
        {
            return $result;
        }

        // Return all specified elements.
        foreach ( $this->custom_elements[$position] as &$element )
        {
            $result .= '<div class="crud-custom-element crud-custom-element-position-'.$position.'">'.$element.'</div>';
        }

        return $result;
    }

}