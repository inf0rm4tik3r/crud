<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Elements;

class Crud_Interactive_Elem
{
    // Reference to the crud instance this element got created with.
    private $crud = null;

    // Internal identifier for this element.
    private $name = null;

    // Displayed name. May contain HTML code .
    private $label = '';

    // A parent node. May be used to nest elements.
    private $parent = null;

    // Specifies where this element should be rendered.
    private $positions = array();
    const ALLOWED_POSITIONS = array(
        'list.entry', 'edit.header',
    );

    private $function = null;
    private $file = null;
    private $url = '';
    // Also accepts 'modal'!
    private $url_target = null;


    public function __construct(&$crud)
    {
        $this->crud = $crud;
    }


    public function name($name)
    {
        // Check if the name is correctly specified.
        if ( !(isset($name) and is_string($name) and !empty($name)) )
        {
            $this->crud->error(
                '<b>'.__FUNCTION__.'(): You must specify the name as a non empty string.</b><br>'
                .'Arguments specified:<br>'.$this->crud->args_as_list(compact('name'))
            );
        }

        // Check if the name is already in use.

        // Set the specified name.
        $this->name = $name;
    }
    public function get_name()
    {
        return $this->name;
    }

    public function label($label)
    {
        $this->label = $label;
    }
    public function get_label()
    {
        return $this->label;
    }

    public function parent($parent)
    {
        $this->parent = $parent;

        // Update the structure of interactive elements in the crud instance.
        $this->crud->set_interactive_elem_parent($this, $parent);
    }
    public function get_parent()
    {
        return $this->parent;
    }


}