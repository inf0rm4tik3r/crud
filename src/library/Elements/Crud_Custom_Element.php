<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Elements;

use Lukaspotthast\Crud\Rendering\Renderer;

class Crud_Custom_Element
{

    /**
     *
     */
    const SUPPORTED_POSITIONS = [
        'top', 'bottom'
    ];

    /**
     * @var string The HTML markup of this element.
     */
    private $html = '';

    /**
     * Crud_Custom_Element constructor.
     */
    public function __construct()
    {
    }

    public function from_view(string $view, array $args) : self
    {
        $this->set_html( Renderer::render_view($view, $args) );
        return $this;
    }

    public function set_html(string $html) : void
    {
        $this->html = $html;
    }

    public function get_html() : string
    {
        return $this->html;
    }

}