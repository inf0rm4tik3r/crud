<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Lukaspotthast\Crud\Database\DBHelper;
use Lukaspotthast\Crud\Database\Table_Set;
use Lukaspotthast\Crud\Database\Table_Column_Name;
use stdClass;

/**
 * Trait Crud_Default_Values
 * @package Lukaspotthast\Crud
 */
trait Crud_Default_Values
{

    /**
     * Sets a default value for the specified column. <br>
     * This value will be used: <br>
     *  - as the default value for a field input for that column in the create view
     *  - if no field data got provided when an entry gets saved
     *
     * @param mixed $table_column_name
     * @param mixed $value The value that should be used as the default value for the specified column.
     */
    public function set_default($table_column_name, $value) : void
    {
        $args = compact('table_column_name', 'value');

        $parsed = Table_Column_Name::parse($table_column_name, $this);
        $table_name = $parsed['table_name'];
        $column_name = $parsed['column_name'];

        // Check if the field argument got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'field', $args) )
        {
            return;
        }

        // The column of name $column_name must exist in table $table_name!
        if ( !$this->get_table_set()->has_column($table_name, $column_name) )
        {
            $this->error(
                __FUNCTION__.'(): A column of name "'.$column_name.'" in table "'.$table_name.'" could no be found.',
                $args
            );
            return;
        }

        // Check the type of $value.
        if ( is_string($value) )
        {
            $type = 'string';
        }
        else if ( is_int($value) )
        {
            $type = 'integer';
        }
        else if ( is_float($value) )
        {
            $type = 'float';
        }
        else if ( is_double($value) )
        {
            $type = 'double';
        }
        else if ( is_array($value) )
        {
            $type = 'array';
        }
        else if ( DBHelper::is_laravel_query_object($value) )
        {
            $type = 'query';
        }
        else
        {
            $this->error(
                __FUNCTION__.'(): The type of $value ('.gettype($value).') is not supported as a default value.',
                $args
            );
            return;
        }

        // MAYBE: Check if type of field matches $value's type.

        // Resolve the column schema and update it.
        $column_schema = $this->get_table_set()->get_column($table_name, $column_name);
        $column_schema->set_custom_default($type, $value);
    }

    /**
     * Acquires the default value for the specified column in the specified table.<br>
     * This function will access the given <code>Crud_Table_Set</code> to do so.<br>
     * The returned data will be the RAW default value (it may be a string with field-tags or even a Laravel
     * QueryBuilder object!
     * Use the <code>process_default(string, mixed, ?stdClass)</code> function to parse the value.
     *
     * @param string $table_name The table name to search in. Must not be empty.
     * @param string $column_name The column name for which the default value should be returned. Must not be empty.
     * @param Table_Set $table_set This table set is getting accessed to find relevant information.
     * @return array|null Returns null on error or an array of the following structure:
     * <pre>
     *      [
     *          'default_value' => mixed,
     *          'default_type' => string
     *      ]
     * </pre>
     */
    public function get_default(string $table_name, string $column_name, Table_Set $table_set)
    {
        $args = compact('table_name', 'column_name', 'table_set');

        // The table name must be specified as a string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $table_name, 'table_name', $args) )
        {
            return null;
        }

        // The column name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return null;
        }

        // Try to resolve the column schema.
        $col = $table_set->get_column($table_name, $column_name);

        // If null got returned, the column does not exist.
        if ( is_null($col) )
        {
            $this->error(
                __FUNCTION__.'(): A column of name "'.$column_name.'" in table "'.$table_name.'" could no be found.',
                $args
            );
            return null;
        }

        /*
         * Inputs are correct and we found the field / column schema.
         * It is stored in $col.
         */

        // Use the custom default value if defined.
        if ( $col->has_custom_default() )
        {
            $default_value = $col->get_custom_default_value();
            $default_type = $col->get_custom_default_type();
        }
        // Otherwise take the default value stored in / defined through the database.
        else if ( $col->has_default() )
        {
            $default_value = $col->get_default_value();
            $default_type = $col->get_default_type();
        }
        // If no default value could be found, try to calculate one based on the type of the field.
        else
        {
            $default_value = DBHelper::get_default_for_col_type($col->get_type()->get_name());
            $default_type = gettype($default_value);
        }

        // If the $col represents an enum, try to use its first option.
        if ( $col->get_type()->get_name() === 'enum' )
        {
            if ( count($col->get_type()->get_options()) > 0 )
            {
                $default_value = $col->get_type()->get_options()[0];
            }
        }

        return [
            'default_value' => $default_value,
            'default_type' => $default_type
        ];
    }

    /**
     * @param string $default_type
     * @param $default
     * @param null|stdClass $entry
     * @return mixed !!!
     */
    public function process_default_value(string $default_type = 'undefined', $default, ?stdClass $entry)
    {
        switch ( $default_type )
        {
            // In case no default type got specified, we will just return the given default value as is.
            case 'undefined':
                return $default;
                break;

            case 'integer':
                return $default;
                break;

            case 'float':
                return $default;
                break;

            case 'double':
                return $default;
                break;

            case 'array':
                return json_encode($default);
                break;

            case 'query':
                // TODO: needs revision !

                // Replace placeholders with actual data
                if ( $entry !== null )
                {
                    $default = $this->replace_field_tags($default, $entry);
                }

                // Perform the query
                $default = $this->db()->select($default);

                // If the query result is a collection / an array, serialize it for output.
                if (is_array($default))
                {
                    if (count($default) > 1)
                    {
                        $default = serialize($default);
                    }
                    else
                    {
                        if (count($default) !== 0)
                        {
                            $current_object = $default[0];
                            $default = '';
                            foreach ($current_object as &$property)
                            {
                                $default .= $property;
                            }

                            $current_object_data = array();
                            foreach ($current_object as &$property)
                            {
                                $current_object_data[] = $property;
                            }
                            $default = implode(', ', $current_object_data);
                        }
                        else
                        {
                            $default = '';
                        }
                    }
                }

                return $default;
                break;

            case 'string':
                // If the default value is a string, we need to search and replace field-tags.
                if ( $entry !== null )
                {
                    $default = $this->replace_field_tags($default, $entry);
                }
                return $default;
                break;


            default:
                $this->error(__FUNCTION__.'(): Unknown default_type specified.', compact('default_type', 'default'));
                return null;
                break;
        }
    }

}