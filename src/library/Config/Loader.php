<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Config;

use Lukaspotthast\Support\Filesystem\Filesystem;
use Lukaspotthast\Support\Str;

/**
 * Class Loader
 * @package Lukaspotthast\Crud\Config
 */
abstract class Loader
{

    /**
     * @param string $directorypath
     * @param bool   $recursive
     * @return array
     */
    public static function load_from_folder(string $directorypath, bool $recursive = true): array
    {
        $items = [];
        $list  = Filesystem::list_elements($directorypath, $recursive);
        self::load_items($items, $list);
        return $items;
    }

    /**
     * @param array $items
     * @param array $list
     * @return array
     */
    private static function load_items(array &$items, array &$list): array
    {
        foreach ( $list['elements'] as &$element )
        {
            if ( $element['is_directory'] )
            {
                $items[$element['name']] = [];

                if ( $element['children'] !== null )
                {
                    self::load_items($items[$element['name']], $element['children']);
                }
            }
            else if ( $element['is_file'] )
            {
                /** @noinspection PhpIncludeInspection */
                $content = include $list['root'] . $element['name'];

                if ( is_array($content) )
                {
                    self::parse_array($content);
                    $items[$element['file_name']] = $content;
                }
            }
        }

        return $items;
    }

    /**
     * @param array $content
     */
    private static function parse_array(array &$content)
    {
        foreach ( $content as $key => &$value )
        {
            // Example: [... "save_aborted.header" => "Attention!" ...]
            if ( Str::contains($key, '.') )
            {
                $segments = explode('.', $key);
                unset($content[$key]);
                self::modify_association($content, $segments, $value);
            }

            else if ( is_array($value) )
            {
                self::parse_array($value);
            }
        }
    }

    /**
     * @param array $arr
     * @param array $segments
     * @param       $value
     */
    private static function modify_association(array &$arr, array &$segments, &$value)
    {
        $array         = &$arr;
        $segment_count = count($segments);

        if ( $segment_count >= 2 )
        {
            $segment = reset($segments);

            // Create an array at the current segment (key).
            if ( !array_key_exists($segment, $array) )
            {
                $array[$segment] = [];
                array_shift($segments);
                self::modify_association($array[$segment], $segments, $value);
            }

            // $segment already exists as a key! The value at it is not an array.
            else if ( !is_array($array[$segment]) )
            {
                $temp            = $array[$segment];
                $array[$segment] = ['' => $temp];
                array_shift($segments);
                self::modify_association($array[$segment], $segments, $value);
            }

            // $segment already exists as a key but the value at it is an array!
            else
            {
                array_shift($segments);
                self::modify_association($array[$segment], $segments, $value);
            }
        }
        else
        {
            $last_segment         = reset($segments);
            $array[$last_segment] = &$value;
            if ( is_array($value) )
            {
                self::parse_array($value);
            }
        }
    }

}