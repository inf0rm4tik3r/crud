<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Config;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Config;
use Lukaspotthast\Crud\Frameworks\Laravel\Laravel_Config;

/**
 * Configuration accessor class.
 *
 * @package Lukaspotthast\Crud\Config
 */
abstract class Config
{

    private static $initialized = false;

    /**
     *
     */
    public static function init(): void
    {
        if ( self::$initialized )
        {
            return;
        }

        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Config::config_init();
                break;

            case Frameworks::LARAVEL:
                Laravel_Config::config_init();
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }

        self::$initialized = true;
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function set(string $key, $value): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Config::config_set($key, $value);
                break;

            case Frameworks::LARAVEL:
                Laravel_Config::config_set($key, $value);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function has(string $key): bool
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                return PHP_Config::config_has($key);
                break;

            case Frameworks::LARAVEL:
                return Laravel_Config::config_has($key);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    /**
     * @param string $key
     * @param null   $default_value
     * @return mixed
     */
    public static function get(string $key, $default_value = null)
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                return PHP_Config::config_get($key, $default_value);
                break;

            case Frameworks::LARAVEL:
                return Laravel_Config::config_get($key, $default_value);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

}