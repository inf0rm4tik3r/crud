<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

trait Crud_Inline_Edit
{

    private $is_inlineedit;
    private $col_inline_editable; // array
    private $inlineedit_mode;
    private $inlineedit_activator;

    private function reset_inline_edit()
    {
        $this->is_inlineedit = true;
        $this->col_inline_editable = array(
            $this->get_id_column_name() => false
        );
        $this->inlineedit_mode = 'modal';
        $this->inlineedit_activator = 'btn';
    }

    /**
     * Defines if field data can be manipulated in the list view.
     * If set active, all fields (which are not defined as disabled) can be edited and saved.
     *
     * @param bool $is_inlineedit Optional:<br>
     *                                true: Activate the inlineedit functionality.<br>
     *                                false: Deactivate the inlineedit functionality.<br>
     *                                Defaults to: true
     */
    public function inlineedit(bool $is_inlineedit = true) : void
    {
        if (!(isset($is_inlineedit) and is_bool($is_inlineedit)))
        {
            $this->error(
                'inlineedit(): $is_inlineedit must be a boolean value.<br>'
                .'Arguments passed:<br>'
                .Tools::args_as_list(array('is_inlineedit' => $is_inlineedit))
            );
            return;
        }

        $this->is_inlineedit = $is_inlineedit;
    }

    /**
     * Determines if the inlineedit functionality is activated.
     *
     * @return bool true/false.
     */
    public function is_inlineedit(): bool
    {
        return $this->is_inlineedit;
    }

    public function set_inlineedit_mode($mode)
    {
        if (!(isset($mode) and is_string($mode) and !empty($mode)))
        {
            $this->error(
                'set_inlineedit_mode(): The $mode must be specified as a nonempty string.<br>'
                .'Arguments passed:<br>'
                .Tools::args_as_list(array('mode' => $mode))
            );
        }

        $this->inlineedit_mode = $mode;
    }

    public function get_inlineedit_mode(): string
    {
        return $this->inlineedit_mode;
    }

    public function set_inlineedit_activator($activator)
    {
        if (!(isset($activator) and is_string($activator) and !empty($activator)))
        {
            $this->error(
                'set_inlineedit_mode(): The $activator must be specified as a nonempty string.<br>'
                .'Arguments passed:<br>'
                .Tools::args_as_list(array('activator' => $activator))
            );
        }

        $this->inlineedit_activator = $activator;
    }

    public function get_inlineedit_activator(): string
    {
        return $this->inlineedit_activator;
    }

    /**
     * Defines whether or not the column reference by $column_name should be inline-editable.<br>
     * Pseudo function for col_inline_editable($col_name, $inline_editable).<br>
     * @see Crud::col_inline_editable($col_name, $inline_editable)
     *
     * @param string $col_name The column to specify thy property for.
     * @param bool $inline_editable Should the column be inline-editable? (true / false)
     */
    public function allow_inlineedit(string $col_name, bool $inline_editable) : void
    {
        $this->col_inline_editable($col_name, $inline_editable);
    }

    /**
     * Defines whether or not the column reference by $column_name should be inline-editable.
     *
     * @param string $col_name The column to specify thy property for.
     * @param bool $inline_editable Should the column be inline-editable? (true / false)
     */
    public function col_inline_editable(string $col_name, bool $inline_editable) : void
    {
        // Check if the specified column got correctly specified.
        if ( !(isset($col_name) and is_string($col_name) and !empty($col_name)) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): You must specify the column as a non empty string!</b><br>'
                .'Arguments passed:<br>'.Tools::args_as_list(compact('col_name', 'inline_editable'))
            );
            return;
        }

        // Check if the specified column really exists.
        if ( !$this->table_set->get_main_table()->column_exists($col_name) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): The specified column does not exist!</b><br>'
                .'Arguments passed:<br>'.Tools::args_as_list(compact('col_name', 'inline_editable'))
            );
            return;
        }

        // Check if the editable parameter got correctly specified
        if ( !(isset($inline_editable) and is_bool($inline_editable)) )
        {
            $this->error(
                '<b>'.__FUNCTION__.'(): You must specify the "editable" parameter as a boolean!</b><br>'
                .'Arguments passed:<br>'.Tools::args_as_list(compact('col_name', 'inline_editable'))
            );
            return;
        }

        // Arguments were checked. We can add the supplied data to the instance.
        $this->col_inline_editable[$col_name] = $inline_editable;
    }

    /**
     * Determines if inline editing is allowed for the specified column.<br>
     * -NO ARGUMENT CHECKS-
     *
     * @param string $col_name The column which is to be checked.
     * @return bool true / false
     */
    public function is_col_inline_editable(string $col_name) : bool
    {
        return array_key_exists($col_name, $this->col_inline_editable) ? $this->col_inline_editable[$col_name] : true;
    }

}