<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Rendering;

/**
 * Trait Renderer_Trait
 * @package Lukaspotthast\Crud\Rendering
 */
trait Renderer_Trait
{

    /**
     * @var Renderer
     */
    private $renderer;

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * Renders this instance.
     * Wrapper for get_renderer()->render().
     * @return string The rendered HTML as a string.
     */
    public function render(): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->renderer->render();
    }

    /**
     * @return Renderer
     */
    public function renderer(): Renderer
    {
        return $this->renderer;
    }

}