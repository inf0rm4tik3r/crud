<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Rendering;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\View;
use Throwable;

class Renderer
{

    /**
     * Stores a reference to the parent crud object.
     * @var Crud
     */
    private $crud;

    /**
     * @var View\Compilation\Compiler
     */
    private static $view_compiler;

    public static function static_init()
    {
        self::$view_compiler = new View\Compilation\Compiler();
    }

    /**
     * Crud_Renderer constructor.
     * @param Crud $crud Reference to the crud instance which should be rendered through this renderer.
     */
    public function __construct(Crud &$crud)
    {
        $this->crud = $crud;
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * Renders the view specified by the first parameter with all entries from the second parameter.
     *
     * @param string $view_name The view to render.
     * @param array  $data      The data which should be passed into the view. Access data by using the initial array
     *                          keys.
     * @return string Rendered HTML string.
     */
    public static function render_view(string $view_name, array $data): string
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return self::$view_compiler->render_view($view_name, $data);
    }

    /**
     * Prepares the current instance to be rendered.
     */
    private function prepare_rendering(): void
    {
        // The verification key should change for every request. This prevents total failure, should a verification
        // collision between different instances occur (and it looks cool!).
        // An instance could then just get reloaded to fix the collision. (In regard to the field input id-generation.)
        $this->crud->regenerate_verification_key();

        // Updates the values of the connect_field where clause of every nested instance to the current entry id,
        // so that we only see entries connected to the currently rendering entry.
        $parent_entry = $this->crud->get_table_set()->get_entry($this->crud->get_current_entry_id(), false);
        $this->crud->update_nested_table_instances_where_connection($parent_entry);
    }

    /**
     * Perform task after the rendering got completed.
     */
    private function after_rendering(): void
    {
        if ( $this->crud->get_requester() === 'main' )
        {
            /*
             * Why do we need to store all nested instance at this point:
             * When constructing a crud instance, one might add nested tables.
             * The nested_table() function then CREATES a new instance object, which will be used for the nested table.
             * BUT: The main instance may only render its list view (which is the default!). As of now, nested
             * instances will not be rendered there. AND: no rendering = no storing.
             * Conclusion:
             * If the 'main' requester (default, not 'ajax') requests the rendering, all nested table instances need
             * to be stored once, so that all configurations made to all nested table instances get saved.
             * Otherwise everything we do to the instance object returned by $crud->nested_table() would get lost.
             */
            $this->crud->store_nested_table_instances();
        }

        // Stores the current state of the instance in the session for later use.
        $this->crud->get_storage()->store_instance();
    }

    /**
     * Renders this instance in its current state.
     * @return string The html content. Ready to be sent to the browser.
     * @throws Throwable
     * @throws Throwable
     * @throws Throwable
     */
    public function render(): string
    {
        $this->prepare_rendering();

        // A table should have been (and needs to be) set!
        $this->crud->check_is_table_set();

        // Just return a "loader" when:
        // The main PHP script called the render() function for an asynchronous instance.
        // OR:
        // A nested table (set to asynchronous loading) needs to be rendered and its requester is set to 'main'
        // (which is the case when the instance gets rendered upon opening its parent entry).
        if ( ($this->crud->is_asynchronous() and $this->crud->get_requester() === 'main') )
        {
            $crud_instance_html_array = $this->render_crud_loader();
        }
        // On every AJAX request or non asynchronous rendering, the instance gets rendered normally.
        else
        {
            $crud_instance_html_array = $this->render_crud_instance();
        }

        $this->after_rendering();

        // On a "main" request (through a get route), the wrapper needs to be rendered.
        if ( $this->crud->get_requester() === 'main' )
        {
            return $this->output(self::render_view('wrapper', [
                'crud'                     => $this->crud,
                'crud_instance_html_array' => $crud_instance_html_array,
            ]));
        }
        // Every AJAX request should only reload the instance rendering.
        else
        {
            return $this->output(self::render_view('instance', [
                'crud'                     => $this->crud,
                'crud_instance_html_array' => $crud_instance_html_array,
            ]));
        }
    }

    /**
     * Performs beforerender events and renders the input for the specified field.
     *
     * @param string $field_name
     * @return string
     * @throws Throwable
     */
    public function render_field(string $field_name): string
    {
        Debug::start_measure('render_field', 'CRUD - "' . $this->crud->get_instance_name() . '" - rendering field');

        $current_entry = $this->crud->get_table_set()->get_entry($this->crud->get_current_entry_id(), true);

        // PERFORM 'beforerender' callbacks:
        $this->crud->callback_controller()->standard_callbacks()
            ->perform_beforerender_callbacks($this->crud->get_current_entry_id(), $current_entry);

        // We only render the view for the desired field if no errors occurred in the first place.
        $html = 'Oh! No output generated... °_°';
        if ( !$this->crud->has_errors() )
        {
            $html = self::render_view('fields.field', [
                'crud'       => $this->crud,
                'entry_id'   => $this->crud->get_current_entry_id(),
                'field_name' => $field_name,
                'col'        => $this->crud->get_table_set()->get_main_table()->get_schema()->get_column_schema($field_name),
                'data'       => $current_entry,
            ]);
        }

        Debug::stop_measure('render_field');

        return $this->output($html);
    }

    /**
     * @return array
     * @throws Throwable
     */
    private function render_crud_loader(): array
    {
        // The instance is not loaded completely.
        // The JavaScript part of the application will recognize this and fetch the full rendering.
        $this->crud->set_loaded(false);

        Debug::start_measure('render_crud_loader', 'CRUD - "' . $this->crud->get_instance_name() . '" - loader rendering');

        $output = [];

        // Load the base view which gets displayed on every crud page.
        $output['base'] = self::render_view('base', ['crud' => $this->crud]);

        Debug::stop_measure('render_crud_loader');

        return $output;
    }

    /**
     * This function will render everything which will go inside a "crud-instance" div.
     * @return array
     * @throws Throwable
     */
    private function render_crud_instance(): array
    {
        // The function will completely render this instance. Which in the context of asynchronous loading means
        // that the instance is then loaded.
        $this->crud->set_loaded(true);

        Debug::start_measure('render_crud_instance', 'CRUD - "' . $this->crud->get_instance_name() . '" - instance rendering');

        $output = [];

        $current_entry = $this->crud->get_table_set()->get_entry($this->crud->get_current_entry_id(), true);

        // PERFORM 'beforerender' callbacks:
        $this->crud->callback_controller()->standard_callbacks()
            ->perform_beforerender_callbacks($this->crud->get_current_entry_id(), $current_entry);

        // Render the base and header views which get displayed on every crud page.
        $output['base']   = self::render_view('base', ['crud' => $this->crud]);
        $output['header'] = self::render_view('header', ['crud' => $this->crud]);

        // If errors occurred until now, we do not even need to render the view.
        if ( !$this->crud->has_errors() )
        {
            switch ( $this->crud->get_task() )
            {
                case 'list':
                    Debug::start_measure('list-data');
                    $list_data = [
                        'crud'    => $this->crud,
                        'columns' => $this->crud->acquire_ordered_columns(),
                        'data'    => $this->crud->acquire_list_data(),
                    ];
                    Debug::stop_measure('list-data');

                    Debug::start_measure('list');
                    $output['body'] = self::render_view('list', $list_data);
                    Debug::stop_measure('list');
                    break;

                case 'structure':
                    $output['body'] = self::render_view(
                        'structure',
                        [
                            'crud'    => $this->crud,
                            'columns' => $this->crud->get_table_set()->get_main_table()->get_schema()->get_column_schemata(),
                        ]
                    );
                    break;

                case 'create':
                    $output['body'] = self::render_view(
                        'create',
                        [
                            'crud'   => $this->crud,
                            'fields' => $this->crud->acquire_ordered_fields(),
                            'data'   => $this->crud->get_table_set()->create_default_entry(),
                        ]
                    );
                    break;

                case 'edit':
                    Debug::start_measure('edit-data');
                    $edit_data = [
                        'crud'   => $this->crud,
                        'fields' => $this->crud->acquire_ordered_fields(),
                        'data'   => $current_entry,
                    ];
                    Debug::stop_measure('edit-data');

                    Debug::start_measure('edit');
                    $output['body'] = self::render_view('edit', $edit_data);
                    Debug::stop_measure('edit');
                    break;

                case 'view':
                    $output['body'] = self::render_view(
                        'view',
                        [
                            'crud'   => $this->crud,
                            'fields' => $this->crud->acquire_ordered_fields(),
                            'data'   => $current_entry,
                        ]
                    );
                    break;

                default:
                    $this->crud->error('render(): Unknown task specified.');
                    break;
            }
        }

        Debug::stop_measure('render_crud_instance');

        return $output;
    }

    /**
     * @return string
     * @throws Throwable
     * @throws Throwable
     * @throws Throwable
     * @throws Throwable
     * @throws Throwable
     */
    private function render_errors(): string
    {
        Debug::start_measure('render_crud_errors', 'CRUD - "' . $this->crud->get_instance_name() . '" - errors rendering');

        $crud_instance_html_array           = [];
        $crud_instance_html_array['base']   = self::render_view('base', ['crud' => $this->crud]);
        $crud_instance_html_array['header'] = self::render_view('header', ['crud' => $this->crud]);
        $crud_instance_html_array['body']   = self::render_view('error', ['crud' => $this->crud]);

        // Errors should not persist.
        $this->crud->reset_errors();

        // On a "main" request (through a get route), the wrapper needs to be rendered.
        if ( $this->crud->get_requester() === 'main' )
        {
            $final = self::render_view('wrapper', [
                'crud'                     => $this->crud,
                'crud_instance_html_array' => $crud_instance_html_array,
            ]);
        }
        // Every AJAX request should only reload the instance rendering.
        else
        {
            $final = self::render_view('instance', [
                'crud'                     => $this->crud,
                'crud_instance_html_array' => $crud_instance_html_array,
            ]);
        }

        Debug::stop_measure('render_crud_errors');

        return $final;
    }

    /**
     * @param string $html
     * @return string
     * @throws Throwable
     */
    private function output(string $html): string
    {
        // If errors occurred, we print them on a custom error view.
        // This includes errors which might originated form previous rendering attempts.
        if ( $this->crud->has_errors() )
        {
            return $this->render_errors();
        }

        // Otherwise passthrough.
        return $html;
    }

}

Renderer::static_init();