<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Http;

use Lukaspotthast\Support\Arr;

class Request
{
    /**
     * @var string Request method used: 'GET', 'POST', ...
     */
    private $method;

    private $header;

    private $input;
    private $files;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->input = array_replace_recursive([], $_GET, $_POST);
        $this->files = $_FILES;
    }

    public function has(string $key): bool
    {
        return Arr::has($this->input, $key);
    }

    public function input(string $key)
    {
        return Arr::get($this->input, $key);
    }

    public function all(): array
    {
        return $this->input;
    }

}