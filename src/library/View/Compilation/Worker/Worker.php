<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Worker;

use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

abstract class Worker
{

    /**
     * @var NodeVisitorAbstract
     */
    protected $node_visitor;

    /**
     * @var NodeTraverser
     */
    protected $node_traverser;

}