<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Worker;

use Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors\Declare_Extractor_Visitor;
use PhpParser\NodeTraverser;

class Declare_Extractor extends Worker
{

    public function __construct()
    {
        $this->node_visitor = new Declare_Extractor_Visitor();

        $this->node_traverser = new NodeTraverser();
        $this->node_traverser->addVisitor($this->node_visitor);
    }

    public function extract_declare_statements($ast)
    {
        return $this->node_traverser->traverse($ast);
    }

    public function get_node_visitor(): Declare_Extractor_Visitor
    {
        return $this->node_visitor;
    }

}