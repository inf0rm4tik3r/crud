<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Worker;

use Lukaspotthast\Crud\View;
use Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors\Flattener_Visitor;
use PhpParser\NodeTraverser;

class Flattener extends Worker
{

    public function __construct(View\Compilation\Compiler $compiler)
    {
        $this->node_visitor = new Flattener_Visitor($compiler);

        $this->node_traverser = new NodeTraverser();
        $this->node_traverser->addVisitor($this->node_visitor);
    }

    public function flatten($ast)
    {
        return $this->node_traverser->traverse($ast);
    }

    public function get_node_visitor(): Flattener_Visitor
    {
        return $this->node_visitor;
    }

}