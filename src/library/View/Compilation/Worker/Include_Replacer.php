<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Worker;

use Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors\Include_Replacer_Visitor;
use PhpParser\NodeTraverser;

class Include_Replacer extends Worker
{

    public function __construct()
    {
        $this->node_visitor = new Include_Replacer_Visitor();

        $this->node_traverser = new NodeTraverser();
        $this->node_traverser->addVisitor($this->node_visitor);
    }

    public function replace_include($ast)
    {
        return $this->node_traverser->traverse($ast);
    }

    public function get_node_visitor(): Include_Replacer_Visitor
    {
        return $this->node_visitor;
    }

}