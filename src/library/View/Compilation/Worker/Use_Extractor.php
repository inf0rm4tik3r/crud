<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Worker;

use Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors\Use_Extractor_Visitor;
use PhpParser\NodeTraverser;

class Use_Extractor extends Worker
{

    public function __construct()
    {
        $this->node_visitor = new Use_Extractor_Visitor();

        $this->node_traverser = new NodeTraverser();
        $this->node_traverser->addVisitor($this->node_visitor);
    }

    public function extract_use_statements($ast)
    {
        return $this->node_traverser->traverse($ast);
    }

    public function get_node_visitor(): Use_Extractor_Visitor
    {
        return $this->node_visitor;
    }

}