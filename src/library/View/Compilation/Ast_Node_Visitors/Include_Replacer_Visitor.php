<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors;

use PhpParser\Node;
use PhpParser\NodeVisitorAbstract;

/**
 * Class Include_Replacer_Visitor
 * @package Lukaspotthast\Crud\View\Compilation\Ast_Node_Traversers
 */
class Include_Replacer_Visitor extends NodeVisitorAbstract
{

    /**
     * @var array
     */
    private $view_ast = [];

    public function leaveNode(Node $node)
    {
        if ( $node instanceof Node\Stmt\InlineHTML and $node->value === 'INCLUDE' )
        {
            return $this->view_ast;
        }
        return null;
    }

    public function set_view_ast(array $view_ast)
    {
        $this->view_ast = $view_ast;
    }

}