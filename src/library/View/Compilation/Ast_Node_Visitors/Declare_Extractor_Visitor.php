<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors;

use Lukaspotthast\Support\Arr;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

/**
 * Class Declare_Extractor_Visitor
 * @package Lukaspotthast\Crud\View\Compilation\Ast_Node_Traversers
 */
class Declare_Extractor_Visitor extends NodeVisitorAbstract
{

    /**
     * This array is responsible for holding the extracted declare-statement nodes. <br>
     * They should be fetched using the {@link get_declare_statements()}function
     * after this node-visitor ran over an AST.
     *
     * @var array
     *      Array of: {@link Node\Stmt\Declare_}
     */
    private $declare_stmts = [];

    public function beforeTraverse(array $nodes)
    {
        $this->declare_stmts = [];
    }

    /**
     * Looks for Nodes of the type: Node\Stmt\Declare_, stores them into the declare_stmts array and removes them
     * from the AST.
     *
     * @param Node $node
     * @return int|null|Node|Node[]
     */
    public function leaveNode(Node $node)
    {
        if ($node instanceof Node\Stmt\Declare_)
        {
            array_push($this->declare_stmts, $node);
            return NodeTraverser::REMOVE_NODE;
        }
        return null;
    }

    /**
     * Check if two declare-statement nodes are equal.
     *
     * @param Node\Stmt\Declare_ $a
     *      The first declare-statement node to compare.
     *
     * @param Node\Stmt\Declare_ $b
     *      The second declare-statement node to compare.
     *
     * @return bool
     *      TRUE if $a equals $b, FALSE otherwise.
     */
    public function declare_nodes_equal(Node\Stmt\Declare_ $a, Node\Stmt\Declare_ $b): bool
    {
        $a_dec = $a->declares[0];
        $b_dec = $b->declares[0];

        return ( ($a_dec->key === $b_dec->key) and
                 ($a_dec->value->value === $b_dec->value->value) );
    }

    /**
     * Helper function which converts an array of {@link Node\Stmt\Declare_} object to an array of strings. <br>
     * Useful for debugging.
     *
     * @param array $declare_stmts
     * @return array
     */
    public function declare_stmts_to_string_array(array $declare_stmts): array
    {
        $strings = [];
        foreach ( $declare_stmts as /** @var $declare_stmt Node\Stmt\Declare_ */ &$declare_stmt )
        {
            array_push($strings, $this->declare_stmt_to_string($declare_stmt));
        }
        return $strings;
    }

    /**
     * Helper function which creates the string representation of a {@link Node\Stmt\Declare_} object.
     *
     * @param Node\Stmt\Declare_ $declare_stmt
     * @return string
     */
    public function declare_stmt_to_string(Node\Stmt\Declare_ $declare_stmt): string
    {
        $dec = $declare_stmt->declares[0];
        return $dec->key . $dec->value->value;
    }

    /**
     * Returns an array of declare-statement nodes which got extracted during the last run of this node-visitor.
     *
     * @return array
     *      Array of: {@link Node\Stmt\Declare_} objects.
     */
    public function get_declare_statements(): array
    {
        return $this->declare_stmts;
    }

    /**
     * Same as {@link get_declare_statements}, but leaves out duplicates.
     *
     * @return array
     *      Array of: {@link Node\Stmt\Declare_} objects.
     */
    public function get_declare_statements_unique(): array
    {
        return Arr::unique($this->declare_stmts, [$this, 'declare_nodes_equal']);
    }

}