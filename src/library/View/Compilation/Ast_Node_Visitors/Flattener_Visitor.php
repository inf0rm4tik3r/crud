<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors;

use Lukaspotthast\Crud\View;
use PhpParser\Node;
use PhpParser\NodeVisitorAbstract;

/**
 * Class Flattener_Visitor
 * @package Lukaspotthast\Crud\View\Compilation\Ast_Node_Traversers
 */
class Flattener_Visitor extends NodeVisitorAbstract
{

    /**
     * @var View\Compilation\Compiler;
     */
    private $view_compiler;

    /**
     * Stores the name of the view which currently gets processed.<br>
     * This information is necessary, so that the node traverser can check if a certain include statement wants to
     * include itself (recursive view generation). This can not be flattened!
     * @var string
     */
    private $view_filename;

    public function __construct(View\Compilation\Compiler $view_compiler)
    {
        $this->view_compiler = $view_compiler;
    }

    /**
     * @param Node $node
     * @return null|Node
     */
    public function leaveNode(Node $node)
    {
        if ($node instanceof Node\Expr\StaticCall)
        {
            // The node must represent something like: "\Renderer::render_view('view', [])"
            if ( $node->class->getLast() === 'Renderer' and
                 $node->name === 'render_view' and
                 count($node->args) === 2 and
                 $node->args[0]->value instanceof Node\Scalar\String_ and
                 $node->args[1]->value instanceof Node\Expr\Array_ )
            {
                /** @var Node\Scalar\String_ $view */
                $view = $node->args[0]->value;
                /** @var Node\Expr\Array_ $data */
                $data = $node->args[1]->value;

                // The view to render must not equal the currently traversed view.
                // Self recursion can not be flattened! That would lead to an infinite replacement loop!
                if ( $this->view_compiler->get_view_filename($view->value) !== $this->view_filename )
                {
                    // Replace the current node (which resembles the "Renderer::rencer_view" call) with the loader for
                    // the referenced view.
                    return $this->view_compiler->create_include_replacement($view, $data);
                }
            }
        }
        return null;
    }

    public function set_view_filename(string $view_filename)
    {
        $this->view_filename = $view_filename;
    }

}