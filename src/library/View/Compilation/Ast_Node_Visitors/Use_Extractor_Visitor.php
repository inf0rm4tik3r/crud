<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation\Ast_Node_Visitors;

use Lukaspotthast\Crud\Support\Str;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;

/**
 * Class Use_Extractor_Visitor
 * @package Lukaspotthast\Crud\View\Ast_Node_Traversers
 */
class Use_Extractor_Visitor extends NodeVisitorAbstract
{

    /**
     * This array is responsible for holding the extracted use-statement nodes. <br>
     * They should be fetched using the {@link get_use_statements()} function after this node-visitor ran over an AST.
     *
     * @var array
     *      Array of: {@link Node\Stmt\Use_}
     */
    private $use_stmts = [];

    public function beforeTraverse(array $nodes)
    {
        $this->use_stmts = [];
    }

    /**
     * Looks for Nodes of the type: Node\Stmt\Use_, stores them into the use_stmts array and removes them from the AST.
     *
     * @param Node $node
     * @return int|null|Node|Node[]
     */
    public function leaveNode(Node $node)
    {
        if ($node instanceof Node\Stmt\Use_)
        {
            array_push($this->use_stmts, $node);
            return NodeTraverser::REMOVE_NODE;
        }
        return null;
    }

    /**
     * Check if two use-statement nodes are equal. <br>
     * They are if there aliases and all there name parts are different. <br>
     * Each use-statement name is composed of many strings separated by '\'. For example 'foo\bar\namespace' would
     * result in the following parts array: ['foo', 'bar', 'namespace']
     *
     * @param Node\Stmt\Use_ $a
     *      The first use-statement node to compare.
     *
     * @param Node\Stmt\Use_ $b
     *      The second use-statement node to compare.
     *
     * @return bool
     *      TRUE if $a equals $b, FALSE otherwise.
     */
    private function use_nodes_equal(Node\Stmt\Use_ $a, Node\Stmt\Use_ $b): bool
    {
        // They should not be considered equal if there aliases are different.
        if ( $a->uses[0]->alias !== $b->uses[0]->alias )
        {
            return false;
        }

        $a_parts = $a->uses[0]->name->parts;
        $b_parts = $b->uses[0]->name->parts;

        // The amount of parts of the names of a and b must match in order for them to be equal.
        if ( count($a_parts) !== count($b_parts) )
        {
            return false;
        }

        // If the names of a and b are of the same part-length, each indexed part of a must match the part of b with
        // the same index.
        for ( $i = 0; $i < count($a_parts); $i++ )
        {
            if ( $a_parts[$i] !== $b_parts[$i] )
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Helper function which converts an array of {@link Node\Stmt\Use_} object to an array of strings. <br>
     * Useful for debugging.
     *
     * @param array $use_stmts
     * @return array
     */
    public function use_stmts_to_string_array(array $use_stmts): array
    {
        $strings = [];
        foreach ( $use_stmts as /** @var $use_stmt Node\Stmt\Use_ */ &$use_stmt )
        {
            array_push($strings, $this->use_stmt_to_string($use_stmt));
        }
        return $strings;
    }

    /**
     * Helper function which creates the string representation of a {@link Node\Stmt\Use_} object.
     *
     * @param Node\Stmt\Use_ $use_stmt
     * @return string
     */
    public function use_stmt_to_string(Node\Stmt\Use_ $use_stmt): string
    {
        $use = '';
        foreach ( $use_stmt->uses[0]->name->parts as &$part )
        {
            $use .= $part.'\\';
        }
        return Str::remove_last($use, 1);
    }

    /**
     * Returns an array of use-statement nodes which got extracted during the last run of this node-visitor.
     *
     * @return array
     *      Array of: {@link Node\Stmt\Use_} objects.
     */
    public function get_use_statements(): array
    {
        return $this->use_stmts;
    }

    /**
     * Same as {@link get_use_statements}, but leaves out duplicates.
     *
     * @return array
     *      Array of: {@link Node\Stmt\Use_} objects.
     */
    public function get_use_statements_unique(): array
    {
        $unique = [];
        if ( !empty($this->use_stmts) )
        {
            foreach ( $this->use_stmts as &$use_stmt )
            {
                $needed = true;
                foreach ( $unique as &$in )
                {
                    if ( $this->use_nodes_equal($use_stmt, $in) )
                    {
                        $needed = false;
                    }
                }
                if ( $needed )
                {
                    array_push($unique, $use_stmt);
                }
            }
        }
        return $unique;
    }

}