<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\View\Compilation;

use Lukaspotthast\Crud\View\Compilation\Worker\Declare_Extractor;
use Lukaspotthast\Crud\View\Compilation\Worker\Flattener;
use Lukaspotthast\Crud\View\Compilation\Worker\Include_Replacer;
use Lukaspotthast\Crud\View\Compilation\Worker\Use_Extractor;
use Lukaspotthast\Support\Filesystem\Filesystem;
use Lukaspotthast\Support\Filesystem\Path;
use PhpParser\ErrorHandler\Collecting;
use PhpParser\Node;
use PhpParser\NodeAbstract;
use PhpParser\Parser;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter;
use RuntimeException;
use Throwable;

/**
 * Class Compiler
 * @package Lukaspotthast\Crud\View\Compilation
 */
class Compiler
{

    /**
     * Set this constant to false to bypass the view compilation. Every view will then be rendered as is.
     * @var bool
     */
    private const COMPILE_VIEWS = true;

    /**
     * The compiler will always (re)compile a view if this flag is set to true.
     * @var bool
     */
    private const FORCE_COMPILATION = false;

    /**
     * Path to the root folder in which views reside. Rendering can be accomplished by just specifying a path relative
     * to this folder.
     * @var string
     */
    private $views_path;

    /**
     * Path to the root folder in which the compiled views will be stored.
     * @var string
     */
    private $compiled_views_path;

    /**
     * @var bool
     */
    private $parser_initialized;

    /**
     * The parser object used to parse PHP code to an AST.
     * @var Parser\Php7
     */
    private $parser;

    /**
     * @var Flattener
     */
    private $flattener;

    /**
     * @var Use_Extractor
     */
    private $use_extractor;

    /**
     * @var Declare_Extractor;
     */
    private $declare_extractor;

    /**
     * @var Include_Replacer
     */
    private $include_replacer;

    /**
     * Printer object used to generate code from the ASTs.
     * @var PrettyPrinter\Standard
     */
    private $pretty_printer;

    /**
     * Rendering_Engine constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Statically initializes the rendering engine.
     */
    private function init(): void
    {
        $this->init_view_paths();
        $this->parser_initialized = false;
    }

    /**
     * Initializes the path variables which are necessary for the compilation and rendering process.
     */
    private function init_view_paths(): void
    {
        // __DIR__ points to: '.../crud/src/library/View/Compilation'
        // We want:           '.../crud/src/resources/views'
        $this->views_path          = Path::convert_slashes(Path::normalize(__DIR__ . '/../../../resources/views'));
        $this->compiled_views_path = Path::convert_slashes(Path::normalize(__DIR__ . '/../../../storage/views/compiled'));

        // TODO: simplify
        // Remove leading slashes if paths contain a ':' character.
        if ( strpos($this->views_path, ':') !== false )
        {
            $this->views_path = Path::remove_leading_slashes($this->views_path);
        }
        if ( strpos($this->compiled_views_path, ':') !== false )
        {
            $this->compiled_views_path = Path::remove_leading_slashes($this->compiled_views_path);
        }

        Filesystem::directory_must_exist($this->views_path);
        Filesystem::directory_must_exist($this->compiled_views_path);
    }

    private function init_parser(): void
    {
        // Only initialize the parsing components if they are not already initialized.
        if ( !$this->parser_initialized )
        {
            // Strict PHP 7 parser!
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->parser         = (new ParserFactory())->create(ParserFactory::ONLY_PHP7);
            $this->pretty_printer = new PrettyPrinter\Standard();

            $this->flattener         = new Flattener($this);
            $this->use_extractor     = new Use_Extractor();
            $this->declare_extractor = new Declare_Extractor();
            $this->include_replacer  = new Include_Replacer();

            $this->parser_initialized = true;
        }
    }

    /**
     * Renders the view specified by the first parameter with all entries from the second parameter.
     *
     * @param string $view_name
     *      View to render. Must be a relative path specifying a view in 'crud/src/resources/views'.
     *
     * @param array  $data
     *      Data which gets passed into the view. Access it by using the array keys you specified.
     *
     * @return string
     *      The rendered view.
     *
     * @throws Throwable
     */
    public function render_view(string $view_name, array $data): string
    {
        $view_path = $this->get_view_filename($view_name);

        // Fetch the desired view directly if the view-compilation is turned off.
        if ( !self::COMPILE_VIEWS )
        {
            return $this->fetch($view_path, $data);
        }

        $compiled_view_path = $this->get_compiled_view_filename($view_name);

        // Compile the requested view if not already compiled and up to date.
        if ( $this->needs_compilation($view_path, $compiled_view_path) )
        {
            $this->compile_view($view_path, $compiled_view_path);
        }

        return $this->fetch($compiled_view_path, $data);
    }

    /**
     * Checks if the specified view needs to be recompiled.
     *
     * @param string $view_path
     * @param string $compiled_view_path
     * @return bool
     */
    private function needs_compilation(string $view_path, string $compiled_view_path): bool
    {
        return
            self::FORCE_COMPILATION or
            $this->compilation_missing($compiled_view_path) or
            $this->compilation_expired($view_path, $compiled_view_path);
    }

    private function compilation_missing(string $compiled_view_path): bool
    {
        return !Filesystem::file_exists($compiled_view_path);
    }

    private function compilation_expired(string $view_path, string $compiled_view_path): bool
    {
        return Filesystem::last_modification($view_path) > Filesystem::last_modification($compiled_view_path);
    }

    /**
     * @param string $view_path
     * @param array  $data
     * @return string
     * @throws Throwable
     */
    private function fetch(string $view_path, array $data = []): string
    {
        try
        {
            ob_start();
            $this->protected_include_scope($view_path, $data);
            $output = ob_get_clean();
        }
            // If we caught an exception, we'll silently flush the output buffer so that no partially rendered views
            // get thrown out to the client and confuse the user with junk.
        catch ( Throwable $e ) // PHP 7+
        {
            ob_end_clean();
            throw $e;
        }

        return $output;
    }

    private function protected_include_scope(): void
    {
        extract(func_get_args()[1], EXTR_SKIP);

        /** @noinspection PhpIncludeInspection */
        include func_get_args()[0];
    }

    /**
     * Compiles the file specified through $view_path and stores the result in $compiled_view_path.
     *
     * @param string $view_filename
     *      Path to the view file which should be compiled. For example: '/views/foo/bar/view.php'
     *
     * @param string $compiled_view_filename
     *      Path to the compiled view file. For example: '/compiled/foo/bar/view.php'
     */
    public function compile_view(string $view_filename, string $compiled_view_filename): void
    {
        //Debug::info('Starting compilation of: '.$view_filename);

        // The parsing functionality is needed for the compilation process.
        $this->init_parser();

        $view = Filesystem::read($view_filename);

        // 1) Read the view and parse it into an abstract syntax tree (AST).
        $ast = $this->parser->parse($view);

        // 2) Traverse the AST with the "flattening_traverser".
        //    This will replace any "Renderer::render_view" call with the appropriate view code.
        $this->flattener->get_node_visitor()->set_view_filename($view_filename);
        $ast = $this->flattener->flatten($ast);

        // 3) Extract all use-statements from the AST.
        //    They must be filtered, to be unique, and then be pushed to the front of the AST.
        // Why?: use-statements are only allowed at the beginning of a PHP file!
        $ast = $this->use_extractor->extract_use_statements($ast);

        // 4) Put all necessary use-statements in front of the AST, which currently has no use-statements at all.
        $ast = array_merge($this->use_extractor->get_node_visitor()->get_use_statements_unique(), $ast);

        // 5) Extract all declare-statements from the AST.
        $ast = $this->declare_extractor->extract_declare_statements($ast);

        // 6) Put all necessary declare-statements in front of the AST.
        $ast = array_merge($this->declare_extractor->get_node_visitor()->get_declare_statements_unique(), $ast);

        // 7) Print and write the compiled view.
        $compiled = $this->pretty_printer->prettyPrintFile($ast);
        Filesystem::write($compiled_view_filename, $compiled);

        //Debug::info('Finished compilation of: '.$view_filename);
        //Debug::info('Writing compiled view: '.$compiled_view_filename);
    }

    public function create_include_replacement(Node\Scalar\String_ $view_name_to_include, Node\Expr\Array_ $data): NodeAbstract
    {
        $view     = $this->get_compiled_view($view_name_to_include->value);
        $view_ast = $this->parser->parse($view);

        $replacer_code = '
            <?php
                call_user_func(
                    function()
                    {
                        $args = func_get_args()[0];
                        extract($args);
                        ob_start();
                        ?>INCLUDE<?php
                        return ob_get_clean();
                    },
                    ' . $this->pretty_printer->prettyPrintExpr($data) . '
                );
            ?>
        ';

        // Create an AST from the $replacer_code variable.
        $replacer_ast = $this->parser->parse($replacer_code, new Collecting());

        // The include replacer will replace the "INCLUDE"-string-element in the replacer AST with the AST of the
        // compiled view.
        $this->include_replacer->get_node_visitor()->set_view_ast($view_ast);
        $replacer_ast = $this->include_replacer->replace_include($replacer_ast);

        // TODO: why?
        unset($replacer_ast[0]);
        unset($replacer_ast[2]);
        $replacer_ast = array_values($replacer_ast); // reindexing: 0, 1, ...

        // $replacer_ast should now be an array, which only contains one "FuncCall" node.
        // This node should be returned.
        if ( !(count($replacer_ast) === 1 and $replacer_ast[0] instanceof Node\Expr\FuncCall) )
        {
            throw new RuntimeException(
                __CLASS__ . ': ' . __FUNCTION__ . '(): Include replacer must only contain a "FuncCall" node!'
            );
        }

        $replacement = $replacer_ast[0];

        // TODO: Maybe: Store loaded views in cache. Reload them if accessed more the once.
        //Debug::info('Loaded replacement: '.$view_name_to_include->value);
        //Debug::info($replacement);

        return $replacement;
    }

    private function get_compiled_view(string $view_name): string
    {
        $view_path          = $this->get_view_filename($view_name);
        $compiled_view_path = $this->get_compiled_view_filename($view_name);

        if ( !Filesystem::file_exists($compiled_view_path) )
        {
            $this->compile_view($view_path, $compiled_view_path);
        }

        return Filesystem::read($compiled_view_path);
    }

    private function fix_view_name(string $view_name): string
    {
        // Replace possible usage of dots as folder separators.
        return str_replace('.', '/', $view_name);
    }

    public function get_view_filename(string $view_name): string
    {
        return $this->views_path . '/' . $this->fix_view_name($view_name) . '.php';
    }

    public function get_compiled_view_filename(string $view_name): string
    {
        return $this->compiled_views_path . '/' . $this->fix_view_name($view_name) . '.php';
    }

}