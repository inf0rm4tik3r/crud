<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Illuminate\Http\UploadedFile;
use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Behaviour\Behaviour_Controller;
use Lukaspotthast\Crud\Behaviour\Behaviours;
use Lukaspotthast\Crud\Callback\Callback_Controller;
use Lukaspotthast\Crud\Callback\Callbacks_Trait;
use Lukaspotthast\Crud\Condition\Condition;
use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Database\Crud_Joins;
use Lukaspotthast\Crud\Database\Crud_Relations;
use Lukaspotthast\Crud\Database\DBHelper;
use Lukaspotthast\Crud\Database\Has_Database_Connection;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Database\Table;
use Lukaspotthast\Crud\Database\Table_Set;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\Elements\Crud_Custom_Elements;
use Lukaspotthast\Crud\Elements\Crud_Nav_Buttons;
use Lukaspotthast\Crud\Field\Crud_Field_Types;
use Lukaspotthast\Crud\Field\Crud_Fields;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Info\Crud_Column_Info;
use Lukaspotthast\Crud\Info\Crud_Field_Info;
use Lukaspotthast\Crud\Inlineedit\Crud_Field_Inlining;
use Lukaspotthast\Crud\Label\Crud_Label_Visibility;
use Lukaspotthast\Crud\Label\Crud_Label_Wrapping;
use Lukaspotthast\Crud\Label\Label_Controller;
use Lukaspotthast\Crud\Label\Labels_Trait;
use Lukaspotthast\Crud\Language\Languages;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Log\Log;
use Lukaspotthast\Crud\Message\Message_Controller;
use Lukaspotthast\Crud\Message\Messages_Trait;
use Lukaspotthast\Crud\Nested\Crud_Nested_Tables;
use Lukaspotthast\Crud\Pagination\Crud_Result_Limits;
use Lukaspotthast\Crud\Pagination\Paginator;
use Lukaspotthast\Crud\Pattern\Crud_Column_Pattern;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Rendering\Renderer_Trait;
use Lukaspotthast\Crud\Search\Crud_Search;
use Lukaspotthast\Crud\Search\Crud_Search_Tabs;
use Lukaspotthast\Crud\Search\Search_Tab;
use Lukaspotthast\Crud\Security\Crud_Permissions;
use Lukaspotthast\Crud\Security\Crud_Verification_Key;
use Lukaspotthast\Crud\Session\Session;
use Lukaspotthast\Crud\Sort\Crud_Order_By;
use Lukaspotthast\Crud\Storage\Storage;
use Lukaspotthast\Crud\Tooltip\Crud_Column_Tooltip;
use Lukaspotthast\Crud\Tooltip\Field_Tooltip_Collection;
use Lukaspotthast\Crud\Tooltip\Field_Tooltips_Trait;
use Lukaspotthast\Crud\Validation\Crud_Validation;
use Lukaspotthast\Support\Datetime_Formats;
use Lukaspotthast\Support\Error\Throws_Errors_Interface;
use Lukaspotthast\Support\Error\Throws_Errors_Trait;
use Lukaspotthast\Support\Serialization;
use Lukaspotthast\Support\Sort;
use Lukaspotthast\Support\Str;
use stdClass;

/**
 * Class Crud
 * @package Lukaspotthast\Crud\Crud
 */
class Crud implements Throws_Errors_Interface
{
    private static $framework = Frameworks::PHP;
    private static $initialized = false;

    /**
     * One of the constants defined in "Languages"
     * @var string
     */
    private static $language = Languages::EN;

    private const ROOT_PATH = __DIR__ . '/..'; // Go up to 'src'.
    private const CONFIG_PATH = self::ROOT_PATH.'/config/';
    private const LIBRARY_PATH = self::ROOT_PATH.'/library/';
    private const PUBLIC_PATH = self::ROOT_PATH.'/public/';
    private const RESOURCES_PATH = self::ROOT_PATH.'/resources/';
    private const LANG_PATH = self::ROOT_PATH.'/resources/lang/';
    private const VIEWS_PATH = self::ROOT_PATH.'/resources/views/';
    private const STORAGE_PATH = self::ROOT_PATH.'/storage/';


    public const PERFORM_TYPECHECKS = true;

    private static $asset_path = null;

    public static function set_asset_path(string $asset_path): void
    {
        self::$asset_path = $asset_path;
    }

    private static function connection_protocol(): string
    {
        return isset($_SERVER['HTTPS']) ?
            (($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1 || $_SERVER['SERVER_PORT'] === 443) ? 'https' : 'http') :
            (($_SERVER['SERVER_PORT'] === 443) ? 'https' : 'http');
    }
    private static function connection_is_secure(): bool
    {
        return self::connection_protocol() === 'https';
    }

    public static function get_asset_path(): string
    {
        if ( self::$asset_path === null )
        {
            return strtolower(explode('/', self::connection_protocol())[0]).'://'.$_SERVER['HTTP_HOST'].'/';
        }
        else
        {
            return self::$asset_path;
        }
    }


    use Throws_Errors_Trait;

    use Has_Database_Connection;

    use Crud_Joins;
    use Crud_Verification_Key;

    const SUPPORTED_FIELD_TYPES = [
        'varchar' => ['textarea', 'texteditor', 'code', 'color', 'select', 'multiselect', 'password', 'image'],
        'tinytext' => ['textarea', 'texteditor', 'code', 'color', 'select', 'multiselect', 'password', 'signature'],
        'text' => ['textarea', 'texteditor', 'code', 'color', 'select', 'multiselect', 'password', 'signature'],
        'mediumtext' => ['textarea', 'texteditor', 'code', 'color', 'select', 'multiselect', 'password', 'signature'],
        'longtext' => ['textarea', 'texteditor', 'code', 'color', 'select', 'multiselect', 'password', 'signature'],
        'enum' => [],
        'smallint' => ['select'],
        'mediumint' => ['select'],
        'int' => ['select'],
        'bigint' => ['select'],
        'tinyblob' => ['file', 'image'],
        'mediumblob' => ['file', 'image'],
        'blob' => ['file', 'multifile', 'image'],
        'longblob' => ['file', 'multifile', 'image'],
        'subselect' => ['password', 'texteditor']
    ];
    use Crud_Field_Types;

    use Datetime_Formats;
    use Crud_Order_By;
    use Crud_Result_Limits;
    /* Label visibility */
    public const SHOWN = 493;
    public const HIDDEN = 494;
    public const NONE = 495;
    use Crud_Label_Visibility;
    /* Label wrapping */
    public const WRAP_DYNAMIC = 162;
    public const WRAP_ALWAYS = 163;
    public const WRAP_NEVER = 164;
    use Crud_Label_Wrapping;
    /* Tooltips */
    use Field_Tooltips_Trait;
    use Crud_Column_Tooltip;
    /* Info */
    use Crud_Field_Info;
    use Crud_Column_Info;
    use Crud_Inline_Edit;
    use Crud_Nav_Buttons;
    use Crud_Validation;
    use Crud_Soft_Delete;
    use Messages_Trait;
    use Crud_Nested_Tables;
    use Crud_Custom_Elements;
    use Crud_Column_Pattern;
    use Crud_Search_Tabs;
    use Crud_Fields;
    use Crud_Default_Values;

    /* Permissions */
    const PERMISSION_SUPPORTED_ELEMENTS = [
        'list', 'create', 'view', 'edit', 'save', 'delete', 'structure',
        'print', 'export', 'search', 'order_by'
    ];
    use Crud_Permissions;

    /* Visibility */
    const VISIBILITY_SUPPORTED_ELEMENTS = [
        'header', 'title', 'entry_info',
        'save_btn', 'save_and_return_btn', 'save_and_new_btn', 'delete_btn',
        'list_btn',
        'limits', 'pagination'
    ];
    use Crud_Visibility;

    use Labels_Trait;
    use Crud_Field_Inlining;
    use Crud_Relations;

    /* NEW TRAIT USAGE */
    use Behaviours;
    use Callbacks_Trait;
    use Renderer_Trait;


    /**
     * @var Assert
     */
    private $assert;

    /** @var Storage */
    private $storage;

    private $debug_mode = true;

    private const TABLE_MODE = 0;
    private const DATA_MODE = 1;
    private $mode = self::TABLE_MODE;

    /**
     * Used to store the instance in the session.
     * @var string
     */
    private $instance_name;

    /**
     * The table set.
     * @var Table_Set
     */
    private $table_set;


    public const AVAILABLE_TASKS = ['list', 'create', 'edit', 'view', 'structure'];
    /**
     * States the current task the instance is in.
     * @var string
     */
    private $task;
    /** @var boolean */
    private $in_list = false;
    /** @var boolean */
    private $in_create = false;
    /** @var boolean */
    private $in_edit = false;
    /** @var boolean */
    private $in_view = false;
    /** @var boolean */
    private $in_structure = false;

    private $current_entry_id; // Needs to be set before the edit view gets rendered.

    /** @var string */
    private $requester;


    private $use_asynchronous_loading;
    private $is_loaded;

    private $id_column_name;

    /**
     * @var Paginator
     */
    private $paginator;

    private $where_query; // Condition
    /**
     * @var Crud_Search
     */
    private $search;

    private $column_visibility; // array
    private $column_arrangement; // array
    private $column_width; // array(col => width)

    private $field_visibility; // array
    private $field_arrangement; // array

    private $render_success_message_after_save; // bool

    /**
     * @var array
     */
    private $custom_wrapper_classes;
    /**
     * Crud constructor.
     *
     * @param string $instance_name
     */
    private function __construct(string $instance_name = '')
    {
        // Each instance needs a UNIQUE name, so that we can access it in later ajax calls and
        // are able to reference the JavaScript object of this instance in JS code.
        $this->set_instance_name($instance_name);

        // After construction, each property field has its technical zero value assigned
        // (null, 0, false, etc.)
        // To be able to use this instance, all properties must be initialized.
        $this->prepare(true, true, true);

        // The calculation of verification keys is based on an instances name.
        // After the name got updated, the key has to be regenerated.
        $this->regenerate_verification_key();
    }

    /**
     * The framework variable tells the CRUD system how to operate. This includes the handling of session data,
     * configurations, logging and debugging. <br>
     * Call this function with one of the constants defined in "Lukaspotthast\Crud\Frameworks\Frameworks".
     * Use Frameworks::PHP if you do not use any of the other frameworks. <br>
     * Do not forget to call the Crud::init() function after defining the framework.
     *
     * @param string $framework
     *      The framework to operate with. One of Frameworks::PHP, Frameworks::LARAVEL, ...
     */
    public static function set_framework(string $framework): void
    {
        self::$framework = $framework;
    }

    /**
     * The framework we are operating with / on. One of the constants defined in
     * "Lukaspotthast\Crud\Frameworks\Frameworks".
     *
     * @return string
     */
    public static function get_framework(): string
    {
        return self::$framework;
    }

    /**
     * Statically initializes the session, configuration, debugging and logging capabilities.
     */
    public static function init(): void
    {
        Session::init();
        Config::init();
        Translator::init();
        Debug::init();
        Log::init();

        self::$initialized = true;
    }

    /**
     * This is the main function which is to be used to obtain an instance of this crud system.<br>
     * It is highly recommended to call this method with a unique name for the first parameter.<br>
     * (A name that will not be used by any other crud instance on your site.)<br>
     * // TODO: explain why...
     * @param string $instance_name
     *      OPTIONAL: The name of the instance. (RECOMMENDED TO SET!)<br>
     *      If no name gets specified a unique name will be generated.
     * @param bool $with_reset
     *      OPTIONAL: If set true, the returned instance will be resetted to a state in which it must be reinitialized.
     * @param bool $with_hard_reset
     *      OPTIONAL: If set true, the returned instance will be resetted to its core default. Persisting state like
     *      the current view, current page, etc. will be lost as well.
     * @param bool $with_per_request_reset
     *      OPTIONAL: If set true, the returned instance will be resetted for a new request.
     * @return Crud instance.
     */
    public static function get_instance(
        string $instance_name = '',
        bool $with_reset = true,
        bool $with_hard_reset = false,
        bool $with_per_request_reset = true): Crud
    {
        // Force initialization if necessary.
        if ( !self::$initialized )
        {
            self::init();
        }

        // If no instance name was given,
        // return a new instance name with a unique identifier.
        if ( !(isset($instance_name) and is_string($instance_name) and !empty($instance_name)) )
        {
            return new self();
        }

        // Try to load the requested instance from the session.
        $crud_storage = new Storage(null);
        $instance = $crud_storage->restore_instance($instance_name);

        // The instance must be prepared for a new request if found.
        if ( isset($instance) )
        {
            $instance->prepare($with_reset, $with_hard_reset, $with_per_request_reset);
            return $instance;
        }

        // An instance name was specified, but no instance was found in the session.
        // A new instance mus be created.
        return new self($instance_name);
    }

    /**
     * Prepares this instance so that it can be used again.
     *
     * @param bool $reset
     * @param bool $hard_reset
     * @param bool $per_request_reset
     */
    private function prepare(bool $reset, bool $hard_reset, bool $per_request_reset) : void
    {
        $this->reset_errors();
        $this->set_error_message_prefix('CRUD: ');

        if ($hard_reset)
        {
            $this->hard_reset();
        }
        if ($reset)
        {
            $this->reset();
        }
        if ($per_request_reset)
        {
            $this->per_request_reset();
        }
    }

    /**
     * Requesting an instance through the {@link Crud::get_instance()} function will only invoke this function if the
     * instance object gets created for the first time. Should the instance has been restored from the session, this
     * function will not be called! <br>
     * <br>
     * THEREFORE: Use this function to initialize data which only needs to get initialized once.
     */
    private function hard_reset(): void
    {
        $this->renderer = new Renderer($this);

        $this->set_db_connection('crud_'.$this->get_instance_name(), [
            'host' => 'crud.lukas-potthast.de',
            'database' => 'd0279aa8',
            'username' => 'd0279aa8',
            'password' => 'eD5e3GapNhQq7AYG',
        ]);

        $this->storage = new Storage($this);

        $this->assert = new Assert($this, $this->db_connection);

        $this->set_task('list');
        $this->current_entry_id = null; // TODO: should use the next line.
        //$this->set_current_entry_id(null);

        // Reset table assumptions.
        $this->reset_id_column_name();

        // Reset the search by creating a new search object.
        $this->search = new Crud_Search($this);

        // Create (reset) the paginator. Starts at page "1".
        $this->paginator = new Paginator();

        // Reset limit / pagination options.
        $this->hard_reset_result_limits();

        // Reset the current order_by selection.
        $this->hard_reset_order_by();

        /* Initializing traits */

        $this->label_controller = new Label_Controller();
        $this->message_controller = new Message_Controller();

        $this->behaviour_controller = new Behaviour_Controller($this);
        $this->callback_controller = new Callback_Controller($this);
    }

    /**
     * This function typically gets called if an instance gets requested through the {@link Crud::get_instance()}
     * function, which happens if a route initializes the instance for display. <br>
     * <br>
     * It should be used to reset this instance to a state from which it can get reinitialized. <br>
     * THEREFORE: Data which should persist through page reloads must not be deleted here.
     * <br>
     * Deletion of persisting data should only happen in the {@link hard_reset()} function!
     */
    private function reset(): void
    {
        $this->table_set = new Table_Set($this);

        // Reset permissions and visibility.
        // Near the top, so that other reset methods can query the permission and visibility data.
        $this->reset_permissions();
        $this->reset_visibility();

        $this->reset_label_visibility();
        $this->reset_label_wrapping();

        // Reset the requester.
        $this->requester = 'main';

        // Reset the where query.
        $this->where_query = new Condition($this);

        // Reset search tabs.
        $this->reset_search_tabs();

        // Reset asynchronous loading variables.
        $this->use_asynchronous_loading = Config::get('crud.use_asynchronous_loading', false);
        $this->is_loaded = false;

        // Reset inlineedit.
        $this->reset_inline_edit();

        // Reset data manipulated by columns()
        $this->column_visibility = array();
        $this->column_arrangement = array();

        // Reset other column specific data.
        $this->reset_column_pattern();
        $this->column_width = array(
            $this->id_column_name => '2em'
        );

        $this->field_visibility = array();
        $this->field_arrangement = array();

        $this->reset_field_layout();

        $this->reset_field_inlining();

        // Reset relations.
        $this->reset_relations();

        // Reset field types.
        $this->reset_field_types();

        // Reset nested tables.
        $this->reset_nested_tables();

        // Tooltips
        $this->field_tooltip_collection = new Field_Tooltip_Collection($this);
        $this->reset_column_tooltips();

        // Reset field/column info.
        $this->reset_field_info();
        $this->reset_column_info();

        // Reset custom buttons.
        $this->reset_nav_buttons();

        // Reset validation rulesets.
        $this->reset_validation();

        // Reset definition of custom elements.
        $this->reset_custom_elements();

        // Reset soft delete state.
        $this->reset_soft_delete();

        // Reset datetime formats.
        $this->reset_datetime_formats();

        $this->set_render_success_message_after_save(Config::get('crud.render_success_message_after_save'));

        $this->custom_wrapper_classes = [];

        /* NEW RESETS */
        $this->label_controller->reset();

        $this->behaviour_controller->reset();
        $this->callback_controller->reset();

    }

    /**
     * This function should be called whenever an instance gets requested in an AJAX-POST-requests but also
     * if if gets created.
     */
    private function per_request_reset()
    {
        // Reset whether or not a verification error occurred.
        $this->reset_verification_error();

        // Reset messages.
        $this->message_controller->reset();
    }

    public function save_ajax(array $field_data, string $task_from, string $task_to, ?string $entry_id): void
    {
        $id = isset($entry_id) ? $entry_id : $this->current_entry_id;

        $postdata = $field_data; // array-copy

        /*
         * PERFORM 'beforeinsert' or 'beforeupdate' callbacks:
         * $postdata contains ONLY information for known columns!
         */
        switch ($task_from)
        {
            case 'list':
                // Perform no tasks.
                break;

            case 'create':
                // Every beforeinsert callback can abort the insert request by returning 'abort'!
                $result = $this->callback_controller()->standard_callbacks()->perform_beforeinsert_callbacks($postdata);
                break;

            case 'edit':
                // Every beforeupdate callback can abort the update request by returning 'abort'!
                // 'beforeupdate' callback must have access to the entries data currently stored in the database.
                $result = $this->callback_controller()->standard_callbacks()->perform_beforeupdate_callbacks($id, $this->table_set->get_entry($id, true), $postdata);
                break;

            default:
                $args = compact('field_data', 'task_from', 'task_to', 'entry_id');
                $this->error(__FUNCTION__.'(): unknown/unsupported task_from parameter was supplied.', $args);
                return;
        }

        if ( isset($result) )
        {
            if ( $result['abort'] === true )
            {
                $this->message_controller->new(
                    $this,
                    Translator::get('crud.save_aborted.header'),
                    Translator::get('crud.save_aborted.text'),
                    'danger'
                );
                // The save-process gets aborted!
                return;
            }
            else
            {
                $postdata = $result['postdata_merged'];
            }
        }

        /*
         * All fields of type "enum" must have a value assigned in the UPDATE call.
         * Its actually only the INSERT for which this is necessary but we will make sure enums are set no matter what.
         */
        //Debug::info($postdata);
        foreach ( $this->get_table_set()->get_column_names() as $table_name => &$table_column_names )
        {
            foreach ( $table_column_names as &$column_name )
            {
                $col = $this->get_table_set()->get_column($table_name,  $column_name);
                if ( $col->get_type()->get_name() === 'enum' )
                {
                    $in_postdata_cn = array_key_exists($column_name, $postdata);
                    $in_postdata_tcn = array_key_exists($table_name.'.'.$column_name, $postdata);
                    $in_postdata_cn_empty = ($in_postdata_cn and empty($postdata[$column_name]));
                    $in_postdata_tcn_empty = ($in_postdata_tcn and empty($postdata[$table_name.'.'.$column_name]));

                    if ( !($in_postdata_cn or $in_postdata_tcn) or $in_postdata_cn_empty or $in_postdata_tcn_empty )
                    {
                        // Use the current value if we already have an entry.
                        // Otherwise use the default value of the column.
                        $current_entry = $this->get_table_set()->get_entry($entry_id);
                        $current_value = !is_null($current_entry) ? $current_entry->{$column_name}['value'] : null;
                        $value_to_set = !is_null($current_value) ? $current_value : $this->get_default($table_name, $column_name, $this->get_table_set())['default_value'];
                        $postdata[$column_name] = $value_to_set;
                    }
                }

            }
        }
        //Debug::info($postdata);

        // Stores all field names in $table_fields which actually belong to the table and are not automatically
        // incremented.
        $table_fields = array();
        foreach ($this->table_set->get_main_table()->get_schema()->get_table_column_schemata() as &$table_col)
        {
            /** @var Column_Schema $table_col */
            // We do not want to set or modify primary + auto incrementing columns.
            if ( !$table_col->is_auto_incrementing() )
            {
                array_push($table_fields, $table_col->get_name());
            }
        }

        // We now use the calculated $table_fields array to determine which form input should actually be used for
        // creating or updating a record. Every input not present in the $table_fields array gets dropped.
        $tabledata = $postdata; // array-copy
        foreach ($tabledata as $field => &$value)
        {
            // We drop any input supplied by ajax that does not match a column in our database.
            if (!in_array($field, $table_fields))
            {
                unset($tabledata[$field]);
            }
        }

        // If the entry which is currently saved is an entry from a nested table, the connect field has to be filled.
        if ($this->is_nested())
        {
            // TODO: not use get_instance ...
            $parent = $this->get_instance($this->parent_instance_name, false);
            $entry = $parent->get_table_set()->get_entry($parent->get_current_entry_id(), false);
            $connect_field_value = $entry->{$this->parent_connect_field};

            $tabledata[$this->connect_field] = $connect_field_value;
        }

        // Perform actions based on the internal type of the submitted data.
        foreach ($tabledata as $field => &$value)
        {
            $internal_type = $this->get_internal_type($field);

            switch ($internal_type)
            {
                case 'password':
                    if ( empty($value) )
                    {
                        unset($tabledata[$field]);
                    }
                    else
                    {
                        $value = bcrypt($value); // TODO: PHP alternative!
                    }

                    break;

                default:
                    break;
            }
        }

        // Check if files were uploaded.
        foreach ($tabledata as $field => &$value)
        {
            if ( $value instanceof UploadedFile )
            {
                $file = $value;

                $destination = 'library/crud/uploads/';

                $name = $file->getClientOriginalName();
                $filename = str_replace('.', '', microtime(true)).'--'.rand(11111,99999).'--'.$name;

                $file->move($destination, $filename);
                $tabledata[$field] = $filename;
            }
        }

        // The entry can now be created or updated:
        switch ($task_from)
        {
            case 'list': // fallthrough
            case 'edit':
                // Update the main table.
                $this->db()->table($this->table_set->get_main_table()->get_name())
                    ->where('id', '=', $id)
                    ->update($tabledata);

                // The cached data fur $id must be invalidated.
                $this->table_set->invalidate_entry_cache($id);

                $this->callback_controller()->standard_callbacks()->perform_afterupdate_callbacks($id, $this->table_set->get_entry($id, true));
                break;

            case 'create':
                // Add missing data for those table columns who do not have a default and were not present in the input data.
                foreach ($this->table_set->get_main_table()->get_schema()->get_table_column_schemata() as &$col)
                {
                    /** @var Column_Schema $col */
                    if ( !array_key_exists($col->get_name(), $tabledata) and !$col->is_auto_incrementing() )
                    {
                        $tabledata[$col->get_name()] = DBHelper::get_default_for_col_type($col->get_type()->get_name());
                    }
                }

                $new_entry_id = $this->db()->table($this->table_set->get_main_table()->get_name())->insertGetId($tabledata);

                // The cached data fur $id must be invalidated.
                $this->table_set->invalidate_entry_cache($id);

                $new_entry = $this->table_set->get_entry($new_entry_id, true);
                if ($task_to === 'edit')
                {
                    $this->current_entry_id = $new_entry_id;
                }
                $this->callback_controller()->standard_callbacks()->perform_afterinsert_callbacks($new_entry_id, $new_entry);
                break;

            default:
                $args = compact('field_data', 'task_from', 'task_to', 'entry_id');
                $this->error(__FUNCTION__.'(): unknown task_from parameter was supplied.', $args);
                return;
        }

        $this->set_task($task_to);

        // Add a message to this instance displaying a success information for this save process.
        if ( $this->should_render_success_message_after_save() )
        {
            $this->message_controller->new(
                $this,
                '',
                Translator::get('crud.success_message_after_save'),
                'success',
                true,
                true,
                false
            );
        }
    }

    public function delete_ajax(string $id)
    {
        if ($this->task === 'edit')
        {
            // If the id supplied by the ajax call does not match the id of the current edit view (which is stored
            // in this instance), the user must have manipulated his AJAX call to delete an entry he is
            // not currently editing. We strictly prevent that possibility and generate an error!
            if ($id === $this->current_entry_id)
            {
                $this->delete($this->current_entry_id);
            }
            else
            {
                $error_msg = 'You are not allowed to delete an entry ('.$id.') '
                    .'which you are not currently editing ('.$this->current_entry_id.')!';
                $this->error($error_msg);
            }
        }
        else
        {
            // TODO:  perform a check whether the user is allowed to delete this entry
            // match id against array of printed values...
            $this->delete($id);
        }
    }

    private function delete(int $id) : void
    {
        $current_entry = $this->table_set->get_entry($this->current_entry_id, true);

        // PERFORM 'beforeremove' callbacks:
        // Any beforeremove callback can abort the delete request by returning 'abort'!
        // The perform_beforeremove_callbacks() function will return true in case that the deletion should be aborted.
        $abort = $this->callback_controller()->standard_callbacks()->perform_beforeremove_callbacks($id, $current_entry);
        if ($abort === true)
        {
            return;
        }

        // Delete the entry.
        if ( $this->uses_soft_delete() )
        {
            $this->soft_delete_entry($id);
        }
        else
        {
            $this->delete_entry($id);
        }

        $this->callback_controller()->standard_callbacks()->perform_afterremove_callbacks($id, $current_entry);
    }

    /**
     * Performs a database call to delete the entry with the specified id.
     * @param int $id
     */
    private function delete_entry(int $id) : void
    {
        $this->db()->table($this->table_set->get_main_table()->get_table_name())
            ->where($this->get_id_column_name(), '=', $id)
            ->delete();
    }

    /**
     * @return Condition
     */
    public function &get_where_query() : Condition
    {
        return $this->where_query;
    }

    public function acquire_list_data()
    {
        Debug::start_measure('acquire_list_data()');

        // Update the paginator by setting the amount of records which could be retrieved.
        $this->paginator->set_total_result_count( $this->get_list_data_amount() );

        // Start querying the actual list data...
        $query = $this->create_list_data_query();

        Debug::start_measure('SQL-get');
        $data = $query->get();
        Debug::stop_measure('SQL-get');

        // Sort the data by the specified columns and functions. Skip and limit entries appropriately.
        if ( $this->needs_custom_ordering() )
        {
            Debug::start_measure('custom-sort');
            $to_sort = $data->all();
            Sort::sort_by($to_sort, $this->get_order_by_columns(), $this->order_by_functions);
            $to_sort = array_slice(
                $to_sort,
                // Skip
                $this->paginator->get_pagination()->get_first_result() - 1,
                // Take
                $this->paginator->get_pagination()->get_results_per_page()
            );
            $data = collect($to_sort);
            Debug::stop_measure('custom-sort');
        }

        Debug::start_measure('SQL-get-process');
        $data = $this->table_set->process_collection($data);
        Debug::stop_measure('SQL-get-process');

        Debug::stop_measure('acquire_list_data()');

        return $data;
    }


    /**
     * @param bool $without_ordering
     * @param bool $without_limit_and_pagination
     * @return mixed
     */
    private function create_list_data_query($without_ordering = false, $without_limit_and_pagination = false)
    {
        // The query which gets fully build in the next lines of code.
        $query = $this->db()->table($this->table_set->get_main_table()->get_name());

        // Add the programmatically defined where clauses.
        $query = $query->where(function ($sub_query) {
            $this->get_where_query()->add_to($sub_query);
        });

        // Add where clauses defined through active search tabs.
        Search_Tab::add_search_tab_queries($this->search_tabs, $query);

        // Add the user defined (by using the search) where clauses.
        /*
        $query = $query->where(function ($sub_query) {
            $this->get_search_where_query()->add_to($sub_query);
        });
        */

        // Only show not deleted elements if soft-delete is enabled.
        if ( $this->uses_soft_delete() )
        {
            if ( $this->table_set->get_main_table()->get_schema()->get_table_column_schema($this->get_soft_delete_column()) ) // TODO : rework !!!
            {
                if ( !$this->shows_deleted_entries() )
                {
                    $query->where($this->get_soft_delete_column(), '=', 0);
                }
                // else: Show all...
            }
            else
            {
                $this->error(
                    'create_list_data_query(): The soft-delete functionality was activated for this instance, 
                    but a "'.$this->get_soft_delete_column().'" column does not exist in "'.$this->table_set->get_main_table()->get_name().'".');
            }
        }

        // Only apply the ordering, skip and limit to the SQL query if no custom ordering is needed.
        if ( !$this->needs_custom_ordering() )
        {
            // Order the data according to this instances state.
            if ($without_ordering === false)
            {
                foreach ($this->get_order_by_columns() as $column => &$order_type)
                {
                    $query->orderBy($column, $order_type);
                }
            }

            // Limit the amount of results based on the current settings.
            if ($without_limit_and_pagination === false)
            {
                if ( $this->paginator->get_pagination()->get_results_per_page() !== null )
                {
                    $query->skip( $this->paginator->get_pagination()->get_first_result() - 1 );
                    $query->limit( $this->paginator->get_pagination()->get_results_per_page() );
                }
            }
        }

        return $query;
    }

    private function get_list_data_amount()
    {
        Debug::start_measure('get_list_data_amount()');
        $query = $this->create_list_data_query(true, true);
        Debug::start_measure('SQL-count');
        $count = $query->count();
        Debug::stop_measure('SQL-count');
        Debug::stop_measure('get_list_data_amount()');

        return $count;
    }

    /**
     * Replaces all occurrences of {field_tag} with respective data from $entry.
     *
     * @param stdClass $entry The database entry to retrieve data from.
     * @param string $string The string in which placeholders should be replaced.
     * @return string The input string, in which {field_tags} got replaced with their respective data from $entry.
     */
    public function replace_field_tags(string $string, stdClass $entry) : string
    {
        foreach ($entry as $field_name => &$field_info)
        {
            $string = str_replace(
                '{'.$field_name.'}',
                Serialization::maybe_serialize($field_info['value']),
                $string
            );
        }

        return $string;
    }

    public function acquire_ordered_columns()
    {
        $val = $this->acquire_ordered_elements($this->column_visibility, $this->column_arrangement);
        return $val;
    }

    public function acquire_ordered_fields()
    {
        return $this->acquire_ordered_elements($this->field_visibility, $this->field_arrangement);
    }

    private function acquire_ordered_elements($elements_visibility, $elements_arrangement): array
    {
        $ordered_elements = array();

        // Every element belongs into the ordered_element array if only element were hidden...
        $all_false = true;
        $all_true = false;
        foreach ($elements_visibility as &$visibility)
        {
            switch ($visibility)
            {
                case true:
                    $all_false = false;
                    break;
                case false:
                    $all_true = false;
                    break;
            }
        }

        // If only elements were set to be shown, use them in their order.
        if ($all_true)
        {
            foreach ($elements_arrangement as &$element)
            {
                if ($this->element_shown($element, $elements_visibility))
                {
                    $ordered_elements[] = $this->table_set->get_main_table()->get_schema()->get_column_schema($element);
                }
            }
        }
        // If only elements were set to be hidden, use standard elements in their order.
        else
        {
            if ($all_false)
            {
                foreach ($this->table_set->get_main_table()->get_schema()->get_column_schemata() as &$col /** @var Column_Schema $col */)
                {
                    if ( $this->field_shown($col->get_name()) )
                    {
                        $ordered_elements[] = $col;
                    }
                }
            }
            // If we have a mixture, only use the defined elements in their order and hide all others.
            else
            {
                foreach ($elements_arrangement as &$element)
                {
                    if ($this->element_shown($element, $elements_visibility))
                    {
                        $ordered_elements[] = $this->table_set->get_main_table()->get_schema()->get_column_schema($element);
                    }
                }
            }
        }

        return $ordered_elements;
    }


    public function reset_id_column_name() : void
    {
        $id_column_name = Config::get('crud.id_column_name');
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $id_column_name, 'id_column_name', []) )
        {
            $id_column_name = 'id';
        }
        $this->set_id_column_name($id_column_name);
    }

    public function set_id_column_name(string $id_column_name) : void
    {
        $args = compact('id_column_name');

        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $id_column_name, 'id_column_name', $args) )
        {
            return;
        }

        $this->id_column_name = $id_column_name;
    }

    public function get_id_column_name()
    {
        return $this->id_column_name;
    }

    /**
     * Define the table to work with.
     * This function is mandatory and should be invoked directly after the creation of an instance!
     *
     * @param $table_name string The name of the table in the database.
     */
    public function table(string $table_name) : void
    {
        // Check if the specified table really exists.
        if ( !$this->assert->table_exists(__FUNCTION__, $table_name, compact('table_name')) )
        {
            return;
        }

        // Set / load the main table to operate on.
        $this->get_table_set()->set_main_table($table_name);
    }

    /**
     * Define a selection of columns (either as a comma separated string or as an array), and specify if those
     * should be visible or hidden in the listview.
     *
     * @param $columns (mixed) The columns to show / hide.
     * @param $show (boolean) true: columns will be visible, false: columns will be hidden.
     */
    public function columns($columns, $show = true)
    {
        if (isset($columns) and isset($show) and $show === true or $show === false)
        {
            // Column data was supplied as an array of strings.
            $is_array = is_array($columns);
            $is_string = is_string($columns);

            if ($is_array === true or $is_string === true)
            {
                if ($is_string === true)
                {
                    $columns = Str::to_array($columns, ',');
                }
                foreach ($columns as &$col)
                {
                    if (is_string($col) and $this->table_set->get_main_table()->get_schema()->has_column_schema($col))
                    {
                        $this->column_visibility[$col] = $show;
                        $this->column_arrangement[] = $col;
                    } else
                    {
                        $this->error('columns(): Column "'.$col.'" does not exist.');
                    }
                }
            } else
            {
                $this->error('columns(): $columns parameter was neither an array nor a string.');
            }
        } else
        {
            $this->error('columns(): Wrong parameters specified. Only use array/string and true/false.');
        }
    }

    /* State checking */

    // TODO: Move into table_set ?
    public function check_is_table_set() : bool
    {
        if ( !$this->table_set->get_main_table()->is_loaded() )
        {
            $this->error(__FUNCTION__.'(): A table needs to be set. Therefore call the table() function.');
            return false;
        }
        return true;
    }

    /**
     * @param string $new_field_name
     * @param string $type
     * @param string $initial_value
     * @param array $options
     */
    public function subselect(string $new_field_name, string $type = 'subselect', $initial_value = '', array $options = []) : void
    {
        $this->table_set->get_main_table()->get_schema()->add_custom_column(
            Table::create_column($this, $new_field_name, $type, $initial_value, $options),
            $this
        );
    }

    /**
     * Defines if the output of the current instance should be generated in an asynchronous request.<br>
     * This will speed up the loading time of the page in which a crud instance lies, but will (overall)
     * further delay the display of the instances output.
     *
     * @param bool $use_asynchronous_loading Optional:<br>
     *                                       true: Activate asynchronous loading.<br>
     *                                       false: Deactivate asynchronous loading.<br>
     *                                       Defaults to: true
     */
    public function asynchronous(bool $use_asynchronous_loading = true) : void
    {
        $args = compact('use_asynchronous_loading');

        if ( !$this->assert->is_bool(__FUNCTION__, $use_asynchronous_loading, 'use_asynchronous_loading', $args) )
        {
            return;
        }

        $this->use_asynchronous_loading = $use_asynchronous_loading;
    }

    /**
     * Checks if the output of this instance should be loaded asynchronously.
     *
     * @return bool true/false.
     */
    public function is_asynchronous() : bool
    {
        return $this->use_asynchronous_loading;
    }

    /**
     * Defines if the instance is completely loaded onto the site.
     *
     * @param bool $is_loaded true: The rendering of this instance is complete.<br>
     *                        false: The rendering of this instance is incomplete.
     */
    public function set_loaded(bool $is_loaded) : void
    {
        $args = compact('is_loaded');

        if ( !$this->assert->is_bool(__FUNCTION__, $is_loaded, 'is_loaded', $args) )
        {
            return;
        }

        $this->is_loaded = $is_loaded;
    }

    /**
     * Determines whether the instance output is complete.
     *
     * @return bool true/false
     */
    public function is_loaded() : bool
    {
        return $this->is_loaded;
    }

    /**
     * @param string $column
     * @param string $width
     */
    public function column_width(string $column, string $width) : void
    {
        $args = compact('column', 'width');

        // Check if the column got specified correctly.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column, 'column', $args) )
        {
            return;
        }

        // Check if the column really exists.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column, $args) )
        {
            return;
        }

        // Check if the $width got specified correctly.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $width, 'width', $args) )
        {
            return;
        }

        // Check if a width for this column already got defined.
        if ( $this->has_column_width($column) )
        {
            $this->warning(__FUNCTION__.'(): A width for column "'.$column.'" was already defined. Overwriting...');
        }

        // Input is valid. We can add the width to this instance.
        $this->column_width[$column] = $width;
    }

    public function has_column_width($col)
    {
        return array_key_exists($col, $this->column_width);
    }

    public function get_column_width($col)
    {
        return $this->column_width[$col];
    }


    public function get_renderer() : Renderer
    {
        return $this->renderer;
    }




    /*
     *  HELPER FUNCTIONS
     */

    /**
     * Determines whether the specified column should be shown in the list view.
     *
     * @param $column (string) The column name that is to be checked.
     * @return bool true: column should be visible, false: column should be hidden.
     */
    public function column_shown($column)
    {
        return $this->element_shown($column, $this->column_visibility);
    }

    /**
     * Determines whether the specified field should be shown in the create/view/edit view.
     *
     * @param $field (string) The field name that is to be checked.
     * @return bool true: field should be visible, false: field should be hidden.
     */
    public function field_shown($field)
    {
        return $this->element_shown($field, $this->field_visibility);
    }

    private function element_shown($element, $array)
    {
        if (count($array) === 0)
        {
            // Show all elements if no visibility was specified through a function.
            return true;
        } else
        {
            $all_false = true;
            $all_true = false;
            foreach ($array as &$visibility)
            {
                switch ($visibility)
                {
                    case true:
                        $all_false = false;
                        break;
                    case false:
                        $all_true = false;
                        break;
                }
            }

            // Show/hide all elements, tat were not affected by the visibility array.
            if (!array_key_exists($element, $array))
            {
                // If only elements were set to be shown: hide all unaffected elements
                if ($all_true === true)
                {
                    return false;
                } // If only elements were set to be hidden: show all unaffected elements
                else
                {
                    if ($all_false === true)
                    {
                        return true;
                    } // If we have a mixture: hide all unaffected elements
                    else
                    {
                        return false;
                    }
                }

            }

            // If there was visibility data supplied:
            // Only show the column if the elements visibility array states true for this element.
            return array_key_exists($element, $array) ? $array[$element] : false;
        }
    }

    // TODO: Unfinished and untested !!!
    public function plugin(string $plugin, bool $load = false) : ?string
    {
        $args = compact('plugin', 'load');
        if (!(isset($plugin) and is_string($plugin)))
        {
            $this->error(__FUNCTION__.'(): You have to supply one string parameter.', $args);
        }

        $path = __DIR__ . '/plugins/' .$plugin.'.php';
        if (!file_exists($path))
        {
            $this->error(__FUNCTION__.'(): The specified plugin: "'.$plugin.'" is not available.', $args);
            return null;
        }

        if (isset($load) and $load === true)
        {
            require_once $path;
        }

        return $path;
    }

    /*
     * SIMPLE GETTERS AND SETTERS
     */

    public function get_table_set(): Table_Set
    {
        return $this->table_set;
    }

    /**
     * Sets the name with which an instance shall be accessed.<br>
     * Each instance automatically gets its name set when it gets constructed.
     * Calling this method yourself is not necessary.
     * @param string $instance_name The name for the current instance.
     */
    public function set_instance_name(string $instance_name): void
    {
        // We will use the current timestamp as a unique identifier if no name got specified.
        if ( empty($instance_name) )
        {
            $instance_name = 'crud_'.strval( microtime() );
        }

        // Update the name of the current instance.
        $this->instance_name = $instance_name;
    }

    /**
     * @return string The name of the current instance.
     */
    public function get_instance_name(): string
    {
        return $this->instance_name;
    }


    /**
     * @param string $task The new task.
     */
    public function set_task(string $task): void
    {
        $this->task = $task;
        $this->in_list = $task === 'list';
        $this->in_create = $task === 'create';
        $this->in_edit = $task === 'edit';
        $this->in_view = $task === 'view';
        $this->in_structure = $task === 'structure';
    }

    public function get_task(): string
    {
        return $this->task;
    }

    public function in_list(): bool
    {
        return $this->in_list;
    }

    public function in_create(): bool
    {
        return $this->in_create;
    }

    public function in_edit(): bool
    {
        return $this->in_edit;
    }

    public function in_view(): bool
    {
        return $this->in_view;
    }

    public function in_structure(): bool
    {
        return $this->in_structure;
    }

    /**
     * Determines which entry should be current and used for rendering.
     *
     * @param $entry_id int The id of the (from now on) current entry.
     */
    public function set_current_entry_id($entry_id)
    {
        $this->current_entry_id = $entry_id;

        $entry = $this->table_set->get_entry($entry_id, false); // Without processing!

        /*
        // Updates the values of the connect_field where clause of every nested instance to the current entry id,
        // so that we only see entries connected to the currently rendering entry.
        $this->update_nested_table_instances_where_connection( $entry );
        */
    }

    public function get_current_entry_id()
    {
        return $this->current_entry_id;
    }

    public function get_translations()
    {
        $localization = array();

        $localization['browse'] = Translator::get('crud.browse');
        $localization['files_selected'] = Translator::get('crud.files_selected');
        $localization['modal_action_cancel'] = Translator::get('crud.modal_action_cancel');
        $localization['modal_action_save'] = Translator::get('crud.modal_action_save');
        $localization['inlineedit_modal_header'] = Translator::get('crud.inlineedit_modal_header');

        return $localization;
    }

    public function warning(string $msg) : void
    {
        $msg = 'CRUD: WARNING: '.$msg;
        Debug::warning($msg);
        Log::warning($msg);
    }

    public function set_requester(string $requester) : void
    {
        switch ($requester)
        {
            case 'main':
                $this->requester = $requester;
                break;

            case 'ajax':
                $this->requester = $requester;
                break;

            default:
                $this->error(
                    __FUNCTION__.'(): Unknown requester ("'.$requester.'"). Only "main" and "ajax" are supported.'
                );
        }
    }

    public function set_render_success_message_after_save(bool $should_render) : void
    {
        // Check if the should_render argument was properly specified.
        if ( !$this->assert->is_bool(__FUNCTION__, $should_render, 'should_render', compact('should_render')) )
        {
            return;
        }

        $this->render_success_message_after_save = $should_render;
    }

    public function should_render_success_message_after_save() : bool
    {
        return $this->render_success_message_after_save;
    }

    public function get_requester() : string
    {
        return $this->requester;
    }


    public function set_debug_mode(bool $debug_mode) : void
    {
        $this->debug_mode = $debug_mode;
    }

    public function in_debug_mode() : bool
    {
        return $this->debug_mode;
    }

    public function in_table_mode() :  bool
    {
        return $this->mode === self::TABLE_MODE;
    }

    public function in_data_mode() :  bool
    {
        return $this->mode === self::DATA_MODE;
    }

    public function set_wrapper_classes(array $classes) : void
    {
        $args = compact($classes);
        foreach ($classes as &$class)
        {
            if ( $this->assert->is_string(__FUNCTION__, $class, 'class', $args) )
            {
                array_push($this->custom_wrapper_classes, $class);
            }
        }
    }

    public function get_wrapper_classes() : array
    {
        // Return the classes defined in the configuration file if no classes are specified in this instance.
        if ( empty($this->custom_wrapper_classes) )
        {
            $custom_wrapper_classes = Config::get('crud.custom_wrapper_classes');
            $array_of_strings = true;
            if ( $this->assert->is_array(__FUNCTION__, $custom_wrapper_classes, 'custom_wrapper_classes', []) )
            {
                foreach ( $custom_wrapper_classes as &$class )
                {
                    if ( !$this->assert->is_string(__FUNCTION__, $class, 'class', []) )
                    {
                        // The array item was not a string.
                        $array_of_strings = false;
                    }
                }
            }
            else
            {
                // The configuration value was not an array.
                $array_of_strings = false;
            }

            // Return the loaded classes or [] on an error.
            return $array_of_strings ? $custom_wrapper_classes : [];
        }

        // Otherwise return the classes defined in this instance.
        return $this->custom_wrapper_classes;
    }

    public function get_search(): Crud_Search
    {
        return $this->search;
    }

    public function &get_assert(): Assert
    {
        return $this->assert;
    }

    public function get_paginator(): Paginator
    {
        return $this->paginator;
    }

    public static function set_language(string $language): void
    {
        self::$language = $language;
    }

    public static function get_language(): string
    {
        return self::$language;
    }

    /* PATHS */

    public static function root_path(): string
    {
        return self::ROOT_PATH;
    }

    public static function config_path(): string
    {
        return self::CONFIG_PATH;
    }

    public static function library_path(): string
    {
        return self::LIBRARY_PATH;
    }

    public static function public_path(): string
    {
        return self::PUBLIC_PATH;
    }

    public static function resources_path(): string
    {
        return self::RESOURCES_PATH;
    }

    public static function lang_path(): string
    {
        return self::LANG_PATH;
    }

    public static function views_path(): string
    {
        return self::VIEWS_PATH;
    }

    public static function storage_path(): string
    {
        return self::STORAGE_PATH;
    }

    public function get_storage(): Storage
    {
        return $this->storage;
    }

}