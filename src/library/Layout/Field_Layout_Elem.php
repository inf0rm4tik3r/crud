<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Layout;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Layout\Elements\Section;
use Lukaspotthast\Crud\Layout\Elements\Tab;

/**
 * Class Field_Layout_Elem
 * @package Lukaspotthast\Crud\Layout
 */
abstract class Field_Layout_Elem
{

    /**
     * Holds a reference to the crud instance this layout belongs to.
     */
    protected $crud;

    public abstract function tab(string $tab_name): Tab;
    public abstract function has_tabs(): bool;
    public abstract function has_tab(string $tab_name): bool;
    public abstract function get_tabs(): array;
    public abstract function get_tab(int $key): Tab;

    public abstract function section(string $section_name): Section;
    public abstract function has_sections(): bool;
    public abstract function has_section(string $section_name): bool;
    public abstract function get_sections(): array;
    public abstract function get_section(int $key): Section;

    public abstract function fields(...$fields_parameters);
    public abstract function has_fields(): bool;
    public abstract function has_field(string $field_name): bool;
    public abstract function get_fields(): array;
    public abstract function get_field(int $key); // string|array

    public abstract function separator($css, $classes, $html): void;
    public abstract function get_separator(int $key): array;

    /*
    public abstract function element(string $element_identifier);
    public abstract function get_element(int $key): string;
    */

    public abstract function set_crud(Crud &$crud);
    public abstract function get_crud(): Crud;
    public abstract function set_name(string $name);
    public abstract function get_name(): string;

}