<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Layout\Elements;

use Lukaspotthast\Crud\Layout\Field_Layout;

class Tab extends Field_Layout
{
    private $tab_name;

    public function __construct(&$crud, string $tab_name)
    {
        parent::__construct($crud);
        $this->set_name($tab_name);
    }

    public function set_name(string $tab_name)
    {
        $this->tab_name = $tab_name;
    }

    public function get_name() : string
    {
        return $this->tab_name;
    }
}