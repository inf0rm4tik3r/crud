<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Layout\Elements;

use Lukaspotthast\Crud\Layout\Field_Layout;

class Section extends Field_Layout
{
    private $section_name;

    public function __construct(&$crud, string $section_name)
    {
        parent::__construct($crud);
        $this->set_name($section_name);
    }

    public function set_name(string $section_name)
    {
        $this->section_name = $section_name;
    }

    public function get_name() : string
    {
        return $this->section_name;
    }
}