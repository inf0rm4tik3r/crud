<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Layout;

use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Layout\Elements\Section;
use Lukaspotthast\Crud\Layout\Elements\Tab;

class Field_Layout extends Field_Layout_Elem
{
    const PAGE = 1;
    const TAB = 2;
    const SECTION = 3;
    const FIELD = 4;
    const SEPARATOR = 5;

    private $is_empty = true;
    private $element_refs =  array();
    private $tabs = array();
    private $sections = array();
    private $fields = array();
    private $separators = array();

    /**
     * @var Assert
     */
    private $assert;

    public function __construct(Crud &$crud)
    {
        $this->crud = $crud;
        $this->assert = $crud->get_assert();
    }

    public function is_empty()
    {
        return $this->is_empty;
    }

    /* ELEMENTS */
    private function add_element_ref($identifier, &$from_array)
    {
        $this->is_empty = false;
        end($from_array);
        array_push( $this->element_refs, array('identifier' => $identifier, 'key' => key($from_array)) );
        reset($from_array);
    }

    public function get_element_refs()
    {
        return $this->element_refs;
    }


    /* TABS */
    public function tab(string $tab_name): Tab
    {
        $crud = $this->get_crud();
        $tab = new Tab($crud, $tab_name);

        array_push($this->tabs, $tab);
        $this->add_element_ref(self::TAB, $this->tabs);

        return $tab;
    }

    public function has_tabs(): bool
    {
        return !empty($this->tabs);
    }

    public function has_tab(string $tab_name): bool
    {
        foreach ($this->tabs as &$tab)
        {
            if ($tab->get_name() === $tab_name)
            {
                return true;
            }
        }
        return false;
    }

    public function get_tabs() : array
    {
        return $this->tabs;
    }

    public function get_tab(int $key): Tab
    {
        return $this->tabs[$key];
    }


    /* SECTIONS */
    public function section(string $section_name): Section
    {
        $crud = $this->get_crud();
        $section = new Section($crud, $section_name);

        array_push($this->sections, $section);
        $this->add_element_ref(self::SECTION, $this->sections);

        return $section;
    }

    public function has_sections() : bool
    {
        return !empty($this->sections);
    }

    public function has_section(string $section_name) : bool
    {
        foreach ($this->sections as &$section)
        {
            if ($section->get_name() === $section_name)
            {
                return true;
            }
        }
        return false;
    }

    public function get_sections() : array
    {
        return $this->sections;
    }

    public function get_section(int $key) : Section
    {
        return $this->sections[$key];
    }

    /* FIELDS */
    public function fields(...$fields_parameters)
    {
        foreach ($fields_parameters as &$fields)
        {
            // Check if the current parameter is a string.
            if ( is_string($fields) )
            {
                // Is comma separated list?
                if ( strpos($fields, ',') !== false )
                {
                    $fields = explode(',', $fields);
                    // Execution continues with array check below!
                }
                // 'fields' holds a single field name.
                else
                {
                    $this->add_field($fields);
                }
            }

            // Check if the current parameter is an array.
            if ( is_array($fields) )
            {
                foreach ($fields as &$field)
                {
                    $this->add_field($field);
                }
            }
        }

        return $this;
    }

    private function add_field($field)
    {
        if ( is_array($field) )
        {
            foreach ($field as &$col_field)
            {
                $args = compact('col_field');

                // Check if the field really exists.
                if ( !$this->assert->is_column_or_nested(
                    $this->crud->get_table_set()->get_main_table(),
                    $this->crud, __FUNCTION__, $col_field, $args) )
                {
                    return;
                }
            }

            array_push($this->fields, $field);
            $this->add_element_ref(self::FIELD, $this->fields);
        }
        else
        {
            $args = compact('field');

            // Check if the field really exists.
            if ( !$this->assert->is_column_or_nested(
                $this->crud->get_table_set()->get_main_table(),
                $this->crud, __FUNCTION__, $field, $args) )
            {
                return;
            }

            array_push($this->fields, $field);
            $this->add_element_ref(self::FIELD, $this->fields);
        }
    }

    public function has_fields() : bool
    {
        return !empty($this->fields);
    }

    public function has_field(string $field_name) : bool
    {
        return in_array($field_name, $this->fields);
    }

    public function get_fields() : array
    {
        return $this->fields;
    }

    public function get_field(int $key)
    {
        return $this->fields[$key];
    }

    /* SEPARATOR */

    public function separator($css = '2em', $classes = array(), $html = '') : void
    {
        // Sanitize the css parameter.
        if ( !isset($css) )
        {
            $css = '2em';
        }
        if ( is_string($css) )
        {
            $css = array('height' => $css);
        }

        // Sanitize the classes parameter.
        if ( !isset($classes) )
        {
            $classes = array();
        }
        if ( is_string($classes) )
        {
            $classes = explode(' ', $classes);
        }

        // Sanitize the html parameter.
        if ( !isset($html) )
        {
            $html = '';
        }

        array_push($this->separators, compact('css', 'classes', 'html'));
        $this->add_element_ref(self::SEPARATOR, $this->separators);
    }

    public function get_separator(int $key) : array
    {
        return $this->separators[$key];
    }

    /* DATA */

    public function set_crud(Crud &$crud)
    {
        $this->crud = $crud;
    }

    public function get_crud() : Crud
    {
        return $this->crud;
    }

    public function set_name(string $tab_name)
    {
        // Does nothing. A general layout element does not need a name.
    }

    public function get_name() : string
    {
        // A general layout element has no name.
        return '';
    }
}