<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Inlineedit;

trait Crud_Field_Inlining
{

    /**
     * Stores which fields are getting displayed inline.
     * @var array
     */
    private $inlined_fields;

    /**
     *
     */
    public function reset_field_inlining() : void
    {
        $this->inlined_fields = array();
    }


    /**
     * Specifies that $field should be displayed inline.
     * Only affects fields that are capable of being rendered inline.
     *
     * @param string $field_name The field that should be rendered inline.
     * @param bool $inline Optional:<br>
     *                     true = inline, false = no inline<br>
     *                     Defaults to: true
     */
    public function inline(string $field_name, bool $inline = true) : void
    {
        $args = compact('field_name', 'inline');

        // The field name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // The field must exists.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // The $value parameter is optional, but needs to be a boolean value.
        if ( !$this->assert->is_bool(__FUNCTION__, $inline, 'inline', $args) )
        {
            return;
        }

        // Everything went good. Set the specified fields inline status to the given value.
        $this->inlined_fields[$field_name] = $inline;
    }

    /**
     * Determines whether the specified field (column name) should be displayed inline.
     *
     * @param string$field_name The field that ist to be checked to be rendered inline.
     * @return bool true = inlined, false = not inlined
     */
    public function is_inlined(string $field_name) : bool
    {
        if ( array_key_exists($field_name, $this->inlined_fields) )
        {
            return $this->inlined_fields[$field_name];
        }
        return false;
    }

}