<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Condition;

use JsonSerializable;

/**
 * Class Condition_Clause
 * @package Lukaspotthast\Crud\Condition
 */
class Condition_Clause implements JsonSerializable
{

    /**
     * @var string
     */
    private $column_name;

    /**
     * @var string
     */
    private $operator;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var string|null
     */
    private $identifier;

    public function to_string(): string
    {
        return json_encode($this->column_name).' '.$this->operator.' '. json_encode($this->value);
    }

    public function to_json(): string
    {
        return json_encode($this);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'col' => $this->column_name,
            'op' => $this->operator,
            'val' => $this->value,
            'identifier' => $this->identifier
        ];
    }

    public function set_column_name(string $column_name): void
    {
        $this->column_name = $column_name;
    }

    public function get_column_name(): string
    {
        return $this->column_name;
    }

    public function set_operator(string $operator): void
    {
        $this->operator = $operator;
    }

    public function get_operator(): string
    {
        return $this->operator;
    }

    public function set_value(/*{any}*/ $value): void
    {
        $this->value = $value;
    }

    public function get_value() /*:{any}*/
    {
        return $this->value;
    }

    public function set_identifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function get_identifier(): ?string
    {
        return $this->identifier;
    }

}