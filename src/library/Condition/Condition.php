<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Condition;

use Illuminate\Database\Query\Builder;
use Lukaspotthast\Crud\Assert\Assert;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Tools;
use JsonSerializable;

/**
 * Class Condition
 * @package Lukaspotthast\Crud\Condition
 */
class Condition implements JsonSerializable
{
    /**
     * List of supported where operators.
     */
    const OPERATORS = array(
        // Basic
        '=',
        '!=', '<>',
        '<', '<=',
        '>', '>=',

        // Like
        '*%', 'STARTS_WITH',
        '%*', 'ENDS_WITH',
        '%*%', 'CONTAINS',
        '*', 'EQUALS',

        // IN
        'IN', '!IN',

        // Between
        '><', '!><',
        'BETWEEN', '!BETWEEN',
    );

    /**
     * Reference to the parent crud instance.
     * @var Crud
     */
    private $crud;

    /**
     * @var Assert
     */
    private $assert;

    /**
     * Reference to the parent Condition or null if the current object is not a sub-query.
     * @var Condition | null
     */
    private $parent = null;

    private $query = [];
    private $pos = 0;

    public function __construct(Crud &$crud, &$parent = null)
    {
        $this->crud = $crud;
        $this->assert = $crud->get_assert();
        $this->parent = $parent;
    }

    /**
     * Ensures that all necessary cloning gets done.
     */
    public function __clone()
    {
        // A copied element should have the same $crud reference.
        // A copied element should have the same $parent reference.

        // We need to create a "deep copy" of the $query.
        foreach ( $this->query as &$query_elem )
        {
            if ( $query_elem instanceof Condition_Clause )
            {
                $query_elem = clone $query_elem;
            }
            else if ( $query_elem instanceof Condition )
            {
                $query_elem = clone $query_elem;
            }
        }
    }

    public function debug() : Condition
    {
        dump($this);
        dump($this->to_json());
        dump($this->to_string());
        return $this;
    }

    /**
     * Creates a deep copy of this object and replaces the field/column names of all where-clauses with the calculated
     * id-strings of their respective browser inputs.<br>
     * Necessary when sending this object to the browser. The JavaScript API is then able to directly access the
     * fields values and does not have to calculate the input ids for each field on its own.
     * @return Condition Copy of the object on which this function gets called.
     */
    public function replaced_field_names(): Condition
    {
        $new = clone $this;

        foreach ( $new->query as &$query_elem )
        {
            if ( $query_elem instanceof Condition_Clause )
            {
                // TODO: decompose column "table_name.column_name"
                $col = $this->crud->get_table_set()->get_column('', $query_elem->get_column_name());
                $query_elem->set_column_name( Tools::encode_input_id($this->crud, $col) );
            }
            else if ( $query_elem instanceof Condition )
            {
                $query_elem = $query_elem->replaced_field_names();
            }
        }

        return $new;
    }

    public function to_string(): string
    {
        $out = '';

        foreach ( $this->query as &$query_elem )
        {
            if ( is_string($query_elem) )
            {
                switch ($query_elem) {
                    case 'AND':
                        $out .= ' && ';
                        break;
                    case 'OR':
                        $out .= ' || ';
                        break;
                }
            }
            else if ( $query_elem instanceof Condition_Clause )
            {
                $out .= '('.$query_elem->to_string().')';
            }
            else if ( $query_elem instanceof Condition )
            {
                $out .= '('.$query_elem->to_string().')';
            }
        }

        return $out;
    }

    public function to_json(): string
    {
        return json_encode($this);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return $this->query;
    }

    public function reset(): Condition
    {
        $this->query = [];
        $this->pos = 0;
        return $this;
    }

    public function where(string $column_name, string $operator, /*{any}*/ $value, string $identifier = null): Condition
    {
        $args = compact('column_name','operator', 'value', 'identifier');

        // Check if the $column_name parameter is correctly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return $this;
        }

        // Check if the specified column really exists.
        if ( !$this->assert->column_exists($this->crud->get_table_set()->get_main_table(), __FUNCTION__, $column_name, $args) )
        {
            return $this;
        }

        // Check if the operator parameter is correctly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $operator, 'operator', $args) )
        {
            return $this;
        }

        // Make the operator uppercase. This allows the usage of 'in' instead of 'IN'
        $operator = strtoupper($operator);

        // Check if the specified operator is supported.
        if ( !in_array($operator, self::OPERATORS) )
        {
            $this->crud->error(
                __FUNCTION__.'(): The specified operator is not supported!<br>'
                .'Use one of: '.json_encode(self::OPERATORS), $args
            );
            return $this;
        }

        // Check if the value parameter is correctly specified.
        if ( !isset($value) )
        {
            // Passing 'null' to the value parameter is allowed, if the operator is set to '=' or '!='.
            // The column is then checked to be null or not null.
            if ( $operator === '=' or $operator === '!=' )
            {
                // do nothing...
            }
            else
            {
                $this->crud->error(__FUNCTION__.'(): A value must be specified!', $args);
                return $this;
            }
        }

        // Check if the specified value matches the operators conditions.
        switch ($operator)
        {
            // Value must be specified as an array.
            case 'IN':
            case '!IN':
                if ( !is_array($value) )
                {
                    $this->crud->error(
                        __FUNCTION__.'(): The operators "IN" and "!IN" require the value to be specified as an array!',
                        $args
                    );
                    return $this;
                }
                break;

            // Value must be specified as an array of size 2.
            case '><':
            case '!><':
            case 'BETWEEN':
            case '!BETWEEN':
                if ( ! (is_array($value) and count($value) === 2) )
                {
                    $this->crud->error(
                        __FUNCTION__.'(): The operators "><", "!><", "BETWEEN" and "!BETWEEN" require the value '
                        .'to be specified as an array of size 2!', $args
                    );
                    return $this;
                }
                break;
        }

        // Check if the identifier got correctly specified and is not already in use.
        if ( isset($identifier) )
        {
            if ( !$this->assert->is_non_empty_string(__FUNCTION__, $identifier, 'identifier', $args) )
            {
                return $this;
            }

            if ( !is_null($this->find_ref($identifier)) )
            {
                $this->crud->error(
                    __FUNCTION__.'(): The specified identifier was already used for another where clause.<br>'
                    .'Identifiers must be unique!', $args
                );
                return $this;
            }
        }

        // MAYBE: TODO: Check if value is valid for operator and field type.

        // Add an 'AND' connector if this where() call immediately followed the previous call to this function.
        // Happens if the 'OR' connector was not specified.
        if ( ($this->pos - 1) >= 0 and $this->query[$this->pos - 1] instanceof Condition_Clause )
        {
            $this->and();
        }

        // Add the where clause to the query.
        $ccc = new Condition_Clause();
        $ccc->set_column_name($column_name);
        $ccc->set_operator($operator);
        $ccc->set_value($value);
        $ccc->set_identifier($identifier);
        //array_splice($this->query, $this->pos++, 0, $ccc);
        $this->query[$this->pos++] = $ccc;

        // Allows method chaining.
        return $this;
    }

    public function update_value(string $identifier, $value): void
    {
        // Create a displayable list of the supplied arguments so that they can get printed should we
        // encounter an error.
        $args = compact('identifier', 'value');

        // Check if the identifier got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $identifier, 'identifier', $args) )
        {
            return;
        }

        // Retrieve the corresponding where clause.
        $clause =& $this->find_ref($identifier);

        // Check if the identifier corresponded to a where clause.
        if ( is_null($clause) )
        {
            $this->crud->error(__FUNCTION__.'(): The specified identifier did not correspond to any where clause!', $args);
            return;
        }

        // Check if the value parameter is correctly specified.
        if ( !isset($value) )
        {
            // Passing 'null' to the value parameter is allowed, if the operator is set to '=' or '!='.
            // The column is then checked to be null or not null.
            if ( $clause->get_operator() === '=' or $clause->get_operator() === '!=' )
            {
                // do nothing...
            }
            else
            {
                $this->crud->error(__FUNCTION__.'(): A value must be specified!', $args);
            }
        }

        // Set the new value.
        $clause->set_value( $value );
    }

    public function find(string $identifier): ?Condition_Clause
    {
        return $this->find_ref($identifier);
    }

    /**
     *
     * @param string $identifier
     * @return Condition_Clause|null
     */
    private function &find_ref(string $identifier): ?Condition_Clause
    {
        // Try to find the identifier on the current level.
        foreach ( $this->query as &$query_element )
        {
            if ($query_element instanceof Condition_Clause)
            {
                if ($query_element->get_identifier() === $identifier)
                {
                    return $query_element;
                }
            }
            else if ($query_element instanceof Condition)
            {
                // Try to find the identifier in a sub level.
                $found =& $query_element->find_where_ref($identifier);
                if (!is_null($found))
                {
                    return $found;
                }
            }
            // otherwise continue.
        }

        $null = null;
        return $null;
    }

    public function and() : Condition
    {
        // Do not add this connector if the last element was already a connector.
        if ( ($this->pos - 1) >= 0 and
             ($this->query[$this->pos - 1] === 'AND' or $this->query[$this->pos - 1] === 'OR') )
        {
            return $this;
        }

        //array_splice($this->query, $this->pos++, 0, array('AND'));
        $this->query[$this->pos++] = "AND";
        return $this;
    }

    public function or()
    {
        // Do not add this connector if the last element was already a connector.
        if ( ($this->pos - 1) >= 0 and
            ($this->query[$this->pos - 1] === 'AND' or $this->query[$this->pos - 1] === 'OR') )
        {
            return $this;
        }

        //array_splice($this->query, $this->pos++, 0, array('OR'));
        $this->query[$this->pos++] = "OR";
        return $this;
    }

    public function open()
    {
        // Add an 'AND' connector if this where() call immediately followed the previous call to this function.
        // Happens if the 'OR' connector was not specified.
        if ( ($this->pos - 1) >= 0 and $this->query[$this->pos - 1] instanceof Condition_Clause )
        {
            $this->and();
        }

        $sub_query = new Condition($this->crud, $this);
        array_splice($this->query, $this->pos++, 0, array($sub_query));
        return $sub_query;
    }

    public function close()
    {
        return $this->get_parent();
    }

    public function get_parent()
    {
        return $this->parent;
    }

    public function get_query()
    {
        return $this->query;
    }

    public function get_pos()
    {
        return $this->pos;
    }

    public function add_to(Builder &$query): Builder
    {
        $glue = null;

        foreach ( $this->query as &$query_elem )
        {
            if ( is_string($query_elem) )
            {
                if ( $query_elem === 'AND' or $query_elem === 'OR' )
                {
                    $glue = $query_elem;
                }
            }

            else if ( $query_elem instanceof Condition_Clause )
            {
                // Add a where clause to the current query.
                if ( isset($glue) and $glue === 'OR' )
                {
                    switch ( $query_elem->get_operator() )
                    {
                        case '=':
                        case '!=':
                            if ( is_null($query_elem->get_value()) )
                            {
                                if ( $query_elem->get_operator() === '=' )
                                    $query = $query->orWhereNull($query_elem->get_column_name());
                                if ( $query_elem->get_operator() === '!=' )
                                    $query = $query->orWhereNotNull($query_elem->get_column_name());
                            }
                            else
                            {
                                $query = $query->orWhere($query_elem->get_column_name(), $query_elem->get_operator(), $query_elem->get_value());
                            }
                            break;

                        case '<':
                        case '<=':
                        case '>':
                        case '>=':
                        case '<>':
                            $query = $query->orWhere($query_elem->get_column_name(), $query_elem->get_operator(), $query_elem->get_value());
                            break;

                        case '*%':
                        case 'STARTS_WITH':
                            $query = $query->orWhere($query_elem->get_column_name(), 'like', $query_elem->get_value().'%');
                            break;

                        case '%*':
                        case 'ENDS_WITH':
                            $query = $query->orWhere($query_elem->get_column_name(), 'like', '%'.$query_elem->get_value());
                            break;

                        case '%*%':
                        case 'CONTAINS':
                            $query = $query->orWhere($query_elem->get_column_name(), 'like', '%'.$query_elem->get_value().'%');
                            break;

                        case '*':
                        case 'EQUALS':
                            $query = $query->orWhere($query_elem->get_column_name(), 'like', $query_elem->get_value());
                            break;

                        case 'IN':
                            $query = $query->orWhereIn($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '!IN':
                            $query = $query->orWhereNotIn($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '><':
                        case 'BETWEEN':
                            $query = $query->orWhereBetween($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '!><':
                        case '!BETWEEN':
                            $query = $query->orWhereNotBetween($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        default:
                            $this->crud->error(__FUNCTION__.'(): Unknown where operator: "'.$query_elem->get_operator().'"');
                            break;
                    }
                }
                else
                {
                    switch ( $query_elem->get_operator() )
                    {
                        case '=':
                        case '!=':
                            if ( is_null($query_elem->get_value()) )
                            {
                                if ( $query_elem->get_operator() === '=' )
                                    $query = $query->whereNull($query_elem->get_column_name());
                                if ( $query_elem->get_operator() === '!=' )
                                    $query = $query->whereNotNull($query_elem->get_column_name());
                            }
                            else
                            {
                                $query = $query->where($query_elem->get_column_name(), $query_elem->get_operator(), $query_elem->get_value());
                            }
                            break;

                        case '<':
                        case '<=':
                        case '>':
                        case '>=':
                        case '<>':
                            $query = $query->where($query_elem->get_column_name(), $query_elem->get_operator(), $query_elem->get_value());
                            break;

                        case '*%':
                        case 'STARTS_WITH':
                            $query = $query->where($query_elem->get_column_name(), 'like', $query_elem->get_value().'%');
                            break;

                        case '%*':
                        case 'ENDS_WITH':
                            $query = $query->where($query_elem->get_column_name(), 'like', '%'.$query_elem->get_value());
                            break;

                        case '%*%':
                        case 'CONTAINS':
                            $query = $query->where($query_elem->get_column_name(), 'like', '%'.$query_elem->get_value().'%');
                            break;

                        case '*':
                        case 'EQUALS':
                            $query = $query->where($query_elem->get_column_name(), 'like', $query_elem->get_value());
                            break;

                        case 'IN':
                            $query = $query->whereIn($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '!IN':
                            $query = $query->whereNotIn($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '><':
                        case 'BETWEEN':
                            $query = $query->whereBetween($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        case '!><':
                        case '!BETWEEN':
                            $query = $query->whereNotBetween($query_elem->get_column_name(), $query_elem->get_value());
                            break;

                        default:
                            $this->error(__FUNCTION__.'(): Unknown where operator: "'.$query_elem->get_operator().'"');
                            break;
                    }
                }

                // Reset the glue. Otherwise something like ->where()->or()->where()->where()
                // would produce: (where or where or where) instead of (where or where and where)
                $glue = null;
        }

            else if ( $query_elem instanceof Condition )
            {
                // Add a sub_query (in parenthesis) to the current query.
                if ( isset($glue) and $glue === 'OR' )
                {
                    $query = $query->orWhere(function ($sub_query) use ($query_elem) {
                        //$this->add_to($sub_query, $query_elem->get_query()); //TODO: !!!
                        $query_elem->add_to($sub_query);
                    });
                }
                else
                {
                    $query = $query->where(function ($sub_query) use ($query_elem) {
                        //$this->add_to($sub_query, $query_elem->get_query()); //TODO: !!!
                        $query_elem->add_to($sub_query);
                    });
                }

                // Reset the glue. Otherwise something like ->where()->or()->where()->where()
                // would produce: (where or where or where) instead of (where or where and where)
                $glue = null;
            }

            else
            {
                $this->crud->error(__FUNCTION__.'(): Unknown where type.');
            }
        }

        return $query;
    }

}