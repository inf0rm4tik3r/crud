<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Tooltip;

trait Field_Tooltips_Trait
{

    /**
     * @var Field_Tooltip_Collection
     */
    private $field_tooltip_collection;

    /**
     * Sets a tooltip for the specified field.<br>
     * The tooltip will be shown next to the label for this field.
     *
     * @param string $field_name        The field for which the tooltip should be specified.
     * @param string $tooltip_html      The HTML code of the tooltip.
     * @param array  $tasks             Optional:<br>
     *                                  If you want the tooltip to only show up in specific tasks, specify them
     *                                  here.<br> If the tooltip should be shown in every task, supply an empty
     *                                  array.<br> Defaults to: array()
     * @param bool   $override_existent Optional:<br>
     *                                  Should a previously defined tooltip for this task be overridden?<br>
     *                                  Defaults to: false
     */
    public function field_tooltip(
        string $field_name,
        string $tooltip_html,
        array $tasks = [],
        bool $override_existent = false
    ): void
    {
        $this->field_tooltip_collection->add($field_name, new Field_Tooltip($tooltip_html), $tasks, $override_existent);
    }

    /**
     * @return Field_Tooltip_Collection
     */
    public function field_tooltip_collection(): Field_Tooltip_Collection
    {
        return $this->field_tooltip_collection;
    }

}