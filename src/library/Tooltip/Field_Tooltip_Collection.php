<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Tooltip;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Exception\Tooltip_Exception;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Args;

class Field_Tooltip_Collection
{

    /**
     * @var Crud
     */
    private $crud;

    /**
     * @var Field_Tooltip_Repository
     */
    private $repo;

    /**
     * Field_Tooltip_Collection constructor.
     * @param Crud $crud
     */
    public function __construct(Crud $crud)
    {
        $this->crud = $crud;
        $this->repo = new Field_Tooltip_Repository([]);
    }

    /**
     * Clears the collection.
     */
    public function reset(): void
    {
        $this->repo->init([]);
    }

    /**
     * Sets a tooltip for the specified field.<br>
     * The tooltip will be shown next to the label for this field.
     *
     * @param string        $field_name
     *      The field for which the tooltip should be specified.
     *
     * @param Field_Tooltip $tooltip
     *      The HTML code of the tooltip.
     *
     * @param array         $tasks
     *      Optional:<br>
     *      If you want the tooltip to only show up in specific tasks, specify them here. <br>
     *      If the tooltip should be shown in every task, supply an empty array. <br>
     *      Defaults to: []
     *
     * @param bool          $override_existent
     *      Optional: <br>
     *      Should a previously defined tooltip for this task be overridden? <br>
     *      Defaults to: false
     */
    public function add(
        string $field_name,
        Field_Tooltip $tooltip,
        array $tasks = [],
        bool $override_existent = false
    ): void
    {
        Arg::typecheck($field_name, '!string');
        Arg::typecheck($tooltip, Field_Tooltip::class);
        Arg::typecheck($tasks, ['string']);
        Arg::typecheck($override_existent, 'bool');

        $args = compact('field', 'tooltip_html', 'tasks', 'override_existent');

        // Check whether the specified field really exists.
        if ( !$this->crud->get_assert()->is_column_or_nested(
            $this->crud->get_table_set()->get_main_table(),
            $this->crud,
            __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // The tasks parameter is optional, but needs to be an empty array by default.
        if ( empty($tasks) )
        {
            $tasks = Crud::AVAILABLE_TASKS;
        }
        // Check if all specified tasks really exist.
        else
        {
            foreach ( $tasks as &$task )
            {
                if ( !in_array($task, Crud::AVAILABLE_TASKS) )
                {
                    $this->crud->error(
                        'field_tooltip(): One of the specified tasks: "' . $task . '" is not available.
                        Arguments passed:<br>
                        ' . Args::as_list($args)
                    );
                    return;
                }
            }
        }

        // Check if a tooltip for this field and task already got defined.
        if ( !$override_existent )
        {
            foreach ( $tasks as &$task )
            {
                if ( $this->has($field_name, $task) )
                {
                    $this->crud->error(
                        'A tooltip for the field "' . $field_name . '" for task "' . $task . '" already exists.<br>'
                        . 'Overwriting it with "' . Arg::stringify($tooltip) . '" was not allowed.'
                    );
                    return;
                }
            }
        }

        // The tooltip must be set for each task.
        foreach ( $tasks as &$task )
        {
            $this->repo->set($field_name . '.' . $task, $tooltip);
        }
    }

    /**
     * Checks if a tooltip has been defined for the given field name and task.
     *
     * @param string $field_name
     *      The field name that should be checked.
     *
     * @param string $task
     *      The task that should be checked.
     *
     * @return bool
     *      True if a tooltip is defined for the field $field_name in task $task. Otherwise false.
     */
    public function has(string $field_name, string $task): bool
    {
        Arg::typecheck($field_name, '!string');
        Arg::typecheck($task, '!string');

        // Check if a tooltip for the specified field and task got defined:
        return $this->repo->has($field_name . '.' . $task);
    }

    /**
     * Returns the tooltip for the specified field name and task.<br>
     * This function assumes that the user previously checked the existence of a tooltip with the
     * has_field_tooltip() function.
     *
     * @param string $field_name
     *      The field name for which the tooltip should be returned.
     *
     * @param string $task
     *      The task for which the tooltip should be returned.
     *
     * @return Field_Tooltip
     *      Throw a Tooltip_Exception should the tooltip not exist.
     *
     * @throws Tooltip_Exception
     */
    public function get(string $field_name, string $task): Field_Tooltip
    {
        Arg::typecheck($field_name, '!string');
        Arg::typecheck($task, '!string');
        $path = $field_name . '.' . $task;

        if ( $this->repo->has($path) )
        {
            return $this->repo->get($path);
        }

        throw new Tooltip_Exception('There is no tooltip specified for: "' . $path . '"');
    }

}