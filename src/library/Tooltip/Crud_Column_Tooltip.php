<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Tooltip;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Tools;

trait Crud_Column_Tooltip
{
    private $column_tooltips; // array

    private function reset_column_tooltips() : void
    {
        $this->column_tooltips = array();
    }

    /**
     * Sets a tooltip for the specified column.<br>
     * The tooltip will be shown next to the label for this column.
     *
     * @param string $column The column for which the tooltip should be specified.
     * @param string $tooltip_html The HTML code of the tooltip.
     * @param array $tasks Optional:<br>
     *      If you want the tooltip to only show up in specific tasks, specify them here.<br>
     *      If the tooltip should be shown in every task, supply an empty array.<br>
     *      Defaults to: array()
     * @param bool $override_existent Optional:<br>
     *      Should a previously defined tooltip for this task be overridden?<br>
     *      Defaults to: false
     */
    public function column_tooltip($column, $tooltip_html, $tasks = array(), $override_existent = false)
    {
        // Check if correct arguments were passed.
        if ( !(isset($column) and is_string($column) and strlen($column) !== 0 and
             isset($tooltip_html) and is_string($tooltip_html) and strlen($tooltip_html) !== 0) )
        {
            $this->error(
                'column_tooltip(): $column and $tooltip_html must both be specified as non empty strings.
                Arguments passed:<br>
                '.Tools::args_as_list(array('column' => $column, 'tooltip_html' => $tooltip_html))
            );
            return;
        }

        // Check whether the specified column really exists.
        if (!$this->table_set->get_main_table()->get_schema()->has_column_schema($column))
        {
            $this->error('column_tooltip(): The specified column "'.$column.'" does not exist.');
            return;
        }

        // The tasks parameter is optional, but needs to be an empty array by default.
        if (!(isset($tasks) and is_array($tasks)) or count($tasks) === 0)
        {
            $tasks = Crud::AVAILABLE_TASKS;
        }
        // Check if all specified tasks really exist.
        else
        {
            foreach ($tasks as &$task)
            {
                if (!in_array($task, Crud::AVAILABLE_TASKS))
                {
                    $this->error(
                        'column_tooltip(): One of the specified tasks: "'.$task.'" is not available.
                        Arguments passed:<br>
                        '.Tools::args_as_list(array(
                            'column'       => $column,
                            'tooltip_html' => $tooltip_html,
                            'tasks'        => json_encode($tasks)
                        ))
                    );
                    return;
                }
            }
        }

        // The override_existent parameter is optional, but needs to be a boolean by default.
        if (!(isset($override_existent) and is_bool($override_existent)))
        {
            $override_existent = false;
        }

        // Check if a tooltip for this column and task already got defined.
        if (array_key_exists($column, $this->column_tooltips))
        {
            foreach ($tasks as &$task)
            {
                if (array_key_exists($task, $this->column_tooltips[$column]) and !$override_existent)
                {
                    $this->error(
                        'column_tooltip(): A tooltip for the column "'.$column.'" for task "'.$task.
                        '" already exists.<br>'.
                        'Overwriting it with "'.$tooltip_html.'" was not allowed.'
                    );
                    return;
                }
            }
        }

        // Everything went good. The tooltip needs to be set.
        if (!array_key_exists($column, $this->column_tooltips))
        {
            $this->column_tooltips[$column] = array();
        }
        foreach ($tasks as &$task)
        {
            if (!array_key_exists($task, $this->column_tooltips[$column]))
            {
                $this->column_tooltips[$column][$task] = array();
            }
            // Actually set the tooltip:
            $this->column_tooltips[$column][$task] = $tooltip_html;
        }
    }

    /**
     * Checks if a tooltip has been defined for the given column and task.
     *
     * @param string $column The column which should be checked.
     * @param string $task The task that should be checked.
     * @return bool True if a tooltip is defined for the column $column in task $task. Otherwise false.
     */
    public function has_column_tooltip(string $column, string $task): bool
    {
        $args = compact('column', 'task');

        // Check if correct arguments were passed.
        if (!(isset($column) and is_string($column) and strlen($column) !== 0 and
            isset($task) and is_string($task) and strlen($task) !== 0)
        )
        {
            $this->error(
                'has_column_tooltip(): column and task must be specified as non empty strings.<br>
                Arguments passed:<br>'.Tools::args_as_list(array('column' => $column, 'task' => $task))
            );
            return false;
        }

        // Check whether the specified column really exists.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column, $args) )
        {
            return false;
        }

        // Check if the specified tasks really exist.
        if (!in_array($task, Crud::AVAILABLE_TASKS))
        {
            $this->error(
                'column_tooltip(): The specified task: "'.$task.'" is not available.
                    Arguments passed:<br>
                    '.Tools::args_as_list(array(
                    'column' => $column,
                    'task'   => $task
                ))
            );
            return false;
        }

        // Check if a tooltip for the specified column and task got defined:
        $defined = false;
        // There is no tooltip if $column is not an index of the tooltip information array.
        if (array_key_exists($column, $this->column_tooltips))
        {
            // We want to know if the tooltip got defined for a specific task.
            if ( array_key_exists($task, $this->column_tooltips[$column]) and
                 is_string($this->column_tooltips[$column][$task]) )
            {
                $defined = true;
            }
        }
        return $defined;
    }

    /**
     * Returns the tooltips HTML code for the specified column and task.
     *
     * @param string $column The column for which the tooltips HTML code should be returned.
     * @param string $task The task for which the tooltips HTML code should be returned.
     * @return string NULL, should the tooltip not exist.
     */
    public function get_column_tooltip($column, $task)
    {
        // Return the stored tooltips html for the specified column.
        if ( array_key_exists($column, $this->column_tooltips) and
             array_key_exists($task, $this->column_tooltips[$column]) )
        {
            return $this->column_tooltips[$column][$task];
        }
        return null;
    }

}