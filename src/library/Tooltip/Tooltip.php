<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Tooltip;

/**
 * Class Tooltip
 * @package Lukaspotthast\Crud\Tooltip
 */
class Tooltip
{

    /**
     * @var string
     */
    private $html;

    /**
     * Tooltip constructor.
     * @param string $html
     */
    public function __construct(string $html = '')
    {
        $this->set_html($html);
    }

    public function __toString(): string
    {
        return 'Tooltip: {HTML: ' . $this->html . '}';
    }

    /**
     * @param string $html
     */
    public function set_html(string $html): void
    {
        $this->html = $html;
    }

    /**
     * @return string
     */
    public function get_html(): string
    {
        return $this->html;
    }

}