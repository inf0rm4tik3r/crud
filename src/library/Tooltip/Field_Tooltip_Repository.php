<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Tooltip;

use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Repository;

class Field_Tooltip_Repository extends Repository
{

    /**
     * Adds a Field_Tooltip object to the repository.
     *
     * @param string        $path
     * @param Field_Tooltip $value
     */
    public function set(string $path, $value): void
    {
        Arg::typecheck($value, Field_Tooltip::class);
        parent::set($path, $value);
    }

}