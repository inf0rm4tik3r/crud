<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

/**
 * Interface Provides_Debugging
 * @package Lukaspotthast\Crud\Frameworks
 */
interface Provides_Debugging
{

    /**
     *
     */
    public static function debug_init(): void;

    /**
     * @param string $type
     * @param        $value
     */
    public static function debug_message(string $type, $value): void;

    /**
     * @param string $identifier
     * @param string $name
     */
    public static function debug_start_measure(string $identifier, string $name): void;

    /**
     * @param string $identifier
     */
    public static function debug_stop_measure(string $identifier): void;

}