<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\PHP;

use Lukaspotthast\Crud\Frameworks\Provides_Session;

/**
 * Class PHP_Session
 * @package Lukaspotthast\Crud\Frameworks\PHP
 */
class PHP_Session implements Provides_Session
{

    /**
     *
     */
    public static function session_init(): void
    {
        if ( session_status() !== PHP_SESSION_ACTIVE )
        {
            session_start();
        }
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function session_put(string $key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public static function session_get(string $key)
    {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : null;
    }

}