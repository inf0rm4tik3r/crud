<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\PHP;

use Lukaspotthast\Crud\Config\Loader;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Provides_Config;
use Lukaspotthast\Support\Repository;

/**
 * Class PHP_Config
 * @package Lukaspotthast\Crud\Frameworks\PHP
 */
class PHP_Config implements Provides_Config
{

    /**
     * @var Repository
     */
    private static $repository;

    /**
     *
     */
    public static function config_init(): void
    {
        self::$repository = new Repository(Loader::load_from_folder(Crud::config_path()));
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function config_set(string $key, $value): void
    {
        self::$repository->set($key, $value);
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function config_has(string $key): bool
    {
        return self::$repository->has($key);
    }

    /**
     * @param string $key
     * @param null   $default_value
     * @return mixed
     */
    public static function config_get(string $key, $default_value = null)
    {
        return self::$repository->get($key, $default_value);
    }

    /**
     * @return Repository
     */
    public static function get_repository(): Repository
    {
        return self::$repository;
    }

}