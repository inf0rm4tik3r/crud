<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\PHP;

use DebugBar\StandardDebugBar;
use Lukaspotthast\Crud\Frameworks\Provides_Debugging;

/**
 * Class PHP_Debugging
 * @package Lukaspotthast\Crud\Frameworks\PHP
 */
class PHP_Debugging implements Provides_Debugging
{

    /**
     * @var StandardDebugBar|null
     */
    private static $debugbar = null;

    /**
     *
     */
    public static function debug_init(): void
    {
    }

    /**
     * @param string $type
     * @param        $value
     */
    public static function debug_message(string $type, $value): void
    {
        if ( self::has_debugbar() )
        {
            self::$debugbar['messages']->{$type}($value);
        }
    }

    /**
     * @param string $identifier
     * @param string $name
     */
    public static function debug_start_measure(string $identifier, string $name = ''): void
    {
        if ( self::has_debugbar() )
        {
            self::$debugbar['time']->startMeasure($identifier, $name);
        }
    }

    /**
     * @param string $identifier
     */
    public static function debug_stop_measure(string $identifier): void
    {
        if ( self::has_debugbar() )
        {
            self::$debugbar['time']->stopMeasure($identifier);
        }
    }

    /**
     * @param $debugbar
     */
    public static function set_debugbar($debugbar): void
    {
        self::$debugbar = $debugbar;
    }

    /**
     * @return bool
     */
    public static function has_debugbar(): bool
    {
        return self::$debugbar !== null;
    }

    /**
     * @return StandardDebugBar
     */
    public static function get_debugbar(): StandardDebugBar
    {
        return self::$debugbar;
    }

}