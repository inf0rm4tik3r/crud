<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\PHP;

use Lukaspotthast\Crud\Frameworks\Provides_Logging;

/**
 * Class PHP_Logging
 * @package Lukaspotthast\Crud\Frameworks\PHP
 */
class PHP_Logging implements Provides_Logging
{

    /**
     *
     */
    public static function log_init(): void
    {
    }

    /**
     * @param string $type
     * @param string $message
     */
    public static function log_message(string $type, string $message): void
    {
        print_r('CRUD: ' . $type . ' - ' . $message);
    }

}