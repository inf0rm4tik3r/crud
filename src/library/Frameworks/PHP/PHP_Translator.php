<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\PHP;

use Lukaspotthast\Crud\Config\Loader;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Provides_Translation;
use Lukaspotthast\Support\Repository;

/**
 * Class PHP_Translator
 * @package Lukaspotthast\Crud\Frameworks\PHP
 */
class PHP_Translator implements Provides_Translation
{

    /**
     * @var Repository
     */
    private static $repository;

    /**
     *
     */
    public static function translator_init(): void
    {
        self::$repository = new Repository(Loader::load_from_folder(Crud::lang_path()));
    }

    /**
     * @param string      $key
     * @param             $value
     * @param null|string $language
     */
    public static function translator_set(string $key, $value, ?string $language = null): void
    {
        $language = is_null($language) ? Crud::get_language() : $language;
        self::$repository->set($language . '.' . $key, $value);
    }

    /**
     * @param string      $key
     * @param null|string $language
     * @return bool
     */
    public static function translator_has(string $key, ?string $language = null): bool
    {
        $language = is_null($language) ? Crud::get_language() : $language;
        return self::$repository->has($language . '.' . $key);
    }

    /**
     * @param string      $key
     * @param null|string $language
     * @return mixed
     */
    public static function translator_get(string $key, ?string $language = null)
    {
        $language = is_null($language) ? Crud::get_language() : $language;
        return self::$repository->get($language . '.' . $key, null);
    }

    /**
     * @return Repository
     */
    public static function get_repository(): Repository
    {
        return self::$repository;
    }

}