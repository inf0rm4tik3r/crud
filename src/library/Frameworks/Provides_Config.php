<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

/**
 * Interface Provides_Config
 * @package Lukaspotthast\Crud\Frameworks
 */
interface Provides_Config
{

    /**
     *
     */
    public static function config_init(): void;

    /**
     * @param string $key
     * @param        $value
     */
    public static function config_set(string $key, $value): void;

    /**
     * @param string $key
     * @return bool
     */
    public static function config_has(string $key): bool;

    /**
     * @param string $key
     * @param null   $default_value
     * @return mixed
     */
    public static function config_get(string $key, $default_value = null);

}