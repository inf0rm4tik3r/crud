<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

/**
 * Interface Provides_Logging
 * @package Lukaspotthast\Crud\Frameworks
 */
interface Provides_Logging
{

    /**
     *
     */
    public static function log_init(): void;

    /**
     * @param string $type
     * @param string $message
     */
    public static function log_message(string $type, string $message): void;

}