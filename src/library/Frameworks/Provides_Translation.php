<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

/**
 * Interface Provides_Translation
 * @package Lukaspotthast\Crud\Frameworks
 */
interface Provides_Translation
{

    /**
     *
     */
    public static function translator_init(): void;

    /**
     * @param string      $key
     * @param             $value
     * @param null|string $language
     */
    public static function translator_set(string $key, $value, ?string $language = null): void;

    /**
     * @param string      $key
     * @param null|string $language
     * @return bool
     */
    public static function translator_has(string $key, ?string $language = null): bool;

    /**
     * @param string      $key
     * @param null|string $language
     * @return mixed
     */
    public static function translator_get(string $key, ?string $language = null);

}