<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

abstract class Frameworks
{

    public const DEFAULT = self::PHP;

    public const PHP     = 'PHP';
    public const LARAVEL = 'Laravel';
    //...

}