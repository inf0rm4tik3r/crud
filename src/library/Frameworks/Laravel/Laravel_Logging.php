<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

// The Log Facade provided by the Laravel application.
use /** @noinspection PhpUndefinedClassInspection */
    Log;
use Lukaspotthast\Crud\Frameworks\Provides_Logging;

/**
 * Class Laravel_Logging
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Laravel_Logging implements Provides_Logging
{

    /**
     *
     */
    public static function log_init(): void
    {
    }

    /**
     * @param string $type
     * @param string $message
     */
    public static function log_message(string $type, string $message): void
    {
        /** @noinspection PhpUndefinedClassInspection */
        Log::{$type}($message);
    }

}