<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

// The Debugbar Facade provided by the Laravel application.
use /** @noinspection PhpUnusedAliasInspection */
    Debugbar;
use Lukaspotthast\Crud\Frameworks\Provides_Debugging;

/**
 * Class Laravel_Debugging
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Laravel_Debugging implements Provides_Debugging
{

    /**
     *
     */
    public static function debug_init(): void
    {
    }

    /**
     * @param string $type
     * @param        $value
     */
    public static function debug_message(string $type, $value): void
    {
        /** @noinspection PhpUndefinedClassInspection */
        Debugbar::{$type}($value);
    }

    /**
     * @param string $identifier
     * @param string $name
     */
    public static function debug_start_measure(string $identifier, string $name = ''): void
    {
        if ( empty($name) )
        {
            $name = $identifier;
        }
        /** @noinspection PhpUndefinedClassInspection */
        Debugbar::startMeasure($identifier, $name);
    }

    /**
     * @param string $identifier
     */
    public static function debug_stop_measure(string $identifier): void
    {
        /** @noinspection PhpUndefinedClassInspection */
        Debugbar::stopMeasure($identifier);
    }

}