<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

use Lukaspotthast\Crud\Frameworks\Provides_Session;

/**
 * Class Laravel_Session
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Laravel_Session implements Provides_Session
{

    /**
     *
     */
    public static function session_init(): void
    {
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function session_put(string $key, $value): void
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        session()->put($key, $value);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function session_get(string $key)
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        return session($key);
    }

}