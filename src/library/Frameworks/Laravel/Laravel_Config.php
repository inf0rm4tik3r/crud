<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

use Lukaspotthast\Crud\Frameworks\Provides_Config;

/**
 * Class Laravel_Config
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Laravel_Config implements Provides_Config
{

    /**
     *
     */
    public static function config_init(): void
    {
    }

    /**
     * @param string $key
     * @param        $value
     */
    public static function config_set(string $key, $value): void
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        config([$key, $value]);
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function config_has(string $key): bool
    {
        $time = microtime(true);

        /** @noinspection PhpUndefinedFunctionInspection */
        $value = config($key, $time);

        // If the helper returned $time, we know that no configuration is present for the specified key.
        if ( $value === $time )
        {
            return false;
        }

        return true;
    }

    /**
     * @param string $key
     * @param null   $default_value
     * @return mixed
     */
    public static function config_get(string $key, $default_value = null)
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        return config($key, $default_value);
    }

}