<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Asset\Asset;

class Crud_Service_Provider extends ServiceProvider
{

    /**
     * Determines if the package-loading can be deferred.
     * The loading process would then only take place when classes from this package would be used in a request.
     * BUT: This package adds routes to the system. They must exist for every request! => false.
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        // Give the possibility to issue a vendor:publish command to copy the libraries configuration file and assets.
        $this->publishes([ $this->packageConfigFile() => $this->appConfigFile() ], 'lukaspotthast-crud-config');

        foreach (Asset::PACKAGE_ASSETS as &$asset_info )
        {
            $this->publishes(
                [ $asset_info['path'] => $this->appAssetsPath().'/'.$asset_info['output'] ],
                'lukaspotthast-crud-assets'
            );
        }

        foreach (Asset::PACKAGE_PLUGIN_ASSETS as &$asset_info )
        {
            $this->publishes(
                [ $asset_info['path'] => $this->appAssetsPath().'/plugins/'.$asset_info['output'] ],
                'lukaspotthast-crud-plugins');
        }

        // Crud routes.
        $this->loadRoutesFrom($this->routesPath());

        // Crud views.
        $this->loadViewsFrom($this->viewsPath(), 'crud');

        // Crud translations.
        $this->loadTranslationsFrom(__DIR__ . '/../../../resources/lang', 'crud');
    }

    public function register()
    {
        $this->mergeConfigFrom($this->packageConfigFile(), 'crud');

        $loader = AliasLoader::getInstance();
        $loader->alias('Crud', Crud::class);
        $loader->alias('Asset', Asset::class);
    }

    public function packageConfigFile() : string
    {
        return __DIR__ . '/../../../config/crud.php';
    }

    public function appConfigFile() : string
    {
        return config_path('crud.php');
    }

    public function routesPath() : string
    {
        return __DIR__ . '/../../../routes/web.php';
    }

    public function viewsPath() : string
    {
        return __DIR__ . '/../../../resources/views';
    }

    public function appAssetsPath() : string
    {
        return public_path('libraries/crud');
    }

}