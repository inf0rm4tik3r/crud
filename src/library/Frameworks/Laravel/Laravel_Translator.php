<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

use Lukaspotthast\Crud\Frameworks\Provides_Translation;

/**
 * Class Laravel_Translator
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Laravel_Translator implements Provides_Translation
{

    /**
     *
     */
    public static function translator_init(): void
    {
    }

    /**
     * @param string      $key
     * @param             $value
     * @param null|string $language
     */
    public static function translator_set(string $key, $value, ?string $language = null): void
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        trans(['crud::' . $key, $value]);
    }

    /**
     * @param string      $key
     * @param null|string $language
     * @return bool
     */
    public static function translator_has(string $key, ?string $language = null): bool
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        return trans('crud::' . $key) !== null;
    }

    /**
     * @param string      $key
     * @param null|string $language
     * @return mixed
     */
    public static function translator_get(string $key, ?string $language = null)
    {
        /** @noinspection PhpUndefinedFunctionInspection */
        return trans('crud::' . $key);
    }

}