<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks\Laravel;

use Illuminate\Routing\Controller as Laravel_Controller;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Crud_Controller as Main_Controller;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Http\Request;

/**
 * Class Crud_Controller
 * @package Lukaspotthast\Crud\Frameworks\Laravel
 */
class Crud_Controller extends Laravel_Controller
{

    /**
     * @param $route_name
     * @return string
     */
    public function handle($route_name): string
    {
        Crud::set_framework(Frameworks::LARAVEL);

        $c = new Main_Controller();
        return $c->{str_replace('-', '_', $route_name)}(new Request());
    }

}