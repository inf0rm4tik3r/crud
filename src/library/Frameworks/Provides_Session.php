<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Frameworks;

/**
 * Interface Provides_Session
 * @package Lukaspotthast\Crud\Frameworks
 */
interface Provides_Session
{

    /**
     *
     */
    public static function session_init(): void;

    /**
     * @param string $key
     * @param        $value
     */
    public static function session_put(string $key, $value): void;

    /**
     * @param string $key
     * @return mixed
     */
    public static function session_get(string $key);

}