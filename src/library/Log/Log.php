<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Log;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Frameworks\PHP\PHP_Logging;
use Lukaspotthast\Crud\Frameworks\Laravel\Laravel_Logging;

/**
 * Class Log
 * @package Lukaspotthast\Crud\Log
 */
class Log
{

    public static function init(): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Logging::log_init();
                break;

            case Frameworks::LARAVEL:
                Laravel_Logging::log_init();
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function debug($data): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Logging::log_message('debug', $data);
                break;

            case Frameworks::LARAVEL:
                Laravel_Logging::log_message('debug', $data);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function info($data): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Logging::log_message('info', $data);
                break;

            case Frameworks::LARAVEL:
                Laravel_Logging::log_message('info', $data);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function warning($data): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Logging::log_message('warning', $data);
                break;

            case Frameworks::LARAVEL:
                Laravel_Logging::log_message('warning', $data);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

    public static function error($data): void
    {
        switch ( Crud::get_framework() )
        {
            case Frameworks::PHP:
                PHP_Logging::log_message('error', $data);
                break;

            case Frameworks::LARAVEL:
                Laravel_Logging::log_message('error', $data);
                break;

            default:
                die('Unsupported framework specified!');
                break;
        }
    }

}