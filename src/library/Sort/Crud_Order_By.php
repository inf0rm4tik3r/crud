<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Sort;

use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Tools;
use Lukaspotthast\Support\Func;
use Lukaspotthast\Support\Str;
use Opis\Closure\SerializableClosure;

/**
 * Trait Crud_Order_By
 * @package Lukaspotthast\Crud\Sort
 */
trait Crud_Order_By
{
    private $order_by_crud_ref = null;
    private $order_by_columns; // array

    /**
     * Stores functions used to order certain columns.
     * @var array
     */
    private $order_by_functions;

    private $default_order_by_columns; // array

    private $supported_order_by_modes; // array
    private $order_by_mode; // string

    // Setting this option to true, works, but not as intended.
    // The standard order_by options are still getting added on a page refresh.
    // Leaving the instance with the default order_by and the users order_by selections from before the reload.
    private $keep_order_by = false;

    private $allowed_order_by_columns; // array

    private function hard_reset_order_by()
    {
        $this->reset_order_by_selection();

        // If no other order_by option is chosen, the displayed entries will be ordered by their id.
        if ( method_exists($this, 'get_id_column_name') )
        {
            $this->default_order_by_columns = [
                $this->get_id_column_name() => 'asc',
            ];
        }
        else
        {
            $this->default_order_by_columns = [];
        }

        $this->allowed_order_by_columns = Config::get('crud.allowed_order_by_columns');

        $this->order_by_functions = [];
    }

    private function reset_order_by()
    {
        $this->supported_order_by_modes = ['single', 'multi', 'multi_with_cmd'];
        $this->order_by_mode            = Config::get('crud.order_by_mode'); // multi_with_cmd

        // Reset the current order_by selection.
        if ( !$this->keep_order_by )
        {
            $this->hard_reset_order_by();
        }
    }

    public function reset_order_by_selection()
    {
        $this->order_by_columns = [];
    }

    /**
     * Describe a column you wish the result to be ordered by. You can call this function multiple times with
     * different columns in the $columns argument to order the output by many columns (for example: date and time).
     *
     * @param string|array $columns    The columns you want to order by. Either a string with comma separated columns or
     *                                 an array of columns defined as individual strings.
     * @param string       $order_type Optional:<br>
     *                                 The ordering you wish to use. Accepted values are 'asc' and 'desc'.<br>
     *                                 Defaults to: 'asc'
     * @return Crud_Order_By current object.
     */
    public function order_by($columns, string $order_type = 'asc'): self
    {
        $args = compact('columns', 'order_type');

        // Check if columns are defined.
        if ( !isset($columns) )
        {
            $this->order_by_crud_ref->error(
                '<b>' . __FUNCTION__ . '(): Columns need to be specified.</b><br>'
                . 'Arguments passed:<br>' . Tools::args_as_list($args)
            );
            return $this;
        }

        // Check whether the columns parameter is in the right format.
        $is_array  = is_array($columns);
        $is_string = is_string($columns);
        if ( !($is_array or $is_string) )
        {
            $this->order_by_crud_ref->error(
                '<b>' . __FUNCTION__ . '():  The "$columns" parameter was neither an array nor a string.</b><br>'
                . 'Arguments passed:<br>' . Tools::args_as_list($args)
            );
            return $this;
        }

        // The order_type parameter is optional. But when set, it needs to be 'asc' or 'desc'.
        if ( !isset($order_type) or (isset($order_type) and $order_type !== 'asc' and $order_type !== 'desc') )
        {
            $this->order_by_crud_ref->error(
                'order_by(): The "$order_type" parameter only supports \'asc\' or \'desc\' as options.<br>'
                . 'Arguments passed:<br>'
                . Tools::args_as_list($args)
            );
            return $this;
        }

        // We only work with lower-case types, so comparison gets easier.
        $order_type = strtolower($order_type);

        // Check if the $columns were supplied as a strings. If true, convert them to an array.
        if ( $is_string )
        {
            $columns = Str::to_array($columns, ',');
        }

        // Check if all supplied columns actually exist.
        foreach ( $columns as &$col )
        {
            // The order_by request only gets saved if the supplied column is actually a column of the current table.
            if ( !(is_string($col) and $this->assert->table_column_exists($this->table_set->get_main_table(), __FUNCTION__, $col, $args)) )
            {
                return $this;
            }
        }

        // Add each supplied column to the instance.
        foreach ( $columns as &$col )
        {
            $this->order_by_columns[$col] = $order_type;
        }

        return $this;
    }


    public function order_by_func($table_column_name, $function): void
    {
        if ( Func::is_closure($function) )
        {
            $function = new SerializableClosure($function);
        }

        $this->order_by_functions[$table_column_name] = $function;
    }

    public function needs_custom_ordering(): bool
    {
        foreach ( $this->order_by_functions as $column_name => &$function )
        {
            if ( array_key_exists($column_name, $this->get_order_by_columns()) )
            {
                return true;
            }
        }
        return false;
    }


    /**
     * @param $column_name
     * @return null|string
     */
    public function get_order_by_status($column_name)
    {
        $args = compact('column_name');

        // Check if a column name got defined.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return null;
        }

        // The supplied column must be a column of the current table.
        if ( !$this->assert->table_column_exists($this->table_set->get_main_table(), __FUNCTION__, $column_name, $args) )
        {
            return null;
        }

        // Where to operate on.
        $order_by_columns =& $this->get_order_by_columns();

        // Get the current ordering status for the specified column.
        $status = null;
        if ( array_key_exists($column_name, $order_by_columns) )
        {
            $status = $order_by_columns[$column_name];
        }

        return $status;
    }

    public function &get_order_by_columns()
    {
        if ( empty($this->order_by_columns) )
        {
            return $this->default_order_by_columns;
        }
        else
        {
            return $this->order_by_columns;
        }
    }

    public function remove_order_by($column_name)
    {
        $args = compact('column_name');

        // Check if a column name got defined.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return null;
        }

        // The supplied column must be a column of the current table.
        if ( !$this->assert->table_column_exists($this->table_set->get_main_table(), __FUNCTION__, $column_name, $args) )
        {
            return null;
        }

        // Where to operate on.
        $order_by_columns =& $this->get_order_by_columns();

        // Remove the ordering status.
        if ( array_key_exists($column_name, $order_by_columns) )
        {
            unset($order_by_columns[$column_name]);
        }
    }

    public function order_by_mode(string $mode_name): void
    {
        $args = compact('mode_name');

        // Check if the mode got properly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $mode_name, 'mode_name', $args) )
        {
            return;
        }

        // Check if the mode is one of the supported order_by modes.
        if ( !in_array($mode_name, $this->supported_order_by_modes) )
        {
            $this->error(
                __FUNCTION__ . '(): The specified mode is not available!<br>'
                . 'Use one of: ' . json_encode($this->supported_order_by_modes),
                $args
            );
            return;
        }

        // Set the new order_by mode.
        $this->order_by_mode = $mode_name;
    }

    public function get_order_by_mode()
    {
        return $this->order_by_mode;
    }

    public function allow_order_by_column($column_name, $bool)
    {
        $args = compact('column_name', 'bool');

        // Check if the column name got correctly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $column_name, 'column_name', $args) )
        {
            return;
        }

        // Check if the column really exists.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $column_name, $args) )
        {
            return;
        }

        // Check if the truth value was correctly specified.
        if ( !$this->assert->is_bool(__FUNCTION__, $bool, 'bool', $args) )
        {
            return;
        }

        // All parameters are specified and valid.
        $this->allowed_order_by_columns[$column_name] = $bool;
    }

    public function is_order_by_column_allowed($column_name)
    {
        // If ordering is not allowed in general, it cannot be allowed for a specific column.
        if ( !$this->is_allowed('order_by') )
        {
            return false;
        }

        if ( array_key_exists($column_name, $this->allowed_order_by_columns) )
        {
            return $this->allowed_order_by_columns[$column_name];
        }

        // If no 'allowed' status is set for the given column name, ordering should generally be allowed.
        return true;
    }

}