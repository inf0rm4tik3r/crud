<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud;

use Lukaspotthast\Crud\Config\Config;

/**
 * Trait Crud_Soft_Delete.
 * @package Lukaspotthast\Crud
 */
trait Crud_Soft_Delete
{
    /**
     * The table column in which the "deleted" status will be stored.
     * @var string
     */
    private $soft_delete_column;

    /**
     * Determines whether the soft-delete functionality is used/active.
     * @var bool
     */
    private $use_soft_delete;

    /**
     * Determines if soft-deleted entries are still accessible.
     * @var bool
     */
    private $show_deleted_entries;


    /**
     * Sets default values for trait properties.
     */
    public function reset_soft_delete()
    {
        $this->soft_delete_column = Config::get('crud.soft_delete_column');
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $this->soft_delete_column, 'soft_delete_column (CONFIGURATION)', []) )
        {
            $this->soft_delete_column = 'deleted';
        }

        $this->use_soft_delete = Config::get('crud.use_soft_delete');
        if ( !$this->assert->is_bool(__FUNCTION__, $this->use_soft_delete, 'use_soft_delete (CONFIGURATION)', []) )
        {
            $this->use_soft_delete = false;
        }

        $this->show_deleted_entries = Config::get('crud.show_deleted_entries');
        if ( !$this->assert->is_bool(__FUNCTION__, $this->show_deleted_entries, 'show_deleted_entries (CONFIGURATION)', []) )
        {
            $this->use_soft_delete = true;
        }
    }

    /**
     * Restores the entry with the specified id.<br>
     * (Sets the "deleted" flag of this entry to true/1).
     *
     * @param int $id
     *      The id of the entry which should be soft-deleted.
     */
    private function soft_delete_entry(int $id) : void
    {
        // Update the entry with the id "$id".
        $this->db()->table($this->get_table_set()->get_main_table()->get_table_name())
            ->where($this->get_id_column_name(), '=', $id)
            ->update([ $this->get_soft_delete_column() => 1 ]);

        // It is possibly that the entry with this id already got loaded and cached.
        // We therefore need to invalidate the cache for the previously updated entry,
        // so that the changes are visible when later acquiring this entry id.
        $this->get_table_set()->invalidate_entry_cache($id);
    }

    /**
     * Restores the entry with the specified id.<br>
     * (Sets the "deleted" flag of this entry to false/0).<br>
     * <br>
     * Throws an error if an id gets passed, which does not correspond to a "currently restorable" entry
     * and if $force is set to false (default).<br>
     * The "currently restorable" entries are the ones the "user", which interacts with the system has currently
     * access to.<br>
     * (For example: The entry he is currently editing, or the entries which are currently shown in list view.)<br>
     * Set the $force parameter to true to bypass this behaviour when manually calling this function.
     *
     * @param $id
     *      The id of the entry which should be restored.
     * @param $force
     *      If set true, the system will try to restore the entry with the supplied id, without performing the check
     *      whether or not restoring this entry is permitted in the current state of the instance.
     */
    public function restore_entry(int $id, bool $force = false) : void
    {
        $args = compact('id', 'force');

        // Return immediately if the id was not correctly specified.
        if ( !$this->assert->is_int(__FUNCTION__, $id, 'id', $args) )
        {
            return;
        }

        // Calculate whether the restore is allowed.
        $allowed = false;

        if ($this->task === 'edit')
        {
            // If the id supplied by the ajax call does not match the id of the current edit view (which is stored
            // in this instance), the user must have manipulated his AJAX call to restore an entry he is
            // not currently editing. We strictly prevent that possibility and generate an error!
            if ($id === (int)$this->get_current_entry_id())
            {
                $allowed = true;
            }
            else
            {
                $this->error(
                    __FUNCTION__.'(): You are not allowed to restore entry "'.$id.'", because you '
                    .'are currently not editing it!', $args
                );
            }
        }
        else
        {
            // TODO:  perform a check whether the user is allowed to restore this entry
            // match id against array of printed values...
            $allowed = true;
        }

        // Perform the database call to restore the entry.
        if ( $allowed )
        {
            $this->db()->table($this->get_table_set()->get_main_table()->get_table_name())
                ->where($this->get_id_column_name(), '=', $id)
                ->update([ $this->get_soft_delete_column() => 0 ]);

            // It is possibly that the entry with this id already got loaded and cached.
            // We therefore need to invalidate the cache for the previously updated entry,
            // so that the changes are visible when later acquiring this entry id.
            $this->get_table_set()->invalidate_entry_cache($id);
        }
    }

    /**
     * Activate or deactivate the use of the soft-delete feature.<br>
     * If enabled, items will only be marked as deleted, but not actively deleted from the underlying database.
     *
     * @param bool $use_soft_delete
     *      true: use soft-delete<br>
     *      false: do not use soft-delete.
     */
    public function use_soft_delete(bool $use_soft_delete = true) : void
    {
        if ( !$this->assert->is_bool(__FUNCTION__, $use_soft_delete, 'use_soft_delete', compact('use_soft_delete')) )
        {
            return;
        }

        $this->use_soft_delete = $use_soft_delete;
    }

    /**
     * Returns true if soft-delete got activated, false otherwise.
     * @return bool
     */
    public function uses_soft_delete() : bool
    {
        return $this->use_soft_delete;
    }

    /**
     * Activates / deactivates the accessibility / display of deleted entries to the user.
     * @param bool $show_deleted_entries
     */
    public function show_deleted_entries(bool $show_deleted_entries) : void
    {
        $this->show_deleted_entries = $show_deleted_entries;
    }

    /**
     * Returns true if deleted entries should be accessible / displayed.
     * @return bool
     */
    public function shows_deleted_entries() : bool
    {
        return $this->show_deleted_entries;
    }

    /**
     * @param string $soft_delete_column The column which should be used store the 'deleted'-bit. Default is 'deleted'.
     */
    public function set_soft_delete_column(string $soft_delete_column) : void
    {
        $this->soft_delete_column = $soft_delete_column;
    }

    /**
     * Returns the name of the soft-delete column.
     * @return string
     */
    public function get_soft_delete_column() : string
    {
        return $this->soft_delete_column;
    }

}