<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Field;

use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use stdClass;

trait Crud_Field_Types
{
    private $field_types; // array(field => type)

    private $texteditor_driver; // string
    private $select_driver; // string
    private $colorpicker_driver; // string


    private function reset_field_types() : void
    {
        $this->field_types = [];
        $this->reset_texteditor_driver();
        $this->reset_select_driver();
        $this->reset_colorpicker_driver();
    }

    private function compute_field_type(stdClass $col) : string
    {
        if ( array_key_exists($col->Field, $this->field_types) )
        {
            return $this->field_types[$col->Field];
        }
        else
        {
            return $col->real_type['name'];
        }
    }

    private function get_stored_field_config(string $field) : ?Crud_Field_Config
    {
        if ( array_key_exists($field, $this->field_config) )
        {
            return $this->field_config[$field];
        }
        else
        {
            return null;
        }
    }

    private function store_field_config(string $field, Crud_Field_Config $config) : void
    {
        $this->field_config[$field] = $config;
    }

    /**
     * Acquire a configuration object for the specified fiel to specify default values, data, parameters and more.<br>
     * Of which type the returned object is, relies on the type of the field!<br>
     * For example: Did the type got changed 'to select' by a call to change_type(), an instance of
     * Crud_Type_Select_Config gets returned.
     *
     * @param string $field The field for which the configuration object should be returned.
     * @param bool recreate If set to true, this method will always generate a fresh configuration object for you.
     *                      If set to false (default), this method will return a previously generated configuration
     *                      if present.
     * @return null|Crud_Field_Config Returns null if an error occurred. Otherwise a reference to the configuration
     *                                object for the specified field is returned.
     */
    public function &field_config(string $field, bool $recreate = false) : ?Crud_Field_Config
    {
        $args = compact('field', 'recreate');

        // Check if the field got propperly specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field, 'field', $args) )
        {
            return null;
        }

        // Check if the recreate parameter got properly specified.
        if ( !$this->assert->is_bool(__FUNCTION__, $recreate, 'recreate', $args) )
        {
            return null;
        }

        // Check if the specified field really exists.
        $col = $this->assert->set_column_exists($this->get_table_set(), __FUNCTION__, $field, $args);
        if ( is_null($col) )
        {
            return null;
        }

        /*
         * Create or return a configuration file based on the type of the specified field.
         */
        $field_type = $this->compute_field_type($col);
        switch ( $field_type )
        {
            case 'smallint':
            case 'mediumint':
            case 'int':
                $config = $this->get_stored_field_config($field);
                if ( is_null($config) )
                {
                    $config = new Crud_Int_Field_Config();
                }
                $this->store_field_config($field, $config);
                return $config;
                break;

            case 'select': // or else fallthrough
            case 'multiselect':
                $config = $this->get_stored_field_config($field);
                if ( is_null($config) )
                {
                    $config = new Crud_Select_Field_Config();
                }
                $this->store_field_config($field, $config);
                return $config;
                break;

            case 'file':
            case 'multifile':
                $config = $this->get_stored_field_config($field);
                if ( is_null($config) )
                {
                    $config = new Crud_Select_Field_Config();
                }
                $this->store_field_config($field, $config);
                return $config;
                break;

            default:
                $this->error(
                    __FUNCTION__.'(): The specified field with type "'.$field_type.'" is not configurable!', $args
                );
                return null;
        }
    }

    /*
    * Allowed types should be:
    *
    * - password
    * -
    *
    *
    *
    * - int
    * - float
    * - decimal
    * - varchar
    * - text
    *
    *
    */
    public function change_type(string $field, string $type) : void
    {
        $args = compact('field', 'type');

        // Check if the field got specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field, 'field', $args) )
        {
            return;
        }

        // Check if the type got specified.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $type, 'type', $args) )
        {
            return;
        }

        // Check if the field really exists.
        if ( !$this->assert->column_exists($this->get_table_set()->get_main_table(), __FUNCTION__, $field, $args) )
        {
            return;
        }

        /** @var Column_Schema $col */
        $col = $this->table_set->get_main_table()->get_schema()->get_column_schema($field);

        // Check if the type is supported.
        // TODO: update the supported types to include common types such as int bool varchar text etc.
        if ( !$this->type_supported($col->get_type()->get_name(), $type) )
        {
            $this->error(
                __FUNCTION__.'(): The specified type is not supported for the specified field of type "'.
                $col->real_type['name'].'"!<br>'
                .'Use one of these for the respective column types:<br>'.$this->types_as_list(),
                $args
            );
            return;
        }

        // Input is valid. We can store the requested type.
        $this->field_types[$field] = $type;
    }

    private function type_supported(string $col_type, string $type) : bool
    {
        if ( ! array_key_exists($col_type, self::SUPPORTED_FIELD_TYPES) )
        {
            return false;
        }

        return in_array($type, self::SUPPORTED_FIELD_TYPES[$col_type]);
    }

    private function types_as_list()
    {
        $list = '<ul>';
        foreach (self::SUPPORTED_FIELD_TYPES as $col_type => &$supported_types)
        {
            $list .= '<li>'.$col_type.'</li>';
            $list .= '<ul>';
            foreach ($supported_types as &$type)
            {
                $list .= '<li>'.$type.'</li>';
            }
            $list .= '</ul>';
        }
        $list .= '</ul>';
        return $list;
    }

    public function get_internal_type($field)
    {
        if ( array_key_exists($field, $this->field_types) )
        {
            return $this->field_types[$field];
        }

        if ( array_key_exists($field, $this->relations) )
        {
            return 'relation';
        }

        return 'default';
    }

    public function get_display_type($field)
    {
        if ( array_key_exists($field, $this->field_types) )
        {
            return $this->field_types[$field];
        }

        if ( array_key_exists($field, $this->relations) )
        {
            return $this->relations[$field]->get_type();
        }

        return 'default';
    }


    /*
     * Texteditor driver
     */

    public function reset_texteditor_driver() : void
    {
        // Default
        $this->texteditor_driver = '';

        $texteditor_driver = Config::get('crud.texteditor_driver');
        if ( is_string($texteditor_driver) )
        {
            $this->set_texteditor_driver($texteditor_driver);
        }
    }

    public function set_texteditor_driver(string $texteditor_driver) : void
    {
        $args = compact('texteditor_driver');

        // The texteditor_driver parameter must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $texteditor_driver, 'texteditor_driver', $args) )
        {
            return;
        }

        $this->texteditor_driver = $texteditor_driver;
    }

    public function get_texteditor_driver() : string
    {
        return $this->texteditor_driver;
    }

    /*
     * Select driver
     */

    public function reset_select_driver() : void
    {
        // Default
        $this->select_driver = '';

        $select_driver = Config::get('crud.select_driver');
        if ( is_string($select_driver) )
        {
            $this->set_select_driver($select_driver);
        }
    }

    public function set_select_driver(string $select_driver) : void
    {
        $args = compact('select_driver');

        // The select_driver parameter must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $select_driver, 'select_driver', $args) )
        {
            return;
        }

        $this->select_driver = $select_driver;
    }

    public function get_select_driver() : string
    {
        return $this->select_driver;
    }

    /*
     * Colorpicker driver
     */

    public function reset_colorpicker_driver() : void
    {
        // Default
        $this->colorpicker_driver = '';

        // Try to load the 'colorpicker_driver' config value.
        $colorpicker_driver = Config::get('crud.colorpicker_driver');
        if ( is_string($colorpicker_driver) )
        {
            $this->set_colorpicker_driver($colorpicker_driver);
        }
    }

    public function set_colorpicker_driver(string $colorpicker_driver) : void
    {
        $args = compact('colorpicker_driver');

        // The colorpicker_driver parameter must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $colorpicker_driver, 'colorpicker_driver', $args) )
        {
            return;
        }

        $this->colorpicker_driver = $colorpicker_driver;
    }

    public function get_colorpicker_driver() : string
    {
        return $this->colorpicker_driver;
    }

}