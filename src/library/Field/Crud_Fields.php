<?php
declare(strict_types = 1);

namespace Lukaspotthast\Crud\Field;

use Lukaspotthast\Crud\Layout\Field_Layout;

trait Crud_Fields
{

    /**
     * @var Field_Layout
     */
    private $field_layout;

    /**
     *
     */
    public function reset_field_layout() : void
    {
        $this->field_layout = new Field_Layout($this);
    }

    /**
     * Returns the field layout object for this crud instance.<br>
     * When retrieving the field layout to define it (which should be the main intention when calling this function),
     * DO NOT set the parameter to true, or your changes will not be saved / applied to the rendering.
     *
     * @param bool $generate_default_layout_if_necessary
     *      The user may not have defined his custom field layout. In this case, the system can generate a default
     *      layout based on all available columns / fields. The primary auto incrementing column will be excluded
     *      from that layout. Should only be set to true when calling this function in the rendering process.
     * @return Field_Layout
     *      This function will always return the Crud_Field_Layout object.<br>
     *      Consider: May returns an empty Crud_Field_Layout object if the parameter is not set to true.
     */
    public function get_field_layout(bool $generate_default_layout_if_necessary = false) : Field_Layout
    {
        if ( $this->field_layout->is_empty() and $generate_default_layout_if_necessary )
        {
            return $this->create_default_layout();
        }

        return $this->field_layout;
    }

    /**
     * This function provides a default field layout which is based on all available columns / field.<br>
     * Primary auto incrementing column will be excluded.
     *
     * @return Field_Layout A temporary Crud_Field_Layout object only used for rendering.
     */
    private function create_default_layout(): Field_Layout
    {
        $default_layout = new Field_Layout($this);

        $column_names_multi_level = $this->table_set->get_column_names();

        foreach ( $column_names_multi_level as $table_name => &$column_names )
        {
            foreach ( $column_names as &$column_name )
            {
                // We try to exclude the primary auto incrementing column.
                if ( !( $table_name === $this->table_set->get_main_table()->get_name() and
                        $column_name === $this->get_id_column_name() ) )
                {
                    $default_layout->fields($column_name);
                }
            }
        }

        return $default_layout;
    }

}