<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Exception;

use RuntimeException;
use Throwable;

/**
 * Base class for all Exceptions defined in this library.
 *
 * @package Lukaspotthast\Crud\Exception
 */
class Crud_Exception extends RuntimeException
{

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}