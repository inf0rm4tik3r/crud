<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Exception;
use Throwable;

/**
 * Class Tooltip_Exception
 * @package Lukaspotthast\Crud\Exception
 */
class Tooltip_Exception extends Crud_Exception
{

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}