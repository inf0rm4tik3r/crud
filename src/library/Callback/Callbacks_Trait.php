<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

/**
 * Trait Callbacks_Trait
 * @package Lukaspotthast\Crud\Callback
 */
trait Callbacks_Trait
{

    /**
     * @var Callback_Controller
     */
    private $callback_controller;

    public function callback_controller(): Callback_Controller
    {
        return $this->callback_controller;
    }

    public function callback($type, $function, $arguments = []): void
    {
        $this->callback_controller->standard_callbacks()->add($type, $function, $arguments);
    }

    public function field_callback($type, $function, $arguments = []): void
    {
        $this->callback_controller->field_callbacks()->add($type, $function, $arguments);
    }

    public function column_callback($type, $function, $arguments = []): void
    {
        $this->callback_controller->column_callbacks()->add($type, $function, $arguments);
    }

    /*
    public function data_callback(): void
    {
        $this->callback_controller->add_data_callback();
    }
    */

}