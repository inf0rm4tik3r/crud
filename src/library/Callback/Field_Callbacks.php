<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Callback\Types\Field_Callback;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Func;
use Opis\Closure\SerializableClosure;

/**
 * Class Field_Callbacks:
 * Stores its callbacks as: array[field_name => Field_Callback]
 *
 * @package Lukaspotthast\Crud\Callback
 */
class Field_Callbacks extends Callback_Collection
{
    /**
     * Field_Callbacks constructor.
     * @param Crud $crud
     */
    public function __construct(Crud &$crud)
    {
        parent::__construct($crud);
    }

    /**
     * Provides the ability to define a function which gets called if the specified field should be rendered.
     * The output from the specified function will be rendered instead of the built in field rendering.
     *
     * @param string $field_name
     *      The field for which the custom rendering function should be defined.
     *
     * @param string|array $callable
     *      Either a direct name of a function, or an array with 2 elements: an object, and a string
     *      that represents a member-function of that object.
     *
     * @param array $arguments
     *      Optional: Array of additional data which gets passed to the specified function.
     *      Defaults to: array()
     *
     * @param bool $override_existent
     *      Optional: A callback for the specified field could have already been defined.
     *      If this option is set to true, the old information gets overridden. Otherwise this gets prevented!
     *      Defaults to: false
     */
    public function add(string $field_name, $callable, array $arguments = [], bool $override_existent = false): void
    {
        Arg::typecheck(func_get_args(), ['!string', 'callable', 'array', 'bool']);

        // The field needs to be present in the current state of the instance.
        if ( !$this->crud->get_table_set()->get_main_table()->column_exists($field_name) )
        {
            $msg = __FUNCTION__.'(): The specified field "'.Arg::stringify($field_name).'" does not exist.';
            $this->crud->error($msg);
            return;
        }

        // If a callback for this column was already defined and overwriting it was not allowed, an error gets printed.
        if ( $this->has($field_name) and !$override_existent )
        {
            $msg = __FUNCTION__.'(): A field callback for "'.Arg::stringify($field_name).'" is already defined, but '
                .'overwriting it was not allowed.<br>'
                .'Function "'.Arg::stringify($callable).'" was not set to be the active callback for this column!';
            $this->crud->error($msg);
            return;
        }

        if ( Func::is_closure($callable) )
        {
            $callable = new SerializableClosure($callable);
        }

        // If everything went good, the callback information gets added to the instance.
        $this->callbacks[$field_name] = new Field_Callback($this->crud, $callable, $arguments);
    }

    /**
     * Check if a field callback is set for the specified field name.
     *
     * @param string $field_name
     *      The field name to check.
     *
     * @return bool
     *      True if a callback is defined, false otherwise.
     */
    public function has(string $field_name): bool
    {
        Arg::typecheck($field_name, '!string');
        return array_key_exists($field_name, $this->callbacks);
    }

    /**
     * Retrieve the stored field callback for the specified field name. <br>
     * Remember: Use the {@link Field_Callbacks::has} method first to check if a callback exists.
     *
     * @param string $field_name
     *          The field name to check.
     *
     * @return Field_Callback
     *          The field callback object.
     */
    public function get(string $field_name): Field_Callback
    {
        Arg::typecheck($field_name, '!string');
        return $this->callbacks[$field_name];
    }

}