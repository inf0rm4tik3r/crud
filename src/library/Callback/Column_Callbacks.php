<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Callback\Types\Column_Callback;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Arg;
use Lukaspotthast\Support\Func;
use Opis\Closure\SerializableClosure;

/**
 * Class Column_Callbacks
 * @package Lukaspotthast\Crud\Callback
 */
class Column_Callbacks
{

    /**
     * @var Crud
     */
    private $crud;

    /**
     * @var array (column_name => Column_Callback)
     */
    private $column_callbacks;

    /**
     * Column_Callbacks constructor.
     * @param Crud $crud
     */
    public function __construct(Crud $crud)
    {
        $this->crud = $crud;
    }

    /**
     *
     */
    public function reset(): void
    {
        $this->column_callbacks = [];
    }

    /**
     * TODO: Store values of Column_Callback in the array!
     * Provides the ability to define a function which gets called if the specified field should be rendered.
     * The output from the specified function will be rendered instead of the built in field rendering.
     *
     * @param string $column The column for which the custom rendering function should be defined.
     * @param mixed $function Either a direct name of a function, or an array with 2 elements: an object, and a string
     *                                  that represents a member-function of that object.
     * @param array $arguments Optional: Array of additional data which gets passed to the specified function.
     *                                  Defaults to: array()
     * @param bool $override_existent Optional: A callback for the specified field could have already been defined.
     *                                  If this option is set to true, the old information gets overridden. Otherwise
     *                                  thiw gets prevented: Defaults to: false
     */
    public function add($column, $function, $arguments = array(), $override_existent = false)
    {
        // The first 3 parameters are always necessary!
        if ( ! (isset($column) and is_string($column) and $column !== '' and
             isset($function) and ( (is_array($function) and count($function) === 2) or (is_string($function) and !empty($function)) or Func::is_closure($function) ) and
             isset($filepath) and is_string($filepath)) )
        {
            $msg = '
                column_callback(): You have to supply the first 3 parameters!<br>
                The function parameter can also be an array holding an object and a member-function.<br>
                Supplied arguments:
                <ul>
                    <li>$column: "'.$column.'" - Needs to be a non empty string.</li>
                    <li>$function: "'.(is_array($function) ? serialize($function) : $function).'" - Needs to be a string ar an array($object, $member_function).</li>
                    <li>$filepath: "'.$filepath.'" - Needs to be a non empty string.</li>
                </ul>
            ';
            $this->crud->error($msg);
            return;
        }

        // The arguments parameter should not be null. If not needed, an empty array is sufficient.
        if ( !isset($arguments) )
        {
            $arguments = array();
        }

        // The $override_existent parameter needs to be a boolean. If not we set it to its default.
        if ( !(isset($override_existent) and is_bool($override_existent)) )
        {
            $override_existent = false;
        }

        // The column needs to be present in the current state of the instance.
        if ( !$this->crud->get_table_set()->get_main_table()->column_exists($column) )
        {
            $this->crud->error(__FUNCTION__.'(): The specified column "'.$column.'" does not exist.');
            return;
        }

        // If a callback for this column was already defined and overwriting it was not allowed, an error gets printed.
        if ( array_key_exists($column, $this->column_callbacks) and $override_existent === false )
        {
            $msg = '
                column_callback(): A callback for column "'.$column.'" is already defined, but overwriting it was not 
                allowed.<br>
                Function "'.(is_array($function) ? serialize($function) : $function).'" in file "'.$filepath.'" 
                was not set to be the active callback for this column!
            ';
            $this->crud->error($msg);
            return;
        }

        // Check if the provided function exists and is callable.
        if ( !Func::is_callable($function, $this->crud) )
        {
            $msg = '
                column_callback(): The provided function "'.(is_array($function) ? serialize($function) : $function).'" 
                does not exist or is not callable in "'.$filepath.'"!
            ';
            $this->crud->error($msg);
            return;
        }

        if ( Func::is_closure($function) )
        {
            $function = new SerializableClosure($function);
        }

        // If everything went good, the callback information gets added to the instance.
        $this->column_callbacks[$column] = array(
            'function'  => $function,
            'filepath'  => $filepath,
            'arguments' => $arguments
        );
    }

    /**
     * Check if a column callback is set for the specified column name.
     *
     * @param string $column_name
     *      The column name to check.
     *
     * @return bool
     *      True if a callback is defined, false otherwise.
     */
    public function has(string $column_name): bool
    {
        Arg::typecheck($column_name, '!string');
        return array_key_exists($column_name, $this->column_callbacks);
    }

    /**
     * Retrieve the stored column callback for the specified column name. <br>
     * Remember: Use the {@link Column_Callbacks::has} method first to check if a callback exists.
     *
     * @param string $column_name
     *          The column name to check.
     *
     * @return Column_Callback
     *          The column callback object.
     */
    public function get(string $column_name): Column_Callback
    {
        Arg::typecheck($column_name, '!string');
        return $this->column_callbacks[$column_name];
    }

}