<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Closure;
use Lukaspotthast\Crud\Callback\Types\Afterinsert_Callback;
use Lukaspotthast\Crud\Callback\Types\Afterremove_Callback;
use Lukaspotthast\Crud\Callback\Types\Afterupdate_Callback;
use Lukaspotthast\Crud\Callback\Types\Beforeinsert_Callback;
use Lukaspotthast\Crud\Callback\Types\Beforeremove_Callback;
use Lukaspotthast\Crud\Callback\Types\Beforerender_Callback;
use Lukaspotthast\Crud\Callback\Types\Beforeupdate_Callback;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Func;
use Lukaspotthast\Support\Str;
use Opis\Closure\SerializableClosure;
use stdClass;

/**
 * Class Standard_Callbacks
 * @package Lukaspotthast\Crud\Callback
 */
class Standard_Callbacks extends Callback_Collection
{

    /**
     * The available callback types.
     */
    const TYPES = [
        'beforerender',
        'beforeinsert', 'afterinsert',
        'beforeupdate', 'afterupdate',
        'beforeremove', 'afterremove',
    ];

    /**
     * Standard_Callbacks constructor.
     * @param Crud $crud
     */
    public function __construct(Crud &$crud)
    {
        parent::__construct($crud);
    }

    /**
     * @param string|array $type
     * @return array|null
     */
    public function parse_type($type): ?array
    {
        // The $type argument must no be empty!
        if ( empty($type) )
        {
            $msg = __FUNCTION__ . '(): The $type argument must not be empty!';
            $this->crud->error($msg, compact('type'));
            return null;
        }

        $is_string = is_string($type);
        $is_array  = is_array($type);

        // The $type has to be a string or an array.
        if ( !($is_string or $is_array) )
        {
            $msg = __FUNCTION__ . '(): The $type argument has to be a string or an array!';
            $this->crud->error($msg, compact('type'));
            return null;
        }

        // $type must be converted to an array.
        if ( $is_string )
        {
            $types = Str::to_array($type, ',');
        }
        // $type is already an array.
        else
        {
            $types = $type;
        }

        // Each declared type must be a supported callback types.
        foreach ( $types as &$t )
        {
            /** @var string $t */
            if ( !in_array($t, self::TYPES) )
            {
                $msg = __FUNCTION__ . '(): "' . $t . '" is not a viable callback type. Use one of: '
                       . implode(', ', self::TYPES);
                $this->crud->error($msg, compact('types'));
                return null;
            }
        }

        return $types;
    }

    // TODO: add callback with optional identifiers

    /**
     *
     *
     * @param string|array  $type
     *      The type
     *
     * @param Closure|array $function
     *      The function
     *
     * @param array         $arguments
     *      Args
     */
    public function add($type, $function, array $arguments = []): void
    {
        $args = compact('type', 'function', 'arguments');

        // Parse the callback type. The callback function must be defined for each type.
        $types = $this->parse_type($type);
        if ( $types === null )
        {
            $msg = __FUNCTION__ . '(): Unable to parse the $type variable.';
            $this->crud->error($msg, $args);
            return;
        }
        /* $types is now guaranteed to be an array of supported callback types with a size of >= 1 element(s). */

        // Check if the caller supplied a valid function (exists and is callable).
        if ( !Func::is_callable($function, $this->crud) )
        {
            $msg = __FUNCTION__ . '(): The provided function "' . serialize($function) . '" does not exist or is not callable!';
            $this->crud->error($msg, $args);
            return;
        }

        // Wrap the function if it got defined as a closure, so that it can get serialized.
        if ( Func::is_closure($function) )
        {
            $function = new SerializableClosure($function);
        }

        // TODO: Maybe: Save the callback once and link to it from the specified types.

        foreach ( $types as &$type )
        {
            if ( !(isset($this->callbacks[$type]) and is_array($this->callbacks[$type])) )
            {
                $this->callbacks[$type] = [];
            }

            $callback = null;
            switch ( $type )
            {
                case 'beforerender':
                    $callback = new Beforerender_Callback($this->crud, $function, $arguments);
                    break;
                case 'beforeinsert':
                    $callback = new Beforeinsert_Callback($this->crud, $function, $arguments);
                    break;
                case 'afterinsert':
                    $callback = new Afterinsert_Callback($this->crud, $function, $arguments);
                    break;
                case 'beforeupdate':
                    $callback = new Beforeupdate_Callback($this->crud, $function, $arguments);
                    break;
                case 'afterupdate':
                    $callback = new Afterupdate_Callback($this->crud, $function, $arguments);
                    break;
                case 'beforeremove':
                    $callback = new Beforeremove_Callback($this->crud, $function, $arguments);
                    break;
                case 'afterremove':
                    $callback = new Afterremove_Callback($this->crud, $function, $arguments);
                    break;
                default:
                    die(__CLASS__ . ' - ' . __FUNCTION__ . '(): Unsupported type.');
            }
            array_push($this->callbacks[$type], $callback);
        }
    }

    /**
     * @param null|int      $id    The id of the currently displayed entry. May be null if no entry gets displayed.
     * @param null|stdClass $entry The entry or null if no entry is getting displayed.
     */
    public function perform_beforerender_callbacks(?int $id, ?stdClass $entry): void
    {
        foreach ( $this->get_callbacks('beforerender') as &$callback )
        {
            /** @var Beforerender_Callback $callback */
            $callback->perform($id, $entry);
        }
    }

    public function perform_beforeinsert_callbacks(array $postdata): array
    {
        // Stores whether or not the INSERT should be aborted by the application.
        $abort = false;

        // We begin with the initial postdata array. This will get modified with each callback that returned a
        // modified postdata array.
        $postdata_merged = $postdata;

        foreach ( $this->get_callbacks('beforeinsert') as &$callback )
        {
            /** @var Beforeinsert_Callback $callback */
            $result = $callback->perform($postdata);

            if ( $result === 'abort' )
            {
                $abort = true;
            }
            else if ( is_array($result) )
            {
                // A (potentially modified) postdata array got returned.
                // We merge all postdata arrays together to accommodate each result.
                $postdata_merged = array_merge($postdata_merged, $result);
            }
            else if ( !is_null($result) )
            {
                $msg = 'beforeinsert callback: You can only return a modified $postdata array or "abort"';
                $this->crud->error($msg);
            }
        }

        return compact('abort', 'postdata_merged');
    }

    public function perform_afterinsert_callbacks(int $id, stdClass $entry): void
    {
        foreach ( $this->get_callbacks('afterinsert') as &$callback )
        {
            /** @var Afterinsert_Callback $callback */
            $callback->perform($id, $entry);
        }
    }

    public function perform_beforeupdate_callbacks(int $id, stdClass $entry, array $postdata): array
    {
        // Stores whether or not the UPDATE should be aborted by the application.
        $abort = false;

        // We begin with the initial postdata array. This will get modified with each callback that returned a
        // modified postdata array.
        $postdata_merged = $postdata;

        foreach ( $this->get_callbacks('beforeupdate') as &$callback )
        {
            /** @var Beforeupdate_Callback $callback */
            $result = $callback->perform($id, $entry, $postdata);

            // The result is 'abort' if the user intentionally wanted to abort the update.
            if ( $result === 'abort' )
            {
                $abort = true;
            }
            else if ( is_array($result) )
            {
                // A (potentially modified) postdata array got returned.
                // We merge all postdata arrays together to accommodate each result.
                $postdata_merged = array_merge($postdata_merged, $result);
            }
            else if ( !is_null($result) )
            {
                $msg = 'beforeupdate callback: You can only return a modified $postdata array or "abort".';
                $this->crud->error($msg);
            }
        }

        return compact('abort', 'postdata_merged');
    }

    public function perform_afterupdate_callbacks(int $id, stdClass $entry): void
    {
        foreach ( $this->get_callbacks('afterupdate') as &$callback )
        {
            /** @var Afterupdate_Callback $callback */
            $callback->perform($id, $entry);
        }
    }

    public function perform_beforeremove_callbacks(int $id, stdClass $entry): bool
    {
        $abort = false;

        foreach ( $this->get_callbacks('beforeremove') as &$callback )
        {
            /** @var Beforeremove_Callback $callback */
            $result = $callback->perform($id, $entry);

            // We set the abort flag to true if one of the defined callbacks returned 'abort'.
            if ( $result === 'abort' )
            {
                $abort = true;
            }
        }

        return $abort;
    }

    public function perform_afterremove_callbacks(int $id, stdClass $entry): void
    {
        foreach ( $this->get_callbacks('afterremove') as &$callback )
        {
            /** @var Afterremove_Callback $callback */
            $callback->perform($id, $entry);
        }
    }

    public function get_callbacks(string $callback_type): array
    {
        // If no callbacks were defined for the given callback type, return a (traversable) callbacks-array of length 0.
        if ( !$this->has_callbacks($callback_type) )
        {
            return [];
        }

        return $this->callbacks[$callback_type];
    }

    public function has_callbacks(string $callback_type): bool
    {
        return array_key_exists($callback_type, $this->callbacks);
    }

}