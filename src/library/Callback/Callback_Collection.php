<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Crud;

/**
 * Class Callback_Collection
 * @package Lukaspotthast\Crud\Callback
 */
abstract class Callback_Collection
{

    /**
     * @var Crud
     */
    protected $crud;

    /**
     * Stores the defined callbacks.
     * @var array
     */
    protected $callbacks;

    /**
     * Callback_Collection constructor.
     * @param Crud $crud
     */
    public function __construct(Crud $crud)
    {
        $this->crud = $crud;
    }

    public function reset(): void
    {
        $this->callbacks = [];
    }

}