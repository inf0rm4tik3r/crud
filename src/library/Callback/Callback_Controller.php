<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Crud;

/**
 * Class Callback_Controller
 * @package Lukaspotthast\Crud\Callback
 */
class Callback_Controller
{

    /**
     * @var array
     */
    private $standard_callbacks;
    private $column_callbacks;
    private $field_callbacks;
    private $field_data_callbacks;

    /**
     * Callback_Controller constructor.
     * @param Crud $crud
     */
    public function __construct(Crud &$crud)
    {
        $this->standard_callbacks   = new Standard_Callbacks($crud);
        $this->column_callbacks     = new Column_Callbacks($crud);
        $this->field_callbacks      = new Field_Callbacks($crud);
        $this->field_data_callbacks = new Field_Data_Callbacks($crud);
    }

    public function reset(): void
    {
        $this->standard_callbacks->reset();
        $this->column_callbacks->reset();
        $this->field_callbacks->reset();
        $this->field_data_callbacks->reset();
    }

    public function standard_callbacks(): Standard_Callbacks
    {
        return $this->standard_callbacks;
    }

    public function column_callbacks(): Column_Callbacks
    {
        return $this->column_callbacks;
    }

    public function field_callbacks(): Field_Callbacks
    {
        return $this->field_callbacks;
    }

    public function field_data_callbacks(): Field_Data_Callbacks
    {
        return $this->field_data_callbacks;
    }

}