<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Support\Call;

abstract class Callback
{

    protected const NAME = 'unnamed_callback';

    /**
     * @var Crud
     */
    private $crud;

    /**
     * @var Call
     */
    private $callable;

    /**
     * Callback constructor.
     * @param       $crud
     * @param       $function
     * @param array $arguments
     */
    public function __construct(Crud $crud, callable $function, array $arguments)
    {
        $this->crud     = $crud;
        $this->callable = new Call($function, $arguments);
    }

    /**
     * @param array $arguments
     * @return mixed
     */
    public function execute(array $arguments)
    {
        return $this->callable->execute($arguments, $this->crud);
    }

    protected function get_crud(): Crud
    {
        return $this->crud;
    }

    protected function get_callable(): Call
    {
        return $this->callable;
    }

}