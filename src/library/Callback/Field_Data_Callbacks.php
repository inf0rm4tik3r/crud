<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback;

use Lukaspotthast\Crud\Callback\Types\Field_Data_Callback;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Support\Arg;
use Lukaspotthast\Crud\Support\Crud_Callable;
use Lukaspotthast\Crud\Support\Func;
use Opis\Closure\SerializableClosure;
use stdClass;

/**
 * Class Field_Data_Callbacks:
 * Stores its callbacks as: array[field_name => Field_Data_Callback]
 *
 * @package Lukaspotthast\Crud\Callback
 */
class Field_Data_Callbacks extends Callback_Collection
{

    /**
     * Field_Data_Callbacks constructor.
     * @param Crud $crud
     */
    public function __construct(Crud $crud)
    {
        parent::__construct($crud);
    }

    /**
     *
     */
    public function reset(): void
    {
        $this->callbacks = [];
    }

    /**
     * This function will call the defined callbacks for the given database entry. <br>
     * The entry_id may be NULL if this function is getting called for a 'default entry'! <br>
     * Assumes that the entry got previously modified through a call to process_loaded_data( [$entry] ). <br>
     * Updates the 'value' part of the entries elements if necessary. <br>
     *
     * @param stdClass $entry The entry which should be processed.
     */
    public function perform_callbacks(stdClass &$entry): void
    {
        $entry_id = $entry->{$this->crud->get_id_column_name()}['value'];

        foreach ( $entry as $field_name => &$field_info )
        {
            if ( $this->has($field_name) )
            {
                /** @var Field_Data_Callback $callback */
                $callback = $this->get($field_name);

                $result = $callback->perform($field_info['value'], $field_name, $entry_id, $entry);


                // It should only be allowed to return strings from a field data callback.
                if ( !is_string($result) )
                {
                    $this->crud->error(
                        __FUNCTION__.'(): A field_data_callback function must return a string, or false on error.',
                        [
                            'entry' => $entry,
                            'entry_id' => $entry_id,
                            'callback_function' => $callback->get_callable(),
                            'callback_arguments' => $callback->get_arguments()
                        ]
                    );
                    continue;
                }

                $field_info['value'] = $result;
            }
        }
    }

    /**
     * Provides the ability to define a function which gets called if the data for the specified field must be received.
     * The output from the specified function will be used instead of the value retrieved from the database.
     *
     * @param string $field_name The field for which the custom rendering function should be defined.
     * @param mixed $function Either a direct name of a function, or an array with 2 elements: an object, and a string
     *                                  that represents a member-function of that object.
     * @param array $arguments Optional: Array of additional data which gets passed to the specified function.
     *                                  Defaults to: array()
     * @param bool $override_existent Optional: A callback for the specified field could have already been defined.
     *                                  If this option is set to true, the old information gets overridden. Otherwise
     *                                  thiw gets prevented: Defaults to: false
     */
    public function add(string $field_name, $function, array $arguments = [], bool $override_existent = false): void
    {
        $args = compact('field_name', 'function', 'filepath', 'arguments', 'override_existent');

        // The field name must be specified as a non empty string.
        if ( !$this->assert->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return;
        }

        // The function must be specified.
        if ( !$this->assert->is_set(__FUNCTION__, $function, 'function', $args) )
        {
            return;
        }

        // The function must either be an array of length 2, a non empty sting, or a closure.
        if ( ! ((is_array($function) and count($function) === 2) or
                (is_string($function) and !empty($function)) or
                (Functions::is_closure($function)) ) )
        {
            $this->error(
                __FUNCTION__.'(): The function must either be specified as an array of length 2, a non empty string, '
                .'or a closure.', $args);
            return;
        }

        // Arguments must be specified as an array.
        if ( !$this->assert->is_array(__FUNCTION__, $arguments, 'arguments', $args) )
        {
            return;
        }

        // The override_existent parameter must be specified as a boolean.
        if ( !$this->assert->is_bool(__FUNCTION__, $override_existent, 'override_existent', $args) )
        {
            return;
        }

        // The field needs to be present in the current state of the instance.
        if ( !$this->assert->column_exists($this->table_set->get_main_table(), __FUNCTION__, $field_name, $args) )
        {
            return;
        }

        // If a callback for this column was already defined and overwriting it was not allowed, an error gets printed.
        if ( $this->has($field_name) and $override_existent === false )
        {
            $msg = __FUNCTION__.'(): A field data callback for "'.$field_name.'" is already defined, but '
                .'overwriting it was not allowed. The callback could not be set!';
            $this->crud->error($msg, $args);
            return;
        }

        // The provided function must exists and be callable.
        if ( !Func::is_callable($function, $this->crud) )
        {
            $msg = __FUNCTION__.'(): The provided function "'.Arg::stringify($function)
                .'" does not exist or is not callable!';
            $this->crud->error($msg, $args);
            return;
        }

        // Closures must be wrapped in a SerializableClosure object, so the they can get serialized and unserialized.
        if ( Func::is_closure($function) )
        {
            $function = new SerializableClosure($function);
        }

        // If everything went good, the callback information gets added to the instance.
        $this->callbacks[$field_name] = new Field_Data_Callback($this->crud, $function, $arguments);
    }

    /**
     * Determines whether or not a field data callback is set for the specified field.
     *
     * @param string $field_name The field name to check.
     * @return bool True if a callback is defined, false otherwise.
     */
    public function has(string $field_name): bool
    {
        $args = compact('field_name');

        if ( !$this->crud->get_assert()->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return false;
        }

        // There cannot be a defined field callback for a non existent field. A check is not necessary.

        return array_key_exists($field_name, $this->callbacks);
    }

    /**
     * Retrieves the stored callback information (function, filepath, arguments) for the specified field.
     * Remember: Use the has_field_data_callback($field) method first to check whether or not a callback does exist.
     *
     * @param string $field_name The field name to check.
     * @return Crud_Callable The Crud_Callable object. Use its execute() function to execute the callback.
     */
    public function get(string $field_name): Crud_Callable
    {
        $args = compact('field_name');

        if ( !$this->crud->get_assert()->is_non_empty_string(__FUNCTION__, $field_name, 'field_name', $args) )
        {
            return null;
        }

        // There cannot be a defined field callback for a non existent field. A check is not necessary.

        return $this->callbacks[$field_name];
    }

}