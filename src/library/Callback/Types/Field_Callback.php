<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Field_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Field_Callback
 */
class Field_Callback extends Callback
{

    /**
     * @param          $field_value
     * @param string   $field_name
     * @param          $entry_id
     * @param stdClass $entry
     * @param array    $additional_arguments
     * @return mixed
     */
    public function perform($field_value, string $field_name, $entry_id, stdClass $entry, array $additional_arguments = [])
    {
        $arguments = [
            'field_value'      => $field_value,
            'field_name'       => $field_name,
            'primary_key'      => $entry_id,
            'entry_data'       => $entry,
            'crud'             => parent::get_crud(),
            'custom_arguments' => array_merge(parent::get_callable()->get_arguments(), $additional_arguments),
        ];

        return parent::execute($arguments);
    }

}