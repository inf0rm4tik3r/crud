<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Afterinsert_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Afterinsert_Callback
 */
class Afterinsert_Callback extends Callback
{

    public const NAME = 'afterinsert';

    public function perform(int $id, stdClass $entry): void
    {
        $arguments = [
            'primary_key' => $id,
            'entry'       => $entry,
            'postdata'    => null,

            'callback_task'    => self::NAME,
            'crud'             => parent::get_crud(),
            'custom_arguments' => parent::get_callable()->get_arguments(),
        ];

        parent::execute($arguments);
    }

}