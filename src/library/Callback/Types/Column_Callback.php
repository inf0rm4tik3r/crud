<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Column_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Column_Callback
 */
class Column_Callback extends Callback
{

    /**
     * @param mixed    $cell_data
     * @param string   $column_name
     * @param          $entry_id
     * @param stdClass $entry
     * @param array    $additional_arguments
     * @return mixed
     */
    public function perform($cell_data, string $column_name, $entry_id, stdClass $entry, array $additional_arguments = [])
    {
        $arguments = [
            'field_value'      => $cell_data,
            'field_name'       => $column_name,
            'primary_key'      => $entry_id,
            'entry_data'       => $entry,
            'crud'             => parent::get_crud(),
            'custom_arguments' => array_merge(parent::get_callable()->get_arguments(), $additional_arguments),
        ];

        return parent::execute($arguments);
    }

}