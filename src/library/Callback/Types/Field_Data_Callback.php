<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Field_Data_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Field_Data_Callback
 */
class Field_Data_Callback extends Callback
{

    public function perform($field_value, string $field_name, $entry_id, stdClass $entry) /* : mixed */
    {
        $arguments = [
            'field_value'      => $field_value,
            'field_name'       => $field_name,
            'primary_key'      => $entry_id,
            'entry_data'       => $entry,
            'crud'             => parent::get_crud(),
            'custom_arguments' => parent::get_callable()->get_arguments(),
        ];

        return parent::execute($arguments);
    }

}