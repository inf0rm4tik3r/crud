<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;

/**
 * Class Beforeinsert_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Beforeinsert_Callback
 */
class Beforeinsert_Callback extends Callback
{

    public const NAME = 'beforeinsert';

    public function perform(array $postdata): array
    {
        $arguments = [
            'primary_key' => null,
            'entry'       => null,
            'postdata'    => $postdata,

            'callback_task'    => self::NAME,
            'crud'             => parent::get_crud(),
            'custom_arguments' => parent::get_callable()->get_arguments(),
        ];

        return parent::execute($arguments);
    }

}