<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Beforeupdate_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Beforeupdate_Callback
 */
class Beforeupdate_Callback extends Callback
{

    public const NAME = 'beforeupdate';

    public function perform(int $id, stdClass $entry, array $postdata)
    {
        $arguments = [
            'primary_key' => $id,
            'entry'       => $entry,
            'postdata'    => $postdata,

            'callback_task'    => self::NAME,
            'crud'             => parent::get_crud(),
            'custom_arguments' => parent::get_callable()->get_arguments(),
        ];

        return parent::execute($arguments);
    }

}