<?php

declare(strict_types = 1);

namespace Lukaspotthast\Crud\Callback\Types;

use Lukaspotthast\Crud\Callback\Callback;
use stdClass;

/**
 * Class Beforeremove_Callback
 * @package Lukaspotthast\Crud\Callback\Types\Beforeremove_Callback
 */
class Beforeremove_Callback extends Callback
{

    public const NAME = 'beforeremove';

    public function perform(int $id, stdClass $entry) /* ? */
    {
        $arguments = [
            'primary_key' => $id,
            'entry'       => $entry,
            'postdata'    => null,

            'callback_task'    => self::NAME,
            'crud'             => parent::get_crud(),
            'custom_arguments' => parent::get_callable()->get_arguments(),
        ];

        return parent::execute($arguments);
    }

}