<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Support\Args;

/** @var $crud Crud */
?>

<div class="crud-error">

    <?php
    $continue_on_error = Translator::get('crud.continue_on_error.text');
    $link_name         = Translator::get('crud.continue_on_error.link_replacement');

    $link              = '<a href="#" data-action="reload" data-after="list">' . $link_name . '</a>';
    $continue_on_error = str_replace('{link}', $link, $continue_on_error);
    ?>

    <div class="crud-continue">
        <span style="padding-left: 0.2em">
            <i class="fa fa-long-arrow-right"></i> <?= $continue_on_error ?>
        </span>
    </div>
    <br>

    <div class="crud-message">
        <div class="crud-alert crud-alert-danger" data-crud-alert>

            CRUD: <?= Translator::get('crud.errors_occurred') ?>:<br><br>

            <ul>
                <?php foreach ( $crud->get_errors() as &$error ): ?>
                    <li style="margin-bottom: 1em;">
                        <b><?= $error['msg'] ?></b>

                        <?php if ( isset($error['args']) ): ?>
                            <br>
                            Arguments passed:<br>
                            <?= Args::as_list($error['args']) ?>
                        <?php endif; ?>

                        <?php if ( isset($error['backtrace']) ): ?>
                            <br>
                            <b>Backtrace</b>:<br>
                            <div class="crud-backtrace">
                                <?= nl2br(htmlspecialchars($error['backtrace'])) ?>
                            </div>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>

        </div>
    </div>

</div>