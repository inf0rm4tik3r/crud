<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;

/** @var $crud Crud */
/** @var $crud_instance_html_array array */
?>

<?php // BASE ?>
    <div class="crud-base">
        <?= array_key_exists('base', $crud_instance_html_array) ? $crud_instance_html_array['base'] : '' ?>
    </div>

<?php // HEADER ?>
    <div class="crud-header">
        <?= array_key_exists('header', $crud_instance_html_array) ? $crud_instance_html_array['header'] : '' ?>
    </div>

<?php // BODY ?>
    <div class="crud-body">
        <?= array_key_exists('body', $crud_instance_html_array) ? $crud_instance_html_array['body'] : '' ?>
    </div>

<?php // FOOTER ?>
<?php /*
<div class="crud-footer">
    <?= array_key_exists('footer', $crud_instance_html_array) ? $crud_instance_html_array['footer'] : '' ?>
</div>
*/ ?>