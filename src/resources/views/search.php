<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $columns array */

/** @var $default_entry stdClass */
$default_entry = $crud->get_table_set()->create_default_entry();
?>

<div class="crud-search-container">

    <?php // Fields row ?>
    <div class="crud-row">

        <?php foreach ( $columns as &$col /** @var Column_Schema $col */ ): ?>
            <div class="crud-col-sm-6 crud-col-md-4 crud-col-xl-3">
                <div class="form-group">
                    <div class="crud-search-field-label">
                        <?= $crud->labeled($col->get_name()) ?>
                    </div>

                    <div class="crud-search-input">
                        <?=
                        Renderer::render_view('fields.field', [
                            'crud'            => $crud,
                            'entry_id'        => null,
                            'col'             => $col,
                            'data'            => $default_entry,
                            'as_search_input' => true,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>

    <?php // Button row ?>
    <div class="crud-row crud-nav crud-mb-0">

        <div class="crud-col crud-btn-wrapper">
            <?php // Start search ?>
            <button type="button" class="crud-btn crud-btn-primary" data-action="apply-search">
                <i class="fa fa-search" aria-hidden="true"></i>&nbsp;
                <?= Translator::get('crud.search') ?>
            </button>
        </div>

        <div class="crud-col crud-col-flex-end crud-btn-wrapper">
            <?php // Reset ?>
            <button type="button" class="crud-btn crud-btn-primary" data-task="reset-search">
                <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;
                <?= Translator::get('crud.reset_search') ?>
            </button>
        </div>

    </div>

</div> <?php // .crud-search-container ?>
