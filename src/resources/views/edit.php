<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $fields array */
/** @var $data stdClass */

// If the current view is not allowed to be rendered, we can simply add an error and abort the rendering.
if ( !$crud->is_allowed('edit') )
{
    $crud->error('The "edit" view is not allowed!');
    return;
}

$has_nav_elements = (
    ($crud->is_allowed('save') and $crud->is_shown('save_btn')) or
    ($crud->is_allowed('save') and $crud->is_shown('save_and_return_btn')) or
    ($crud->is_allowed('delete') and $crud->is_shown('delete_btn')) or
    ($crud->is_allowed('list') and $crud->is_shown('list_btn'))
);

// Render currently available messages.
echo Renderer::render_view('messages', ['crud' => $crud]);
?>

<?php if ( $has_nav_elements ): ?>

    <div class="crud-row crud-nav crud-mt-0">

        <div class="crud-col crud-btn-wrapper">
            <?php if ( $crud->is_allowed('save') and $crud->is_shown('save_btn') ): ?>
                <button type="button"
                        data-action="save"
                        data-after="edit"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-hdd-o" aria-hidden="true"></i> <?= Translator::get('crud.save') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_allowed('save') and $crud->is_shown('save_and_return_btn') ): ?>
                <button type="button"
                        data-action="save"
                        data-after="list"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-hdd-o" aria-hidden="true"></i> <?= Translator::get('crud.save_return') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_allowed('delete') and $crud->is_shown('delete_btn') ): ?>
                <?php if ( $crud->uses_soft_delete() ): ?>

                    <?
                    // Check if the current entry got soft-deleted. Show either a soft-delete or a restore button.
                    if ( $data->{$crud->get_soft_delete_column()}['value'] === 0 ):
                        ?>
                        <button type="button"
                                data-action="delete"
                                data-after="edit"
                                data-confirm-method="modal"
                                data-modal-template="soft-delete"
                                class="crud-btn crud-btn-danger">
                            <i class="fa fa-trash" aria-hidden="true"></i> <?= Translator::get('crud.delete') ?>
                        </button>
                    <?php elseif ( $data->{$crud->get_soft_delete_column()}['value'] === 1 ): ?>
                        <button type="button"
                                data-action="restore"
                                data-after="edit"
                                data-confirm-method="modal"
                                data-modal-template="restore"
                                class="crud-btn crud-btn-success">
                            <i class="fa fa-arrow-circle-up"
                               aria-hidden="true"></i> <?= Translator::get('crud.restore') ?>
                        </button>
                    <?php endif; ?>

                <?
                // If soft-delete is not used. Show the default delete button.
                else:
                    ?>
                    <button type="button"
                            data-action="delete"
                            data-after="list"
                            data-confirm-method="modal"
                            data-modal-template="delete"
                            class="crud-btn crud-btn-danger">
                        <i class="fa fa-trash" aria-hidden="true"></i> <?= Translator::get('crud.delete') ?>
                    </button>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="crud-col crud-col-flex-end crud-btn-wrapper">
            <button type="button"
                    data-action="list"
                    class="crud-btn crud-btn-default">
                <i class="fa fa-table" aria-hidden="true"></i> <?= Translator::get('crud.listview') ?>
            </button>
        </div>

    </div>

<?php endif; ?>

<?php if ( $crud->is_shown('entry_info') ): ?>
    <div class="crud-entry">
        <?= Translator::get('crud.entry_nr') ?> <?= $data->id['value'] ?>
    </div>
<?php endif; ?>

<div data-fields>
    <?=
    Renderer::render_view('fields.fields_layout', [
        'crud'         => $crud,
        'fields'       => $fields,
        'field_layout' => $crud->get_field_layout(true),
        'tab_count'    => 0,
        'data'         => $data,
    ]);
    ?>
</div>