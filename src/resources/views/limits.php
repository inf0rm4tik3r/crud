<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;

/** @var $crud Crud */

$pagination     = $crud->get_paginator()->get_pagination();
$active_classes = 'crud-btn-active crud-btn-disabled';
?>

<?php if ( $crud->is_shown('limits') ): ?>
    <div class="crud-limit-options">
        <div class="crud-btn-wrapper crud-btn-group">
            <?php foreach ( $crud->get_result_limit_options() as &$option ): ?>
                <?php
                $current = is_null($pagination->get_results_per_page()) ? -1 : $pagination->get_results_per_page();
                $active  = $option['limit'] === $current;
                ?>
                <button type="button"
                        data-action="limit"
                        data-limit="<?= $option['limit'] ?>"
                        class="crud-btn crud-btn-md crud-btn-default <?= $active ? $active_classes : '' ?>">
                    <?= $option['text'] ?>
                </button>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>