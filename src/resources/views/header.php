<?php
// All HTML markup from this file will be Placed inside a ".crud-header" div.

declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;

/** @var $crud Crud */
?>

<?php if ( $crud->is_shown('header') ): ?>
    <div class="crud-row">

        <div class=" crud-col-md-8 crud-col-sm-7 crud-col-6 ">
            <?php if ( $crud->is_shown('title') ): ?>
                <span class="crud-instance-title">
                    <?= $crud->labeled($crud->get_table_set()->get_main_table()->get_name()) ?>
                </span>
            <?php endif; ?>
        </div>

        <div class="  crud-col-md-4 crud-col-sm-5 crud-col-6 crud-col-flex-end">
            <ul class="crud-icon-list">
                <li class="crud-icon-list-item" <?php /*data-action="open-header-menu" */ ?>>
                    <i class="fa fa-ellipsis-v"></i>
                </li>

                <li class="crud-icon-list-item" data-action="toggle-fullscreen">
                    <i class="fa fa-arrows-alt"></i>
                </li>

                <li class="crud-icon-list-item" data-action="reload">
                    <i class="fa fa-refresh"></i>
                </li>

                <li class="crud-icon-list-item" data-action="show-system-info">
                    <i class="fa fa-info"></i>
                </li>
            </ul>
        </div>

    </div>
<?php endif; ?>