<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Search\Search_Tab;
use Lukaspotthast\Crud\Security\XSS;

/** @var $crud Crud */
?>

<?php if ( $crud->has_search_tabs() ): ?>
    <div class="crud-search-tabs">
        <ul class="nav justify-content-center">

            <?php foreach ( $crud->get_search_tabs() as &$search_tab /** @var $search_tab Search_Tab */ ): ?>
                <li class="nav-item crud-search-tab <?= $search_tab->is_active() ? 'active' : '' ?>">

                    <span class="nav-link"
                          data-action="toggle-search-tab"
                          data-search-tab="<?= XSS::secure($search_tab->get_internal_name()) ?>"
                    >
                        <?php if ( $search_tab->has_icon_classes() ): ?>
                            <i class="<?= XSS::secure($search_tab->get_icon_classes()) ?>"></i>
                        <?php endif; ?>

                        <?= XSS::secure($search_tab->get_display_name()) ?>
                    </span>

                </li>
            <?php endforeach; ?>

        </ul>
    </div>
<?php endif; ?>