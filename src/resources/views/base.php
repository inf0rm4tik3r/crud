<?php
// Crud configuration which is send to the browser with every render output.

declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Support\Date;

/** @var $crud Crud */
?>

    <input type="hidden" name="crud_state" value="<?=
    htmlspecialchars(json_encode([
        'instance_name'             => $crud->get_instance_name(),
        'instance_verification_key' => $crud->get_verification_key(),
        'instance_is_loaded'        => $crud->is_loaded() ? 'true' : 'false',
        'instance_is_nested'        => $crud->is_nested() ? 'true' : 'false',
        'parent_instance_name'      => $crud->get_parent_instance_name() !== null ? ($crud->get_parent_instance_name()) : null,

        'task'  => $crud->get_task(),
        'entry' => $crud->get_current_entry_id(),
        //'pagination'                => $crud->get_paginator()->get_pagination()->to_json(),
        //'ordered_by'                => json_encode($crud->get_order_by_columns()),

        'date_format'           => $crud->get_date_format(),
        'date_separator'        => $crud->get_date_separator(),
        'time_format'           => $crud->get_time_format(),
        'moment_date_format'    => Date::convert_php_to_moment_format($crud->get_date_format()),
        'moment_date_separator' => Date::convert_php_to_moment_format($crud->get_date_separator()),
        'moment_time_format'    => Date::convert_php_to_moment_format($crud->get_time_format()),

        'custom_validators'       => json_encode($crud->get_custom_validators()),
        'custom_async_validators' => json_encode($crud->get_custom_async_validators()),

        'texteditor_driver'  => $crud->get_texteditor_driver(),
        'select_driver'      => $crud->get_select_driver(),
        'colorpicker_driver' => $crud->get_colorpicker_driver(),

        'is_inlineedit'        => $crud->is_inlineedit() ? 'true' : 'false',
        'inlineedit_mode'      => $crud->get_inlineedit_mode(),
        'inlineedit_activator' => $crud->get_inlineedit_activator(),

        'language_short' => Crud::get_language(),
        'translations'   => json_encode($crud->get_translations()),
    ]))
    ?>">

<?php // This overlay is displayed while the instance waits for an ajax call to return. ?>
    <div class="crud-overlay"></div>
    <div class="crud-overlay-spinner">
        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
        <span class="sr-only">Loading...</span>
    </div>

<?php // Basic Bootstrap modal templates. Reused by the JavaScript code for each notification. Data needs to be provided! ?>
<?= Renderer::render_view('modal_templates', ['crud' => $crud]); ?>