<?php

declare(strict_types = 1);

use Lukaspotthast\Crud\Asset\Asset;
use Lukaspotthast\Crud\Config\Config;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Frameworks\Frameworks;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $crud_instance_html_array array */

$wrapper_classes = '';

// Check if the color blindness corrections should be applied.
$enable_color_blindness_corrections = Config::get('crud.enable_color_blindness_corrections');
if ( !$crud->get_assert()->is_bool(
    'wrapper.blade.php',
    $enable_color_blindness_corrections,
    'enable_color_blindness_corrections',
    []
) )
{
    $enable_color_blindness_corrections = false;
}
if ( $enable_color_blindness_corrections === true )
{
    $wrapper_classes .= 'color-blindness-corrections ';
}

// Add classes defined by the user.
$wrapper_classes .= implode(' ', $crud->get_wrapper_classes());
?>

<div class="crud-instance-wrapper <?= $wrapper_classes ?>">
    <?php // The wrapper is only getting rendered when the render() function gets executed from a GET-route. ?>
    <?php // Here we should define all resources needed by the crud system. ?>

    <?php
    echo Asset::get_if_autoloaded('crud');
    echo Asset::get_if_autoloaded('crud_minified');
    echo Asset::get_if_autoloaded('jquery');
    echo Asset::get_if_autoloaded('jcrop');
    echo Asset::get_if_autoloaded('jqueryui');
    echo Asset::get_if_autoloaded('popper.js');
    echo Asset::get_if_autoloaded('tooltip.js');
    echo Asset::get_if_autoloaded('fontawesome');
    echo Asset::get_if_autoloaded('copybtn');
    echo Asset::get_if_autoloaded('datetimepicker');
    echo Asset::get_if_autoloaded('tinymce');
    echo Asset::get_if_autoloaded('ckeditor');
    echo Asset::get_if_autoloaded('select2');
    echo Asset::get_if_autoloaded('signature-pad');
    echo Asset::get_if_autoloaded('spectrum');
    echo Asset::get_if_autoloaded('moment.js');
    echo Asset::get_if_autoloaded('validate.js');
    ?>

    <?php // DELETED: Basic csrf protection for ajax requests. ?>
    <?php if ( Crud::get_framework() === Frameworks::LARAVEL ): ?>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': "<?= csrf_token() ?>"
                }
            });
        </script>
    <?php endif; ?>

    <?php // The rendering of the crud instance goes inside the "crud-instance" div! ?>
    <div class="crud-instance">
        <?= Renderer::render_view('instance', compact('crud', 'crud_instance_html_array')); ?>
    </div>
</div>
