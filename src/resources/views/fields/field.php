<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Callback\Types\Field_Callback;
use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $col Column_Schema */
/** @var $data stdClass */
/** @var $entry_id string */
/** @var $as_search_input boolean */

if ( !isset($as_search_input) )
{
    $as_search_input = false;
}

// Let the user render the field..
if ( $crud->callback_controller()->field_callbacks()->has($col->get_name()) )
{
    /** @var Field_Callback $callback */
    $callback = $crud->callback_controller()->field_callbacks()->get($col->get_name());
    echo $callback->perform($data->{$col->get_name()}, $col->get_name(), $entry_id, $data, compact('as_search_input'));
}
// ..or use the default renderer.
else
{
    $args = [
        'crud'            => $crud,
        'field'           => $col,
        'data'            => isset($data->{$col->get_name()}) ? $data->{$col->get_name()} : '',
        'as_search_input' => $as_search_input,

        'constraints_json' => htmlspecialchars($crud->get_validation_ruleset($col->get_name())->get_rules_json()),
        'behaviours_json'  => htmlspecialchars($crud->behaviour_controller()->get_field_behaviours_as_json($col->get_name())),
        'editable'         => 'true' //$crud->is_editable()
    ];

    switch ( $col->get_type()->get_name() )
    {
        case 'varchar':
            echo Renderer::render_view('fields.types.varchar', $args);
            break;

        case 'enum':
            echo Renderer::render_view('fields.types.enum', $args);
            break;

        case 'tinytext':
        case 'mediumtext':
        case 'text':
        case 'longtext':
            echo Renderer::render_view('fields.types.text', $args);
            break;

        case 'tinyint':
        case 'bool':
            if ( $col->get_type()->get_name() === 'tinyint' and $col->get_type()->get_display_length() === 1 )
            {
                echo Renderer::render_view('fields.types.boolean', $args);
            }
            else
            {
                echo Renderer::render_view('fields.types.int', $args);
            }
            break;

        case 'smallint':
        case 'mediumint':
        case 'int':
        case 'bigint':
            echo Renderer::render_view('fields.types.int', $args);
            break;

        case 'float':
        case 'double':
        case 'decimal':
            echo Renderer::render_view('fields.types.decimal', $args);
            break;

        case 'date':
            echo Renderer::render_view('fields.types.date', $args);
            break;

        case 'time':
            echo Renderer::render_view('fields.types.time', $args);
            break;

        case 'datetime':
        case 'timestamp':
            echo Renderer::render_view('fields.types.datetime', $args);
            break;

        case 'tinyblob':
        case 'mediumblob':
        case 'blob':
        case 'longblob':
            echo Renderer::render_view('fields.types.blob', $args);
            break;

        default:
            echo '
                <div class="alert alert-danger">
                    No field renderer for field type "' . $col->get_type()->get_name() . '" available.
                </div>
            ';
            break;
    }
}