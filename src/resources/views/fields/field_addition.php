<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;

/** @var $crud Crud */
/** @var $id string */
/** @var $field Column_Schema */
?>

<?php // Validation message output. ?>
    <div id="<?= $id ?>-validation-msg-output" class="crud-validation-msg-output"></div>

<?php // Display a field info if defined. ?>
<?php if ( $crud->has_field_info($field->get_name()) and !$crud->in_view() ): ?>
    <div class="crud-field-info">
        <?= $crud->get_field_info($field->get_name()) ?>
    </div>
<?php endif; ?>