<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

// Use different HTML markup if a search input gets rendered.
if ( isset($as_search_input) and $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';
?>

<?php if ( $crud->in_create() or $crud->in_edit() ): ?>
    <div class="input-group">
        <label class="input-group-btn crud-margin-0">
            <span class="btn btn-secondary btn-md">
                <?= Translator::get('crud.browse') ?>
                <input
                        id="<?= $id ?>"
                        data-crud-input
                        data-group="<?= $group ?>"
                        data-validation-constraints="<?= $constraints_json ?>"
                        data-behaviours="<?= $behaviours_json ?>"
                        data-editable="<?= $editable ?>"
                        type="file"
                        data-db-type="file"
                        style="display: none;"
                        multiple
                >
            </span>
        </label>
        <input type="text" class="<?= $classes ?>" readonly>
    </div>

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>

<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>
    -blob-

    <?php // Unsupported task. ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>
<?php endif; ?>
