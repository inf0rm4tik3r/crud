<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $data array */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

$final_data_unprotected = $data['value'] === null ?
    $data['default'] :
    $data['value'];
$final_data             = XSS::secure($final_data_unprotected);

// Use different HTML markup if a search input gets rendered.
if ( isset($as_search_input) and $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';
?>

<?php // CREATE, EDIT or LIST ?>
<?php if ( $crud->in_create() or $crud->in_edit() ): ?>

    <?php // Default text field ?>
    <?php if ( $data['display_type'] === 'default' or $data['display_type'] === 'textarea' ): ?>
        <textarea
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                spellcheck="true"
                rows="4"
        ><?= $final_data ?></textarea>

        <?php // Texteditor ?>
    <?php elseif ( $data['display_type'] === 'texteditor' ): ?>
        <textarea
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                spellcheck="true"
        ><?= $final_data ?></textarea>

        <?php // Color ?>
    <?php elseif ( $data['display_type'] === 'color' ): ?>
        <input
                type="text"
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-inline="<?= $crud->is_inlined($field->get_name()) ? 'true' : 'false' ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                value="<?= $final_data ?>"
        >

        <?php // Code ?>
    <?php elseif ( $data['display_type'] === 'code' ): ?>
        <button
                type="button"
                class="btn btn-secondary crud-code-flip-button"
                data-action="edit-code"
                data-show="<?= $id ?>"
                data-hide="<?= $id ?>-view"
        >
            <?= Translator::get('crud.code_highlight.edit') ?>
        </button>

        <button
                type="button"
                class="btn btn-secondary crud-code-flip-button"
                data-action="view-code"
                data-show="<?= $id ?>-view"
                data-hide="<?= $id ?>"
        >
            <?= Translator::get('crud.code_highlight.view') ?>
        </button>

        <textarea
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-editable="true"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                spellcheck="false"
                rows="8"
        ><?= $final_data ?></textarea>

        <pre
                id="<?= $id ?>-view"
                class="<?= $classes ?> crud-code-highlight"
        ><?= $final_data ?></pre>

        <?php // Signature ?>
    <?php elseif ( $data['display_type'] === 'signature' ): ?>
        <?php
        // Prepare the current signature data, so that it can be processed through JavaScript.
        $signature_data = json_decode($final_data_unprotected);
        $final_data     = json_encode('');

        if ( !is_null($signature_data) )
        {
            if ( isset($signature_data->data) )
            {
                $final_data = json_encode($signature_data->data);
            }
            else if ( isset($signature_data->data_url) )
            {
                $final_data = $signature_data->data_url;
            }
        }
        ?>
        <div class="crud-signature-pad">
            <div class="crud-signature-pad-controls">
                <div class="crud-btn-wrapper">
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-unlock-button"
                            data-action="unlock-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-unlock-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.unlock_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.unlock_tooltip') ?>"
                        <?php endif; ?>
                            style="display: block"
                    >
                        <?= Translator::get('crud.signature_pad.unlock') ?>
                    </button>
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-lock-button"
                            data-action="lock-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-lock-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.lock_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.lock_tooltip') ?>"
                        <?php endif; ?>
                            style="display: none"
                    >
                        <?= Translator::get('crud.signature_pad.lock') ?>
                    </button>
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-draw-button"
                            data-action="draw-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-clear-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.draw_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.draw_tooltip') ?>"
                        <?php endif; ?>
                            style="display: none"
                    >
                        <?= Translator::get('crud.signature_pad.draw') ?>
                    </button>
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-erase-button"
                            data-action="erase-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-clear-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.erase_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.erase_tooltip') ?>"
                        <?php endif; ?>
                            style="display: none"
                    >
                        <?= Translator::get('crud.signature_pad.erase') ?>
                    </button>
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-undo-button"
                            data-action="undo-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-clear-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.undo_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.undo_tooltip') ?>"
                        <?php endif; ?>
                            style="display: none"
                    >
                        <?= Translator::get('crud.signature_pad.undo') ?>
                    </button>
                    <button
                            type="button"
                            class="crud-btn crud-btn-secondary crud-signature-pad-clear-button"
                            data-action="clear-signature-pad"
                            data-input-ref="<?= $id ?>"
                            data-clear-btn
                        <?php if ( !empty(Translator::get('crud.signature_pad.clear_tooltip')) ): ?>
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= Translator::get('crud.signature_pad.clear_tooltip') ?>"
                        <?php endif; ?>
                            style="display: none"
                    >
                        <?= Translator::get('crud.signature_pad.clear') ?>
                    </button>
                </div>
            </div>
            <div class="crud-signature-pad-canvas-wrapper">
                <canvas
                        id="<?= $id ?>"
                        class="crud-signature-pad-canvas"
                        data-crud-input
                        data-group="<?= $group ?>"
                        data-db-type="<?= $field->get_type()->get_name() ?>"
                        data-internal-type="<?= $data['internal_type'] ?>"
                        data-display-type="<?= $data['display_type'] ?>"
                        data-data='<?= $final_data ?>'
                        data-validation-constraints="<?= $constraints_json ?>"
                        data-behaviours="<?= $behaviours_json ?>"
                        data-editable="<?= $editable ?>">
                </canvas>
                <div class="crud-signature-pad-canvas-overlay"></div>
            </div>
        </div>

        <?php // Unknown type ?>
    <?php else: ?>
        <div class="alert alert-danger">
            Unknown internal_type... ({{ $data['internal_type'] }})
        </div>
    <?php endif; ?>

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>

    <?php // VIEW, LIST ?>
<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>

    <?php if ( $data['internal_type'] === 'texteditor' ): ?>
        <div class="crud-texteditor-view">
            <?= $final_data_unprotected ?>
        </div>

        <?php // SIGNATURE ?>
    <?php elseif ( $data['internal_type'] === 'signature' ): ?>
        <?php
        // Prepare the current signature data, so that it can be processed through JavaScript.
        $signature_data = json_decode($final_data_unprotected);
        ?>

        <?php if ( !is_null($signature_data) and isset($signature_data->data_url) ): ?>
            <div class="crud-signature-image-wrapper">
                <img src="<?= $signature_data->data_url ?>" class="crud-signature-image">
            </div>
        <?php endif; ?>

        <?php // Code ?>
    <?php elseif ( $data['internal_type'] === 'code' ): ?>
        <textarea
                id="<?= $id ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="false"
                style="display: none"
        ></textarea>

        <pre
                id="<?= $id ?>-view"
                class="<?= $classes ?>"
        ><?= $final_data ?></pre>

    <?php else: ?>
        <?= $final_data ?>
    <?php endif; ?>

    <?php // Unsupported task ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>

<?php endif; ?>

