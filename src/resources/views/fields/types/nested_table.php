<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $field_name string */

if ( $crud->in_create() )
{
    // All entries in a nested table get connected to a "parent entry".
    // Therefore nested table instances are not accessible if the parent entry did not got created jet.
    echo Renderer::render_view('no_entry_no_edit', []);
}
else
{
    $instance = $crud->get_nested_table_instance($field_name);

    // Whenever a nested table instance is getting rendered through this file,
    // the instances wrapper must be rendered with it!
    // So: regardless of who the requester was until now, we set it to 'main', which is our default and will lead
    // to a full rendering.
    $instance->set_requester('main');

    echo $instance->render();
}

// Closing tag is necessary for view-compilation!!
?>