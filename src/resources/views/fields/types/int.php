<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $data array */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

$final_data = $data['value'] === null ?
    $data['default'] :
    $data['value'];
// Only the relevant entries from the array should get secured.
// Which will take place at the direct output position.
if ( !is_array($final_data) )
{
    $final_data = XSS::secure($final_data);
}


// Use different HTML markup if a search input gets rendered.
if ( isset($as_search_input) and $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';
?>

<?php // CREATE, EDIT ?>
<?php if ( $crud->in_create() or $crud->in_edit() ): ?>

    <?php // Default ?>
    <?php if ( $data['display_type'] === 'default' ): ?>
        <input
                type="number"
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-inline="<?= $crud->is_inlined($field->get_name()) ? 'true' : 'false' ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                value="<?= $final_data ?>"
                maxlength="<?= $field->get_type()->get_length() ?>"
                spellcheck="false"
        >

        <?php // Select ?>
    <?php elseif ( ($data['display_type'] === 'select' or $data['display_type'] === 'multiselect') and is_array($data['value']) ): ?>
        <select
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>" <?php // select / multiselect ?>
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                style="width: 100%;"
        >
            <?php foreach ( $final_data as $key => &$option ): ?>
                <?
                // Determine if the current value should be selected.
                $selected = $crud->in_create() ? ($key == $data['default']) : $option['is_current'];
                ?>
                <option <?= $selected ? 'selected="selected"' : '' ?> value="<?= $key ?>">
                    <?= $option['is_selectable'] ? '' : '[-]' ?> <?= XSS::secure($option['value']) ?><?php // TODO: Why should an option not be selectable? ?>
                </option>
            <?php endforeach; ?>
        </select>

        <?php // unknown ?>
    <?php else: ?>
        <div class="alert alert-danger">
            Unknown display type... ({{ $data['display_type'] }})
        </div>
    <?php endif; ?>

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>

    <?php // VIEW, LIST ?>
<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>

    <?php // Default ?>
    <?php if ( $data['internal_type'] === 'default' ): ?>
        <?= $final_data ?>

        <?php // Relation ?>
    <?php elseif ( $data['display_type'] === 'select' and is_array($final_data) ): ?>
        <?php foreach ( $final_data as &$option ): ?>
            <?php if ( $option['is_current'] ): ?>
                <?= $option['value'] ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php // unknown display type ?>
    <?php else: ?>
        <div class="alert alert-danger">
            Unknown display type...
        </div>
    <?php endif; ?>

    <?php // Unsupported task ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>

<?php endif; ?>

