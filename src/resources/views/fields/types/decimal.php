<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $data array */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

$final_data = $data['value'] === null ?
    $data['default'] :
    $data['value'];
$final_data = XSS::secure($final_data);

// "step" calculation:
$real_type_length_exploded = explode(',', $field->get_type()->get_length());
$real_type_length          = $real_type_length_exploded[0];
$real_type_decimal_length  = $real_type_length_exploded[1];
$step                      = 1;
if ( $real_type_decimal_length > 0 )
{
    $step = '0.';
    for ( $i = 0; $i < $real_type_decimal_length - 1; $i++ )
    {
        $step .= '0';
    }
    $step .= '1';
}

// Use different HTML markup if a search input gets rendered.
if ( isset($as_search_input) and $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';
?>

<?php // CREATE, EDIT ?>
<?php if ( $crud->in_create() or $crud->in_edit() ): ?>
    <input
            type="number"
            id="<?= $id ?>"
            class="<?= $classes ?>"
            step="<?= $step ?>"
            min="<?= $field->get_type()->is_unsigned() ? '0' : '' ?>"
            data-crud-input
            data-group="<?= $group ?>"
            data-db-type="<?= $field->get_type()->get_name() ?>"
            data-internal-type="<?= $data['internal_type'] ?>"
            data-display-type="<?= $data['display_type'] ?>"
            data-length="<?= $field->get_type()->get_length() ?>"
            data-inline="<?= $crud->is_inlined($field->get_name()) ? 'true' : 'false' ?>"
            data-validation-constraints="<?= $constraints_json ?>"
            data-behaviours="<?= $behaviours_json ?>"
            value="<?= $final_data ?>"
            maxlength="<?= $real_type_length ?>"
            spellcheck="false"
    >

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>

    <?php // VIEW, LIST ?>
<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>
    <?= $final_data ?>

    <?php // Unsupported task ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>
<?php endif; ?>

