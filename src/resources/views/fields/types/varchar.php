<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Crud\Tools;
use Lukaspotthast\Support\Color;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $data array */
/** @var $as_search_input bool */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

$final_data = $data['value'] === null ?
    $data['default'] :
    $data['value'];
// Only the relevant entries from the array should get secured.
// Which will take place at the direct output position.
if ( !is_array($final_data) )
{
    if ( $crud->in_create() or $crud->in_edit() or $as_search_input )
    {
        $final_data = XSS::secure($final_data, ['allow_html' => false]);
    }
    else
    {
        $final_data = XSS::secure($final_data);
    }
}


// Use different HTML markup if a search input gets rendered.
if ( $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';

$textarea_rows = '1';

$limit_classes = 'crud-limit';
if ( isset($hide_limit) and $hide_limit === true )
{
    $limit_classes .= " crud-display-none";
}
?>

<?php // CREATE, EDIT ?>
<?php if ( $crud->in_create() or $crud->in_edit() or $as_search_input ): ?>

    <?php // Default ?>
    <?php if ( $data['display_type'] === 'default' ): ?>
        <input
                type="text"
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                value="<?= $final_data ?>"
        >
        <?php if ( !$crud->in_view() ): ?>
            <span id="<?= $id ?>_limit" class="pull-right not-selectable <?= $limit_classes ?>">
                <?= $field->get_type()->get_length() ?>
            </span>
        <?php endif; ?>

        <?php // Textarea ?>
    <?php elseif ( $data['display_type'] === 'textarea' ): ?>
        <textarea
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                maxlength="<?= $field->get_type()->get_length() ?>"
                rows="<?= $textarea_rows ?>"
                spellcheck="false"
        ><?= $final_data ?></textarea>
        <?php if ( !$crud->in_view() ): ?>
            <span id="<?= $id ?>_limit" class="pull-right not-selectable <?= $limit_classes ?>">
                <?= $field->get_type()->get_length() ?>
            </span>
        <?php endif; ?>

        <?php // Color ?>
    <?php elseif ( $data['display_type'] === 'color' ): ?>
        <input
                type="text"
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-inline="<?= $crud->is_inlined($field->get_name()) ? 'true' : 'false' ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                value="<?= $final_data ?>"
        >

        <?php // Select ?>
    <?php elseif ( ($data['display_type'] === 'select' or $data['display_type'] === 'multiselect') and is_array($data['value']) ): ?>
        <select
                id="<?= $id ?>"
                class="<?= $classes ?>"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>" <?php // select / multiselect ?>
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviours="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                style="width: 100%;"
        >
            <?php
            foreach ( $data['value'] as $key => &$option )
            {
                // Determine if the current value should be selected.
                $selected = $crud->in_create() ?
                    ($key == $data['default']) :
                    $option['is_current'];

                echo '
                        <option ' . ($selected ? 'selected="selected"' : '') . ' value="' . $key . '">
                            ' . ($option['is_selectable'] ? '' : '[-] ') . XSS::secure($option['value']) . '
                        </option>
                    ';
            }
            ?>
        </select>

        <?php // Password ?>
    <?php elseif ( $data['display_type'] === 'password' ): ?>
        <input
                type="password"
                id="<?= $id ?>"
                class="<?= $classes ?> crud-password-input"
                data-crud-input
                data-group="<?= $group ?>"
                data-db-type="<?= $field->get_type()->get_name() ?>"
                data-length="<?= $field->get_type()->get_length() ?>"
                data-internal-type="<?= $data['internal_type'] ?>"
                data-display-type="<?= $data['display_type'] ?>"
                data-validation-constraints="<?= $constraints_json ?>"
                data-behaviour="<?= $behaviours_json ?>"
                data-editable="<?= $editable ?>"
                placeholder="<?= Translator::get('crud.password_placeholder') ?>"
        >
        <?php if ( !$crud->in_view() ): ?>
            <span id="<?= $id ?>_limit" class="pull-right not-selectable <?= $limit_classes ?>">
                <?= $field->get_type()->get_length() ?>
            </span>
        <?php endif; ?>

        <?php // Image ?>
    <?php elseif ( $data['display_type'] === 'image' ): ?>
        <?php // Upload ?>
        <div class="form-group input-group">
            <label class="input-group-btn crud-margin-0">
                <span class="btn btn-secondary btn-md">
                    <?= Translator::get('crud.browse') ?>
                    <input
                            id="<?= $id ?>"
                            data-crud-input
                            data-group="<?= $group ?>"
                            type="file"
                            accept="image/*"
                            data-dn-type="image" <?php // // TODO:  change types ?>
                            data-internal-type="default"
                            data-display-type="default"
                            data-editable="<?= $editable ?>"
                            style="display: none;"
                        <?php // multiple ?>
                    >
                </span>
            </label>
            <input type="text" class="<?= $classes ?>" readonly>
            <span style="position: relative">
                <span class="fa fa-close crud-file-select-info-clear"></span>
            </span>
            <span class="input-group-btn">
                <span class="btn btn-danger btn-md">
                    <i class="fa fa-trash"></i>
                </span>
            </span>
        </div>

        <?php // File ?>
        <?php if ( !empty($final_data) ): ?>
            <img
                    src="<?= Crud::get_asset_path() . 'library/crud/uploads/' . $final_data ?>"
                    class="crud-image"
            >
        <?php endif; ?>

        <?php // unknown ?>
    <?php else: ?>
        <div class="alert alert-danger">
            Unknown display type... ({{ $data['display_type'] }})
        </div>
    <?php endif; ?>

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>


    <?php // VIEW, LISTS ?>
<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>

    <?php // Default ?>
    <?php if ( $data['internal_type'] === 'default' ): ?>
        <?= $final_data ?>

        <?php // Color ?>
    <?php elseif ( $data['display_type'] === 'color' ): ?>
        <?
        $bright     = Color::is_bright(Color::hex_color_luminance($final_data));
        $text_color = $bright ? '#000000' : '#ffffff';
        ?>
        <div class="crud-color-preview" style="
                background-color: <?= $final_data ?>;
                color: <?= $text_color ?>
                ">
            <?= $final_data ?>
        </div>

        <?php // Select ?>
    <?php elseif ( $data['display_type'] === 'select' and is_array($data['value']) ): ?>
        <?
        foreach ( $data['value'] as &$value ): ?>
            <?php if ( $value['is_current'] ): ?>
                <?= XSS::secure($value['value']) ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php // Multiselect ?>
    <?php elseif ( $data['display_type'] === 'multiselect' and is_array($data['value']) ): ?>
        <?
        $output = '' ?>
        <?php foreach ( $data['value'] as &$value ): ?>
            <?php if ( $value['is_current'] ): ?>
                <?php $output .= XSS::secure($value['value']) . ', ' ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <?= substr($output, 0, -2) ?>

        <?php // unknown display type ?>
    <?php else: ?>
        <div class="alert alert-danger">
            Unknown display type... ({{ $data['display_type'] }})
        </div>
    <?php endif; ?>

    <?php // Unsupported task ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>
<?php endif; ?>