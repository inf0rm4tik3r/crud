<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $field Column_Schema */
/** @var $data array */
/** @var $constraints_json string */
/** @var $behaviours_json string */
/** @var $editable string */

$final_data = $data['value'] === null ?
    $data['default'] :
    $data['value'];
$final_data = XSS::secure($final_data);

// Use different HTML markup if a search input gets rendered.
if ( isset($as_search_input) and $as_search_input === true )
{
    $id    = Tools::encode_input_id($crud, $field, 'search');
    $group = 'search';
}
else
{
    $id    = Tools::encode_input_id($crud, $field);
    $group = 'entry';
}

$classes = 'crud-input-field';
?>

<?php // CREATE, EDIT ?>
<?php if ( $crud->in_create() or $crud->in_edit() ): ?>
    <select
            id="<?= $id ?>"
            class="<?= $classes ?>"
            data-crud-input
            data-group="<?= $group ?>"
            data-db-type="<?= $field->get_type()->get_name() ?>"
            data-internal-type="<?= $data['internal_type'] ?>"
            data-display-type="<?= $data['display_type'] ?>" <?php // select / multiselect ?>
            data-validation-constraints="<?= $constraints_json ?>"
            data-behaviours="<?= $behaviours_json ?>"
            data-editable="<?= $editable ?>"
            style="width: 100%;"
    >
        <?php // Empty first option for the select2 placeholder ?>
        <?php foreach ( $field->get_type()->get_options() as &$value ): ?>
            <?php
            // Each option needs to be secured.
            $value = XSS::secure($value);

            // Determine if the current value should be selected.
            $selected = $value === $final_data;
            ?>
            <option <?= $selected ? 'selected="selected"' : '' ?> value="<?= $value ?>"><?= $value ?></option>
        <?php endforeach; ?>
    </select>

    <?php // Additional rendering... ?>
    <?= Renderer::render_view('fields.field_addition', compact('crud', 'field', 'id')) ?>

    <?php // VIEW, LIST ?>
<?php elseif ( $crud->in_view() or $crud->in_list() ): ?>
    <?= $final_data ?>

    <?php // Unsupported task ?>
<?php else: ?>
    <div class="crud-alert crud-alert-danger">
        Unsupported task... "<?= $crud->get_task() ?>"
    </div>
<?php endif; ?>

