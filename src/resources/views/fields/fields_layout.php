<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Debug\Debug;
use Lukaspotthast\Crud\Layout\Field_Layout;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Tools;

/** @var $crud Crud */
/** @var $fields array */
/** @var $field_layout Field_Layout */
/** @var $tab_count int */
/** @var $data stdClass */

$elements        = $field_layout->get_element_refs();
$elements_amount = count($elements);

$last_elem_type = -1;
?>

<?php for ( $i = 0; $i < $elements_amount; $i++ ): ?>
    <?php
    $element      = $elements[$i];
    $element_type = $element['identifier'];
    ?>

    <?php if ( $element_type === 2 ): // Crud_Field_Layout::TAB ? ?>

        <?php // Search the index of the last tab. ?>
        <?php $last_tab_at = $i ?>
        <?php while ( $last_tab_at < $elements_amount and
                      array_key_exists($last_tab_at, $elements) and
                      $elements[$last_tab_at]['identifier'] === $element_type ): ?>
            <?php $last_tab_at++ ?>
        <?php endwhile; ?>

        <?php // Generate the nav tabs. ?>
        <?php $current_tab_number = $tab_count ?>
        <ul class="crud-nav crud-nav-tabs" data-crud-nav-tabs role="tablist">
            <?php for ( $k = $i; $k < $last_tab_at; $k++ ): ?>
                <?
                $temp_element = $elements[$k];
                $tab          = $field_layout->get_tab($temp_element['key']);
                $tab_id       = Tools::encode_tab_id($crud, $current_tab_number++);
                ?>
                <li class="crud-nav-item">
                    <a class="crud-nav-link <?= $k === $i ? 'crud-nav-link-active' : '' ?>" data-action="show-tab"
                       data-tab-ref="<?= $tab_id ?>" role="tab">
                        <?= $tab->get_name() ?>
                    </a>
                </li>
            <?php endfor; ?>
        </ul>

        <?php // Generate the nav panes. ?>
        <?php $current_tab_number = $tab_count ?>
        <div class="crud-tab-content">
            <?php for ( $k = $i; $k < $last_tab_at; $k++ ): ?>
                <?
                $temp_element = $elements[$k];
                $tab          = $field_layout->get_tab($temp_element['key']);
                $tab_id       = Tools::encode_tab_id($crud, $current_tab_number++);
                ?>
                <div class="crud-tab-pane <?= $k === $i ? 'crud-tab-pane-active' : '' ?>" data-tab-id="<?= $tab_id ?>"
                     role="tabpanel">
                    <?=
                    // Recursive rendering.
                    Renderer::render_view('fields.fields_layout', [
                        'crud'         => $crud,
                        'fields'       => $fields,
                        'field_layout' => $field_layout->get_tab($temp_element['key']),
                        'tab_count'    => $tab_count + $last_tab_at - $i,
                        'data'         => $data,
                    ]);
                    ?>
                </div>
            <?php endfor; ?>
        </div>

        <?php // Increase the total tab amount. ?>
        <?php $tab_count += $last_tab_at - $i ?>
        <?php $i = $last_tab_at - 1 ?><?php // -1 because $i gets incremented before the next iteration starts. ?>

    <?php elseif ( $element_type === 3 ): // Crud_Field_Layout::Section ? ?>
        <div class="crud-field-section">
            <div class="crud-field-section-header">
                <?= $field_layout->get_section($element['key'])->get_name() ?>
            </div>
            <div class="crud-field-section-body">
                <?=
                // Recursive rendering.
                Renderer::render_view('fields.fields_layout', [
                    'crud'         => $crud,
                    'fields'       => $fields,
                    'field_layout' => $field_layout->get_section($element['key']),
                    'tab_count'    => $tab_count,
                    'data'         => $data,
                ]);
                ?>
            </div>
        </div>

    <?php elseif ( $element_type === 4 ): // Crud_Field_Layout::FIELD ? ?>
        <?php
        $entry_id    = $crud->get_current_entry_id();
        $field_names = $field_layout->get_field($element['key']);
        if ( !is_array($field_names) )
        {
            $field_names = [$field_names];
        }
        ?>

        <?php // Put each set of elements in their own row. ?>
        <div class="crud-row" data-field-container>
            <?php foreach ( $field_names as &$field_name ): ?>
                <?php
                /*
                * 1 col:  12
                * 2 cols: 6
                * 3 cols: 4
                * 4 cols: 3
                */
                switch ( count($field_names) )
                {
                    case 1:
                        $element_col_class = 'crud-col-sm-12';
                        break;

                    case 2:
                        $element_col_class = 'crud-col-sm-6';
                        break;

                    case 3:
                        $element_col_class = 'crud-col-sm-4';
                        break;

                    case 4:
                        $element_col_class = 'crud-col-sm-3';
                        break;

                    default:
                        $element_col_class = 'crud-col-sm-12';
                        Debug::error('Too many columns in a row. 4 cols at max are supported.');
                        break;
                }

                $row_opened = false;
                ?>

                <div class="<?= $element_col_class ?>">
                    <?php // Single input ?>
                    <div class="crud-row crud-form-group">
                        <?
                        $vis_mode = $crud->get_label_visibility($field_name);
                        switch ( $vis_mode )
                        {
                            case Crud::SHOWN:
                                $label_style = '';
                                break;
                            case Crud::HIDDEN:
                                $label_style = 'style="visibility: hidden"';
                                break;
                            case Crud::NONE:
                                $label_style = 'style="display: none"';
                                break;
                            default:
                                $label_style = '';
                                break;
                        }

                        $label_col = 'crud-col-sm-3';
                        $input_col = 'crud-col-sm-9';

                        $wrap_mode = $crud->get_label_wrapping($field_name);
                        switch ( $wrap_mode )
                        {
                            case Crud::WRAP_DYNAMIC:
                                break;
                            case Crud::WRAP_ALWAYS:
                                $label_col = 'crud-col-12';
                                $input_col = 'crud-col-12';
                                break;
                            case Crud::WRAP_NEVER:
                                $label_col = str_replace(['sm-', 'md-', 'lg-', 'xl-'], '', $label_col);
                                $input_col = str_replace(['sm-', 'md-', 'lg-', 'xl-'], '', $input_col);
                                break;
                            default:
                                $label_style = '';
                                break;
                        }

                        if ( $vis_mode === Crud::NONE )
                        {
                            $input_col = 'crud-col-12';
                        }
                        ?>


                        <?php // LABEL ?>
                        <div class="<?= $label_col ?> ">
                            <label class="crud-field-label bold <?= $crud->in_view() ? 'bold' : '' ?>" <?= $label_style ?>>
                                <?= $crud->labeled($field_name) ?>:
                            </label>
                            <?php if ( $crud->field_tooltip_collection()->has($field_name, $crud->get_task()) ): ?>
                                <?php
                                $tooltip = htmlspecialchars(
                                    $crud->field_tooltip_collection()
                                        ->get($field_name, $crud->get_task())
                                        ->get_html()
                                );
                                ?>
                                <span class="crud-tooltip-origin crud-field-tooltip"
                                      data-tooltip
                                      data-tooltip-placement="right"
                                      data-tooltip-title="<?= $tooltip ?>">
                                    <i class="crud-tooltip-icon fa fa-exclamation-circle"></i>
                                </span>
                            <?php endif; ?>
                        </div>

                        <?php // INPUT ?>
                        <div class="<?= $input_col ?>">
                            <?php if ( $crud->is_nested_table($field_name) ): ?>
                                <?=
                                Renderer::render_view('fields.types.nested_table', [
                                    'crud'       => $crud,
                                    'field_name' => $field_name,
                                ]);
                                ?>
                            <?php else: ?>
                                <?=
                                Renderer::render_view('fields.field', [
                                    'crud'       => $crud,
                                    'entry_id'   => $entry_id,
                                    'field_name' => $field_name,
                                    'col'        => $crud->get_table_set()->get_main_table()->get_schema()->get_column_schema($field_name),
                                    'data'       => $data,
                                ]);
                                ?>
                            <?php endif; ?>
                        </div>

                    </div> <?php // .row.crud-form-group ?>
                </div> <?php // .col-* ?>
            <?php endforeach; ?>
        </div> <?php // .row ?>

    <?php elseif ( $element_type === 5 ): // Crud_Field_Layout::SEPARATOR ? ?>
        <?php
        $separator = $field_layout->get_separator($element['key']);

        $css_list = '';
        foreach ( $separator['css'] as $css_key => $css_value )
        {
            $css_list .= $css_key . ': ' . $css_value . '; ';
        }

        $class_list = implode(' ', $separator['classes']);
        ?>

        <div class="<?= $class_list ?>" style="<?= $css_list ?>">
            <?= $separator['html'] ?>
        </div>

    <?php else: ?>
        <div class="crud-alert crud-alert-danger">
            Unknown field_layout element type!
        </div>

    <?php endif; ?>

    <?php
    $last_elem_type = $element_type
    ?>

<?php endfor; ?>