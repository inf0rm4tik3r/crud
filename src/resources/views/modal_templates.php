<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Security\XSS;

$xss_options = ['allow_html' => true];
?>

<?php // This is the standard code for the bootstrap modal. ?>
<div class="crud-modal-template" tabindex="-1" role="dialog" aria-labelledby="dynamicCrudModal" aria-hidden="true">
    <div class="crud-modal-dialog" role="document">
        <div class="crud-modal-content">
            <div class="crud-modal-header"></div>
            <div class="crud-modal-body"></div>
            <div class="crud-modal-footer"></div>
        </div>
    </div>
</div>


<?php
$requestfail = json_encode([
    'header' => Translator::get('crud.modal_templates.requestfail.header'),
    'body'   => Translator::get('crud.modal_templates.requestfail.body'),
    'footer' => '<button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' . Translator::get('crud.modal_action_cancel') . '</button>',
]);
?>
<input type="hidden" name="crud-modal-template-requestfail" value="<?= XSS::secure($requestfail, $xss_options) ?>">


<?php
$system_info = json_encode([
    'header' => Translator::get('crud.modal_templates.system_info.header'),
    'body'   => Translator::get('crud.modal_templates.system_info.body'),
    'footer' => '<button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' . Translator::get('crud.modal_action_cancel') . '</button>',
]);
?>
<input type="hidden" name="crud-modal-template-system-info" value="<?= XSS::secure($system_info, $xss_options) ?>">


<?php
$delete = json_encode([
    'header' => '<h5 class="crud-modal-title">' . Translator::get('crud.delete_confirm_header') . '</h5>',
    'body'   => '' . Translator::get('crud.delete_confirm_body'),
    'footer' => '
        <div class="crud-btn-wrapper">
            <button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' . Translator::get('crud.modal_action_cancel') . '</button>
            <button type="button" class="crud-btn crud-btn-danger" data-modal-action="confirm" data-izimodal-close>' . Translator::get('crud.modal_action_delete') . '</button>
        </div>
    ',
]);
?>
<input type="hidden" name="crud-modal-template-delete" value="<?= XSS::secure($delete, $xss_options) ?>">


<?php
$soft_delete = json_encode([
    'header' => '<h5 class="crud-modal-title">' . Translator::get('crud.soft_delete_confirm_header') . '</h5>',
    'body'   => '' . Translator::get('crud.soft_delete_confirm_body'),
    'footer' => '
        <div class="crud-btn-wrapper">
            <button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' . Translator::get('crud.modal_action_cancel') . '</button>
            <button type="button" class="crud-btn crud-btn-danger" data-modal-action="confirm" data-izimodal-close>' . Translator::get('crud.modal_action_soft_delete') . '</button>
        </div>
    ',
]);
?>
<input type="hidden" name="crud-modal-template-soft-delete" value="<?= XSS::secure($soft_delete, $xss_options) ?>">


<?php
$restore = json_encode([
    'header' => '<h5 class="crud-modal-title">' . Translator::get('crud.restore_confirm_header') . '</h5>',
    'body'   => '' . Translator::get('crud.restore_confirm_body'),
    'footer' => '
        <div class="crud-btn-wrapper">
            <button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' . Translator::get('crud.modal_action_cancel') . '</button>
            <button type="button" class="crud-btn crud-btn-success" data-modal-action="confirm" data-izimodal-close>' . Translator::get('crud.modal_action_restore') . '</button>
        </div>
    ',
]);
?>
<input type="hidden" name="crud-modal-template-restore" value="<?= XSS::secure($restore, $xss_options) ?>">