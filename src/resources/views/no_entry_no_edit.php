<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Language\Translation\Translator;

?>

<div class="crud-message">
    <div class="crud-alert crud-alert-info" data-crud-alert>
        <?= Translator::get('crud.no_entry_no_edit') ?>
    </div>
</div>