<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Security\XSS;
use Lukaspotthast\Support\Arg;

/** @var $columns array */
?>

<div class="crud-row crud-nav">
    <div class="crud-col crud-col-flex-end crud-btn-wrapper">
        <button type="button"
                data-action="list"
                class="crud-btn crud-btn-default">
            <i class="fa fa-table" aria-hidden="true"></i>&nbsp;
            <?= Translator::get('crud.listview') ?>
        </button>
    </div>
</div>

<div class="crud-table-wrapper">
    <table class="crud-table crud-table-bordered crud-table-hoverable">

        <thead class="">
        <tr>
            <th>origin table</th>
            <th>name</th>
            <th>type</th>
            <th>default</th>
            <th>custom default</th>
            <th>is nullable</th>
            <th>on update current timestamp</th>
            <th>is auto incrementing</th>
            <th>is primary key</th>
            <th>is unique</th>
            <th>privileges</th>
            <th>comment</th>

            <?php // Display an extra column for buttons if needed. ?>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php //dump(reset($columns)); ?>
        <?php foreach ( $columns as &$col /** @var Column_Schema $col */ ): ?>
            <tr>
                <td><?= XSS::secure($col->get_origin_table()->get_name()) ?></td>
                <td><?= XSS::secure($col->get_name()) ?></td>
                <td><?= XSS::secure($col->get_type()->__toString()) ?></td>
                <td><?= XSS::secure(Arg::stringify($col->get_default_value())) ?></td>
                <td><?= XSS::secure(Arg::stringify($col->get_custom_default_value())) ?></td>
                <td><?= XSS::secure($col->is_nullable() ? 'TRUE' : '') ?></td>
                <td><?= XSS::secure($col->on_update_current_timestamp() ? 'TRUE' : '') ?></td>
                <td><?= XSS::secure($col->is_auto_incrementing() ? 'TRUE' : '') ?></td>
                <td><?= XSS::secure($col->is_primary_key() ? 'TRUE' : '') ?></td>
                <td><?= XSS::secure($col->is_unique() ? 'TRUE' : '') ?></td>
                <td><?= XSS::secure(Arg::stringify($col->get_privileges())) ?></td>
                <td><?= XSS::secure($col->get_comment()) ?></td>

                <?php // Crud buttons ?>
                <td class="crud-btn-column">
                    <div class="btn-group crud-btn-group" role="group">

                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>