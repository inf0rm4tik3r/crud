<?php
declare(strict_types=1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Security\XSS;

/** @var $crud Crud */
/** @var $columns array */
/** @var $order_by_columns array */
/** @var $has_table_buttons bool */
?>

<thead class="crud-table-header">
<tr>
    <?php
    // Display an extra column for a "trashcan icon" should softdelete be active.
    if ( $crud->uses_soft_delete() )
    {
        echo '<th style="width: 2em"></th>';
    }

    // col-fields: Field, Type, Null, Key, Default, Extra
    foreach ( $columns as &$col /** @var Column_Schema $col */ ):

        // Do not display columns which should not be visible
        if ( !$crud->column_shown($col->get_name()) )
        {
            continue;
        }

        $is_ordered          = array_key_exists($col->get_name(), $order_by_columns);
        $is_order_by_allowed = $crud->is_order_by_column_allowed($col->get_name());

        // Classes for the <th> element.
        $th_classes = 'crud-column-header';
        if ( $is_ordered )
        {
            $th_classes .= ' crud-column-ordered';
        }
        if ( $is_order_by_allowed )
        {
            $th_classes .= ' crud-order-by-trigger';
        }
        ?>
        <th <?php
        echo 'class="' . $th_classes . '"';
        if ( $is_order_by_allowed )
        {
            echo 'data-action="order-by"';
        }
        if ( $crud->has_column_width($col->get_name()) )
        {
            echo 'style="width: ' . $crud->get_column_width($col->get_name()) . ';"';
        }
        ?>
        >
            <?php // Display the column name,ordering sign (if existent) and tooltips.
            ?>
            <div class="crud-row">
                <div class="crud-col crud-col-flex-start crud-col-flex-top crud-column-header-main-row">
                    <?php // Sort arrow
                    ?>
                    <?php if ( $is_ordered ): ?>
                        <?php $order_type = $order_by_columns[$col->get_name()]; ?>
                        <span class="crud-order-by-sign">
                                    <?php
                                    if ( $order_type === 'asc' )
                                    {
                                        echo '&uarr;';
                                    }
                                    else if ( $order_type === 'desc' )
                                    {
                                        echo '&darr;';
                                    }
                                    ?>
                                </span>
                    <?php endif; ?>

                    <?php // Column name
                    ?>
                    <?php $labeled_column_name = XSS::secure($crud->labeled($col->get_name())) ?>
                    <span
                        class="crud-column-name"
                        data-column-name="<?= $col->get_name() ?>"
                        data-labeled-column-name="<?= $labeled_column_name ?>"
                    >
                                <?= $labeled_column_name ?>
                            </span>

                    <?php // Tooltip sign
                    ?>
                    <?php if ( $crud->has_column_tooltip($col->get_name(), $crud->get_task()) ): ?>
                        <?php $tooltip = XSS::secure($crud->get_column_tooltip($col->get_name(), $crud->get_task())); ?>
                        <span
                            class="crud-tooltip-origin crud-column-tooltip"
                            data-tooltip
                            data-tooltip-placement="top"
                            data-tooltip-title="<?= $tooltip ?>"
                        >
                                    <i class="crud-tooltip-icon fa fa-exclamation-circle"></i>
                                </span>
                    <?php endif; ?>
                </div>
            </div>

            <?php // Display a column info if defined.
            ?>
            <?php if ( $crud->has_column_info($col->get_name()) ): ?>
                <div class="crud-row">
                    <div class="crud-col crud-col-flex-start">
                        <div class="crud-column-info">
                            <?= XSS::secure($crud->get_column_info($col->get_name())) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </th>
    <?php endforeach; ?>

    <?php // Display an extra column for buttons if needed. ?>
    <?php if ( $has_table_buttons ): ?>
        <th></th>
    <?php endif; ?>
</tr>
</thead>