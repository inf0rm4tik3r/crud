<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;

/** @var $crud Crud */
/** @var $columns array */
/** @var $data stdClass */

// If the current view is not allowed to be rendered, we can simply add an error and abort the rendering.
if ( !$crud->is_allowed('list') )
{
    $crud->error('The "list" view is not allowed!');
    return;
}

$has_table_buttons = (
    $crud->is_allowed('view') or
    $crud->is_allowed('edit') or
    $crud->is_allowed('delete')
);

$has_nav_elements = (
    $crud->is_allowed('create') or
    $crud->is_allowed('search') or
    $crud->is_allowed('structure') or
    $crud->is_allowed('print') or
    $crud->is_allowed('export') or
    count($crud->get_custom_header_buttons()) > 0
);

// Render currently available messages.
echo Renderer::render_view('messages', ['crud' => $crud]);
?>

<?php if ( $has_nav_elements ): ?>
    <div class="crud-row crud-nav">

        <?php // LEFT COLUMN ?>
        <div class="crud-col crud-btn-wrapper">
            <?php if ( $crud->is_allowed('create') ): ?>
                <button type="button"
                        data-action="create"
                        class="crud-btn crud-btn-success">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;
                    <?= Translator::get('crud.add') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_allowed('search') ): ?>
                <button type="button"
                        data-action="open-search"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-search" aria-hidden="true"></i>&nbsp;
                    <?= Translator::get('crud.search') ?>
                </button>
            <?php endif; ?>

            <?php foreach ( $crud->get_custom_header_buttons() as $internal_name => $header_button ): ?>
                <button type="button"
                        data-action="custom_header_button"
                        data-name="<?= XSS::secure($internal_name) ?>"
                        class="crud-btn crud-btn-default">
                    <?= XSS::secure($header_button['name']) ?>
                </button>
            <?php endforeach; ?>
        </div>

        <?php // RIGHT COLUMN ?>
        <div class="crud-col crud-col-flex-end crud-btn-wrapper">
            <?php if ( $crud->is_allowed('structure') ): ?>
                <button type="button"
                        data-action="structure"
                        class="crud-btn crud-btn-default">
                    <i class="fa fa-database" aria-hidden="true"></i>&nbsp;
                    <?= Translator::get('crud.structure') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_allowed('print') ): ?>
                <button type="button"
                        data-action="print"
                        class="crud-btn crud-btn-default">
                    <i class="fa fa-print" aria-hidden="true"></i>&nbsp;
                    <?= Translator::get('crud.print') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_allowed('export') ): ?>
                <button type="button"
                        data-action="export"
                        class="crud-btn crud-btn-default">
                    <i class="fa fa-table" aria-hidden="true"></i>&nbsp;
                    <?= Translator::get('crud.export') ?>
                </button>
            <?php endif; ?>
        </div>

    </div>
<?php endif; ?>

<?php
// Search input
if ( $crud->is_allowed('search') )
{
    echo Renderer::render_view('search', compact('crud', 'columns'));
}

// Render custom elements for the top position.
echo $crud->get_custom_elements_for('top');

// Render custom search tabs.
if ( $crud->has_search_tabs() )
{
    echo Renderer::render_view('search_tabs', compact('crud'));
}

$order_by_columns = $crud->get_order_by_columns();
?>

    <div class="crud-table-wrapper">
        <table class="crud-table crud-table-bordered crud-table-hoverable">
            <?=
            Renderer::render_view('table_header', [
                'crud'              => $crud,
                'columns'           => $columns,
                'order_by_columns'  => $order_by_columns,
                'has_table_buttons' => $has_table_buttons,
            ]);
            ?>

            <?=
            Renderer::render_view('table_body', [
                'crud'              => $crud,
                'data'              => $data,
                'columns'           => $columns,
                'order_by_columns'  => $order_by_columns,
                'has_table_buttons' => $has_table_buttons,
            ]);
            ?>

        </table>
    </div>

<?php
echo $crud->get_custom_elements_for('bottom')
?>

<?php if ( $crud->is_shown('limits') or $crud->is_shown('pagination') ): ?>
    <div class="crud-row crud-nav">

        <?php // Result limit options ?>
        <div class="crud-col">
            <?= Renderer::render_view('limits', ['crud' => $crud]); ?>
        </div>

        <?php // Page options ?>
        <div class="crud-col">
            <?= Renderer::render_view('pagination', ['crud' => $crud]); ?>
        </div>

    </div>
<?php endif; ?>