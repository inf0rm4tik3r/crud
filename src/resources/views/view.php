<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;
use Lukaspotthast\Crud\Security\XSS;

/** @var $crud Crud */
/** @var $fields array */
/** @var $data stdClass */

// If the current view is not allowed to be rendered, we can simply add an error and abort the rendering.
if ( !$crud->is_allowed('view') )
{
    $crud->error('The "view" view is not allowed!');
    return;
}

// Render currently available messages.
Renderer::render_view('messages', ['crud' => $crud]);
?>

    <div class="crud-row crud-nav">

        <div class="crud-col">
            <div class="crud-entry" style="padding: 0">
                <?=
                XSS::secure(
                    Translator::get('crud.entry_nr') . ' ' . $data->{$crud->get_id_column_name()}['value']
                );
                ?>
            </div>
        </div>

        <div class="crud-col crud-col-flex-end crud-btn-wrapper">
            <button type="button" data-action="list" class="crud-btn crud-btn-default">
                <i class="fa fa-table" aria-hidden="true"></i> <?= Translator::get('crud.listview') ?>
            </button>
        </div>

    </div>

    <div style="margin-bottom: 25px;"></div>

<?=
Renderer::render_view('fields.fields_layout', [
    'crud'         => $crud,
    'fields'       => $fields,
    'field_layout' => $crud->get_field_layout(true),
    'tab_count'    => 0,
    'data'         => $data,
]);
?>