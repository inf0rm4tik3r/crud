<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Message\Message;
use Lukaspotthast\Crud\Security\XSS;

/** @var $crud Crud */
?>

<?php if ( $crud->message_controller()->any() ): ?>
    <?php foreach ( $crud->message_controller()->get() as &$message /**@var $message Message */ ): ?>

        <div class="crud-message">

            <?php
            $alert_type  = 'crud-alert-' . XSS::secure($message->get_type());
            $dismissible = $message->is_dismissible() ? 'crud-alert-dismissible' : '';
            ?>

            <div class="crud-alert <?= $alert_type ?> <?= $dismissible ?>"
                 role="alert"
                 data-crud-alert
                 data-automatically-closing="<?= $message->should_automatically_close() ? 'true' : 'false' ?>"
                 data-automatic-close-delay="<?= Message::AUTOMATIC_CLOSE_DELAY ?>"
            >

                <button type="button" class="close" data-action="close-alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <?php if ( !empty($message->get_heading()) ): ?>
                    <div class="crud-alert-heading">
                        <?= XSS::secure($message->get_heading()) ?>
                    </div>
                <?php endif; ?>

                <?= XSS::secure($message->get_message()) ?>

            </div>

        </div>

    <?php endforeach; ?>
<?php endif; ?>
