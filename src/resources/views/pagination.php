<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;

/** @var $crud Crud */
?>

<?php if ( $crud->is_shown('pagination') ): ?>

    <div class="crud-btn-wrapper crud-btn-group crud-col-flex-end">
        <?php
        $pagination = $crud->get_paginator()->get_pagination();

        $btn_main_classes   = 'crud-btn crud-btn-md crud-btn-default';
        $btn_disabled_class = ' crud-btn-disabled';
        $btn_active_class   = ' crud-btn-active';
        ?>

        <?php foreach ( $pagination->get_options() as &$page ): ?>
            <?php
            $page_btn_classes = $btn_main_classes;
            if ( $page === -1 )
            {
                $page_btn_classes .= $btn_disabled_class;
            }
            else if ( $page === $pagination->get_current_page() )
            {
                $page_btn_classes .= $btn_disabled_class . $btn_active_class;
            }
            ?>

            <button
                    type="button"
                    data-action="page"
                    data-page="<?= $page ?>"
                    class="<?= $page_btn_classes ?>"
            >
                <?= $page !== -1 ? $page : '&hellip;' ?>
            </button>
        <?php endforeach; ?>

    </div>

<?php endif; ?>