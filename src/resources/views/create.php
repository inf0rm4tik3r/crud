<?php
declare(strict_types = 1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $fields array */
/** @var $data array */

// If the current view is not allowed to be rendered, we can simply add an error and abort the rendering.
if ( !$crud->is_allowed('create') )
{
    $crud->error('The "create" view is not allowed!');
    return;
}

$has_nav_elements = (
    $crud->is_shown('save_btn') or
    $crud->is_shown('save_and_return_btn') or
    $crud->is_shown('save_and_new_btn') or
    ($crud->is_allowed('list') and $crud->is_shown('list_btn'))
);
?>

<?php if ( $has_nav_elements ): ?>
    <div class="crud-row crud-nav">
        <div class="crud-col crud-btn-wrapper">
            <?php if ( $crud->is_shown('save_btn') ): ?>
                <button type="button"
                        data-action="save"
                        data-after="edit"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-hdd-o" aria-hidden="true"></i> <?= Translator::get('crud.save') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_shown('save_and_return_btn') ): ?>
                <button type="button"
                        data-action="save"
                        data-after="list"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-hdd-o" aria-hidden="true"></i> <?= Translator::get('crud.save_return') ?>
                </button>
            <?php endif; ?>

            <?php if ( $crud->is_shown('save_and_new_btn') ): ?>
                <button type="button"
                        data-action="save"
                        data-after="create"
                        class="crud-btn crud-btn-primary">
                    <i class="fa fa-hdd-o" aria-hidden="true"></i> <?= Translator::get('crud.save_new') ?>
                </button>
            <?php endif; ?>
        </div>

        <?php if ( $crud->is_allowed('list') ): ?>
            <div class="crud-col crud-col-flex-end crud-btn-wrapper">
                <button type="button"
                        data-action="list"
                        class="crud-btn crud-btn-default">
                    <i class="fa fa-table" aria-hidden="true"></i> <?= Translator::get('crud.listview') ?>
                </button>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<?php if ( $crud->is_shown('entry_info') ): ?>
    <div class="crud-entry">
        <?= Translator::get('crud.new_entry') ?>
    </div>
<?php endif; ?>

<div data-fields>
    <?=
    Renderer::render_view('fields.fields_layout', [
        'crud'         => $crud,
        'fields'       => $fields,
        'field_layout' => $crud->get_field_layout(true),
        'tab_count'    => 0,
        'data'         => $data,
    ]);
    ?>
</div>