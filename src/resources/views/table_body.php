<?php
declare(strict_types=1);

use Lukaspotthast\Crud\Crud;
use Lukaspotthast\Crud\Database\Schema\Column_Schema;
use Lukaspotthast\Crud\Language\Translation\Translator;
use Lukaspotthast\Crud\Rendering\Renderer;

/** @var $crud Crud */
/** @var $columns array */
/** @var $order_by_columns array */
/** @var $has_table_buttons bool */
?>

<tbody class="">
<?php foreach ( $data as &$row ): ?>

    <tr data-entry-id="<?= $row->{$crud->get_id_column_name()}['value'] ?>">

        <?php // Display a trash can if the current entry got soft-deleted ?>
        <?php if ( $crud->uses_soft_delete() ): ?>
            <td class="crud-entry-deleted">
                <?php if ( $row->{$crud->get_soft_delete_column()}['value'] === 1 ): ?>
                    <i class="fa fa-trash" aria-hidden="true"></i>
                <?php endif; ?>
            </td>
        <?php endif; ?>

        <?php
        // One <td> for each column.
        foreach ( $columns as &$col /** @var Column_Schema $col */ ):

            $is_ordered = array_key_exists($col->get_name(), $order_by_columns);

            // Do not display columns which should not be visible.
            if ( !$crud->column_shown($col->get_name()) )
            {
                continue;
            }

            $col_has_inlineedit = (
                $crud->is_inlineedit() and
                $crud->is_allowed('edit') and
                $crud->is_col_inline_editable($col->get_name()) //and
                //!$crud->is_subselect_col($col)
            );

            $cell_data = $row->{$col->get_name()};

            // Determine how the TD needs to be opened:
            $td_classes = '';
            if ( $col_has_inlineedit )
            {
                $td_classes .= 'crud-inlineedit ';
            }
            if ( $is_ordered )
            {
                $td_classes .= 'crud-column-ordered ';
            }

            $td_attributes = '';
            if ( $crud->get_inlineedit_activator() !== 'btn' )
            {
                // A button would come with the data-action attribute!
                // If the activator is not a button, the td itself needs to activate.
                $td_attributes .= 'data-action="inlineedit"';
            }
            ?>

            <td
                class="<?= $td_classes ?>"
                <?= $td_attributes ?>
            >
                <?php
                // Determine the data that should be displayed in the cell.
                $final_output = '';

                if ( $crud->callback_controller()->column_callbacks()->has($col->get_name()) )
                {
                    // We call the defined column callback:
                    /** @var Column_Callback $callback */
                    $callback     = $crud->callback_controller()->column_callbacks()->get($col->get_name());
                    $final_output = $callback->perform($cell_data, $col->get_name(), $row->id['value'], $row);
                }
                else
                {
                    $final_output = Renderer::render_view('fields.field', [
                        'crud'     => $crud,
                        'col'      => $col,
                        'data'     => $row,
                        'entry_id' => $row->{$crud->get_id_column_name()}['value'],
                    ]);
                }

                // Apply a column pattern if defined.
                if ( $crud->has_column_pattern($col->get_name()) )
                {
                    $final_output = $crud->apply_column_pattern($col->get_name(), $final_output, $row);
                }

                // Output the main cell data.
                echo $final_output;
                ?>

                <?php // Check if inline editing is active and the activator is set to "btn".
                ?>
                <?php if ( $col_has_inlineedit and $crud->get_inlineedit_activator() === 'btn' ): ?>
                    <button
                        type="button"
                        class="crud-btn crud-btn-sm crud-btn-primary crud-inlineedit-btn"
                        data-action="inlineedit"
                    >
                        <i class="fa fa-pencil"></i>
                    </button>
                <?php endif; ?>

            </td>
        <?php endforeach; ?> <?php // foreach column ?>

        <?php // Crud buttons: ?>
        <?php if ( $has_table_buttons ): ?>
            <td class="crud-btn-column">
                <div class="crud-btn-wrapper crud-btn-group">
                    <?php if ( $crud->is_allowed('view') ): ?>
                        <button type="button"
                                data-action="view"
                                data-entry="<?= $row->{$crud->get_id_column_name()}['value'] ?>"
                                class="crud-btn crud-btn-sm crud-btn-primary">
                            <?= Translator::get('crud.view') ?>
                        </button>
                    <?php endif; ?>

                    <?php if ( $crud->is_allowed('edit') ): ?>
                        <button type="button"
                                data-action="edit"
                                data-entry="<?= $row->{$crud->get_id_column_name()}['value'] ?>"
                                class="crud-btn crud-btn-sm crud-btn-warning">
                            <?= Translator::get('crud.edit') ?>
                        </button>
                    <?php endif; ?>

                    <?php // Delete / Soft delete / Restore ?>
                    <?php if ( $crud->is_allowed('delete') ): ?>
                        <?php if ( $crud->uses_soft_delete() ): ?>
                            <?php if ( $row->{$crud->get_soft_delete_column()}['value'] === 0 ): ?>
                                <button type="button"
                                        data-action="delete"
                                        data-entry="<?= $row->{$crud->get_id_column_name()}['value'] ?>"
                                        data-confirm-method="modal"
                                        data-modal-template="soft-delete"
                                        class="crud-btn crud-btn-sm crud-btn-danger">
                                    <?= Translator::get('crud.delete') ?>
                                </button>
                            <?php elseif ( $row->{$crud->get_soft_delete_column()}['value'] === 1 ): ?>
                                <button type="button"
                                        data-action="restore"
                                        data-entry="<?= $row->{$crud->get_id_column_name()}['value'] ?>"
                                        data-confirm-method="modal"
                                        data-modal-template="restore"
                                        class="crud-btn crud-btn-sm crud-btn-success">
                                    <?= Translator::get('crud.restore') ?>
                                </button>
                            <?php endif; ?>
                        <?php else: ?>
                            <button type="button"
                                    data-action="delete"
                                    data-entry="<?= $row->{$crud->get_id_column_name()}['value'] ?>"
                                    data-confirm-method="modal"
                                    data-modal-template="delete"
                                    class="crud-btn crud-btn-sm crud-btn-danger">
                                <?= Translator::get('crud.delete') ?>
                            </button>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </td>
        <?php endif; ?>
    </tr>
<?php endforeach; ?>
</tbody>