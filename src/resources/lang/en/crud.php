<?php

declare(strict_types = 1);

/*
 | --------------------------------------------------------------------------
 | Translation into: English (English)
 | --------------------------------------------------------------------------
 | 
 */

return [
    'add'                                => 'Add',
    'structure'                          => 'Structure',
    'print'                              => 'Print',
    'export'                             => 'Export',
    'view'                               => 'View',
    'edit'                               => 'Edit',
    'delete'                             => 'Delete',
    'limit_all'                          => 'All',
    'save'                               => 'Save',
    'save_return'                        => 'Save and return',
    'save_new'                           => 'Save and new',
    'new_entry'                          => 'New entry...',
    'listview'                           => 'List view',
    'delete_confirm_header'              => 'Delete entry...',
    'delete_confirm_body'                => '<div>Are you sure?</div><div>This entry can not be restored!</div>',
    'modal_action_cancel'                => 'Cancel',
    'modal_action_delete'                => 'Delete',
    'modal_action_ok'                    => 'OK',
    'entry_nr'                           => 'Entry - Nr.',
    'search'                             => 'Search',
    'reset_search'                       => 'Reset',
    'soft_delete'                        => 'Soft delete',
    'restore'                            => 'Restore',
    'soft_delete_confirm_header'         => 'Soft delete...',
    'soft_delete_confirm_body'           => '<div>Do you want to set the deletion flag?</div><div>The entry persists and can be restored if necessary.</div>',
    'modal_action_soft_delete'           => 'Delete',
    'modal_action_restore'               => 'Restore',
    'restore_confirm_header'             => 'Restore...',
    'restore_confirm_body'               => 'Do you want to restore the entry?',
    'select_option.none'                 => '- none -',
    'yes'                                => 'Yes',
    'no'                                 => 'No',
    'password_placeholder'               => 'Enter a new password...',
    'files_selected'                     => 'Files selected',
    'browse'                             => 'Browse...',
    'no_entry_no_edit'                   => 'This information can only be entered after the entry has been created.',
    'errors_occurred'                    => 'The following errors occurred',
    'continue_on_error.text'             => 'Click {link} to continue.',
    'continue_on_error.link_replacement' => 'here',
    'modal_action_save'                  => 'Save',
    'inlineedit_modal_header'            => 'Edit \'{labeled_field_name}\'',
    'save_aborted.header'                => 'Attention!',
    'save_aborted.text'                  => 'The save process was terminated programmatically. Please check your input.',
    'success_message_after_save'         => 'The entry has been successfully saved!',

    'modal_templates.system_info.header' => '<h5 class="crud-modal-title">About</h5>',
    'modal_templates.system_info.body'   => 'CRUD version: 1.0',

    'modal_templates.requestfail.header' => '<h5 class="crud-modal-title">Sorry, something has gone wrong..</h5>',
    'modal_templates.requestfail.body'   => 'Please contact the administrator ouf your website.',

    'signature_pad.unlock'         => '<i class="fa fa-lock" aria-label="unlock"></i>',
    'signature_pad.unlock_tooltip' => 'Unlock',
    'signature_pad.lock'           => '<i class="fa fa-unlock" aria-label="lock"></i>',
    'signature_pad.lock_tooltip'   => 'Lock',
    'signature_pad.draw'           => '<i class="fa fa-pencil" aria-label="draw"></i>',
    'signature_pad.draw_tooltip'   => 'Draw',
    'signature_pad.erase'          => '<i class="fa fa-eraser" aria-label="erase"></i>',
    'signature_pad.erase_tooltip'  => 'Erase',
    'signature_pad.undo'           => '<i class="fa fa-repeat fa-flip-horizontal" aria-label="undo"></i>',
    'signature_pad.undo_tooltip'   => 'Undo',
    'signature_pad.clear'          => '<i class="fa fa-file" aria-label="clear"></i>',
    'signature_pad.clear_tooltip'  => 'Clear',

];
