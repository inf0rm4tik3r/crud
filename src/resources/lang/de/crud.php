<?php

declare(strict_types = 1);

/*
 | --------------------------------------------------------------------------
 | Translation into: German (Deutsch)
 | --------------------------------------------------------------------------
 | 
 */

return [
    'add'                         => 'Hinzufügen',
    'structure'                   => 'Struktur',
    'print'                       => 'Drucken',
    'export'                      => 'Exportieren',
    'view'                        => 'Anzeigen',
    'edit'                        => 'Bearbeiten',
    'delete'                      => 'Löschen',
    'limit_all'                   => 'Alle',
    'save'                        => 'Speichern',
    'save_return'                 => 'Speichern und zurück',
    'save_new'                    => 'Speichern und neu',
    'new_entry'                   => 'Neuer Eintrag...',
    'listview'                    => 'Listenansicht',
    'delete_confirm_header'       => 'Eintrag löschen...',
    'delete_confirm_body'         => 'Sind Sie sicher?<br>Dieser Eintrag kann nicht wiederhergestellt werden!',
    'modal_action_cancel'         => 'Abbrechen',
    'modal_action_delete'         => 'Löschen',
    'modal_action_ok'             => 'OK',
    'entry_nr'                    => 'Eintrag - Nr.',
    'search'                      => 'Suchen',
    'reset_search'                => 'Zurücksetzen',
    'soft_delete'                 => 'Löschen (Soft delete)',
    'restore'                     => 'Wiederherstellen',
    'soft_delete_confirm_header'  => 'Löschkennzeichen setzen...',
    'soft_delete_confirm_body'    => 'Möchten Sie das Löschkennzeichen setzen?<br>Der Eintrag bleibt bestehen und kann bei Bedarf wiederhergestellt werden.',
    'modal_action_soft_delete'    => 'Löschen',
    'modal_action_restore'        => 'Wiederherstellen',
    'restore_confirm_header'      => 'Wiederherstellen...',
    'restore_confirm_body'        => 'Möchten Sie den Eintrag wiederherstellen?',
    'select_option.none'          => '- keine -',
    'yes'                         => 'Ja',
    'no'                          => 'Nein',
    'password_placeholder'        => 'Passwort setzen...',
    'files_selected'              => 'Dateien ausgewählt',
    'browse'                      => 'Durchsuchen...',
    'no_entry_no_edit'            => 'Diese Eingaben können erst getätigt werden, nachdem der Eintrag erstellt wurde.',
    'errors_occurred'             => 'Es sind folgende Fehler aufgetreten',
    'continue_on_error'           => 'Klicke {link} um fortzufahren.',
    'modal_action_save'           => 'Speichern',
    'inlineedit_modal_header'     => '\'{labeled_field_name}\' bearbeiten...',
    'continue_on_error.link_name' => 'hier',
    'save_aborted.header'         => 'Achtung!',
    'save_aborted.text'           => 'Der Speichervorgang wurde programmatisch abgebrochen. Bitte Überprüfen Sie Ihre Eingaben.',
    'success_message_after_save'  => 'Der Eintrag wurde erfolgreich gespeichert!',
];
