$(document).ready(function() {
    CopyBTN.init();
});

var CopyBTN = {

    init: function () {
        var copy_btn_list = $(".js-copybtn");
        //console.log("CopyBTN: init(): Found " + copy_btn_list.length + " buttons.");

        $(copy_btn_list).each(function(i, btn) {
            // Put the button in a span wrapper element, so we are able to add text to the sides.
            CopyBTN.wrap_element(btn, "span");

            // Every button needs a data-target attribute!
            var data_target = $(btn).attr("data-target");
            if ( typeof data_target === typeof undefined || data_target === "" )
            {
                console.error(
                    "CopyBTN: A button needs a data-target attribute holding the id of another element to be able to " +
                    "copy this elements text!", btn
                );
            }
            else
            {
                $(btn).on("click", function(event) {
                    console.log("CopyBTN: Copying data of element with id \"" + data_target + "\"");
                    CopyBTN.copy(btn, data_target);
                });
            }
        });
    },

    copy: function (btn, id_selector) {
        // Select the text which should get copied.
        CopyBTN.select_text_by_id(id_selector);

        try {
            // Try to copy the previously selected text.
            var success = document.execCommand('copy');
            if ( !success )
            {
                console.error("CopyBTN: \"copy\" command was unsuccessful", btn, id_selector);
            }

            // Maybe print an information msg to the left.
            if ( $(btn).hasClass("js-copybtn-linfo") )
            {
                // Add the styling class to the buttons parent element.
                $(btn).parent().addClass("copybtn-linfo");

                // Let the copy info vanish
                setTimeout(function() {
                    $(btn).parent().removeClass("copybtn-linfo");
                }, 3000);
            }

            // Maybe print an information msg to the right.
            if ( $(btn).hasClass("js-copybtn-rinfo") )
            {
                // Add the styling class to the buttons parent element.
                $(btn).parent().addClass("copybtn-rinfo");

                // Let the copy info vanish
                setTimeout(function() {
                    $(btn).parent().removeClass("copybtn-rinfo");
                }, 3000);
            }
        }
        catch (err)
        {
            console.error("CopyBTN: An error occurred: ", err);
        }
        finally {
            // clear the selection we made before (for pure aesthetics).
            CopyBTN.clear_selection();
        }
    },

    wrap_element: function(element, wrapper_tag) {
        // 'element' is the element we want to wrap.
        var parent = element.parentNode;
        var wrapper = document.createElement(wrapper_tag);

        // Set the wrapper as child (instead of the element).
        parent.replaceChild(wrapper, element);

        // Set element as child of wrapper.
        wrapper.appendChild(element);
    },

    select_text_by_id: function (id_selector)
    {
        var doc = document;
        var text = doc.getElementById(id_selector);
        var range;
        var selection;

        if (doc.body.createTextRange)
        {
            range = document.body.createTextRange();
            range.moveToElementText(text);
            range.select();
        }
        else if (window.getSelection)
        {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(text);
            selection.removeAllRanges();
            selection.addRange(range);
        }
    },

    clear_selection: function ()
    {
        if (document.selection )
        {
            document.selection.empty();
        }
        else if (window.getSelection())
        {
            window.getSelection().removeAllRanges();
        }
    }

};