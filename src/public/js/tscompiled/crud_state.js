"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("./crud_tools");
var Crud_State = (function () {
    function Crud_State(crud_instance) {
        this.instance = crud_instance;
    }
    Crud_State.prototype.load = function () {
        var json_state = String(this.instance.find(".crud-base input[name='crud_state']").val());
        if (!json_state) {
            console.error("load(): crud_state was not found in DOM!");
            return;
        }
        var state = JSON.parse(json_state);
        this.sv_instance_name = state.instance_name;
        this.sv_instance_verification_key = state.instance_verification_key;
        this.sv_is_loaded = state.instance_is_loaded === "true";
        this.sv_is_nested = state.instance_is_nested === "true";
        this.sv_parent_instance_name = state.parent_instance_name;
        this.sv_task = state.task;
        this.sv_entry = state.entry;
        this.sv_date_format = state.date_format;
        this.sv_date_separator = state.date_separator;
        this.sv_time_format = state.time_format;
        this.sv_moment_date_format = state.moment_date_format;
        this.sv_moment_date_separator = state.moment_date_separator;
        this.sv_moment_time_format = state.moment_time_format;
        this.sv_custom_validators = JSON.parse(state.custom_validators);
        this.sv_custom_async_validators = JSON.parse(state.custom_async_validators);
        this.sv_texteditor_driver = state.texteditor_driver;
        this.sv_select_driver = state.select_driver;
        this.sv_colorpicker_driver = state.colorpicker_driver;
        this.sv_language_short = state.language_short;
        this.sv_translations = JSON.parse(state.translations);
        this.sv_is_inlineedit = state.is_inlineedit === "true";
        this.sv_inlineedit_mode = state.inlineedit_mode;
        this.sv_inlineedit_activator = state.inlineedit_activator;
        this.sv_has_local_storage = crud_tools_1.Crud_Tools.is_local_storage_available() === true;
    };
    Crud_State.prototype.instance_name = function () { return this.sv_instance_name; };
    Crud_State.prototype.instance_verification_key = function () { return this.sv_instance_verification_key; };
    Crud_State.prototype.is_loaded = function () { return this.sv_is_loaded; };
    Crud_State.prototype.is_nested = function () { return this.sv_is_nested; };
    Crud_State.prototype.parent_instance_name = function () { return this.sv_parent_instance_name; };
    Crud_State.prototype.task = function () { return this.sv_task; };
    Crud_State.prototype.entry = function () { return this.sv_entry; };
    Crud_State.prototype.date_format = function () { return this.sv_date_format; };
    Crud_State.prototype.date_separator = function () { return this.sv_date_separator; };
    Crud_State.prototype.time_format = function () { return this.sv_time_format; };
    Crud_State.prototype.moment_date_format = function () { return this.sv_moment_date_format; };
    Crud_State.prototype.moment_date_separator = function () { return this.sv_moment_date_separator; };
    Crud_State.prototype.moment_time_format = function () { return this.sv_moment_time_format; };
    Crud_State.prototype.custom_validators = function () { return this.sv_custom_validators; };
    Crud_State.prototype.custom_async_validators = function () { return this.sv_custom_async_validators; };
    Crud_State.prototype.texteditor_driver = function () { return this.sv_texteditor_driver; };
    Crud_State.prototype.select_driver = function () { return this.sv_select_driver; };
    Crud_State.prototype.colorpicker_driver = function () { return this.sv_colorpicker_driver; };
    Crud_State.prototype.language_short = function () { return this.sv_language_short; };
    Crud_State.prototype.translations = function () { return this.sv_translations; };
    Crud_State.prototype.is_inlineedit = function () { return this.sv_is_inlineedit; };
    Crud_State.prototype.inlineedit_mode = function () { return this.sv_inlineedit_mode; };
    Crud_State.prototype.inlineedit_activator = function () { return this.sv_inlineedit_activator; };
    Crud_State.prototype.has_local_storage = function () { return this.sv_has_local_storage; };
    return Crud_State;
}());
exports.Crud_State = Crud_State;
//# sourceMappingURL=crud_state.js.map