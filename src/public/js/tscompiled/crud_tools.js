"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Crud_Tools = (function () {
    function Crud_Tools() {
    }
    Crud_Tools.is_set = function (arg) {
        return (arg !== null && typeof arg !== "undefined");
    };
    Crud_Tools.is_function = function (arg) {
        return $.isFunction(arg);
    };
    Crud_Tools.is_object = function (arg) {
        return (!!arg) && (arg.constructor === Object);
    };
    Crud_Tools.is_string = function (arg) {
        return typeof arg === 'string' || arg instanceof String;
    };
    Crud_Tools.execute_function_by_name = function (function_name, context) {
        var rest = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            rest[_i - 2] = arguments[_i];
        }
        var args = [].slice.call(arguments).splice(2);
        var namespaces = function_name.split(".");
        var func = namespaces.pop();
        if (!func) {
            console.error("execute_function_by_name(): No function name got provided.");
            return undefined;
        }
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    };
    Crud_Tools.try_parse_json = function (input) {
        try {
            var o = JSON.parse(input);
            if (o && typeof o === "object") {
                return o;
            }
        }
        catch (e) {
        }
        return false;
    };
    Crud_Tools.try_eval = function (input) {
        return null;
    };
    Crud_Tools.is_local_storage_available = function () {
        try {
            localStorage.setItem('local_storage_functionality_test', 'test');
            localStorage.removeItem('local_storage_functionality_test');
            return true;
        }
        catch (e) {
            return false;
        }
    };
    Crud_Tools.is_array = function (val) {
        return Array.isArray(val);
    };
    Crud_Tools.enforce_array = function (val) {
        if (this.is_array(val)) {
            return val;
        }
        return [];
    };
    Crud_Tools.in_array = function (val, arr) {
        return $.inArray(val, arr) === -1;
    };
    Crud_Tools.unique_array = function (arr) {
        var seen = {};
        return arr.filter(function (item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    };
    Crud_Tools.is_between = function (val, arr) {
        if (arr.length !== 2) {
            console.error("is_between(): The provided array contains more or less then 2 values!", arr);
            return false;
        }
        return val > arr[0] && val < arr[1];
    };
    Crud_Tools.is_empty = function (val) {
        if (this.is_object(val)) {
            return Object.keys(val).length === 0;
        }
        else {
            return val.length === 0;
        }
    };
    Crud_Tools.append_to_form_data = function (fd, index, value) {
        Crud_Tools._append_to_form_data(fd, index, "", value);
    };
    Crud_Tools._append_to_form_data = function (fd, base, keychain, value) {
        if (!Crud_Tools.is_set(value)) {
            console.error("append_to_form_data(): The given value is undefined or null!", keychain, value);
            console.error("append_to_form_data(): Skipping the attachment to: ", fd);
            return;
        }
        if (Crud_Tools.is_object(value) || Crud_Tools.is_array(value)) {
            $.each(value, function (key, inner_value) {
                Crud_Tools._append_to_form_data(fd, base, keychain + "[" + key + "]", inner_value);
            });
        }
        else {
            fd.append(base + keychain, value);
        }
    };
    Crud_Tools.escape_html = function (text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    };
    return Crud_Tools;
}());
exports.Crud_Tools = Crud_Tools;
//# sourceMappingURL=crud_tools.js.map