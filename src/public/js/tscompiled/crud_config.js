"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Crud_Config = {
    instance_wrapper_identifier: ".crud-instance-wrapper",
    instance_identifier: ".crud-instance",
    search_container_identifier: ".crud-search-container",
    input_identifier: ".crud-input",
    inlineedit_activator_identifier: ".crud-inlineedit-activator-btn",
    btn_identifier: ".crud-btn",
    event_prefix: "crud:",
    event_suffix: "",
    reload_route: "/crud/reload",
    reset_route: "/crud/reset",
    load_field_route: "/crud/load-field",
    load_list_view_route: "/crud/load-list-view",
    load_structure_view_route: "/crud/load-structure-view",
    load_create_view_route: "/crud/load-create-view",
    load_edit_view_route: "/crud/load-edit-view",
    load_view_view_route: "/crud/load-view-view",
    load_print_view_route: "/crud/load-print-view",
    load_export_view_route: "/crud/load-export-view",
    load_limit_route: "/crud/load-limit",
    load_page_route: "/crud/load-page",
    save_route: "/crud/save",
    save_inline_route: "/crud/save-inline",
    delete_route: "/crud/delete",
    restore_route: "/crud/restore",
    order_by_route: "/crud/order-by",
    apply_reorder_route: "/crud/apply-reorder",
    toggle_search_tab_route: "/crud/toggle-search-tab",
    load_search_route: "/crud/load-search",
    apply_search_route: "/crud/apply-search",
    reset_search_route: "/crud/reset-search",
    validation_triggers: {
        varchar: ['keyup', 'focus', 'change', 'click']
    },
    behaviour_check_triggers: {
        varchar: ['keyup', 'focus', 'change', 'click']
    }
};
//# sourceMappingURL=crud_config.js.map