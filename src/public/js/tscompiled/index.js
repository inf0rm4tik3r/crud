"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_polyfills_1 = require("./crud_polyfills");
var crud_1 = require("./crud");
$(function () {
    crud_polyfills_1.Crud_Polyfills.load();
    console.log("Initializing CRUD system...");
    crud_1.Crud.init_system();
});
//# sourceMappingURL=index.js.map