"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_config_1 = require("./crud_config");
var crud_1 = require("./crud");
var action_manager_1 = require("./modules/action_manager");
var input_manager_1 = require("./modules/inputs/input_manager");
var ajax_interface_1 = require("./modules/ajax_interface");
var crud_state_1 = require("./crud_state");
var ui_controller_1 = require("./modules/ui_controller");
var tooltip_manager_1 = require("./modules/tooltip_manager");
var modal_1 = require("./modules/modal/modal");
var validation_plugin_helper_1 = require("./modules/inputs/validation/validation_plugin_helper");
var inlineedit_modal_1 = require("./modules/modal/inlineedit_modal");
var Crud_Instance = (function () {
    function Crud_Instance(dom_elem) {
        this.dom_elem = dom_elem;
        this.state = new crud_state_1.Crud_State(this);
        this.nested_instances = [];
        this.ajax_interface = new ajax_interface_1.Ajax_Interface(this);
        this.action_manager = new action_manager_1.Action_Manager(this);
        this.input_manager = new input_manager_1.Input_Manager(this);
        this.tooltip_manager = new tooltip_manager_1.Tooltip_Manager(this);
        this.ui_controller = new ui_controller_1.UI_Controller(this);
    }
    Crud_Instance.prototype.get_elem = function () {
        return this.dom_elem;
    };
    Crud_Instance.prototype.get_state = function () {
        return this.state;
    };
    Crud_Instance.prototype.get_action_manager = function () {
        return this.action_manager;
    };
    Crud_Instance.prototype.get_input_manager = function () {
        return this.input_manager;
    };
    Crud_Instance.prototype.get_ajax_interface = function () {
        return this.ajax_interface;
    };
    Crud_Instance.prototype.get_ui_controller = function () {
        return this.ui_controller;
    };
    Crud_Instance.prototype.get_name = function () {
        return this.state.instance_name();
    };
    Crud_Instance.prototype.has_name = function (name) {
        return this.get_name() === name;
    };
    Crud_Instance.prototype.clear_nested_instances = function () {
        this.nested_instances = [];
    };
    Crud_Instance.prototype.put_nested_instance = function (crud_instance) {
        this.nested_instances.push(crud_instance);
    };
    Crud_Instance.prototype.get_nested_instances = function () {
        return this.nested_instances;
    };
    Crud_Instance.prototype.find_instance = function (instance_name) {
        if (this.has_name(instance_name)) {
            return this;
        }
        for (var i = 0; i < this.get_nested_instances().length; i++) {
            var current = this.get_nested_instances()[i];
            var found = current.find_instance(instance_name);
            if (typeof found !== typeof undefined) {
                return found;
            }
        }
        return undefined;
    };
    Crud_Instance.prototype.find_instances = function (target_name) {
        var found = [];
        if (this.has_name(target_name)) {
            found.push(this);
        }
        for (var i = 0; i < this.get_nested_instances().length; i++) {
            var current = this.get_nested_instances()[i];
            var found_nested = current.find_instances(target_name);
            found = found.concat(found_nested);
        }
        return found;
    };
    Crud_Instance.prototype.init = function (response) {
        var start = window.performance.now();
        if (response) {
            this.destroy();
            this.replace_self(response);
        }
        this.state.load();
        if (!this.state.is_loaded()) {
            this.ajax_interface.reload();
            return;
        }
        this.enable_overlay();
        {
            validation_plugin_helper_1.Validation_Plugin_Helper.init(this);
            this.input_manager.init_inputs();
            this.action_manager.init_actions();
            this.tooltip_manager.init_tooltips();
            this.init_nested_instances();
            this.init_automatically_closing_objects();
            this.ui_controller.show_stored_tabs();
        }
        this.disable_overlay();
        this.trigger("initialized", [this]);
        var end = window.performance.now();
        var duration = end - start;
        console.log("Initialization took: " + duration + " ms");
    };
    Crud_Instance.prototype.destroy = function () {
        this.input_manager.destroy_inputs();
    };
    Crud_Instance.prototype.init_nested_instances = function () {
        this.clear_nested_instances();
        var nested_instances = crud_1.Crud.first_level_instances(this.get_elem());
        $(nested_instances).each(function (i, instance_elem) {
            var crud_instance_nested = new Crud_Instance(instance_elem);
            crud_instance_nested.init(undefined);
            this.put_nested_instance(crud_instance_nested);
        }.bind(this));
    };
    Crud_Instance.prototype.init_automatically_closing_objects = function () {
        this.find("[data-automatically-closing='true']").each(function (i, obj) {
            var delay = $(obj).attr("data-automatic-close-delay");
            if (!delay) {
                console.error("data-automatic-close-delay missing on: ", obj);
                return;
            }
            $(obj).fadeTo(parseInt(delay), 500).slideUp(300, function () {
                $(obj).remove();
            });
        });
    };
    Crud_Instance.prototype.init_inlineedit = function (td) {
        var current_data = $(td).html();
        var entry_id = this.get_entry_id_from_td(td);
        var field_name = this.get_col_name_from_td(td);
        var labeled_field_name = this.get_labeled_col_name_from_td(td);
        var header = this.state.translations().inlineedit_modal_header;
        header = header.replace("{labeled_field_name}", labeled_field_name);
        var modal_data = {
            "header": '<h5 class="crud-modal-title">' + header + '</h5>',
            "body": '',
            "footer": '' +
                '<div class="crud-btn-wrapper">' +
                '<button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' + this.state.translations().modal_action_cancel + '</button> ' +
                '<button type="button" class="crud-btn crud-btn-primary" data-modal-action="save" data-izimodal-close>' + this.state.translations().modal_action_save + '</button>' +
                '</div>'
        };
        var modal = new inlineedit_modal_1.Inlineedit_Modal(this);
        modal.init(modal_data, function (elem) {
            if ($(elem).attr("data-modal-action") === "save") {
                $(modal.get_input_manager().get_inputs()).each(function (key, input_object) {
                    var input = Object.values(input_object)[0];
                    this.ajax_interface.save_inline(entry_id, input);
                }.bind(this));
            }
            modal.destroy_content();
        }.bind(this), this.get_elem());
        this.ajax_interface.load_field(entry_id, field_name, function (response) {
            var html = $("<div data-fields>").html(response)[0];
            modal.set_data({ "body": html });
            modal.init_content();
        }.bind(this));
    };
    Crud_Instance.prototype.enable_overlay = function () {
        var overlay = this.find(".crud-overlay");
        $(overlay).addClass('active');
        $(overlay).css('z-index', '1000');
        if (this.state.is_loaded()) {
            this.find(".crud-overlay-spinner").css('display', 'block');
        }
    };
    Crud_Instance.prototype.disable_overlay = function () {
        var overlay = this.find(".crud-overlay");
        $(overlay).removeClass('active');
        $(overlay).css('z-index', '-1');
        this.find(".crud-overlay-spinner").css('display', 'none');
    };
    Crud_Instance.prototype.toggle_overlay = function () {
        var overlay = this.find(".crud-overlay");
        if ($(overlay).hasClass("active")) {
            this.enable_overlay();
        }
        else {
            this.disable_overlay();
        }
    };
    Crud_Instance.prototype.is_fullscreen = function () {
        return $(this.get_elem()).hasClass("crud-fullscreen");
    };
    Crud_Instance.prototype.enable_fullscreen = function () {
        $(this.get_elem()).addClass("crud-fullscreen");
        this.trigger("fullscreen-enabled", []);
    };
    Crud_Instance.prototype.disable_fullscreen = function () {
        $(this.get_elem()).removeClass("crud-fullscreen");
        this.trigger("fullscreen-disabled", []);
    };
    Crud_Instance.prototype.toggle_fullscreen = function () {
        if (this.is_fullscreen()) {
            this.disable_fullscreen();
        }
        else {
            this.enable_fullscreen();
        }
    };
    Crud_Instance.prototype.find = function (selector) {
        return $(this.get_elem()).find(selector).not(function (i, obj) {
            return !$(obj).closest(crud_config_1.Crud_Config.instance_identifier).is(this.get_elem());
        }.bind(this));
    };
    Crud_Instance.prototype.replace_self = function (html) {
        $(this.get_elem()).html(html);
    };
    Crud_Instance.prototype.limit_input_length = function (input) {
        var max_length = parseInt(String($(input).attr("maxlength")));
        if (typeof max_length !== typeof undefined) {
            var value = $(input).val();
            if (typeof value !== typeof undefined) {
                value = String(value);
                if (value.length > max_length) {
                    $(input).val(value.slice(0, max_length));
                }
            }
        }
    };
    Crud_Instance.prototype.get_entry_id_from_td = function (td) {
        return String($(td).parent().attr("data-entry-id"));
    };
    Crud_Instance.prototype.get_col_name_from_td = function (td) {
        var td_index = $(td).parent().children().index(td);
        return String($(td).parent().parent().parent().children("thead").children().eq(0).children().eq(td_index)
            .find(".crud-column-name").attr("data-column-name"));
    };
    Crud_Instance.prototype.get_labeled_col_name_from_td = function (td) {
        var td_index = $(td).parent().children().index(td);
        return String($(td).parent().parent().parent().children("thead").children().eq(0).children().eq(td_index)
            .find(".crud-column-name").attr("data-labeled-column-name"));
    };
    Crud_Instance.prototype.trigger = function (event_name, data) {
        $(this.get_elem()).trigger(crud_config_1.Crud_Config.event_prefix + event_name + crud_config_1.Crud_Config.event_suffix, data);
    };
    Crud_Instance.prototype.on = function (event_name, callback) {
        $(this.get_elem()).on(event_name, callback);
    };
    Crud_Instance.prototype.get_data_entry_id = function (obj) {
        if (this.state.task() === "list") {
            var data_attr = $(obj).attr("data-entry");
            if (data_attr) {
                return String(data_attr);
            }
            else {
                return undefined;
            }
        }
        else if (this.state.task() === "edit") {
            return this.state.entry();
        }
        else {
            return undefined;
        }
    };
    Crud_Instance.prototype.get_data_after = function (obj) {
        var task_after_attr = $(obj).attr("data-after");
        return task_after_attr ? task_after_attr : this.state.task();
    };
    Crud_Instance.prototype.get_data_confirm = function (obj) {
        var confirm_attr = $(obj).attr("data-confirm");
        var confirm_data = $(obj).attr("data-confirm-data");
        if (confirm_data) {
            confirm_data = JSON.parse(confirm_data);
        }
        return {
            confirm: confirm_attr ? confirm_attr : "",
            confirm_data: confirm_data ? confirm_data : ""
        };
    };
    Crud_Instance.prototype.open_search = function () {
        this.find(crud_config_1.Crud_Config.search_container_identifier).first().slideToggle("fast");
    };
    Crud_Instance.prototype.apply_search = function () {
    };
    Crud_Instance.prototype.reset_search = function () {
    };
    Crud_Instance.prototype.show_info = function (type, data) {
        var modal_data_json = "";
        var modal_data;
        switch (type) {
            case "requestfail":
                modal_data_json = String(this.find("input[name='crud-modal-template-requestfail']").val());
                modal_data = JSON.parse(modal_data_json);
                setTimeout(function () {
                    var modal = new modal_1.Modal(this);
                    modal.init(modal_data, function (elem) {
                    }, this.get_elem());
                }.bind(this), 500);
                break;
            case "system-info":
                modal_data_json = String(this.find("input[name='crud-modal-template-system-info']").val());
                modal_data = JSON.parse(modal_data_json);
                setTimeout(function () {
                    var modal = new modal_1.Modal(this);
                    modal.init(modal_data, function (elem) {
                    }, this.get_elem());
                }.bind(this), 100);
                break;
            default:
                console.error("show_info(): The specified type (" + type + ") could not get processed...");
        }
    };
    Crud_Instance.prototype.init_reorderable = function () {
        var tbody = this.find("tbody").first();
        $(tbody).sortable({
            axis: "y",
            cancel: "a,button",
            cursor: "move",
            handle: ".sort-start",
            opacity: 0.65,
            placeholder: "ui-state-highlight"
        });
        $(tbody).on("sortupdate", function (event, ui) {
            var html_dragged = ui.item.context.innerHTML;
            var td_dragged = $(html_dragged).find(".sort-start").closest("td")[0];
            var id_dragged = String($(td_dragged).attr("data-entry"));
            var old_position = parseInt(String($(td_dragged).attr("data-order")));
            var html_next = ui.item.context.nextElementSibling != null ? ui.item.context.nextElementSibling.innerHTML : null;
            var td_next_row = html_next ? $(html_next).find(".sort-start").closest("td")[0] : undefined;
            var next_position = td_next_row ? parseInt(String($(td_next_row).attr("data-order"))) : undefined;
            var new_position = old_position;
            if (!next_position) {
                new_position = -1;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + -1 + " verschoben.");
            }
            else if (old_position < next_position) {
                new_position = next_position - 1;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + (next_position - 1) + " verschoben.");
            }
            else if (old_position >= next_position) {
                new_position = next_position;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + next_position + " verschoben.");
            }
            this.ajax_interface.apply_reorder(id_dragged, old_position, new_position);
        }.bind(this));
    };
    return Crud_Instance;
}());
exports.Crud_Instance = Crud_Instance;
//# sourceMappingURL=crud_instance.js.map