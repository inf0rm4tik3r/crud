"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_config_1 = require("./crud_config");
var crud_instance_1 = require("./crud_instance");
var Crud = (function () {
    function Crud() {
    }
    Crud.init_system = function () {
        $(Crud.first_level_instances("body")).each(function (key, instance_elem) {
            var instance = new crud_instance_1.Crud_Instance($(instance_elem));
            instance.init(undefined);
            Crud.crud_instance_objects.push(instance);
        });
        $(document).trigger(crud_config_1.Crud_Config.event_prefix + "ready ss" + crud_config_1.Crud_Config.event_suffix, []);
    };
    Crud.first_level_instances = function (in_obj) {
        return $(in_obj)
            .find(crud_config_1.Crud_Config.instance_identifier)
            .not(function (index, element) {
            var parent_instance = $(element).parent().closest(crud_config_1.Crud_Config.instance_identifier);
            var is_nested_instance = $(parent_instance).length > 0;
            var parent_equals_in_obj = $(parent_instance).is(in_obj);
            var keep = !is_nested_instance || (is_nested_instance && parent_equals_in_obj);
            return !keep;
        });
    };
    Crud.instance = function (instance_name) {
        return $("input[name='crud_instance_name'][value='" + instance_name + "']")
            .closest(crud_config_1.Crud_Config.instance_identifier);
    };
    Crud.instances = function () {
        return $(crud_config_1.Crud_Config.instance_identifier);
    };
    Crud.find_instance = function (target_name) {
        var found = [];
        for (var i = 0; i < Crud.crud_instance_objects.length; i++) {
            var current = Crud.crud_instance_objects[i];
            found = found.concat(current.find_instances(target_name));
        }
        if (found.length === 0) {
            return undefined;
        }
        else if (found.length === 1) {
            return found[0];
        }
        else {
            return found;
        }
    };
    Crud.crud_instance_objects = [];
    Crud.synchronizing_state_variables = ["instance_name", "task", "entry"];
    return Crud;
}());
exports.Crud = Crud;
//# sourceMappingURL=crud.js.map