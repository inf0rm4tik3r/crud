"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_config_1 = require("../crud_config");
var Tooltip_Manager = (function () {
    function Tooltip_Manager(crud_instance) {
        this.instance = crud_instance;
    }
    Tooltip_Manager.prototype.init_tooltips = function () {
        this.instance.find("[data-tooltip]").each(function (i, obj) {
            this.init_tooltip(obj);
        }.bind(this));
    };
    Tooltip_Manager.prototype.init_tooltip = function (obj, title_override, placement_override) {
        var title = title_override ? title_override : $(obj).data("tooltip-title");
        if (!title) {
            console.error("init_tooltip(): data-tooltip-title attribute is missing!");
            console.error("init_tooltip(): Skipping tooltip initialization for: ", obj);
            return;
        }
        var placement = placement_override ? placement_override : $(obj).data("tooltip-placement");
        if (!placement) {
            console.log("init_tooltip(): tooltip-placement attribute is missing! Using \"top\" placement.");
            placement = "top";
        }
        var placement_options = ["left", "right", "top", "bottom"];
        if (!$.inArray(placement, placement_options)) {
            console.error("init_tooltip(): The specified placement was not one of: ", placement_options);
            console.error("init_tooltip(): Skipping tooltip initialization for: ", obj);
            return;
        }
        $(obj).tooltipster({
            content: title,
            contentAsHTML: true,
            theme: "tooltipster-borderless",
            side: placement,
            animation: "fade",
            interactive: true,
            delay: [0, 100],
            functionReady: function (instance, helper) {
                $(helper.tooltip).addClass(crud_config_1.Crud_Config.instance_wrapper_identifier.substring(1));
            }
        });
        $(obj).click(function (event) {
        });
    };
    return Tooltip_Manager;
}());
exports.Tooltip_Manager = Tooltip_Manager;
//# sourceMappingURL=tooltip_manager.js.map