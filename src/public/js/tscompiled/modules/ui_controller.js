"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("../crud_tools");
var UI_Controller = (function () {
    function UI_Controller(crud_instance) {
        this.instance = crud_instance;
    }
    UI_Controller.prototype.show_tab = function (tab_id, track_change) {
        if (track_change === void 0) { track_change = true; }
        var active_link_class_name = "crud-nav-link-active";
        var active_pane_class_name = "crud-tab-pane-active";
        var target_pane = $("[data-tab-id='" + tab_id + "']");
        var active_pane = $(target_pane).siblings("." + active_pane_class_name);
        var target_link = $("[data-tab-ref='" + tab_id + "']");
        var active_link = $(target_link).closest("[data-crud-nav-tabs]").find("." + active_link_class_name);
        var active_tab_id = $(active_link).data("tab-ref");
        $(active_pane).removeClass(active_pane_class_name);
        $(target_pane).addClass(active_pane_class_name);
        $(active_link).removeClass(active_link_class_name);
        $(target_link).addClass(active_link_class_name);
        if (track_change === true) {
            this.remove_active_tab_id(active_tab_id);
            this.add_active_tab_id(tab_id);
        }
    };
    UI_Controller.prototype.show_stored_tabs = function () {
        if (!this.instance.get_state().has_local_storage()) {
            return;
        }
        $.each(this.get_active_tab_ids(), function (i, tab_id) {
            this.show_tab(tab_id, false);
        }.bind(this));
    };
    UI_Controller.prototype.get_active_tab_ids = function () {
        if (!this.instance.get_state().has_local_storage()) {
            return [];
        }
        var storage_access = this.instance.get_state().instance_name() + ".active_tab_ids";
        var active_tab_ids_string = String(localStorage.getItem(storage_access));
        return crud_tools_1.Crud_Tools.enforce_array(crud_tools_1.Crud_Tools.try_parse_json(active_tab_ids_string));
    };
    UI_Controller.prototype.store_active_tab_ids = function (active_tab_ids) {
        if (!this.instance.get_state().has_local_storage()) {
            return;
        }
        var storage_access = this.instance.get_state().instance_name() + ".active_tab_ids";
        localStorage.setItem(storage_access, JSON.stringify(active_tab_ids));
    };
    UI_Controller.prototype.remove_active_tab_id = function (tab_id) {
        if (!this.instance.get_state().has_local_storage()) {
            return;
        }
        var active_tab_ids = this.get_active_tab_ids();
        var index;
        while ((index = $.inArray(tab_id, active_tab_ids, 0)) !== -1) {
            active_tab_ids.splice(index, 1);
        }
        this.store_active_tab_ids(active_tab_ids);
    };
    UI_Controller.prototype.add_active_tab_id = function (tab_id) {
        if (!this.instance.get_state().has_local_storage()) {
            return;
        }
        var active_tab_ids = this.get_active_tab_ids();
        if ($.inArray(tab_id, active_tab_ids, 0) === -1) {
            active_tab_ids.push(tab_id);
        }
        this.store_active_tab_ids(active_tab_ids);
    };
    return UI_Controller;
}());
exports.UI_Controller = UI_Controller;
//# sourceMappingURL=ui_controller.js.map