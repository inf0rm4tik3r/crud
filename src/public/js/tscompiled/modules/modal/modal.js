"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Modal = (function () {
    function Modal(crud_instance) {
        this.instance = crud_instance;
    }
    Modal.prototype.init = function (data, modal_callback, append_to) {
        if (append_to === void 0) { append_to = "body"; }
        if (modal_callback === null || typeof modal_callback !== "function") {
            console.error("init_modal(): " + modal_callback + " is not a function!");
        }
        if (append_to === null || typeof append_to === typeof undefined) {
            console.log("init_modal(): append_to variable was not set. Using \"body\".");
            append_to = "body";
        }
        var modal_template = this.instance.find(".crud-modal-template");
        var clone = $(modal_template).clone();
        $(clone).attr("id", "crud-modal-js-copy");
        $(clone).removeClass("crud-modal-template");
        $(clone).addClass("crud-modal");
        $(clone).appendTo(append_to);
        var modal_copy = this.get_modal();
        this.set_data(data, modal_copy);
        var active_elem = null;
        $(modal_copy).iziModal({
            appendTo: append_to,
            appendToOverlay: append_to,
            onClosing: function () {
                active_elem = $(document.activeElement)[0];
            },
            onClosed: function () {
                var result = modal_callback(active_elem);
                $(modal_copy).iziModal('destroy');
                $(modal_copy).remove();
            }
        });
        $(modal_copy).iziModal('open');
    };
    Modal.prototype.get_modal = function () {
        return $("#crud-modal-js-copy")[0];
    };
    Modal.prototype.set_data = function (data, modal_copy) {
        if (!modal_copy) {
            modal_copy = this.get_modal();
        }
        if (data.header) {
            $(modal_copy).find(".crud-modal-header").html(data.header);
        }
        if (data.body) {
            $(modal_copy).find(".crud-modal-body").html(data.body);
        }
        if (data.footer) {
            $(modal_copy).find(".crud-modal-footer").html(data.footer);
        }
    };
    Modal.prototype.get_header = function (modal_copy) {
        if (!modal_copy) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-header")[0];
    };
    Modal.prototype.get_body = function (modal_copy) {
        if (!modal_copy) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-body")[0];
    };
    Modal.prototype.get_footer = function (modal_copy) {
        if (!modal_copy) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-footer")[0];
    };
    return Modal;
}());
exports.Modal = Modal;
//# sourceMappingURL=modal.js.map