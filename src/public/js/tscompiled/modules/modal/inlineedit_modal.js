"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var modal_1 = require("./modal");
var input_manager_1 = require("../inputs/input_manager");
var action_manager_1 = require("../action_manager");
var Inlineedit_Modal = (function (_super) {
    __extends(Inlineedit_Modal, _super);
    function Inlineedit_Modal(instance) {
        var _this = _super.call(this, instance) || this;
        _this.input_manager = new input_manager_1.Input_Manager(instance);
        _this.action_manager = new action_manager_1.Action_Manager(instance);
        return _this;
    }
    Inlineedit_Modal.prototype.init_content = function () {
        this.input_manager.init_inputs(this.get_body());
        this.action_manager.init_actions(this.get_body());
    };
    Inlineedit_Modal.prototype.destroy_content = function () {
        this.input_manager.destroy_inputs();
    };
    Inlineedit_Modal.prototype.get_input_manager = function () {
        return this.input_manager;
    };
    Inlineedit_Modal.prototype.get_action_manager = function () {
        return this.action_manager;
    };
    return Inlineedit_Modal;
}(modal_1.Modal));
exports.Inlineedit_Modal = Inlineedit_Modal;
//# sourceMappingURL=inlineedit_modal.js.map