"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Signature_Pad_Controller = (function (_super) {
    __extends(Signature_Pad_Controller, _super);
    function Signature_Pad_Controller() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.signature_pads = {};
        return _this;
    }
    Signature_Pad_Controller.prototype.store = function (input, signature_pad) {
        this.signature_pads[input.get_id()] = signature_pad;
    };
    Signature_Pad_Controller.prototype.retrieve = function (input) {
        return this.signature_pads[input.get_id()];
    };
    Signature_Pad_Controller.prototype.init = function (input) {
        var sig_pad = new SignaturePad(input.get_elem());
        this.store(input, sig_pad);
        var data = String($(input.get_elem()).attr("data-data"));
        var sig_pad_data = JSON.parse(data);
        if (sig_pad_data.length !== 0) {
            sig_pad.fromData(sig_pad_data);
        }
        window.addEventListener("resize", function () {
            this.draw_canvas(input);
        }.bind(this));
        this.draw_canvas(input);
        setTimeout(function () {
            this.draw_canvas(input);
        }.bind(this), 500);
    };
    Signature_Pad_Controller.prototype.draw_canvas = function (input) {
        var sig_pad = this.retrieve(input);
        var sig_pad_data = sig_pad.toData();
        this.resize_canvas(sig_pad._canvas);
        if (sig_pad_data.length !== 0) {
            sig_pad.fromData(sig_pad_data);
        }
    };
    Signature_Pad_Controller.prototype.resize_canvas = function (canvas) {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        var canvas_rendering_context = canvas.getContext("2d");
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas_rendering_context.scale(ratio, ratio);
    };
    Signature_Pad_Controller.prototype.get_controls = function (input) {
        return $(input.get_elem()).closest(".crud-signature-pad").children(".crud-signature-pad-controls");
    };
    Signature_Pad_Controller.prototype.unlock = function (input) {
        $(input.get_elem()).siblings(".crud-signature-pad-canvas-overlay").css("display", "none");
        var button_wrapper = this.get_controls(input).children(".crud-btn-wrapper")[0];
        console.log(this.get_controls(input));
        $(button_wrapper).children("[data-unlock-btn]").css("display", "none");
        $(button_wrapper).children("[data-lock-btn]").css("display", "block");
        $(button_wrapper).children("[data-clear-btn]").css("display", "block");
        this.draw_canvas(input);
    };
    Signature_Pad_Controller.prototype.lock = function (input) {
        $(input.get_elem()).siblings(".crud-signature-pad-canvas-overlay").css("display", "block");
        var button_wrapper = this.get_controls(input).children(".crud-btn-wrapper")[0];
        $(button_wrapper).children("[data-unlock-btn]").css("display", "block");
        $(button_wrapper).children("[data-lock-btn]").css("display", "none");
        $(button_wrapper).children("[data-clear-btn]").css("display", "none");
        this.draw_canvas(input);
    };
    Signature_Pad_Controller.prototype.enter_draw_mode = function (input) {
        var ctx = this.retrieve(input)._canvas.getContext("2d");
        if (ctx) {
            ctx.globalCompositeOperation = "source-over";
        }
        else {
            console.warn("enter_draw_mode(): No canvas.");
        }
    };
    Signature_Pad_Controller.prototype.enter_erase_mode = function (input) {
        var ctx = this.retrieve(input)._canvas.getContext("2d");
        if (ctx) {
            ctx.globalCompositeOperation = "destination-out";
        }
        else {
            console.warn("enter_erase_mode(): No canvas.");
        }
    };
    Signature_Pad_Controller.prototype.undo = function (input) {
        var ctx = this.retrieve(input)._canvas.getContext("2d");
        var gco_backup = "";
        if (ctx) {
            gco_backup = ctx.globalCompositeOperation;
            ctx.globalCompositeOperation = "source-over";
        }
        var data = this.get_data(input);
        if (data) {
            data.pop();
            this.restore_from_data(input, data);
        }
        if (ctx) {
            ctx.globalCompositeOperation = gco_backup;
        }
    };
    Signature_Pad_Controller.prototype.clear = function (input) {
        this.retrieve(input).clear();
        $(input.get_elem()).trigger("click");
    };
    Signature_Pad_Controller.prototype.get_data = function (input) {
        return this.retrieve(input).toData();
    };
    Signature_Pad_Controller.prototype.get_data_url = function (input) {
        return this.retrieve(input).toDataURL();
    };
    Signature_Pad_Controller.prototype.restore_from_data = function (input, data) {
        var sig_pad = this.retrieve(input);
        sig_pad.fromData(data);
        sig_pad._data = data;
    };
    Signature_Pad_Controller.prototype.restore_from_data_url = function (input, data) {
        this.retrieve(input).fromDataURL(data);
    };
    Signature_Pad_Controller.prototype.destroy = function (input) {
        this.retrieve(input).off();
    };
    return Signature_Pad_Controller;
}(input_controller_1.Input_Controller));
exports.Signature_Pad_Controller = Signature_Pad_Controller;
//# sourceMappingURL=signature_pad_controller.js.map