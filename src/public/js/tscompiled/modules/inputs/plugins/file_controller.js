"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var File_Controller = (function (_super) {
    __extends(File_Controller, _super);
    function File_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    File_Controller.prototype.init = function (input) {
        var elem = input.get_elem();
        $(elem).on("change", function () {
            var input_elem = elem;
            var num_files = input_elem.files ? input_elem.files.length : 0;
            var label = String($(input_elem).val()).replace(/\\/g, '/').replace(/.*\//, '');
            $(elem).trigger('fileselect', [num_files, label]);
        });
        $(elem).on('fileselect', function (event, num_files, label) {
            var text_elem = $(elem).parents('.input-group').find(':text');
            if (num_files > 1) {
                $(text_elem).val(num_files + ' ' + this.instance.get_state().translations().files_selected);
            }
            else {
                $(text_elem).val(label);
            }
        }.bind(this));
    };
    File_Controller.prototype.get_data = function (input) {
        var input_elem = input.get_elem();
        var file_list = input_elem.files;
        if (!file_list) {
            console.error("get_data(): No file selected...");
            return "";
        }
        else {
            return file_list[0];
        }
    };
    File_Controller.prototype.destroy = function (input) {
    };
    return File_Controller;
}(input_controller_1.Input_Controller));
exports.File_Controller = File_Controller;
//# sourceMappingURL=file_controller.js.map