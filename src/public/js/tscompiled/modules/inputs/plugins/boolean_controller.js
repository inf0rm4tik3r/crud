"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Boolean_Controller = (function (_super) {
    __extends(Boolean_Controller, _super);
    function Boolean_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Boolean_Controller.prototype.init = function (input) {
    };
    Boolean_Controller.prototype.get_data = function (input) {
        return $(input.get_elem()).is(":checked") ? "1" : "0";
    };
    Boolean_Controller.prototype.destroy = function (input) {
    };
    return Boolean_Controller;
}(input_controller_1.Input_Controller));
exports.Boolean_Controller = Boolean_Controller;
//# sourceMappingURL=boolean_controller.js.map