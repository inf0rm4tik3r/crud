"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Select_Controller = (function (_super) {
    __extends(Select_Controller, _super);
    function Select_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Select_Controller.prototype.init = function (input, use_multiselect) {
        switch (this.instance.get_state().select_driver()) {
            case "select2":
                var select2 = $(input.get_elem()).select2({
                    placeholder: use_multiselect ? "Optionen wählen..." : '',
                    allowClear: use_multiselect,
                    multiple: use_multiselect,
                    closeOnSelect: !use_multiselect,
                    language: this.instance.get_state().language_short(),
                    dropdownParent: $(input.get_elem()).parent()
                });
                var selected_1 = [];
                var all_options = $(select2).find("option");
                all_options.each(function (i, element) {
                    var obj = element;
                    if (obj.defaultSelected === true) {
                        selected_1.push(String($(this).val()));
                    }
                });
                if (selected_1.length === 0 && all_options.length > 0 && !use_multiselect) {
                    selected_1.push(String($(all_options[0]).val()));
                }
                select2.val(selected_1).trigger('change');
                break;
            default:
                console.error("init(): Unknown select driver specified: ", this.instance.get_state().select_driver());
                break;
        }
    };
    Select_Controller.prototype.get_data = function (input) {
        return $(input.get_elem()).val();
    };
    Select_Controller.prototype.destroy = function (input) {
        switch (this.instance.get_state().select_driver()) {
            case "select2":
                $(input.get_elem()).select2("destroy");
                break;
            default:
                console.error("destroy(): Unknown select driver specified: ", this.instance.get_state().select_driver());
                break;
        }
    };
    return Select_Controller;
}(input_controller_1.Input_Controller));
exports.Select_Controller = Select_Controller;
//# sourceMappingURL=select_controller.js.map