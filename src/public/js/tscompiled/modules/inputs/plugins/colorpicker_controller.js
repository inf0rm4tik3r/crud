"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Colorpicker_Controller = (function (_super) {
    __extends(Colorpicker_Controller, _super);
    function Colorpicker_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Colorpicker_Controller.prototype.init = function (input) {
        switch (this.instance.get_state().colorpicker_driver()) {
            case "spectrum":
                $(input.get_elem()).spectrum({
                    disabled: !input.is_editable(),
                    flat: input.is_inlined(),
                    preferredFormat: "hex",
                    showButtons: false,
                    showInitial: true,
                    showInput: true,
                    allowEmpty: true,
                    showPalette: true,
                    showSelectionPalette: false,
                    hideAfterPaletteSelect: false,
                    palette: [
                        ["#000", "#444", "#666", "#999", "#ccc", "#eee", "#f3f3f3", "#fff"],
                        ["#f00", "#f90", "#ff0", "#0f0", "#0ff", "#00f", "#90f", "#f0f"],
                        ["#f4cccc", "#fce5cd", "#fff2cc", "#d9ead3", "#d0e0e3", "#cfe2f3", "#d9d2e9", "#ead1dc"],
                        ["#ea9999", "#f9cb9c", "#ffe599", "#b6d7a8", "#a2c4c9", "#9fc5e8", "#b4a7d6", "#d5a6bd"],
                        ["#e06666", "#f6b26b", "#ffd966", "#93c47d", "#76a5af", "#6fa8dc", "#8e7cc3", "#c27ba0"],
                        ["#c00", "#e69138", "#f1c232", "#6aa84f", "#45818e", "#3d85c6", "#674ea7", "#a64d79"],
                        ["#900", "#b45f06", "#bf9000", "#38761d", "#134f5c", "#0b5394", "#351c75", "#741b47"],
                        ["#600", "#783f04", "#7f6000", "#274e13", "#0c343d", "#073763", "#20124d", "#4c1130"]
                    ],
                    appendTo: $(input.get_elem()).parent()
                });
                break;
            default:
                console.error("init(): Unknown colorpicker driver specified: ", this.instance.get_state().colorpicker_driver(), "Unable to initialize: ", input);
                break;
        }
    };
    Colorpicker_Controller.prototype.get_data = function (input, options) {
        switch (this.instance.get_state().colorpicker_driver()) {
            case "spectrum":
                var spectrum = $(input.get_elem()).spectrum("get");
                if (spectrum !== null) {
                    return spectrum.toHexString();
                }
                else {
                    console.error("get_data(): Spectrum instance could not be retrieved.");
                    return "";
                }
            default:
                console.error("get_data(): Unknown colorpicker driver specified: ", this.instance.get_state().colorpicker_driver(), "Unable to retrieve data from: ", input);
                return "#000000";
        }
    };
    Colorpicker_Controller.prototype.destroy = function (input) {
        switch (this.instance.get_state().colorpicker_driver()) {
            case "spectrum":
                $(input.get_elem()).spectrum("destroy");
                break;
            default:
                console.error("destroy(): Unknown colorpicker driver specified: ", this.instance.get_state().colorpicker_driver(), "Unable to destroy: ", input);
                break;
        }
    };
    return Colorpicker_Controller;
}(input_controller_1.Input_Controller));
exports.Colorpicker_Controller = Colorpicker_Controller;
//# sourceMappingURL=colorpicker_controller.js.map