"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Datetimepicker_Controller = (function (_super) {
    __extends(Datetimepicker_Controller, _super);
    function Datetimepicker_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Datetimepicker_Controller.prototype.init = function (input, show_datepicker, show_timepicker) {
        if (show_datepicker === void 0) { show_datepicker = true; }
        if (show_timepicker === void 0) { show_timepicker = true; }
        var format = "";
        if (show_datepicker && show_timepicker) {
            format = this.instance.get_state().date_format() +
                this.instance.get_state().date_separator() +
                this.instance.get_state().time_format();
        }
        else if (show_datepicker && !show_timepicker) {
            format = this.instance.get_state().date_format();
        }
        else if (!show_datepicker && show_timepicker) {
            format = this.instance.get_state().time_format();
        }
        else {
            console.error("init_datetimepicker(): At least one of showDatepicker and showTimepicker must be true!");
        }
        var alt_format = format;
        $(input.get_elem()).flatpickr({
            inline: input.is_inlined(),
            dateFormat: format,
            altInput: true,
            altFormat: "d. F Y, H:i",
            locale: "de",
            noCalendar: !show_datepicker,
            weekNumbers: false,
            enableTime: show_timepicker,
            enableSeconds: false,
            time_24hr: true,
            defaultHour: 12,
            defaultMinute: 0,
            onReady: function (selectedDates, dateStr, instance) {
                $(instance.calendarContainer).addClass("crud-instance-wrapper");
                $(instance.input).siblings(".flatpickr-input").removeClass("form-control");
            },
        });
    };
    Datetimepicker_Controller.prototype.get_data = function (input) {
        return String($(input.get_elem()).val());
    };
    Datetimepicker_Controller.prototype.destroy = function (input) {
        $(input.get_elem()).flatpickr().destroy();
    };
    return Datetimepicker_Controller;
}(input_controller_1.Input_Controller));
exports.Datetimepicker_Controller = Datetimepicker_Controller;
//# sourceMappingURL=datetimepicker_controller.js.map