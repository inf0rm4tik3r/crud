"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Limit_Controller = (function (_super) {
    __extends(Limit_Controller, _super);
    function Limit_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Limit_Controller.prototype.init = function (input) {
        var limit_obj_jquery = $("#" + input.get_id() + "_limit");
        if (limit_obj_jquery.length === 0) {
            return;
        }
        var limit_obj = limit_obj_jquery[0];
        if ($(limit_obj).hasClass("crud_limit_infinite")) {
            $(limit_obj).html("&infin;");
            return;
        }
        var data_length = $(input.get_elem()).attr("data-length");
        if (!data_length) {
            console.error("data-length attribute does not exist on: ", input);
            return;
        }
        var max_length = parseInt(data_length);
        this.update_limit(input, max_length, limit_obj);
        $(input.get_elem()).on('change keydown paste', function () {
            this.update_limit(input, max_length, limit_obj);
        }.bind(this));
    };
    Limit_Controller.prototype.update_limit = function (input, max_length, limit_elem) {
        var current_length = String($(input.get_elem()).val()).length;
        var characters_left = max_length - current_length;
        if (characters_left <= 0) {
            $(limit_elem).text(0);
            $(limit_elem).css("color", "#d9281b");
            $(limit_elem).css("font-weight", "bold");
            setTimeout(function () {
                $(limit_elem).css("color", "lightgrey");
                $(limit_elem).css("font-weight", "normal");
            }, 250);
        }
        else {
            $(limit_elem).text(characters_left);
        }
    };
    Limit_Controller.prototype.get_data = function (input) {
        return undefined;
    };
    Limit_Controller.prototype.destroy = function (input) {
    };
    return Limit_Controller;
}(input_controller_1.Input_Controller));
exports.Limit_Controller = Limit_Controller;
//# sourceMappingURL=limit_controller.js.map