"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var crud_tools_1 = require("../../../crud_tools");
var hljs = require("highlight.js");
var Code_Controller = (function (_super) {
    __extends(Code_Controller, _super);
    function Code_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Code_Controller.prototype.init = function (input) {
        var code_edit_id = input.get_id();
        var code_view_id = code_edit_id + "-view";
        var code_edit = input.get_elem();
        var code_view = $("#" + code_view_id)[0];
        hljs.highlightBlock(code_view);
        if (input.is_editable()) {
            var show_view_btn = $("button[data-show='" + code_view_id + "'][data-hide='" + code_edit_id + "']");
            $(show_view_btn).css("display", "none");
            $(code_edit).css("display", "none");
        }
    };
    Code_Controller.prototype.get_data = function (input) {
        return String($(input.get_elem()).val());
    };
    Code_Controller.prototype.destroy = function (input) {
    };
    Code_Controller.prototype.edit = function (obj) {
        var id_to_show = $(obj).attr("data-show");
        var id_to_hide = $(obj).attr("data-hide");
        var to_show = $("#" + id_to_show);
        var to_hide = $("#" + id_to_hide);
        var other_btn = $("button[data-show=" + id_to_hide + "][data-hide=" + id_to_show + "]");
        $(obj).css("display", "none");
        $(other_btn).css("display", "block");
        $(to_hide).css("display", "none");
        $(to_show).css("display", "block");
    };
    Code_Controller.prototype.view = function (obj) {
        var id_to_show = $(obj).attr("data-show");
        var id_to_hide = $(obj).attr("data-hide");
        var to_show = $("#" + id_to_show);
        var to_hide = $("#" + id_to_hide);
        var other_btn = $("button[data-show=" + id_to_hide + "][data-hide=" + id_to_show + "]");
        $(obj).css("display", "none");
        $(other_btn).css("display", "block");
        $(to_hide).css("display", "none");
        $(to_show).html(crud_tools_1.Crud_Tools.escape_html(String($(to_hide).val())));
        hljs.highlightBlock($(to_show)[0]);
        $(to_show).css("display", "block");
    };
    return Code_Controller;
}(input_controller_1.Input_Controller));
exports.Code_Controller = Code_Controller;
//# sourceMappingURL=code_controller.js.map