"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Textarea_Controller = (function (_super) {
    __extends(Textarea_Controller, _super);
    function Textarea_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Textarea_Controller.prototype.init = function (input) {
    };
    Textarea_Controller.prototype.get_data = function (input) {
        return String($(input.get_elem()).val());
    };
    Textarea_Controller.prototype.destroy = function (input) {
    };
    return Textarea_Controller;
}(input_controller_1.Input_Controller));
exports.Textarea_Controller = Textarea_Controller;
//# sourceMappingURL=textarea_controller.js.map