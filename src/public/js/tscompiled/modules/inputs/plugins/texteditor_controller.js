"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var input_controller_1 = require("../input_controller");
var Texteditor_Controller = (function (_super) {
    __extends(Texteditor_Controller, _super);
    function Texteditor_Controller() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Texteditor_Controller.prototype.init = function (input) {
        var self = this;
        switch (this.instance.get_state().texteditor_driver()) {
            case "tinymce":
                tinymce.init({
                    selector: "#" + input.get_id(),
                    height: 200,
                    theme: 'modern',
                    branding: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'utf8_emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                    ],
                    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor utf8_emoticons | codesample help',
                    image_advtab: true,
                    templates: [
                        { title: 'Test template 1', content: 'Test 1' },
                        { title: 'Test template 2', content: 'Test 2' }
                    ],
                    content_css: [
                        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                        '//www.tinymce.com/css/codepen.min.css'
                    ],
                    setup: function (editor) {
                        editor.on("init", function (args) {
                            input.after_init([
                                { "element": editor, "triggers": "keyup" }
                            ]);
                        });
                        editor.on("keyup", function (args) {
                            $(editor).trigger("keyup");
                        });
                    }
                });
                break;
            case "ckeditor":
                CKEDITOR.replace(input.get_id());
                break;
            default:
                console.error("init_texteditor(): Unknown texteditor driver specified: ", this.instance.get_state().texteditor_driver());
                break;
        }
    };
    Texteditor_Controller.prototype.get_data = function (input, options) {
        var format = "html";
        if (options && options.hasOwnProperty("texteditor_format")) {
            format = options["texteditor_format"];
        }
        switch (this.instance.get_state().texteditor_driver()) {
            case "tinymce":
                return tinymce.get(input.get_id()).getContent({ format: format });
            case "ckeditor":
                var ckeditor_instance = CKEDITOR.instances[input.get_id()];
                if (ckeditor_instance === null) {
                    console.error("get_ckeditor_data(): " + input.get_id() + " is not an instance!", true);
                    return "error";
                }
                else {
                    return CKEDITOR.instances[input.get_id()].getData();
                }
            default:
                console.error("get_texteditor_data(): Unknown texteditor driver specified: ", this.instance.get_state().texteditor_driver());
                return "error";
        }
    };
    Texteditor_Controller.prototype.destroy = function (input) {
        switch (this.instance.get_state().texteditor_driver()) {
            case "tinymce":
                tinymce.remove("#" + input.get_id());
                break;
            case "ckeditor":
                var ckeditor_instance = CKEDITOR.instances[input.get_id()];
                if (ckeditor_instance === null || ckeditor_instance === undefined) {
                    console.error("destroy_ckeditor(): " + input.get_id() + " is not an instance!", true);
                }
                else {
                    CKEDITOR.instances[input.get_id()].destroy();
                }
                break;
            default:
                console.error("destroy_texteditor(): Unknown texteditor driver specified: ", this.instance.get_state().texteditor_driver());
                break;
        }
    };
    return Texteditor_Controller;
}(input_controller_1.Input_Controller));
exports.Texteditor_Controller = Texteditor_Controller;
//# sourceMappingURL=texteditor_controller.js.map