"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Validation_Results = (function () {
    function Validation_Results() {
        this.results = {};
    }
    Validation_Results.prototype.add = function (validator_name, validator_results) {
        this.results[validator_name] = validator_results;
    };
    Validation_Results.prototype.get = function () {
        return this.results;
    };
    return Validation_Results;
}());
exports.Validation_Results = Validation_Results;
//# sourceMappingURL=validation_results.js.map