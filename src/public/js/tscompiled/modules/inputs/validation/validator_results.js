"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Validator_Results = (function () {
    function Validator_Results(results) {
        this.results = [];
        this.set(results);
    }
    Validator_Results.prototype.set = function (results) {
        this.results = results;
    };
    Validator_Results.prototype.get = function () {
        return this.results;
    };
    return Validator_Results;
}());
exports.Validator_Results = Validator_Results;
//# sourceMappingURL=validator_results.js.map