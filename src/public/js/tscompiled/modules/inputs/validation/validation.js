"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("../../../crud_tools");
var crud_config_1 = require("../../../crud_config");
var validation_results_1 = require("./validation_results");
var validator_results_1 = require("./validator_results");
var Validation = (function () {
    function Validation(input) {
        this.input = input;
    }
    Validation.prototype.load = function () {
        this.constraints = this.load_constraints();
    };
    Validation.prototype.init = function (activators) {
        if (typeof activators === typeof undefined) {
            var main_activator = {
                'element': this.input,
                'triggers': this.acquire_validation_triggers()
            };
            activators = [];
            activators.push(main_activator);
        }
        if (!crud_tools_1.Crud_Tools.is_set(this.constraints) || crud_tools_1.Crud_Tools.is_empty(this.constraints)) {
            return;
        }
        this.validate();
        $.each(activators, function (key, activator) {
            $(activator['element'].get_elem()).on(activator['triggers'], function () {
                this.validate();
            }.bind(this));
        }.bind(this));
    };
    Validation.prototype.validate = function () {
        var options = {
            texteditor_format: "text"
        };
        var input_data = this.input.get_instance().get_input_manager().acquire_input_data_single(this.input, options);
        if (!input_data) {
            console.error("validate_input(): Unable to load data from input: ", this.input, "Skipping validation...");
            return;
        }
        var value = input_data.value;
        if (this.input.get_internal_type() === "signature") {
            value = JSON.parse(value).data.length === 0 ? '' : 'set';
        }
        var validation_results = new validation_results_1.Validation_Results();
        $.each(this.constraints, function (validator_name, constraint) {
            var validator_constraints = {};
            validator_constraints[validator_name] = constraint;
            validation_results.add(validator_name, new validator_results_1.Validator_Results(validate.single(value, validator_constraints)));
        });
        this.update_validation_state(validation_results);
    };
    Validation.prototype.update_validation_state = function (validation_results) {
        var input_val_msg_output = $("#" + this.input.get_id() + "-validation-msg-output")[0];
        var validation_succeeded = true;
        var msg = "";
        $.each(validation_results.get(), function (validator_name, validator_results) {
            if (typeof validator_results.get() !== typeof undefined) {
                validation_succeeded = false;
            }
        });
        if (validation_succeeded) {
            this.input.remove_class("crud-validation-error");
            this.input.add_class("crud-validation-success");
            msg = "";
        }
        else {
            this.input.remove_class("crud-validation-success");
            this.input.add_class("crud-validation-error");
            msg = "<ul>";
            $.each(validation_results.get(), function (validator_name, validator_results) {
                $.each(validator_results.get(), function (key, line) {
                    msg +=
                        '<li data-validator="' + validator_name + '" data-validator-result-id="' + key + '">' +
                            line +
                            "</li>";
                });
            });
            msg += "</ul>";
        }
        $(input_val_msg_output).html(msg);
    };
    Validation.prototype.load_constraints = function () {
        var constraints_json = $(this.input.get_elem()).attr("data-validation-constraints");
        if (!constraints_json) {
            console.error("load_constraints(): \"data-validation-constraints\" attribute missing on:", this.input);
            return null;
        }
        if (constraints_json.length === 0) {
            return {};
        }
        var constraints = crud_tools_1.Crud_Tools.try_parse_json(constraints_json);
        if (!constraints) {
            console.error("load_constraints(): \"data-validation-constraints\" attribute was not JSON-parsable!", this.input);
            return null;
        }
        return constraints;
    };
    Validation.prototype.acquire_validation_triggers = function () {
        var test = "varchar";
        var triggers = crud_config_1.Crud_Config.validation_triggers[test];
        var trigger_string = "";
        for (var i = 0; i < triggers.length; i++) {
            trigger_string += triggers[i];
            if (i !== triggers.length - 1) {
                trigger_string += " ";
            }
        }
        return trigger_string;
    };
    return Validation;
}());
exports.Validation = Validation;
//# sourceMappingURL=validation.js.map