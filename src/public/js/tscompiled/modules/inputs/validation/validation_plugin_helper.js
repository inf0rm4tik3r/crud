"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("../../../crud_tools");
var Validation_Plugin_Helper = (function () {
    function Validation_Plugin_Helper() {
    }
    Validation_Plugin_Helper.init = function (instance) {
        validate.extend(validate.validators.datetime, {
            parse: function (value, options) {
                var format = undefined;
                if (options.dateOnly) {
                    format = instance.get_state().moment_date_format();
                }
                else {
                    format =
                        instance.get_state().moment_date_format() +
                            instance.get_state().moment_date_separator() +
                            instance.get_state().moment_time_format();
                }
                var moment_obj = moment.utc(value, format);
                if (moment_obj.isValid()) {
                    return moment_obj.unix();
                }
                else {
                    return NaN;
                }
            },
            format: function (value, options) {
                var format = "";
                if (options.dateOnly) {
                    format = instance.get_state().moment_date_format();
                }
                else {
                    format =
                        instance.get_state().moment_date_format() +
                            instance.get_state().moment_date_separator() +
                            instance.get_state().moment_time_format();
                }
                return moment
                    .utc(value)
                    .locale(instance.get_state().language_short())
                    .format(format);
            }
        });
        Validation_Plugin_Helper.clean_validators(instance);
        Validation_Plugin_Helper.init_custom_validators(instance);
        Validation_Plugin_Helper.init_custom_async_validators(instance);
    };
    Validation_Plugin_Helper.clean_validators = function (instance) {
        $.each(validate.validators, function (validator_name, validator) {
            if (!crud_tools_1.Crud_Tools.in_array(validator_name, this.STANDARD_VALIDATORS)) {
                delete validate.validators[validator_name];
            }
        });
    };
    Validation_Plugin_Helper.init_custom_validators = function (instance) {
        $.each(instance.get_state().custom_validators(), function (name, validator) {
            validate.validators[name] = function (value, options, key, attributes) {
                return crud_tools_1.Crud_Tools.execute_function_by_name(validator.js_function_name, window, value, options, key, attributes);
            }.bind(this);
        }.bind(this));
    };
    Validation_Plugin_Helper.init_custom_async_validators = function (instance) {
    };
    Validation_Plugin_Helper.STANDARD_VALIDATORS = [
        'date',
        'datetime',
        'email',
        'equality',
        'exclusion',
        'format',
        'inclusion',
        'length',
        'numericality',
        'presence',
        'url'
    ];
    return Validation_Plugin_Helper;
}());
exports.Validation_Plugin_Helper = Validation_Plugin_Helper;
//# sourceMappingURL=validation_plugin_helper.js.map