"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var behaviours_1 = require("./behaviour/behaviours");
var crud_tools_1 = require("../../crud_tools");
var validation_1 = require("./validation/validation");
var Input = (function () {
    function Input(instance, elem) {
        this.instance = instance;
        this.elem = elem;
    }
    Input.prototype.load = function () {
        var id = $(this.elem).attr("id");
        if (id) {
            this.id = id;
        }
        else {
            console.error("load(): Unable to load the the \"data-inline\" attribute for: ", this.elem);
        }
        var group = $(this.elem).attr("data-group");
        if (group) {
            this.group = group;
        }
        else {
            console.error("load(): Unable to load the the \"data-group\" attribute for: ", this.elem);
        }
        var html_type = $(this.elem).attr("type");
        if (html_type) {
            this.html_type = html_type;
        }
        else {
            this.html_type = undefined;
        }
        var db_type = $(this.elem).attr("data-db-type");
        if (db_type) {
            this.db_type = db_type;
        }
        else {
            console.error("load(): Unable to load the the \"data-db-type\" attribute for: ", this.elem);
        }
        var internal_type = $(this.elem).attr("data-internal-type");
        if (internal_type) {
            this.internal_type = internal_type;
        }
        else {
            console.error("load(): Unable to load the the \"data-internal-type\" attribute for: ", this.elem);
        }
        var display_type = $(this.elem).attr("data-display-type");
        if (display_type) {
            this.display_type = display_type;
        }
        else {
            console.error("load(): Unable to load the the \"data-display-type\" attribute for: ", this.elem);
        }
        var editable = $(this.elem).attr("data-editable");
        if (editable) {
            this.editable = editable !== "false";
        }
        else {
            this.editable = undefined;
            console.error("load(): Unable to load the the \"data-editable\" attribute for: ", this.elem);
        }
        var inlined = $(this.elem).attr("data-inline");
        if (inlined) {
            this.inlined = inlined !== "false";
        }
        else {
            this.inlined = undefined;
        }
        return this;
    };
    Input.prototype.after_init = function (activators) {
        this.validation = new validation_1.Validation(this);
        this.validation.load();
        this.validation.init(activators);
        if (this.group === "entry") {
            this.behaviours = new behaviours_1.Behaviours(this);
            this.behaviours.load();
            this.behaviours.init();
        }
    };
    Input.prototype.show = function () {
        $(this.elem).closest("[data-field-container]").css("display", "block");
    };
    Input.prototype.hide = function () {
        $(this.elem).closest("[data-field-container]").css("display", "none");
    };
    Input.prototype.enable = function () {
        $(this.elem).attr("disabled", null);
    };
    Input.prototype.disable = function () {
        $(this.elem).attr("disabled", "true");
    };
    Input.prototype.acquire_elements_for_class_assignment = function () {
        var objects = [];
        objects.push(this.get_elem());
        $(this.get_elem()).siblings(".select2.select2-container").each(function (i, obj) {
            objects.push(obj);
        });
        $(this.get_elem()).siblings(".flatpickr-input").each(function (i, obj) {
            objects.push(obj);
        });
        return objects;
    };
    Input.prototype.add_class = function (class_names) {
        $.each(this.acquire_elements_for_class_assignment(), function (key, elem) {
            if (crud_tools_1.Crud_Tools.is_array(class_names)) {
                $.each(class_names, function (key, class_name) {
                    $(elem).addClass(class_name);
                });
            }
            else {
                $(elem).addClass(class_names);
            }
        });
    };
    Input.prototype.remove_class = function (class_names) {
        $.each(this.acquire_elements_for_class_assignment(), function (key, elem) {
            if (crud_tools_1.Crud_Tools.is_array(class_names)) {
                $.each(class_names, function (key, class_name) {
                    $(elem).removeClass(class_name);
                });
            }
            else {
                $(elem).removeClass(class_names);
            }
        });
    };
    Input.prototype.get_instance = function () {
        return this.instance;
    };
    Input.prototype.get_elem = function () {
        return this.elem;
    };
    Input.prototype.get_id = function () {
        return this.id;
    };
    Input.prototype.get_group = function () {
        return this.group;
    };
    Input.prototype.get_html_type = function () {
        return this.html_type;
    };
    Input.prototype.get_db_type = function () {
        return this.db_type;
    };
    Input.prototype.get_internal_type = function () {
        return this.internal_type;
    };
    Input.prototype.get_display_type = function () {
        return this.display_type;
    };
    Input.prototype.is_editable = function () {
        return this.editable;
    };
    Input.prototype.is_inlined = function () {
        return this.inlined;
    };
    Input.prototype.get_validation = function () {
        return this.validation;
    };
    Input.prototype.get_behaviours = function () {
        return this.behaviours;
    };
    return Input;
}());
exports.Input = Input;
//# sourceMappingURL=input.js.map