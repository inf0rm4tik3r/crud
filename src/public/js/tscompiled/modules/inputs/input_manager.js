"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var boolean_controller_1 = require("./plugins/boolean_controller");
var code_controller_1 = require("./plugins/code_controller");
var colorpicker_controller_1 = require("./plugins/colorpicker_controller");
var file_controller_1 = require("./plugins/file_controller");
var datetimepicker_controller_1 = require("./plugins/datetimepicker_controller");
var limit_controller_1 = require("./plugins/limit_controller");
var select_controller_1 = require("./plugins/select_controller");
var signature_pad_controller_1 = require("./plugins/signature_pad_controller");
var textarea_controller_1 = require("./plugins/textarea_controller");
var texteditor_controller_1 = require("./plugins/texteditor_controller");
var input_1 = require("./input");
var crud_tools_1 = require("../../crud_tools");
var Input_Manager = (function () {
    function Input_Manager(crud_instance) {
        this.instance = crud_instance;
        this.boolean_controller = new boolean_controller_1.Boolean_Controller(this.instance);
        this.code_controller = new code_controller_1.Code_Controller(this.instance);
        this.colorpicker_controller = new colorpicker_controller_1.Colorpicker_Controller(this.instance);
        this.file_controller = new file_controller_1.File_Controller(this.instance);
        this.datetimepicker_controller = new datetimepicker_controller_1.Datetimepicker_Controller(this.instance);
        this.limit_controller = new limit_controller_1.Limit_Controller(this.instance);
        this.select_controller = new select_controller_1.Select_Controller(this.instance);
        this.signature_pad_controller = new signature_pad_controller_1.Signature_Pad_Controller(this.instance);
        this.textarea_controller = new textarea_controller_1.Textarea_Controller(this.instance);
        this.texteditor_controller = new texteditor_controller_1.Texteditor_Controller(this.instance);
        this.inputs = {};
    }
    Input_Manager.prototype.get_boolean_controller = function () { return this.boolean_controller; };
    Input_Manager.prototype.get_code_controller = function () { return this.code_controller; };
    Input_Manager.prototype.get_colorpicker_controller = function () { return this.colorpicker_controller; };
    Input_Manager.prototype.get_file_controller = function () { return this.file_controller; };
    Input_Manager.prototype.get_datetimepicker_controller = function () { return this.datetimepicker_controller; };
    Input_Manager.prototype.get_select_controller = function () { return this.select_controller; };
    Input_Manager.prototype.get_signature_pad_controller = function () { return this.signature_pad_controller; };
    Input_Manager.prototype.get_texteditor_controller = function () { return this.texteditor_controller; };
    Input_Manager.prototype.init_inputs = function (in_obj) {
        var inputs;
        if (crud_tools_1.Crud_Tools.is_set(in_obj)) {
            inputs = $(in_obj).find("[data-crud-input]");
        }
        else {
            inputs = this.instance.find("[data-crud-input]");
        }
        $(inputs).each(function (key, elem) {
            var input = new input_1.Input(this.instance, elem);
            input.load();
            this.init_input(input);
            this.inputs[input.get_id()] = input;
        }.bind(this));
    };
    Input_Manager.prototype.init_input = function (input) {
        var elem = input.get_elem();
        var finish_init_directly = true;
        switch (input.get_html_type()) {
            case "number":
                $(input).on('change keyup paste', function () {
                    this.instance.limit_input_length(elem);
                }.bind(this));
                break;
        }
        switch (input.get_db_type()) {
            case "default":
                break;
            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "bigint":
                switch (input.get_display_type()) {
                    case "default":
                        break;
                    case "select":
                        this.select_controller.init(input, false);
                        break;
                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;
            case "varchar":
                switch (input.get_display_type()) {
                    case "default":
                        break;
                    case "password":
                        break;
                    case "textarea":
                        this.textarea_controller.init(input);
                        break;
                    case "texteditor":
                        this.texteditor_controller.init(input);
                        finish_init_directly = false;
                        break;
                    case "code":
                        this.code_controller.init(input);
                        break;
                    case "color":
                        this.colorpicker_controller.init(input);
                        break;
                    case "select":
                        this.select_controller.init(input, false);
                        break;
                    case "multiselect":
                        this.select_controller.init(input, true);
                        break;
                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                this.limit_controller.init(input);
                break;
            case "enum":
                switch (input.get_display_type()) {
                    default:
                        this.select_controller.init(input, false);
                        break;
                }
                break;
            case 'timestamp':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.init(input, true, true);
                        break;
                }
                break;
            case 'datetime':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.init(input, true, true);
                        break;
                }
                break;
            case 'date':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.init(input, true, false);
                        break;
                }
                break;
            case 'time':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.init(input, false, true);
                        break;
                }
                break;
            case 'tinytext':
            case 'mediumtext':
            case 'text':
            case 'longtext':
                switch (input.get_display_type()) {
                    case "default":
                    case "textarea":
                        this.textarea_controller.init(input);
                        break;
                    case "texteditor":
                        this.texteditor_controller.init(input);
                        finish_init_directly = false;
                        break;
                    case "signature":
                        this.signature_pad_controller.init(input);
                        break;
                    case "code":
                        this.code_controller.init(input);
                        break;
                    case "color":
                        this.colorpicker_controller.init(input);
                        break;
                    case "select":
                        this.select_controller.init(input, false);
                        break;
                    case "multiselect":
                        this.select_controller.init(input, true);
                        break;
                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;
            case 'file':
            case 'image':
                switch (input.get_display_type()) {
                    default:
                        this.file_controller.init(input);
                        break;
                }
                break;
            default:
                console.error("init_input(): Unknown html data-db-type (" + input.get_db_type() + ")", input);
        }
        if (finish_init_directly === true) {
            input.after_init();
        }
    };
    Input_Manager.prototype.destroy_inputs = function () {
        $.each(this.inputs, function (key, input) {
            this.destroy_input(input);
        }.bind(this));
    };
    Input_Manager.prototype.destroy_input = function (input) {
        switch (input.get_db_type()) {
            case "default":
                break;
            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "bigint":
                switch (input.get_display_type()) {
                    case "default":
                        break;
                    case "select":
                        this.select_controller.destroy(input);
                        break;
                    default:
                        console.error("destroy_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;
            case "varchar":
                switch (input.get_display_type()) {
                    case "default":
                        break;
                    case "password":
                        break;
                    case "textarea":
                        break;
                    case "texteditor":
                        this.texteditor_controller.destroy(input);
                        break;
                    case "code":
                        break;
                    case "color":
                        this.colorpicker_controller.destroy(input);
                        break;
                    case "select":
                        this.select_controller.destroy(input);
                        break;
                    case "multiselect":
                        this.select_controller.destroy(input);
                        break;
                    default:
                        console.error("destroy_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;
            case "enum":
                switch (input.get_display_type()) {
                    default:
                        this.select_controller.destroy(input);
                        break;
                }
                break;
            case 'timestamp':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;
            case 'datetime':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;
            case 'date':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;
            case 'time':
                switch (input.get_display_type()) {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;
            case 'tinytext':
            case 'mediumtext':
            case 'text':
            case 'longtext':
                switch (input.get_display_type()) {
                    case "default":
                    case "textarea":
                        break;
                    case "texteditor":
                        this.texteditor_controller.destroy(input);
                        break;
                    case "signature":
                        this.signature_pad_controller.destroy(input);
                        break;
                    case "code":
                        break;
                    case "color":
                        this.colorpicker_controller.destroy(input);
                        break;
                    case "select":
                        this.select_controller.destroy(input);
                        break;
                    case "multiselect":
                        this.select_controller.destroy(input);
                        break;
                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;
            case 'file':
            case 'image':
                switch (input.get_display_type()) {
                    default:
                        break;
                }
                break;
            default:
                console.error("destroy_input(): Unknown data-db-type (" + input.get_db_type() + ")", input);
        }
        delete this.inputs[input.get_id()];
    };
    Input_Manager.prototype.acquire_input_data = function (group, options) {
        var data_array = [];
        $.each(this.inputs, function (key, input) {
            if (input.get_group() === group) {
                var data = this.acquire_input_data_single(input, options);
                if (data) {
                    data_array.push(data);
                }
            }
        }.bind(this));
        return data_array;
    };
    Input_Manager.prototype.acquire_input_data_single = function (input, options) {
        if (!options) {
            options = {};
        }
        var length_attr = $(input.get_elem()).attr('data-length');
        var length = length_attr ? parseInt(length_attr) : undefined;
        var value = undefined;
        switch (input.get_db_type()) {
            case "varchar":
            case "tinytext":
            case "mediumtext":
            case "text":
            case "longtext":
                switch (input.get_internal_type()) {
                    case "default":
                    case "code":
                    case "password":
                    case "relation":
                        value = String($(input.get_elem()).val());
                        break;
                    case "textarea":
                        value = this.textarea_controller.get_data(input);
                        break;
                    case "texteditor":
                        value = this.texteditor_controller.get_data(input, options);
                        break;
                    case "color":
                        value = this.colorpicker_controller.get_data(input, options);
                        break;
                    case "signature":
                        value = JSON.stringify({
                            'data': this.signature_pad_controller.get_data(input),
                            'data_url': this.signature_pad_controller.get_data_url(input)
                        });
                        break;
                    default:
                        value = undefined;
                        console.error("acquire_input_data_single(): Unknown data-internal-type (" + input.get_internal_type() + ")", input);
                        break;
                }
                break;
            case "tinyint":
                if (length && length === 1) {
                    value = this.boolean_controller.get_data(input);
                }
                break;
            case "file":
            case "image":
                value = this.file_controller.get_data(input);
                break;
            default:
                value = String($(input.get_elem()).val());
                break;
        }
        switch (input.get_display_type()) {
            case "multiselect":
                if (value.length === 0) {
                    value = "";
                }
                break;
        }
        return {
            id: input.get_id(),
            value: value
        };
    };
    Input_Manager.prototype.get_inputs = function () {
        return this.inputs;
    };
    Input_Manager.prototype.get_input = function (id) {
        return this.inputs.hasOwnProperty(id) ? this.inputs[id] : null;
    };
    return Input_Manager;
}());
exports.Input_Manager = Input_Manager;
//# sourceMappingURL=input_manager.js.map