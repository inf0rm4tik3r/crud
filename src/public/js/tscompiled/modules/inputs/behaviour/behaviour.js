"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_config_1 = require("../../../crud_config");
var Behaviour = (function () {
    function Behaviour(input) {
        this.input = input;
    }
    Behaviour.prototype.init = function () {
        this.process();
        $.each(this.get_involved_inputs(), function (key, involved_input) {
            var activators = this.acquire_behaviour_check_triggers(involved_input);
            $(involved_input.get_elem()).on(activators, function () {
                this.process();
            }.bind(this));
        }.bind(this));
    };
    Behaviour.prototype.acquire_behaviour_check_triggers = function (input) {
        var test = "varchar";
        var triggers = crud_config_1.Crud_Config.behaviour_check_triggers[test];
        var trigger_string = "";
        for (var i = 0; i < triggers.length; i++) {
            trigger_string += triggers[i];
            if (i !== triggers.length - 1) {
                trigger_string += " ";
            }
        }
        return trigger_string;
    };
    Behaviour.prototype.process = function () {
        var eval_result = this.condition.evaluate(this.input.get_instance());
        switch (this.action) {
            case "4":
                if (eval_result === true) {
                    this.input.show();
                }
                else {
                    this.input.hide();
                }
                break;
            case "7":
                if (eval_result === true) {
                    this.input.enable();
                }
                else {
                    this.input.disable();
                }
                break;
            default:
                console.error("process(): Unknown behaviour action \"" + this.action + "\". Doing nothing...");
                break;
        }
    };
    Behaviour.prototype.get_involved_input_ids = function () {
        return this.condition.get_involved();
    };
    Behaviour.prototype.get_involved_inputs = function () {
        var input_ids = this.get_involved_input_ids();
        var inputs = [];
        $.each(input_ids, function (key, input_id) {
            var input = this.input.get_instance().get_input_manager().get_input(input_id);
            if (input) {
                inputs.push(input);
            }
            else {
                console.error("get_involved_inputs(): Input for: \"" + input_id + "\" could not be found!");
            }
        }.bind(this));
        return inputs;
    };
    Behaviour.prototype.set_input = function (input) {
        this.input = input;
    };
    Behaviour.prototype.get_input = function () {
        return this.input;
    };
    Behaviour.prototype.set_action = function (action) {
        this.action = action;
    };
    Behaviour.prototype.get_action = function () {
        return this.action;
    };
    Behaviour.prototype.set_condition = function (condition) {
        this.condition = condition;
    };
    Behaviour.prototype.get_condition = function () {
        return this.condition;
    };
    return Behaviour;
}());
exports.Behaviour = Behaviour;
//# sourceMappingURL=behaviour.js.map