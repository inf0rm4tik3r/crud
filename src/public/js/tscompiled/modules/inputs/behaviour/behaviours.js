"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("../../../crud_tools");
var behaviour_1 = require("./behaviour");
var crud_condition_1 = require("../../../crud_condition");
var Behaviours = (function () {
    function Behaviours(input) {
        this.behaviours = null;
        this.input = input;
    }
    Behaviours.prototype.init = function () {
        if (this.behaviours === null) {
            return;
        }
        $.each(this.behaviours, function (key, behaviour) {
            behaviour.init();
        });
    };
    Behaviours.prototype.load = function () {
        this.behaviours = this.load_behaviours();
    };
    Behaviours.prototype.load_behaviours = function () {
        var data_behaviour = $(this.input.get_elem()).attr("data-behaviours");
        if (!crud_tools_1.Crud_Tools.is_set(data_behaviour)) {
            console.error("Behaviour_Controller: load_behaviours(): data-behaviour attribute missing!", this.input);
            return null;
        }
        var data_behaviours = crud_tools_1.Crud_Tools.try_parse_json(data_behaviour);
        if (data_behaviours === false) {
            console.error("Behaviour_Controller: load_behaviours(): " +
                "\"data-behaviour\" attribute was not json-parsable!", data_behaviour);
            return null;
        }
        if (!crud_tools_1.Crud_Tools.is_array(data_behaviours)) {
            console.error("Behaviour_Controller: load_behaviours(): " +
                "\"data-behaviour\" attribute was not a json encoded array!", this.input);
            return null;
        }
        if (crud_tools_1.Crud_Tools.is_empty(data_behaviours)) {
            return null;
        }
        var parsed_behaviours = [];
        var error = false;
        $.each(data_behaviours, function (key, value) {
            var data_behaviour = crud_tools_1.Crud_Tools.try_parse_json(value);
            if (data_behaviour === false) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was not json-parsable!", value);
                error = true;
                return false;
            }
            if (!crud_tools_1.Crud_Tools.is_object(data_behaviour)) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was not an object!", this.input, value);
                error = true;
                return false;
            }
            if (crud_tools_1.Crud_Tools.is_empty(data_behaviour)) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was an empty object!", this.input, value);
                error = true;
                return false;
            }
            data_behaviour.condition = crud_tools_1.Crud_Tools.try_parse_json(String(data_behaviour.condition));
            if (data_behaviour.condition === false) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour condition was not json-parsable!");
                error = true;
                return false;
            }
            if (!crud_tools_1.Crud_Tools.is_array(data_behaviour.condition)) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour condition was not an array!", this.input);
                error = true;
                return false;
            }
            var action = data_behaviour.action;
            var condition = new crud_condition_1.Crud_Condition().from_array(data_behaviour.condition);
            var behaviour = new behaviour_1.Behaviour(this.input);
            behaviour.set_action(String(action));
            behaviour.set_condition(condition);
            parsed_behaviours.push(behaviour);
        }.bind(this));
        if (error) {
            return null;
        }
        return parsed_behaviours;
    };
    return Behaviours;
}());
exports.Behaviours = Behaviours;
//# sourceMappingURL=behaviours.js.map