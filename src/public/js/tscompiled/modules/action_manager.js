"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var modal_1 = require("./modal/modal");
var crud_tools_1 = require("../crud_tools");
var Action_Manager = (function () {
    function Action_Manager(crud_instance) {
        this.instance = crud_instance;
    }
    Action_Manager.prototype.init_actions = function (in_obj) {
        var actions;
        if (crud_tools_1.Crud_Tools.is_set(in_obj)) {
            actions = $(in_obj).find("[data-action]");
        }
        else {
            actions = this.instance.find("[data-action]");
        }
        $(actions).each(function (key, obj) {
            this.init_action(obj);
        }.bind(this));
    };
    Action_Manager.prototype.init_action = function (obj, action_override) {
        var data_action = action_override ? action_override : $(obj).attr("data-action");
        if (!data_action || data_action === "") {
            console.error("init_action(): The data-action attribute is missing or empty!");
            console.error("init_action(): Skipping action initialization for: ", obj);
        }
        switch (data_action) {
            case "list":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_list_view();
                }.bind(this));
                break;
            case "structure":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_structure_view();
                }.bind(this));
                break;
            case "create":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_create_view();
                }.bind(this));
                break;
            case "edit":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_edit_view(String($(obj).attr("data-entry")));
                }.bind(this));
                break;
            case "view":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_view_view(String($(obj).attr("data-entry")));
                }.bind(this));
                break;
            case "print":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_print_view();
                }.bind(this));
                break;
            case "export":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().load_export();
                }.bind(this));
                break;
            case "reload":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().reload($(obj).attr("data-after"));
                }.bind(this));
                break;
            case "reset":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().reset();
                }.bind(this));
                break;
            case "save":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().save_entry(String($(obj).attr("data-after")));
                }.bind(this));
                break;
            case "delete":
                $(obj).on("click", function () {
                    var entry_id = this.instance.get_data_entry_id(obj);
                    if (!entry_id) {
                        console.error("Unable to find entry id!");
                        return;
                    }
                    var task_after = this.instance.get_data_after(obj);
                    var confirm_method = $(obj).attr("data-confirm-method");
                    var modal_template_name = $(obj).attr("data-modal-template");
                    var modal_template_json = String(this.instance.find("input[name='crud-modal-template-" + modal_template_name + "']").val());
                    var modal_template = JSON.parse(modal_template_json);
                    switch (confirm_method) {
                        case "none":
                            this.instance.get_ajax_interface().delete_entry(entry_id, task_after);
                            break;
                        case "system":
                            var text = String($("<div>").html(modal_template.body).text());
                            if (window.confirm(text) === true) {
                                this.instance.get_ajax_interface().delete_entry(entry_id, task_after);
                            }
                            break;
                        case "modal":
                            var modal = new modal_1.Modal(this.instance);
                            modal.init(modal_template, function (element) {
                                if ($(element).attr("data-modal-action") === "confirm") {
                                    this.instance.get_ajax_interface().delete_entry(entry_id, task_after);
                                }
                            }.bind(this), this.instance.get_elem());
                            return;
                        default:
                            console.error("init_action(): delete: \"" + confirm_method + "\" is an unknown confirmation method.");
                            return;
                    }
                }.bind(this));
                break;
            case "restore":
                $(obj).on("click", function () {
                    var entry_id = this.instance.get_data_entry_id(obj);
                    if (!entry_id) {
                        console.error("Unable to find entry id!");
                        return;
                    }
                    var task_after = this.instance.get_data_after(obj);
                    var confirm_method = $(obj).attr("data-confirm-method");
                    var modal_template_name = $(obj).attr("data-modal-template");
                    var modal_template_json = String(this.instance.find("input[name='crud-modal-template-" + modal_template_name + "']").val());
                    var modal_template = JSON.parse(modal_template_json);
                    switch (confirm_method) {
                        case "none":
                            this.instance.get_ajax_interface().restore_entry(entry_id, task_after);
                            break;
                        case "system":
                            var text = String($("<div>").html(modal_template.body).text());
                            if (window.confirm(text) === true) {
                                this.instance.get_ajax_interface().restore_entry(entry_id, task_after);
                            }
                            break;
                        case "modal":
                            var modal = new modal_1.Modal(this.instance);
                            modal.init(modal_template, function (element) {
                                if ($(element).attr("data-modal-action") === "confirm") {
                                    this.instance.get_ajax_interface().restore_entry(entry_id, task_after);
                                }
                            }.bind(this), this.instance.get_elem());
                            return;
                        default:
                            console.error("init_action(): restore: \"" + confirm_method + "\" in an unknown confirmation method.");
                            return;
                    }
                }.bind(this));
                break;
            case "limit":
                if (!($(obj).hasClass("crud-btn-active") || $(obj).hasClass("disabled"))) {
                    $(obj).on("click", function () {
                        this.instance.get_ajax_interface().load_limit(String($(obj).attr("data-limit")));
                    }.bind(this));
                }
                break;
            case "page":
                var page_1 = String($(obj).attr("data-page"));
                if (!(page_1 === "-1" || $(obj).hasClass("crud-btn-disabled") || $(obj).hasClass("crud-btn-active"))) {
                    $(obj).on("click", function () {
                        this.instance.get_ajax_interface().load_page(page_1);
                    }.bind(this));
                }
                break;
            case "order-by":
                $(obj).on("click", function (event) {
                    var column_name = $(obj).find("[data-column-name]").data("column-name");
                    this.instance.get_ajax_interface().order_by(column_name, event.ctrlKey || event.metaKey);
                }.bind(this));
                break;
            case "inlineedit":
                switch (this.instance.get_state().inlineedit_activator()) {
                    case "click":
                    case "dblclick":
                        $(obj).on(this.instance.get_state().inlineedit_activator(), function () {
                            this.instance.init_inlineedit(obj);
                        }.bind(this));
                        break;
                    case "btn":
                        $(obj).on("click", function () {
                            this.instance.init_inlineedit($(obj).closest("td")[0]);
                        }.bind(this));
                        break;
                    default:
                        console.error("init_action(): inlineedit: Unknown inlineedit activator specified: ", this.instance.get_state().inlineedit_activator());
                }
                break;
            case "open-search":
                $(obj).on("click", function () {
                    this.instance.open_search();
                }.bind(this));
                break;
            case "apply-search":
                $(obj).on("click", function () {
                    this.instance.apply_search();
                }.bind(this));
                break;
            case "reset-search":
                $(obj).on("click", function () {
                    this.instance.reset_search();
                }.bind(this));
                break;
            case "toggle-search-tab":
                $(obj).on("click", function () {
                    this.instance.get_ajax_interface().toggle_search_tab(String($(obj).attr("data-search-tab")));
                }.bind(this));
                break;
            case "unlock-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): unlock-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_1 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_1) {
                    console.error("init_action(): unlock-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().unlock(input_1);
                }.bind(this));
                break;
            }
            case "lock-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): lock-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_2 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_2) {
                    console.error("init_action(): lock-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().lock(input_2);
                }.bind(this));
                break;
            }
            case "draw-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): draw-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_3 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_3) {
                    console.error("init_action(): draw-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().enter_draw_mode(input_3);
                }.bind(this));
                break;
            }
            case "erase-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): erase-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_4 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_4) {
                    console.error("init_action(): erase-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().enter_erase_mode(input_4);
                }.bind(this));
                break;
            }
            case "undo-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): undo-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_5 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_5) {
                    console.error("init_action(): undo-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().undo(input_5);
                }.bind(this));
                break;
            }
            case "clear-signature-pad": {
                var signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): clear-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                var input_6 = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input_6) {
                    console.error("init_action(): clear-signature-pad: Input could not be found!");
                    break;
                }
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_signature_pad_controller().clear(input_6);
                }.bind(this));
                break;
            }
            case "edit-code":
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_code_controller().edit(obj);
                }.bind(this));
                break;
            case "view-code":
                $(obj).on("click", function () {
                    this.instance.get_input_manager().get_code_controller().view(obj);
                }.bind(this));
                break;
            case "toggle-overlay":
                $(obj).on("click", function () {
                    this.instance.toggle_overlay();
                }.bind(this));
                break;
            case "toggle-fullscreen":
                $(obj).on("click", function () {
                    this.instance.toggle_fullscreen();
                }.bind(this));
                break;
            case "show-system-info":
                $(obj).on("click", function () {
                    this.instance.show_info("system-info", {});
                }.bind(this));
                break;
            case "show-tab":
                $(obj).on("click", function (event) {
                    event.preventDefault();
                    var tab_id = $(obj).data("tab-ref");
                    this.instance.get_ui_controller().show_tab(tab_id);
                }.bind(this));
                break;
            case "close-alert":
                $(obj).on("click", function () {
                    $(obj).closest("[data-crud-alert]").first().remove();
                }.bind(this));
                break;
            default:
                console.error("init_action(): Unknown action: ", data_action, obj);
                break;
        }
    };
    return Action_Manager;
}());
exports.Action_Manager = Action_Manager;
//# sourceMappingURL=action_manager.js.map