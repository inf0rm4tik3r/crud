"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_config_1 = require("../crud_config");
var modal_1 = require("./modal/modal");
var crud_tools_1 = require("../crud_tools");
var Ajax_Interface = (function () {
    function Ajax_Interface(crud_instance) {
        this.instance = crud_instance;
    }
    Ajax_Interface.prototype.load_field = function (entry_id, field_name, callback_func) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_field", [entry_id, field_name]);
        $.ajax({
            "url": crud_config_1.Crud_Config.load_field_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_entry_id": entry_id,
                "crud_field_name": field_name
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_field", [response]);
            if ($.isFunction(callback_func)) {
                callback_func(response);
            }
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_field(): ajax call failed");
            console.error("load_field(): Status: " + text_status);
            console.error("load_field(): Error: " + error_thrown);
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_list_view = function () {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_list_view");
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_list_view_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": "list"
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_list_view", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_list_view(): ajax call failed");
            console.error("load_list_view(): Status: " + text_status);
            console.error("load_list_view(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_structure_view = function () {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_structure_view");
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_structure_view_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": "structure"
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_structure_view", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_structure_view(): ajax call failed");
            console.error("load_structure_view(): Status: " + text_status);
            console.error("load_structure_view(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_create_view = function () {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_create_view");
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_create_view_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": "create"
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_create_view", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_create_view(): ajax call failed");
            console.error("load_create_view(): Status: " + text_status);
            console.error("load_create_view(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_edit_view = function (entry_identifier) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_edit_view", [entry_identifier]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_edit_view_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": "edit",
                "crud_entry": entry_identifier
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_edit_view", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_edit_view(): ajax call failed");
            console.error("load_edit_view(): Status: " + text_status);
            console.error("load_edit_view(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_view_view = function (entry_identifier) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_view_view", [entry_identifier]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_view_view_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": "view",
                "crud_entry": entry_identifier
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_view_view", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_view_view(): ajax call failed");
            console.error("load_view_view(): Status: " + text_status);
            console.error("load_view_view(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_print_view = function () {
        var modal = new modal_1.Modal(this.instance);
        modal.init({
            header: '<h5 class="crud-modal-title">Info...</h5>',
            body: "Diese Funktion ist noch nicht verfügbar.",
            footer: '<button type="button" class="crud-btn crud-btn-primary" data-modal-action="confirm" data-izimodal-close>OK</button>'
        }, function (element) {
        }, this.instance.get_elem());
    };
    Ajax_Interface.prototype.load_export = function () {
        var modal = new modal_1.Modal(this.instance);
        modal.init({
            header: '<h5 class="crud-modal-title">Info...</h5>',
            body: "Diese Funktion ist noch nicht verfügbar.",
            footer: '<button type="button" class="crud-btn crud-btn-primary" data-modal-action="confirm" data-izimodal-close>OK</button>'
        }, function (element) {
        }, this.instance.get_elem());
        this.reload();
    };
    Ajax_Interface.prototype.reload = function (task_after) {
        if (typeof task_after === typeof undefined) {
            task_after = this.instance.get_state().task();
        }
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_reload");
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.reload_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": task_after
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_reload", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("reload(): ajax call failed");
            console.error("reload(): Status: " + text_status);
            console.error("reload(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.reset = function () {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_reset");
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.reset_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": this.instance.get_state().task()
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_reset", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("reset(): ajax call failed");
            console.error("reset(): Status: " + text_status);
            console.error("reset(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.save_entry = function (task_after) {
        var field_data = this.instance.get_input_manager().acquire_input_data("entry");
        var fd = new FormData();
        fd.append("crud_instance_name", this.instance.get_state().instance_name());
        fd.append("crud_instance_verification_key", this.instance.get_state().instance_verification_key());
        fd.append("crud_task_after", task_after);
        fd.append("crud_task_from", this.instance.get_state().task());
        crud_tools_1.Crud_Tools.append_to_form_data(fd, "crud_field_data", field_data);
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_save", [field_data]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.save_route,
            "method": "POST",
            "contentType": false,
            "processData": false,
            "cache": false,
            "data": fd
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_save", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("save_entry(): ajax call failed");
            console.error("save_entry(): Status: " + text_status);
            console.error("save_entry(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.save_inline = function (entry_id, input) {
        var field_data = [this.instance.get_input_manager().acquire_input_data_single(input)];
        var fd = new FormData();
        fd.append("crud_instance_name", this.instance.get_state().instance_name());
        fd.append("crud_instance_verification_key", this.instance.get_state().instance_verification_key());
        fd.append("crud_task_after", this.instance.get_state().task());
        fd.append("crud_task_from", this.instance.get_state().task());
        fd.append("crud_entry_id", entry_id);
        crud_tools_1.Crud_Tools.append_to_form_data(fd, "crud_field_data", field_data);
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_save_field", [field_data]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.save_inline_route,
            "method": "POST",
            "contentType": false,
            "processData": false,
            "cache": false,
            "data": fd
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("before_save_field", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("save_field(): ajax call failed");
            console.error("save_field(): Status: " + text_status);
            console.error("save_field(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.delete_entry = function (entry_identifier, task_after) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_delete", [entry_identifier, task_after]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.delete_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": task_after,
                "crud_entry_id": entry_identifier
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_delete", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("delete_entry(): ajax call failed");
            console.error("delete_entry(): Status: " + text_status);
            console.error("delete_entry(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.restore_entry = function (entry_identifier, task_after) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_restore", [entry_identifier, task_after]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.restore_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_task_after": task_after,
                "crud_entry_id": entry_identifier
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_restore", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("restore_entry(): ajax call failed");
            console.error("restore_entry(): Status: " + text_status);
            console.error("restore_entry(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_limit = function (limit) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_limit", [limit]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_limit_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_limit": limit
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_limit", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_limit(): ajax call failed");
            console.error("load_limit(): Status: " + text_status);
            console.error("load_limit(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.load_page = function (page) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_load_page", [page]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.load_page_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_page": page
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_load_page", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("load_page(): ajax call failed");
            console.error("load_page(): Status: " + text_status);
            console.error("load_page(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.order_by = function (column_name, should_be_appended) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_order_by", [column_name]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.order_by_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "crud_column_name": column_name,
                "should_be_appended": should_be_appended ? '1' : '0'
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_order_by", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("order_by(): ajax call failed");
            console.error("order_by(): Status: " + text_status);
            console.error("order_by(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.apply_reorder = function (id_dragged, old_position, new_position) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_apply_reorder", [id_dragged, old_position, new_position]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.apply_reorder_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "id_dragged": id_dragged,
                "old_position": old_position,
                "new_position": new_position
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_apply_reorder", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("apply_reorder(): ajax call failed");
            console.error("apply_reorder(): Status: " + text_status);
            console.error("apply_reorder(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    Ajax_Interface.prototype.toggle_search_tab = function (search_tab_internal_name) {
        this.instance.trigger("beforerequest");
        this.instance.trigger("before_toggle_search_tab", [search_tab_internal_name]);
        this.instance.enable_overlay();
        $.ajax({
            "url": crud_config_1.Crud_Config.toggle_search_tab_route,
            "method": "POST",
            "data": {
                "crud_instance_name": this.instance.get_state().instance_name(),
                "crud_instance_verification_key": this.instance.get_state().instance_verification_key(),
                "search_tab_internal_name": search_tab_internal_name
            }
        }).done(function (response, text_status, jqXHR) {
            this.instance.trigger("afterrequest", [response]);
            this.instance.trigger("after_toggle_search_tab", [response]);
            this.instance.init(response);
        }.bind(this)).fail(function (jqXHR, text_status, error_thrown) {
            console.error("toggle_search_tab(): ajax call failed");
            console.error("toggle_search_tab(): Status: " + text_status);
            console.error("toggle_search_tab(): Error: " + error_thrown);
            this.instance.disable_overlay();
            this.instance.trigger("requestfail");
            this.instance.show_info("requestfail", { "jqXHR": jqXHR, "text_status": text_status, "error_thrown": error_thrown });
        }.bind(this));
    };
    return Ajax_Interface;
}());
exports.Ajax_Interface = Ajax_Interface;
//# sourceMappingURL=ajax_interface.js.map