"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Crud_Condition_Clause = (function () {
    function Crud_Condition_Clause(field, operator, value, identifier) {
        if (identifier === void 0) { identifier = ""; }
        this.set_field(field);
        this.set_operator(operator);
        this.set_value(value);
        this.set_identifier(identifier);
    }
    Crud_Condition_Clause.prototype.to_string = function () {
        return JSON.stringify(this.field + " " + this.operator + " " + JSON.stringify(this.value));
    };
    Crud_Condition_Clause.prototype.get_field = function () {
        return this.field;
    };
    Crud_Condition_Clause.prototype.set_field = function (value) {
        this.field = value;
    };
    Crud_Condition_Clause.prototype.get_operator = function () {
        return this.operator;
    };
    Crud_Condition_Clause.prototype.set_operator = function (value) {
        this.operator = value;
    };
    Crud_Condition_Clause.prototype.get_value = function () {
        return this.value;
    };
    Crud_Condition_Clause.prototype.set_value = function (value) {
        this.value = value;
    };
    Crud_Condition_Clause.prototype.get_identifier = function () {
        return this.identifier;
    };
    Crud_Condition_Clause.prototype.set_identifier = function (value) {
        this.identifier = value;
    };
    return Crud_Condition_Clause;
}());
exports.Crud_Condition_Clause = Crud_Condition_Clause;
//# sourceMappingURL=crud_condition_clause.js.map