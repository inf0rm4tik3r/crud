"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var crud_tools_1 = require("./crud_tools");
var crud_condition_clause_1 = require("./crud_condition_clause");
var Crud_Condition = (function () {
    function Crud_Condition() {
    }
    Crud_Condition.prototype.to_string = function () {
        var out = "";
        $.each(this.query, function (key, value) {
            if (crud_tools_1.Crud_Tools.is_string(value)) {
                switch (value) {
                    case 'AND':
                        out += ' && ';
                        break;
                    case 'OR':
                        out += ' || ';
                        break;
                }
            }
            else if (value instanceof crud_condition_clause_1.Crud_Condition_Clause) {
                out += "(" + value.to_string() + ")";
            }
            else if (value instanceof Crud_Condition) {
                out += "(" + value.to_string() + ")";
            }
        });
        return out;
    };
    Crud_Condition.prototype.evaluate = function (instance) {
        var eval_string = this.compute_eval_string(instance);
        if (eval_string === null) {
            console.error("evaluate(): The condition could not be parsed to an evaluable string.");
            return true;
        }
        try {
            var eval_result = eval(eval_string);
            return eval_result === true;
        }
        catch (e) {
            console.error("evaluate(): The eval_string \"" + eval_string + "\" could not get evaluated!");
            return true;
        }
    };
    Crud_Condition.prototype.compute_eval_string = function (instance) {
        return this.compute_eval_string_recursive(instance, this.query);
    };
    Crud_Condition.prototype.compute_eval_string_recursive = function (instance, query) {
        var eval_string = "";
        var has_error = false;
        $.each(query, function (key, value) {
            if (crud_tools_1.Crud_Tools.is_string(value)) {
                switch (value) {
                    case 'AND':
                        eval_string += " && ";
                        break;
                    case 'OR':
                        eval_string += " || ";
                        break;
                }
            }
            else if (value instanceof crud_condition_clause_1.Crud_Condition_Clause) {
                var input = instance.get_input_manager().get_input(value.get_field());
                if (!input) {
                    console.warn("compute_eval_string_recursive(): Input not found: ", value.get_field());
                    has_error = true;
                    return false;
                }
                var input_data = instance.get_input_manager().acquire_input_data_single(input);
                if (!crud_tools_1.Crud_Tools.is_set(input_data)) {
                    console.error("compute_eval_string(): Input data could not be acquired for: ", input);
                    has_error = true;
                    return false;
                }
                var val_current = input_data["value"];
                var val_check = value.get_value();
                switch (value.get_operator()) {
                    case "=":
                        eval_string += eval(val_current + " === " + val_check);
                        break;
                    case "!=":
                    case "<>":
                        eval_string += eval(val_current + " !== " + val_check);
                        break;
                    case "<":
                    case "<=":
                    case ">":
                    case ">=":
                        eval_string += eval(val_current + " " + value.get_operator() + " " + val_check);
                        break;
                    case "*%":
                    case "STARTS_WITH":
                        eval_string += String(String(val_current).startsWith(String(val_check)));
                        break;
                    case "%*":
                    case "ENDS_WITH":
                        eval_string += String(String(val_current).endsWith(String(val_check)));
                        break;
                    case "%*%":
                    case "CONTAINS":
                        eval_string += String(String(val_current).indexOf(String(val_check)) !== -1);
                        break;
                    case "*":
                    case "EQUALS":
                        eval_string += String(String(val_current) === String(val_check));
                        break;
                    case "IN":
                        eval_string += String(crud_tools_1.Crud_Tools.in_array(val_current, val_check));
                        break;
                    case "!IN":
                        eval_string += String(!crud_tools_1.Crud_Tools.in_array(val_current, val_check));
                        break;
                    case "><":
                    case "BETWEEN":
                        eval_string += String(crud_tools_1.Crud_Tools.is_between(val_current, val_check));
                        break;
                    case "!><":
                    case "!BETWEEN":
                        eval_string += String(!crud_tools_1.Crud_Tools.is_between(val_current, val_check));
                        break;
                    default:
                        console.error("compute_eval_string(): Unknown operator found: ", value.get_operator());
                        break;
                }
            }
            else if (value instanceof Crud_Condition) {
                eval_string += "(" + this.compute_eval_string_recursive(instance, value.get_query()) + ")";
            }
        });
        if (has_error) {
            return null;
        }
        return eval_string;
    };
    Crud_Condition.prototype.get_involved = function () {
        var inv = this.compute_involved_recursive(this.query);
        return crud_tools_1.Crud_Tools.unique_array(inv);
    };
    Crud_Condition.prototype.compute_involved_recursive = function (query) {
        var involved_fields = [];
        $.each(query, function (key, value) {
            if (value instanceof crud_condition_clause_1.Crud_Condition_Clause) {
                involved_fields.push(value.get_field());
            }
            else if (value instanceof Crud_Condition) {
                involved_fields = involved_fields.concat(value.get_involved());
            }
        }.bind(this));
        return involved_fields;
    };
    Crud_Condition.prototype.from_array = function (arr) {
        this.query = [];
        $.each(arr, function (key, value) {
            if (crud_tools_1.Crud_Tools.is_object(value)) {
                this.query.push(new crud_condition_clause_1.Crud_Condition_Clause(value["col"], value["op"], value["val"], value["identifier"]));
            }
            else if (crud_tools_1.Crud_Tools.is_array(value)) {
                this.query.push(new Crud_Condition().from_array(value));
            }
            else {
                this.query.push(value);
            }
        }.bind(this));
        return this;
    };
    Crud_Condition.prototype.get_query = function () {
        return this.query;
    };
    return Crud_Condition;
}());
exports.Crud_Condition = Crud_Condition;
//# sourceMappingURL=crud_condition.js.map