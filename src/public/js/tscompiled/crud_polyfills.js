"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Reflect;
var Crud_Polyfills = (function () {
    function Crud_Polyfills() {
    }
    Crud_Polyfills.load = function () {
        if (!Array.isArray) {
            Array.isArray = function (obj) {
                return Object.prototype.toString.call(obj) === "[object Array]";
            };
        }
        if (!(typeof Reflect === "object" && typeof Reflect.ownKeys === "function")) {
            if (typeof Reflect === "undefined") {
                Reflect = {};
            }
            if (typeof Object.getOwnPropertySymbols === "function") {
                Reflect.ownKeys = function (o) {
                    return Object.getOwnPropertyNames(o).concat(Object.getOwnPropertySymbols(o));
                };
            }
            else {
                Reflect.ownKeys = Object.getOwnPropertyNames;
            }
        }
        var reduce = Function.bind.call(Function.call, Array.prototype.reduce);
        var isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable);
        var concat = Function.bind.call(Function.call, Array.prototype.concat);
        var keys = Reflect.ownKeys;
        if (!Object.values) {
            Object.values = function values(O) {
                return reduce(keys(O), function (v, k) { return concat(v, typeof k === 'string' && isEnumerable(O, k) ? [O[k]] : []); }, []);
            };
        }
        if (!Object.entries) {
            Object.entries = function entries(O) {
                return reduce(keys(O), function (e, k) { return concat(e, typeof k === 'string' && isEnumerable(O, k) ? [[k, O[k]]] : []); }, []);
            };
        }
    };
    return Crud_Polyfills;
}());
exports.Crud_Polyfills = Crud_Polyfills;
//# sourceMappingURL=crud_polyfills.js.map