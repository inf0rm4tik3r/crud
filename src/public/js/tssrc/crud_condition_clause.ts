

export class Crud_Condition_Clause {

    private field: string;
    private operator: string;
    private value: any;
    private identifier: string;

    constructor(field: string, operator: string, value: any, identifier: string = "") {
        this.set_field(field);
        this.set_operator(operator);
        this.set_value(value);
        this.set_identifier(identifier);
    }

    public to_string(): string {
        return JSON.stringify(this.field + " " + this.operator + " " + JSON.stringify(this.value));
    }

    public get_field(): string {
        return this.field;
    }

    public set_field(value: string): void {
        this.field = value;
    }

    public get_operator(): string {
        return this.operator;
    }

    public set_operator(value: string): void {
        this.operator = value;
    }

    public get_value(): any {
        return this.value;
    }

    public set_value(value: any): void {
        this.value = value;
    }

    public get_identifier(): string {
        return this.identifier;
    }

    public set_identifier(value: string): void {
        this.identifier = value;
    }

}