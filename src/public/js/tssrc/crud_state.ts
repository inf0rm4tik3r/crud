import {Crud_Types} from "./crud_types";
import {Crud_Tools} from "./crud_tools";
import {Crud_Instance} from "./crud_instance";

export class Crud_State {

    private instance: Crud_Instance;
    
    /*
     * ------------------------------------------------
     * State variables (sv_)
     * ------------------------------------------------
     */
    private sv_instance_name: string;
    private sv_instance_verification_key: string;
    private sv_is_loaded: boolean;
    private sv_is_nested: boolean;
    private sv_parent_instance_name: string;
    private sv_task: string;
    private sv_entry: string;
    private sv_date_format: string;
    private sv_date_separator: string;
    private sv_time_format: string;
    private sv_moment_date_format: string;
    private sv_moment_date_separator: string;
    private sv_moment_time_format: string;
    private sv_custom_validators: Crud_Types.Custom_Validators;
    private sv_custom_async_validators: Crud_Types.Custom_Validators;
    private sv_texteditor_driver: string;
    private sv_select_driver: string;
    private sv_colorpicker_driver: string;
    private sv_language_short: string;
    private sv_translations: Crud_Types.Crud_Translations;
    private sv_is_inlineedit: boolean;
    private sv_inlineedit_mode: string;
    private sv_inlineedit_activator: string;
    private sv_has_local_storage: boolean;
    // ------------------------------------------------

    /**
     * Constructor.
     * @param {Crud_Instance} crud_instance
     */
    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }

    // TODO: Info: All state items now come from a single input Element. Does this conflict with Crud.instance(...) ?

    /**
     * Loads the instances "state" from the DOM.
     * This is an expensive function and should only be called during the initialization of a Crud_Instance.
     */
    public load(): void {
        const json_state = String( this.instance.find(".crud-base input[name='crud_state']").val() );

        if ( !json_state ) {
            console.error("load(): crud_state was not found in DOM!");
            return;
        }

        const state: { [key: string]: any } = JSON.parse(json_state);

        this.sv_instance_name             = state.instance_name;
        this.sv_instance_verification_key = state.instance_verification_key;
        this.sv_is_loaded                 = state.instance_is_loaded === "true";
        this.sv_is_nested                 = state.instance_is_nested === "true";
        this.sv_parent_instance_name      = state.parent_instance_name;

        this.sv_task                      = state.task;
        this.sv_entry                     = state.entry;

        this.sv_date_format               = state.date_format;
        this.sv_date_separator            = state.date_separator;
        this.sv_time_format               = state.time_format;
        this.sv_moment_date_format        = state.moment_date_format;
        this.sv_moment_date_separator     = state.moment_date_separator;
        this.sv_moment_time_format        = state.moment_time_format;

        this.sv_custom_validators         = JSON.parse( state.custom_validators );
        this.sv_custom_async_validators   = JSON.parse( state.custom_async_validators );

        this.sv_texteditor_driver         = state.texteditor_driver;
        this.sv_select_driver             = state.select_driver;
        this.sv_colorpicker_driver        = state.colorpicker_driver;

        this.sv_language_short            = state.language_short;
        this.sv_translations              = JSON.parse( state.translations );

        this.sv_is_inlineedit             = state.is_inlineedit === "true";
        this.sv_inlineedit_mode           = state.inlineedit_mode;
        this.sv_inlineedit_activator      = state.inlineedit_activator;

        this.sv_has_local_storage         = Crud_Tools.is_local_storage_available() === true;
    }

    /*
     * Getter methods for retrieving specific values from this state object.
     */

    public instance_name(): string {return this.sv_instance_name;}
    public instance_verification_key(): string {return this.sv_instance_verification_key;}
    public is_loaded(): boolean {return this.sv_is_loaded;}
    public is_nested(): boolean {return this.sv_is_nested;}
    public parent_instance_name(): string {return this.sv_parent_instance_name;}
    public task(): string {return this.sv_task;}
    public entry(): string {return this.sv_entry;}
    public date_format(): string {return this.sv_date_format;}
    public date_separator(): string {return this.sv_date_separator;}
    public time_format(): string {return this.sv_time_format;}
    public moment_date_format(): string {return this.sv_moment_date_format;}
    public moment_date_separator(): string {return this.sv_moment_date_separator;}
    public moment_time_format(): string {return this.sv_moment_time_format;}
    public custom_validators(): Crud_Types.Custom_Validators {return this.sv_custom_validators;}
    public custom_async_validators(): Crud_Types.Custom_Validators {return this.sv_custom_async_validators;}
    public texteditor_driver(): string {return this.sv_texteditor_driver;}
    public select_driver(): string {return this.sv_select_driver;}
    public colorpicker_driver(): string {return this.sv_colorpicker_driver;}
    public language_short(): string {return this.sv_language_short;}
    public translations(): Crud_Types.Crud_Translations {return this.sv_translations;}
    public is_inlineedit(): boolean {return this.sv_is_inlineedit;}
    public inlineedit_mode(): string {return this.sv_inlineedit_mode;}
    public inlineedit_activator(): string {return this.sv_inlineedit_activator;}
    public has_local_storage(): boolean {return this.sv_has_local_storage;}

}