import {Crud_Config} from "./crud_config";
import {Crud_Types} from "./crud_types";
import {Crud} from "./crud";
import {Action_Manager} from "./modules/action_manager";
import {Input_Manager} from "./modules/inputs/input_manager";
import {Ajax_Interface} from "./modules/ajax_interface";
import {Crud_State} from "./crud_state";
import {UI_Controller} from "./modules/ui_controller";
import {Tooltip_Manager} from "./modules/tooltip_manager";
import {Modal} from "./modules/modal/modal";
import {Input} from "./modules/inputs/input";
import {Validation_Plugin_Helper} from "./modules/inputs/validation/validation_plugin_helper";
import {Inlineedit_Modal} from "./modules/modal/inlineedit_modal";

/**
 * Creates a new Crud_Instance object. The constructor will not call the main initializer function by itself.
 * It is necessary to first call the init() function on a newly created Crud_Instance object!
 */
export class Crud_Instance {

    /* PRIVATE ATTRIBUTES */
    private dom_elem: JQuery;
    private state: Crud_State;
    private nested_instances: Array<Crud_Instance>;

    private ajax_interface: Ajax_Interface;
    private action_manager: Action_Manager;
    private input_manager: Input_Manager;
    private tooltip_manager: Tooltip_Manager;

    private ui_controller: UI_Controller;

    /**
     *
     * @param {JQuery} dom_elem
     */
    constructor(dom_elem: JQuery) {
        this.dom_elem = dom_elem;
        this.state = new Crud_State(this);
        this.nested_instances = [];

        this.ajax_interface = new Ajax_Interface(this);
        this.action_manager = new Action_Manager(this);
        this.input_manager = new Input_Manager(this);
        this.tooltip_manager = new Tooltip_Manager(this);

        this.ui_controller = new UI_Controller(this);
    }

    public get_elem(): JQuery {
        return this.dom_elem;
    }

    public get_state(): Crud_State {
        return this.state;
    }

    public get_action_manager(): Action_Manager {
        return this.action_manager;
    }

    public get_input_manager(): Input_Manager {
        return this.input_manager;
    }

    public get_ajax_interface() {
        return this.ajax_interface;
    }

    public get_ui_controller() {
        return this.ui_controller;
    }

    get_name(): string {
        return this.state.instance_name();
    }

    has_name(name: string): boolean {
        return this.get_name() === name;
    }

    clear_nested_instances() {
        this.nested_instances = [];
    }

    put_nested_instance(crud_instance: Crud_Instance) {
        this.nested_instances.push(crud_instance);
    }

    get_nested_instances(): Array<Crud_Instance> {
        return this.nested_instances;
    }

    find_instance(instance_name: string): Crud_Instance | undefined {
        if ( this.has_name(instance_name) ) {
            return this;
        }

        for ( let i = 0; i < this.get_nested_instances().length; i++ ) {
            let current = this.get_nested_instances()[i];

            let found = current.find_instance(instance_name);
            if ( typeof found !== typeof undefined )
            {
                return found;
            }
        }

        return undefined;
    }

    public find_instances(target_name: string): Crud_Instance[] {
        let found: Crud_Instance[] = [];

        if ( this.has_name(target_name) ) {
            found.push(this);
        }

        for ( let i = 0; i < this.get_nested_instances().length; i++ ) {
            const current = this.get_nested_instances()[i];

            const found_nested = current.find_instances(target_name);
            found = found.concat(found_nested);
        }

        return found;
    }

    /**
     * Initializes a specific instance.
     * Every instance which is nested inside this one gets initialized as well.
     *
     * @param response
     *      Either pass 'undefined' if you want the system to initialize, or a string containing the new HTML markup
     *      for the main crud-div (normally provided by ajax responses).
     */
    init(response: string | undefined): void {
        const start = window.performance.now();

        // Replaces the crud-HTML if data got supplied.
        if ( response ) {
            // Destroy the last view, display and initialize the new view.
            this.destroy();
            this.replace_self(response);
        }

        // The current state of this crud instance must be loaded, so that all functions can later access it.
        this.state.load();

        // If the "is_loaded" variable is set to false, the asynchronous loader was used when rendering this instance.
        // We can then not initialize this instance right away. It needs to be loaded first.
        if ( !this.state.is_loaded() ) {
            this.ajax_interface.reload();
            // Do not initialize any further!
            return;
        }

        // We do not want the user to interact with the instance while it is getting initialized.
        this.enable_overlay();
        {
            Validation_Plugin_Helper.init(this);

            // Finds and initializes every input.
            this.input_manager.init_inputs();

            // Finds all elements with the "data-action" attribute and give them their appropriate behaviour.
            this.action_manager.init_actions();

            // Find and initialize every tooltip.
            this.tooltip_manager.init_tooltips();

            // Initialize every nested instance.
            this.init_nested_instances();

            // Initialize automatically closing objects.
            this.init_automatically_closing_objects();

            // Show previously active tabs.
            this.ui_controller.show_stored_tabs();
        }
        this.disable_overlay();

        // Inform listeners that THIS instance reinitialized.
        // The "initialized" will also fire if a nested instance changes. We therefore add a this reference to
        // the event, so that the user can react to it.
        this.trigger("initialized", [this]);

        const end = window.performance.now();
        const duration = end - start;

        console.log("Initialization took: " + duration + " ms");
        //alert("Initialization took: " + duration + " ms");
    }

    destroy(): void {
        this.input_manager.destroy_inputs();

        // TODO:: destroy nested instances!!
    }

    /**
     *
     */
    init_nested_instances(): void {
        this.clear_nested_instances();

        // Find all 1st level nested instances and initialize them.
        const nested_instances = Crud.first_level_instances(this.get_elem());

        $(nested_instances).each(function (this: Crud_Instance, i: number, instance_elem: JQuery) {

            let crud_instance_nested = new Crud_Instance(instance_elem);
            crud_instance_nested.init(undefined);
            this.put_nested_instance(crud_instance_nested);

        }.bind(this));
    }

    /**
     * Starts a timer on all objects which should automatically be closed after a certain delay.
     */
    init_automatically_closing_objects(): void {
        this.find("[data-automatically-closing='true']").each(function (i, obj) {
            const delay = $(obj).attr("data-automatic-close-delay");
            if ( !delay ) {
                console.error("data-automatic-close-delay missing on: ", obj);
                return;
            }

            // Animated
            $(obj).fadeTo(parseInt(delay), 500).slideUp(300, function () {
                $(obj).remove();
            });

            /*
            // Not animated
            setTimeout(function () {
                $(obj).remove();
            }, parseInt(delay));
            */
        });
    }

    /* INLINEEDIT */

    init_inlineedit(td: HTMLElement): void {
        // TODO: allow crud instances to be loaded and displayed.

        // Save the current value, so that it can be restored later.
        // Only used if the td gets overwritten with the inlineedit code.
        const current_data: string = $(td).html();

        const entry_id = this.get_entry_id_from_td(td);
        const field_name = this.get_col_name_from_td(td);
        const labeled_field_name = this.get_labeled_col_name_from_td(td);

        let header = this.state.translations().inlineedit_modal_header;
        header = header.replace("{labeled_field_name}", labeled_field_name);

        // Define the data displayed in the modal.
        const modal_data = {
            "header": '<h5 class="crud-modal-title">' + header + '</h5>',
            "body": '',
            "footer": '' +
            '<div class="crud-btn-wrapper">' +
            '<button type="button" class="crud-btn crud-btn-secondary" data-modal-action="cancel" data-izimodal-close>' + this.state.translations().modal_action_cancel + '</button> ' +
            '<button type="button" class="crud-btn crud-btn-primary" data-modal-action="save" data-izimodal-close>' + this.state.translations().modal_action_save + '</button>' +
            '</div>'
        };

        // Initialize the modal.
        const modal = new Inlineedit_Modal(this);

        modal.init(modal_data, function (this: Crud_Instance, elem: HTMLElement) {

            // Check if the save button closed the modal.
            if ( $(elem).attr("data-modal-action") === "save" ) {

                // Save the field.
                $(modal.get_input_manager().get_inputs()).each(function (this: Crud_Instance, key: string, input_object: {[id: string]: Input}) {
                    const input: Input = Object.values(input_object)[0];
                    this.ajax_interface.save_inline(entry_id, input);
                }.bind(this));
            }

            // Destroy all available inputs in the modal body.
            modal.destroy_content();

        }.bind(this), this.get_elem());

        this.ajax_interface.load_field(entry_id, field_name, function(this: Crud_Instance, response: any) {

            // Put the field input in the modal body as soon as it is available.
            const html: HTMLElement = $("<div data-fields>").html(response)[0];
            modal.set_data( {"body" : html} );
            modal.init_content();

            // Jump in the first input.
            /*
            $(modal.get_modal()).on("shown.bs.modal", function() {
                const first_input = modal.get_input_manager().get_inputs()
                $().first().focus();
            });
            */

        }.bind(this));
    }

    /* OVERLAY */

    enable_overlay(): void {
        const overlay = this.find(".crud-overlay");
        $(overlay).addClass('active');
        $(overlay).css('z-index', '1000'); // shown

        // Only show the spinner if the instance is already loaded (something is visible to the user).
        if ( this.state.is_loaded() ) {
            this.find(".crud-overlay-spinner").css('display', 'block');
        }
    }

    disable_overlay(): void {
        const overlay = this.find(".crud-overlay");
        $(overlay).removeClass('active');
        $(overlay).css('z-index', '-1'); // hidden

        this.find(".crud-overlay-spinner").css('display', 'none');
    }

    toggle_overlay(): void {
        const overlay = this.find(".crud-overlay");

        if ( $(overlay).hasClass("active") ) {
            // Overlay is active.
            this.enable_overlay();
        } else {
            // Overly is inactive.
            this.disable_overlay();
        }
    }

    /* FULLSCREEN */

    is_fullscreen(): boolean {
        return $(this.get_elem()).hasClass("crud-fullscreen");
    }

    /**
     * Enables the fullscreen mode for this instance.
     */
    enable_fullscreen(): void {
        $(this.get_elem()).addClass("crud-fullscreen");
        this.trigger("fullscreen-enabled", []);
    }

    /**
     * Disables the fullscreen mode for this instance.
     */
    disable_fullscreen (): void {
        $(this.get_elem()).removeClass("crud-fullscreen");
        this.trigger("fullscreen-disabled", []);
    }

    /**
     * Activates or deactivates the fullscreen mode for this instance. Based on the current state.
     */
    toggle_fullscreen(): void {
        if ( this.is_fullscreen() ) {
            // Fullscreen is active.
            this.disable_fullscreen();
        } else {
            // Fullscreen is inactive.
            this.enable_fullscreen();
        }
    }

    /**
     * Returns the element(s) matching the given selector which are in this instance and not in any nested instance!
     * @param selector A string based selector or a jQuery object.
     * @returns A jQuery object.
     */
    find(selector: any): JQuery {
        return $(this.get_elem()).find(selector).not(function (this: Crud_Instance, i: number, obj: HTMLElement) {
            return !$(obj).closest(Crud_Config.instance_identifier).is( this.get_elem() );
        }.bind(this));
    }

    /**
     * Replaces this instances DOM structure whit whatever HTML code gets provided.
     * This function will not initialize anything! DO NOT FORGET to call the init() function after this one.
     * @param html
     */
    replace_self(html: string) {
        $(this.get_elem()).html( html );
    }

    limit_input_length(input: HTMLElement) {
        const max_length = parseInt( String( $(input).attr("maxlength") ) );
        if ( typeof max_length !== typeof undefined ) {

            let value = $(input).val();
            if ( typeof value !== typeof undefined ) {

                value = String(value);
                if ( value.length > max_length ) {
                    $(input).val( value.slice(0, max_length) );
                }
            }
        }
    }

    get_entry_id_from_td(td: HTMLElement): string { // TODO: maybe string?
        return String( $(td).parent().attr("data-entry-id") );
    }

    get_col_name_from_td(td: HTMLElement): string {
        let td_index = $(td).parent().children().index(td);
        //                   tr       tbody    table                      tr's       first            same distance in
        return String( $(td).parent().parent().parent().children("thead").children().eq(0).children().eq(td_index)
            .find(".crud-column-name").attr("data-column-name") );
    }

    get_labeled_col_name_from_td(td: HTMLElement): string {
        let td_index = $(td).parent().children().index(td);
        //                   tr       tbody    table                      tr's       first            same distance in
        return String( $(td).parent().parent().parent().children("thead").children().eq(0).children().eq(td_index)
            .find(".crud-column-name").attr("data-labeled-column-name") );
    }

    /**
     * Triggers the event 'event_name' on this instance.
     * All elements from the 'data' array will be available (as function arguments) to listener functions.
     *
     * @param event_name The event to trigger.
     * @param data Data which will be accessible to listener functions.
     */
    public trigger(event_name: string, data?: any[] | undefined): void {
        $(this.get_elem()).trigger(Crud_Config.event_prefix + event_name + Crud_Config.event_suffix, data);
    }

    /**
     * Set 'callback' to be executed whenever the event with the name 'event_name' gets triggered on this instance.
     *
     * @param event_name The event name to bind the closure to.
     * @param callback Function to execute if the event gets triggered.
     */
    public on(event_name: string, callback: any): void {
        $(this.get_elem()).on(event_name, callback);
    }

    /**
     * Returns the entry value provided by the obj if in list view (data-entry must be set on obj).asd
     * Returns the entry value stored in the current state if in edit mode.
     * Otherwise returns undefined.
     * @param obj
     */
    get_data_entry_id(obj: HTMLElement): string | undefined {
        if ( this.state.task() === "list") {
            const data_attr = $(obj).attr("data-entry");
            if ( data_attr ) {
                return String( data_attr );
            } else {
                return undefined;
            }

        } else if ( this.state.task() === "edit" ) {
            return this.state.entry();

        } else {
            return undefined;
        }
    }

    get_data_after(obj: HTMLElement): string {
        const task_after_attr = $(obj).attr("data-after");

        // Return the current task if none got defined on the given HTMLElement.
        return task_after_attr ? task_after_attr : this.state.task();
    }

    get_data_confirm(obj: HTMLElement): {confirm: string, confirm_data: string} {
        let confirm_attr: string | undefined = $(obj).attr("data-confirm");

        let confirm_data: string | undefined = $(obj).attr("data-confirm-data");
        if (confirm_data) {
            confirm_data = JSON.parse(confirm_data);
        }

        return {
            confirm: confirm_attr ? confirm_attr : "",
            confirm_data: confirm_data ? confirm_data : ""
        };
    }

    /* SEARCH */

    public open_search(): void {
        this.find(Crud_Config.search_container_identifier).first().slideToggle( "fast" );
    }

    public apply_search(): void {

    }

    public reset_search(): void {

    }

    /**
     * This function will open an information window based on the specified type.
     * The supported types as of now are:
     * <li> "requestfail": Informs the user that an ajax request failed. </li>
     *
     * @param {string} type
     * @param {{?:?}} data
     */
    show_info(type: string, data: Object): void {
        let modal_data_json: string = "";
        let modal_data: Crud_Types.Crud_Modal_Data;

        switch (type) {
            case "requestfail":
                modal_data_json = String( this.find("input[name='crud-modal-template-requestfail']").val() );
                modal_data = JSON.parse(modal_data_json);

                // We will wait a while before displaying the information, so that previous opened modals have
                // time to close properly.
                setTimeout(function (this: Crud_Instance) {
                    const modal = new Modal(this);
                    modal.init(modal_data, function (elem: HTMLElement) {
                        // It is not necessary to do something upon closing. The modal just represents an information.
                    }, this.get_elem());
                }.bind(this), 500);

                break;

            case "system-info":
                modal_data_json = String( this.find("input[name='crud-modal-template-system-info']").val() );
                modal_data = JSON.parse(modal_data_json);

                // We will wait a while before displaying the information, so that previous opened modals have
                // time to close properly.
                setTimeout(function (this: Crud_Instance) {
                    const modal = new Modal(this);
                    modal.init(modal_data, function (elem: HTMLElement) {
                        // It is not necessary to do something upon closing. The modal just represents an information.
                    }, this.get_elem());
                }.bind(this), 100);

                break;

            default:
                console.error("show_info(): The specified type (" + type + ") could not get processed...");
        }
    }

    /* PLUGIN CONTROL */

    init_reorderable(): void {
        const tbody = this.find("tbody").first();

        $(tbody).sortable({
            axis: "y",
            cancel: "a,button",
            cursor: "move",
            handle: ".sort-start",
            opacity: 0.65,
            placeholder: "ui-state-highlight"
        }); //.disableSelection();

        // Define ajax callbacks to save changes
        $(tbody).on("sortupdate", function (this: Crud_Instance, event: JQuery.Event, ui: any) {

            // Which entry-id got dragged?
            const html_dragged = ui.item.context.innerHTML;
            const td_dragged: HTMLElement = $(html_dragged).find(".sort-start").closest("td")[0];
            const id_dragged: string = String( $(td_dragged).attr("data-entry") );
            const old_position: number = parseInt( String( $(td_dragged).attr("data-order") ) );

            // What is the new position for the entry?
            const html_next = ui.item.context.nextElementSibling != null ? ui.item.context.nextElementSibling.innerHTML : null;

            const td_next_row: HTMLElement | undefined = html_next ? $(html_next).find(".sort-start").closest("td")[0] : undefined;
            const next_position: number | undefined = td_next_row ? parseInt( String( $(td_next_row).attr("data-order") ) ) : undefined;

            let new_position: number = old_position;
            if ( !next_position ) {
                new_position = -1;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + -1 + " verschoben.");
            }
            else if ( old_position < next_position ) {
                new_position = next_position - 1;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + (next_position - 1) + " verschoben.");
            }
            else if (old_position >= next_position) {
                new_position = next_position;
                console.log("Du hast Eintrag " + id_dragged + " an Position " + next_position + " verschoben.");
            }

            this.ajax_interface.apply_reorder(id_dragged, old_position, new_position);
        }.bind(this));
    }

}