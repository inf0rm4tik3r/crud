/**
 * ----------------------------------------------------
 * CRUD - JavaScript API
 *
 * @author Lukas Potthast
 * @version 1.3.3
 * @date 2017-10-02 20:50
 * ----------------------------------------------------
 */

import {Crud_Polyfills} from "./crud_polyfills";
import {Crud} from "./crud";

/**
 * Initial setup work after a crud-page loads up.
 */
$(function () {
    Crud_Polyfills.load();
    console.log("Initializing CRUD system...");
    Crud.init_system();
});