export abstract class Crud_Tools {

    /**
     * Checks if the given variable is set.
     * A variable is considered to be set when it is neither undefined or null.
     * @param arg The variable to check.
     * @return {boolean} true / false
     */
    public static is_set(arg: any): boolean {
        return (arg !== null && typeof arg !== "undefined");
    }

    /**
     * Checks whether the specified argument is a callable function.
     * @param {any} arg The variable to check.
     * @returns {boolean} true / false
     */
    public static is_function(arg: any): boolean {
        // Currently using the jQuery helper...
        return $.isFunction(arg);
    }

    /**
     * Checks if the specified argument is an object.
     * Even though JavaScript arrays can be seen as objects, this function will return false for an array.
     * @param {any} arg The variable to check.
     * @return {boolean} true / false
     */
    public static is_object(arg: any): boolean {
        //return (arg !== null && typeof arg === "object" && !this.is_array(arg));
        return (!!arg) && (arg.constructor === Object);
    }

    /**
     * Checks if the specified argument is a string.
     * @param {any} arg The variable to check.
     * @return {boolean} true / false
     */
    public static is_string(arg: any): boolean {
        return typeof arg === 'string' || arg instanceof String;
    }

    /**
     * Executes the supplied function name.
     * @param {string} function_name
     * @param context
     * @param rest
     * @returns {any}
     */
    public static execute_function_by_name(function_name: string, context: any, ...rest: any[]): any {
        let args = [].slice.call(arguments).splice(2);
        let namespaces: string[] = function_name.split(".");
        let func = namespaces.pop();
        if ( !func ) {
            console.error("execute_function_by_name(): No function name got provided.");
            return undefined;
        }
        for ( let i: number = 0; i < namespaces.length; i++ ) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    }

    /**
     * Tries to parse the input with JavaScripts builtin JSON parser.
     * Returns false, should the input string not be parsable, otherwise the result of JSON.parse(string).
     * @param input The input to parse.
     */
    public static try_parse_json(input: string): any {
        try {
            let o = JSON.parse(input);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) nor JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns null, and typeof null === "object",
            // so we must check for that, too. Thankfully, null is falsy, so this suffices:
            if (o && typeof o === "object") {
                return o;
            }
        }
        catch (e) {
            // Intentionally do nothing.
        }

        return false;
    }

    public static try_eval(input: string): boolean | null {
        return null;
    }

    /**
     * Tests if the local storage is available and accessible / usable.
     * @returns {boolean} true / false
     */
    public static is_local_storage_available(): boolean {
        try {
            localStorage.setItem('local_storage_functionality_test', 'test');
            localStorage.removeItem('local_storage_functionality_test');
            return true;
        } catch(e) {
            return false;
        }
    }

    /**
     * Checks if "val" is an array.
     * @param {any} val The value which is to be checked.
     * @returns {boolean} true / false
     */
    public static is_array(val: any): boolean {
        return Array.isArray(val);
    }

    /**
     * Returns either the specified value (if it represents an array) or an empty array.
     * @param val The value which is to be checked.
     */
    public static enforce_array(val: any): any[] {
        if  ( this.is_array(val) ) {
            return val;
        }
        return [];
    }

    /**
     * Checks if "val" exists in the array "arr".
     * @param {any} val The value which is to be checked.
     * @param {any[]} arr The array to search.
     * @returns {boolean} true / false
     */
    public static in_array(val: any, arr: any[]): boolean {
        return $.inArray(val, arr) === -1;
    }

    /**
     * Return an array which only contains the unique element of the specified array.
     * @param {any[]} arr The array to process.
     * @return {any[]} Array of unique elements.
     */
    public static unique_array(arr: any[]): any[] {
        const seen: { [index: string]: boolean } = {};
        return arr.filter(function(item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }

    /**
     * Checks if "val" is greater then AND smaller then the first element and second element from "arr".
     * The array must be of length 2!
     * @param val The value to check.
     * @param {any[]} arr The array which holds the upper und higher boundary values.
     * @return {boolean} true / false
     */
    public static is_between(val: any, arr: any[]): boolean {
        if ( arr.length !== 2 ) {
            console.error("is_between(): The provided array contains more or less then 2 values!", arr);
            return false;
        }
        return val > arr[0] && val < arr[1];
    }

    /**
     * Checks if the specified object is an array.
     * @param {any} val The value which is to be checked.
     * @returns {boolean} true / false
     */
    public static is_empty(val: any): boolean {
        if ( this.is_object(val) ) {
            return Object.keys(val).length === 0;
        } else {
            return val.length === 0;
        }
    }

    /**
     * Appends the given value to the specified FormData object. The value can be any primitive, object, array or
     * even files from a file input. Objects and arrays can be arbitrarily nested.
     * Technical info: Only a single string can be used as an index into the FormData object. Therefore multi
     * dimensional data must be added in the following form: "root[subA][subB][...]..."
     * @param {FormData} fd The form data object to append to.
     * @param {string} index The base index with which to access the form data object.
     * @param {any} value The value to process.
     */
    public static append_to_form_data(fd: FormData, index: string, value: any): void {
        Crud_Tools._append_to_form_data(fd, index, "", value);
    }

    /**
     * Internal recursive algorithm to append a value to a FormData object.
     * @param {FormData} fd The form data object to append to.
     * @param {string} base The base index with which to access the form data object.
     * @param {string} keychain The nesting added to the base index (form: "[foo][bar][...]...").
     * @param {any} value The value to process.
     * @private
     */
    private static _append_to_form_data(fd: FormData, base: string, keychain: string, value: any): void {
        // Should the "value" argument be undefined or null, it can / should not be processed!
        if ( !Crud_Tools.is_set(value) ) {
            console.error("append_to_form_data(): The given value is undefined or null!", keychain, value);
            console.error("append_to_form_data(): Skipping the attachment to: ", fd);
            return;
        }

        // If the "value" argument represents an array or an object: Append all children recursively.
        if ( Crud_Tools.is_object(value) || Crud_Tools.is_array(value) ) {
            $.each(value, function(key: string, inner_value: any) {
                Crud_Tools._append_to_form_data(fd, base, keychain + "[" + key + "]", inner_value);
            });
        }
        // Otherwise: Append the value itself.
        else {
            //console.log("append_to_form_data(): appending: ", value);
            fd.append(base + keychain, value);
        }
    }

    /**
     * Alters the string so that it can savely be placed into the DOM, by replacing certain characters like
     * &, >, >, " and ' with their respective HTML entities.
     * @param {string} text The string to process.
     * @return {string} The input string with certain characters replaced.
     */
    public static escape_html(text: string): string {
        const map: {[elem: string]: string} = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function(m) {
            return map[m];
        });
    }

}