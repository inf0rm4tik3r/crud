let Reflect: {[key: string]: any};

export abstract class Crud_Polyfills {

    public static load() {

        // Alternative "is array" checking.
        if ( !Array.isArray ) {
            Array.isArray = <any>function (obj: any): boolean {
                return Object.prototype.toString.call(obj) === "[object Array]";
            };
        }

        /*
         * Reflect.ownKeys
         */
        if ( !(typeof Reflect === "object" && typeof Reflect.ownKeys === "function")) {

            // If "Reflect" doest not exist, create a dummy object to use.
            if ( typeof Reflect === "undefined" ) {
                Reflect = {};
            }

            if ( typeof Object.getOwnPropertySymbols === "function" ) {
                Reflect.ownKeys = function (o: any) {
                    return Object.getOwnPropertyNames(o).concat( <any>Object.getOwnPropertySymbols(o) );
                }
            } else {
                Reflect.ownKeys = Object.getOwnPropertyNames;
            }

        }

        /*
         * Object.values()
         * Object.entries()
         */
        const reduce = Function.bind.call(Function.call, Array.prototype.reduce);
        const isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable);
        const concat = Function.bind.call(Function.call, Array.prototype.concat);
        const keys = Reflect.ownKeys;

        if (!Object.values) {
            Object.values = function values(O: any) {
                return reduce(keys(O), (v: any, k: any) => concat(v, typeof k === 'string' && isEnumerable(O, k) ? [O[k]] : []), []);
            };
        }

        if (!Object.entries) {
            Object.entries = function entries(O: any) {
                return reduce(keys(O), (e: any, k: any) => concat(e, typeof k === 'string' && isEnumerable(O, k) ? [[k, O[k]]] : []), []);
            };
        }

    }

}