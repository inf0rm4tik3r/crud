import { Crud_Config } from "./crud_config";
import { Crud_Instance } from "./crud_instance";

/**
 *
 */
export abstract class Crud {

    /**
     * Stores all top level crud instances.
     * @type {Array}
     */
    private static crud_instance_objects: Array<Crud_Instance> = [];

    private static synchronizing_state_variables = ["instance_name", "task", "entry"];


    /**
     * Initializes every crud instance available in the DOM structure.
     */
    public static init_system(): void {
        // Initialize every crud instance on the page.
        $(Crud.first_level_instances("body")).each(function (key: number, instance_elem:  HTMLElement) {
            const instance = new Crud_Instance( $(instance_elem) );
            instance.init(undefined);
            //console.log("Initialized instance: ", instance_elem, instance);
            Crud.crud_instance_objects.push(instance);
        });

        // Each crud instance has its own segment in the URLs hash value, in which a small subsection of the
        // instances state is saved.
        //Crud.process_hash();

        // The system needs to listen if the hash changes. And if so, needs to react to those changes.
        //$(window).on('hashchange',function(){
        //  Crud.process_hash();
        //});

        // Inform the document that all crud instances got initialized.
        $(document).trigger(Crud_Config.event_prefix + "ready ss" + Crud_Config.event_suffix, [])
    }

    /**
     * Finds and returns all first-level crud instances (instances which are not nested inside
     * other crud instances) inside the in_obj DOM element.
     * The in_obj argument can thereby ba a crud instance itself. This method will accommodate for that.
     * @param {JQuery | HTMLElement | string} in_obj Where to search in. JQuery object or selector string.
     * @returns {JQuery} JQuery collection of first-level instances.
     */
    public static first_level_instances(in_obj: JQuery | HTMLElement | string): JQuery {
        return $(in_obj)
            .find(Crud_Config.instance_identifier)
            .not(function (index: number, element: HTMLElement) {

            const parent_instance = $(element).parent().closest(Crud_Config.instance_identifier);
            const is_nested_instance = $(parent_instance).length > 0;
            const parent_equals_in_obj = $(parent_instance).is(in_obj);

            // Only allows not nested elements, or elements which ARE nested AND are direct descendants of in_obj.
            const keep = !is_nested_instance || (is_nested_instance && parent_equals_in_obj);

            //console.log("first_level_instances(): Option: ", element, parent_instance, is_nested_instance, parent_equals_in_obj);
            //console.log("first_level_instances(): Keep?: ", keep);
            // Info: FALSE needs to be returned if we want to keep the element! (jQuery stuff... ^^)
            return !keep;
        });
    }

    /**
     * Searches the DOM for instances with a specific name and returns the resulting JQuery collection.
     * @param {string} instance_name The instance name to search with.
     * @returns {JQuery}
     */
    public static instance(instance_name: string): JQuery {
        return $("input[name='crud_instance_name'][value='" + instance_name + "']")
            .closest(Crud_Config.instance_identifier);
    }

    /**
     * Returns all crud instance DOM elements in the document. This includes every nested instance!
     * @returns {JQuery} JQuery collection of all available instances.
     */
    public static instances(): JQuery {
        return $(Crud_Config.instance_identifier);
    }

    /**
     * Searches through all loaded crud instances and returns the Crud_Instance objects with the specified name.
     * Returns undefined, if no instance with that name could be found.
     * Returns the instances object, if the target name exactly matched the name of one instance. (typical case)
     * Returns an array of Crud_Instance objects if multiple instances with the target name could be found.
     * @param {string} target_name The instance name to search for.
     * @returns {Crud_Instance | Crud_Instance[]} The found instance or undefined.
     */
    public static find_instance(target_name: string): Crud_Instance | Crud_Instance[] | undefined {
        let found: Crud_Instance[] = [];

        for (let i = 0; i < Crud.crud_instance_objects.length; i++) {
            const current = Crud.crud_instance_objects[i];

            // The Crud object only has access to the first level instances. Every nested instance is part of
            // and gets managed by its parent instance object.
            // We call the recursive find_instances(string) function on all loaded outer instances, to ask them
            // if they or one of their children has the desired name.
            found = found.concat( current.find_instances(target_name) );
        }

        // Returns undefined, if no instance with that name could be found.
        if ( found.length === 0 ) {
            return undefined;
        }
        // Returns the instances object, if the target name exactly matched the name of one instance.
        else if ( found.length === 1 ) {
            return found[0];
        }
        // Returns an array of Crud_Instance objects if multiple instances with the target name could be found.
        else {
            return found;
        }
    }

    /* HASH */
    // TODO: revisit / recode
    /*
    static static process_hash() {
        let hash = window.location.hash;

        // Was a hash provided?
        if (hash.length === 0) {
            return;
        }

        // Remove the "#" sign from the start of the string.
        hash = hash.substring(1);

        // The hash may only consisted of one '#'...
        if (hash.length === 0) {
            return;
        }

        // Get information parts.
        let hash_index = 0;
        while (true) {
            let brace_open = hash.indexOf("{", hash_index);
            hash_index = brace_open;
            if (brace_open === -1) {
                // We break the while(true) loop if no more crud-state-information can be found.
                break;
            }
            let brace_close = hash.indexOf("}", hash_index);
            hash_index = brace_close;
            let crud_state_info_unprocessed = hash.substr(brace_open + 1, brace_close - 1);
            console.log(crud_state_info_unprocessed);

            // crud_state_info_unprocessed now looks like: "instance-mode-id"
            let crud_state_info_parts = crud_state_info_unprocessed.split("-");
            if (crud_state_info_parts.length < 2) {
                continue;
            }
            let instance_name = crud_state_info_parts[0];
            let desired_task = crud_state_info_parts[1];
            let desired_id = crud_state_info_parts[2];

            // Acquire the instance and generate a crud state_object from it.
            let instance = Crud.instance(instance_name);
            if (typeof instance === typeof undefined || instance === null) {
                console.error("process_hash(): Instance '" + instance_name + "' could not be found in DOM.");
                continue;
            }
            let crud_state = Crud.crud_state(instance);

            // Use the extracted information to set the instance into the desired state.
            switch (desired_task) {
                case 'list':
                    Crud.load_list_view(instance, crud_state);
                    break;
                case 'structure':
                    Crud.load_structure_view(instance, crud_state);
                    break;
                case 'create':
                    Crud.load_create_view(instance, crud_state);
                    break;
                case 'edit':
                    Crud.load_edit_view(instance, crud_state, desired_id);
                    break;
                case 'view':
                    Crud.load_view_view(instance, crud_state, desired_id);
                    break;

                default:
                    console.error("process_hash(): Desired task '" + desired_task + "' is unknown.");
                    break;
            }
        }
    }

    public static remove_hash() {
        let scroll_v, scroll_h
        let loc = window.location;
        if ("pushState" in history)
            history.pushState("", document.title, loc.pathname + loc.search);
        else {
            // Prevent scrolling by storing the page's current scroll offset.
            scroll_v = document.body.scrollTop;
            scroll_h = document.body.scrollLeft;

            // Remove the hash.
            loc.hash = "";

            // Restore the scroll offset, should be flicker free.
            document.body.scrollTop = scroll_v;
            document.body.scrollLeft = scroll_h;
        }
    }
    */
}