import {Crud_Tools} from "./crud_tools";
import {Crud_Instance} from "./crud_instance";
import {Crud_Types} from "./crud_types";
import Input_Data = Crud_Types.Input_Data;
import {Crud_Condition_Clause} from "./crud_condition_clause";

export class Crud_Condition {

    private query: any[];

    constructor() {}

    public to_string(): string
    {
        let out = "";
        $.each(this.query, function(key: number, value: any) {
            if ( Crud_Tools.is_string(value) ) {
                switch (value) {
                    case 'AND':
                        out += ' && ';
                        break;
                    case 'OR':
                        out += ' || ';
                        break;
                }
            }
            else if ( value instanceof Crud_Condition_Clause ) {
                out += "(" + value.to_string() + ")";
            }
            else if ( value instanceof Crud_Condition ) {
                out += "(" + value.to_string() + ")";
            }
        });
        return out;
    }
    
    /**
     * Returns undefined whenever this condition could not get evaluated.
     * ...
     * @param {Crud_Instance} instance
     * @return {boolean}
     */
    public evaluate(instance: Crud_Instance): boolean | undefined {
        const eval_string = this.compute_eval_string(instance);
        if ( eval_string === null ) {
            console.error("evaluate(): The condition could not be parsed to an evaluable string.");
            return true;
        }

        try {
            const eval_result = eval(eval_string);
            return eval_result === true;
        }
        catch (e) {
            console.error("evaluate(): The eval_string \"" + eval_string + "\" could not get evaluated!");
            return true;
        }
    }

    /**
     *
     * @param {Crud_Instance} instance
     * @return {string}
     */
    public compute_eval_string(instance: Crud_Instance): string | null {
        return this.compute_eval_string_recursive(instance, this.query);
    }

    /**
     * Returns null should this function encounter an error.
     * ...
     * @param {Crud_Instance} instance
     * @param {any[]} query
     * @return {string}
     */
    private compute_eval_string_recursive(instance: Crud_Instance, query: any[]): string | null {
        let eval_string = "";
        let has_error = false;

        $.each(query, function(key: number, value: any) {
            // Connector
            if ( Crud_Tools.is_string(value) ) {
                switch (value) {
                    case 'AND':
                        eval_string += " && ";
                        break;
                    case 'OR':
                        eval_string += " || ";
                        break;
                }
            }
            // Condition clause (field, operator, value)
            else if ( value instanceof Crud_Condition_Clause ) {
                const input = instance.get_input_manager().get_input( value.get_field() );
                if ( !input ) {
                    console.warn("compute_eval_string_recursive(): Input not found: ", value.get_field());
                    has_error = true;
                    return false;
                }

                const input_data = instance.get_input_manager().acquire_input_data_single(input);

                if ( !Crud_Tools.is_set(input_data) ) {
                    console.error("compute_eval_string(): Input data could not be acquired for: ", input);
                    has_error = true;
                    // Skip the ongoing iteration and stop the $.each loop immediately.
                    return false;
                }

                const val_current = (<Input_Data>input_data)["value"];
                const val_check = value.get_value();

                switch (value.get_operator()) {
                    case "=":
                        eval_string += eval(val_current + " === " + val_check);
                        break;

                    case "!=":
                    case "<>":
                        eval_string += eval(val_current + " !== " + val_check);
                        break;

                    case "<":
                    case "<=":
                    case ">":
                    case ">=":
                        eval_string += eval(val_current + " " + value.get_operator() + " " + val_check);
                        break;

                    case "*%":
                    case "STARTS_WITH":
                        eval_string += String( String(val_current).startsWith( String(val_check) ) );
                        break;

                    case "%*":
                    case "ENDS_WITH":
                        eval_string += String( String(val_current).endsWith( String(val_check) ) );
                        break;

                    case "%*%":
                    case "CONTAINS":
                        eval_string += String( String(val_current).indexOf( String(val_check) ) !== -1 );
                        break;

                    case "*":
                    case "EQUALS":
                        eval_string += String( String(val_current) === String(val_check) );
                        break;

                    case "IN":
                        eval_string += String( Crud_Tools.in_array(val_current, val_check) );
                        break;

                    case "!IN":
                        eval_string += String( !Crud_Tools.in_array(val_current, val_check) );
                        break;

                    case "><":
                    case "BETWEEN":
                        eval_string += String( Crud_Tools.is_between(val_current, val_check) );
                        break;

                    case "!><":
                    case "!BETWEEN":
                        eval_string += String( !Crud_Tools.is_between(val_current, val_check) );
                        break;

                    default:
                        console.error("compute_eval_string(): Unknown operator found: ", value.get_operator());
                        break;
                }
            }
            // Sub-Where-Clause
            else if ( value instanceof Crud_Condition ) {
                eval_string += "(" + this.compute_eval_string_recursive(instance, value.get_query()) + ")";
            }
        });

        // Something went wrong :(
        // We indicate tha the evaluation cannot take place by returning null.
        if ( has_error ) {
            return null;
        }

        return eval_string;
    }

    public get_involved(): string[] {
        const inv = this.compute_involved_recursive(this.query);
        return Crud_Tools.unique_array( inv );
    }

    private compute_involved_recursive(query: any[]): string[] {
        let involved_fields: string[] = [];

        $.each(query, function(this: Crud_Condition, key: number, value: any)  {
            if ( value instanceof Crud_Condition_Clause ) {
                involved_fields.push( value.get_field() );
            }
            else if ( value instanceof Crud_Condition ) {
                involved_fields = involved_fields.concat( value.get_involved() );
            }
        }.bind(this));

        return involved_fields;
    }

    public from_array(arr: any[]): Crud_Condition {
        this.query = [];

        $.each(arr, function(this: Crud_Condition, key: number, value: any) {
            if ( Crud_Tools.is_object(value) ) {
                this.query.push( new Crud_Condition_Clause(
                    value["col"], value["op"], value["val"], value["identifier"]
                ) );
            }
            else if ( Crud_Tools.is_array(value) ) {
                this.query.push( new Crud_Condition().from_array(value) );
            }
            else {
                this.query.push(value);
            }
        }.bind(this));

        return this;
    }

    public get_query() {
        return this.query;
    }
}