import {Input} from "./modules/inputs/input";

export namespace Crud_Types {

    export type Crud_Translations = {
        [elem: string]: string;
    }

    export type Acquire_Input_Options = {
        [elem: string]: string
    }

    export type Input_Data = {
        id: string,
        value: string | string[] | number | File | undefined;
    }

    export type Custom_Validators = {
        [elem: string]: Custom_Validator;
    }

    export type Custom_Validator = {
        js_function_name: string;
    }

    export type Validation_Constraints = {
        [validator_name: string]: Validation_Constraint
    }

    export type Validation_Constraint = {
        [option: string]: string
    }

    export type Input_Activator = {
        element: Input,
        triggers: string
    }

    export type Input_Activators = Input_Activator[];

    export type Crud_Modal_Data = {
        header?: JQuery | HTMLElement | string,
        body?: JQuery | HTMLElement | string,
        footer?: JQuery | HTMLElement | string
    }

}