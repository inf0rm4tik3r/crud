import {Crud_Config} from "../crud_config";
import {Crud_Instance} from "../crud_instance";
import {Modal} from "./modal/modal";
import {Crud_Tools} from "../crud_tools";

export class Action_Manager {

    /*
     * TODO: ACTIONS NEED TO MODIFY THE HASH VALUE.
     */

    private instance: Crud_Instance;
    
    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }
    
    /**
     * Finds all elements with the "data-action" attribute and attaches their appropriate behaviour to them.
     */
    init_actions(in_obj?: HTMLElement): void {
        // Load inputs only from the specified HTMLElement if such an element got defined.
        let actions: JQuery;
        if ( Crud_Tools.is_set(in_obj) ) {
            actions = $(in_obj).find("[data-action]");
        } else {
            actions = this.instance.find("[data-action]")
        }

        $(actions).each(function (this: Action_Manager, key: number, obj: HTMLElement) {
            this.init_action(obj);
        }.bind(this));
    }

    /**
     * Initializes the action of the specified object. This function retrieves the desired action from the
     * "data-action" attribute of the object. If a certain action should get initialized on a DOM element which
     * has no "data-action" attribute, or if the attribute should be overridden, the action_override parameter
     * can be used. If set, this action name has priority over the objects attribute.
     * @param {HTMLElement} obj The object for which an action should be initialized.
     * @param {string} action_override A specific action. Overrides the action specified by the "data-action" attribute.
     */
    init_action(obj: HTMLElement, action_override?: string): void {
        // The data-action attribute must be set either on "obj" itself, or by the override.
        let data_action: string | undefined = action_override ? action_override : $(obj).attr("data-action");
        if ( !data_action || data_action === "" ) {
            console.error("init_action(): The data-action attribute is missing or empty!");
            console.error("init_action(): Skipping action initialization for: ", obj);
        }

        // Each action is defined through a unique string.
        switch (data_action) {
            case "list":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_list_view();
                }.bind(this));
                break;

            case "structure":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_structure_view();
                }.bind(this));
                break;

            case "create":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_create_view();
                }.bind(this));
                break;

            case "edit":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_edit_view( String( $(obj).attr("data-entry") ) );
                }.bind(this));
                break;

            case "view":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_view_view( String( $(obj).attr("data-entry") ) );
                }.bind(this));
                break;

            case "print":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_print_view();
                }.bind(this));
                break;

            case "export":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().load_export();
                }.bind(this));
                break;

            case "reload":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().reload( $(obj).attr("data-after") );
                }.bind(this));
                break;

            case "reset":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().reset();
                }.bind(this));
                break;

            case "save":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().save_entry( String( $(obj).attr("data-after") ) );
                }.bind(this));
                break;

            case "delete":
                $(obj).on("click", function (this: Action_Manager) {
                    let entry_id = this.instance.get_data_entry_id(obj);
                    if ( !entry_id ) {
                        console.error("Unable to find entry id!");
                        return;
                    }

                    let task_after = this.instance.get_data_after(obj);

                    let confirm_method = $(obj).attr("data-confirm-method");
                    let modal_template_name = $(obj).attr("data-modal-template");
                    let modal_template_json = String( this.instance.find("input[name='crud-modal-template-" + modal_template_name + "']").val() );
                    let modal_template = JSON.parse(modal_template_json);

                    switch (confirm_method) {
                        case "none":
                            // No confirmation needed.
                            this.instance.get_ajax_interface().delete_entry(entry_id, task_after);
                            break;
                        case "system":
                            let text = String( $("<div>").html(modal_template.body).text() );
                            if ( window.confirm(text) === true ) {
                                this.instance.get_ajax_interface().delete_entry(entry_id, task_after);
                            }
                            break;
                        case "modal":
                            // Initializes the modal which will display the confirmation question.
                            const modal = new Modal(this.instance);
                            modal.init(modal_template, function (this: Action_Manager, element: HTMLElement) {
                                if ( $(element).attr("data-modal-action") === "confirm" ) {
                                    this.instance.get_ajax_interface().delete_entry(<string>entry_id, task_after);
                                }
                            }.bind(this), this.instance.get_elem());
                            return;
                        default:
                            console.error("init_action(): delete: \"" + confirm_method + "\" is an unknown confirmation method.");
                            return;
                    }
                }.bind(this));
                break;

            case "restore":
                $(obj).on("click", function (this: Action_Manager) {
                    let entry_id = this.instance.get_data_entry_id(obj);
                    if ( !entry_id ) {
                        console.error("Unable to find entry id!");
                        return;
                    }

                    let task_after = this.instance.get_data_after(obj);

                    let confirm_method = $(obj).attr("data-confirm-method");
                    let modal_template_name = $(obj).attr("data-modal-template");
                    let modal_template_json = String( this.instance.find("input[name='crud-modal-template-" + modal_template_name + "']").val() );
                    let modal_template = JSON.parse(modal_template_json);

                    switch (confirm_method) {
                        case "none":
                            // No confirmation needed.
                            this.instance.get_ajax_interface().restore_entry(entry_id, task_after);
                            break;
                        case "system":
                            let text = String( $("<div>").html(modal_template.body).text() );
                            if ( window.confirm(text) === true ) {
                                this.instance.get_ajax_interface().restore_entry(entry_id, task_after);
                            }
                            break;
                        case "modal":
                            // Initializes the modal which will display the confirmation question.
                            const modal = new Modal(this.instance);
                            modal.init(modal_template, function (this: Action_Manager, element: HTMLElement) {
                                if ( $(element).attr("data-modal-action") === "confirm" ) {
                                    this.instance.get_ajax_interface().restore_entry(<string>entry_id, task_after);
                                }
                            }.bind(this), this.instance.get_elem());
                            return;
                        default:
                            console.error("init_action(): restore: \"" + confirm_method + "\" in an unknown confirmation method.");
                            return;
                    }
                }.bind(this));
                break;

            case "limit":
                if ( !($(obj).hasClass("crud-btn-active") || $(obj).hasClass("disabled")) ) {
                    $(obj).on("click", function (this: Action_Manager) {
                        this.instance.get_ajax_interface().load_limit( String( $(obj).attr("data-limit") ) );
                    }.bind(this));
                }
                break;

            case "page":
                let page = String( $(obj).attr("data-page") );
                if ( ! (page === "-1" || $(obj).hasClass("crud-btn-disabled") || $(obj).hasClass("crud-btn-active")) ) {
                    $(obj).on("click", function (this: Action_Manager) {
                        this.instance.get_ajax_interface().load_page(page);
                    }.bind(this));
                }
                break;

            case "order-by":
                $(obj).on("click", function (this: Action_Manager, event: JQuery.Event) {
                    const column_name = $(obj).find("[data-column-name]").data("column-name");
                    this.instance.get_ajax_interface().order_by(column_name, event.ctrlKey || event.metaKey);
                }.bind(this));
                break;

            case "inlineedit":
                switch ( this.instance.get_state().inlineedit_activator() ) {
                    case "click": // or else fallthrough
                    case "dblclick":
                        $(obj).on(this.instance.get_state().inlineedit_activator(), function (this: Action_Manager) {
                            this.instance.init_inlineedit(obj);
                        }.bind(this));
                        break;

                    case "btn":
                        $(obj).on("click", function (this: Action_Manager) {
                            this.instance.init_inlineedit( $(obj).closest("td")[0] );
                        }.bind(this));
                        break;

                    default:
                        console.error(
                            "init_action(): inlineedit: Unknown inlineedit activator specified: ",
                            this.instance.get_state().inlineedit_activator()
                        );
                }
                break;

            case "open-search":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.open_search();
                }.bind(this));
                break;

            case "apply-search":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.apply_search();
                }.bind(this));
                break;

            case "reset-search":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.reset_search();
                }.bind(this));
                break;

            case "toggle-search-tab":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_ajax_interface().toggle_search_tab( String( $(obj).attr("data-search-tab") ) );
                }.bind(this));
                break;

            case "unlock-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): unlock-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input) {
                    console.error("init_action(): unlock-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().unlock(input);
                }.bind(this));
                break;
            }

            case "lock-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if ( !signature_pad_id ) {
                    console.error("init_action(): lock-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if ( !input ) {
                    console.error("init_action(): lock-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().lock(input);
                }.bind(this));
                break;
            }

            case "draw-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if ( !signature_pad_id ) {
                    console.error("init_action(): draw-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if ( !input ) {
                    console.error("init_action(): draw-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().enter_draw_mode(input);
                }.bind(this));
                break;
            }

            case "erase-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if ( !signature_pad_id ) {
                    console.error("init_action(): erase-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if ( !input ) {
                    console.error("init_action(): erase-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().enter_erase_mode(input);
                }.bind(this));
                break;
            }

            case "undo-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): undo-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input) {
                    console.error("init_action(): undo-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().undo(input);
                }.bind(this));
                break;
            }

            case "clear-signature-pad": {
                const signature_pad_id = $(obj).attr("data-input-ref");
                if (!signature_pad_id) {
                    console.error("init_action(): clear-signature-pad: data-input-ref attribute not found!");
                    break;
                }
                const input = this.instance.get_input_manager().get_input(signature_pad_id);
                if (!input) {
                    console.error("init_action(): clear-signature-pad: Input could not be found!");
                    break;
                }

                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_signature_pad_controller().clear(input);
                }.bind(this));
                break;
            }

            case "edit-code":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_code_controller().edit(obj);
                }.bind(this));
                break;

            case "view-code":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.get_input_manager().get_code_controller().view(obj);
                }.bind(this));
                break;

            case "toggle-overlay":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.toggle_overlay();
                }.bind(this));
                break;

            case "toggle-fullscreen":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.toggle_fullscreen();
                }.bind(this));
                break;

            case "show-system-info":
                $(obj).on("click", function (this: Action_Manager) {
                    this.instance.show_info("system-info", {});
                }.bind(this));
                break;

            case "show-tab":
                $(obj).on("click", function (this: Action_Manager, event: JQuery.Event) {
                    // The browser should not jump to the tab pane.
                    event.preventDefault();

                    let tab_id = $(obj).data("tab-ref");
                    this.instance.get_ui_controller().show_tab(tab_id)
                }.bind(this));
                break;

            case "close-alert":
                $(obj).on("click", function () {
                    $(obj).closest("[data-crud-alert]").first().remove();
                }.bind(this));
                break;

            default:
                console.error("init_action(): Unknown action: ", data_action, obj);
                break;
        }
    }
    
}