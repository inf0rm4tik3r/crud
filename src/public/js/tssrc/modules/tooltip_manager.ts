import {Crud_Instance} from "../crud_instance";
import {Crud_Config} from "../crud_config";

export class Tooltip_Manager {
    
    private instance: Crud_Instance;
    
    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }

    /**
     * Initializes all tooltips in this instance.
     * An element is considered a tooltip origin if the "data-tooltip" attribute is set.
     */
    public init_tooltips(): void {
        this.instance.find("[data-tooltip]").each(function (this: Tooltip_Manager, i: number, obj: HTMLElement) {
            this.init_tooltip(obj);
        }.bind(this));
    }

    /**
     * Initializes the tooltip library (Tooltipster) on the specified element.
     * The element therefore needs the "data-tooltip-title" and "data-tooltip-placement" attributes.
     * If necessary, "title_override" and "placement_override" can be used to define the title and placement
     * regardless of the elements attributes. If overrides got specified, the existence of the previously mentioned
     * data attributes will not get checked.
     * @param {HTMLElement} obj The DOM element on which the tooltip library is to be initialized.
     * @param {string} title_override A specific title. Overrides a title specified through "data-tooltip-title".
     * @param {string} placement_override A specific placement. Overrides "data-tooltip-placement".
     */
    public init_tooltip(obj: HTMLElement, title_override?: string, placement_override?: string): void {
        // The title must have been specified.
        let title: string | undefined = title_override ? title_override : $(obj).data("tooltip-title");
        if ( !title ) {
            console.error("init_tooltip(): data-tooltip-title attribute is missing!");
            console.error("init_tooltip(): Skipping tooltip initialization for: ", obj);
            return;
        }

        // The placement should have been specified.
        let placement: string | undefined = placement_override ? placement_override : $(obj).data("tooltip-placement");
        if ( !placement ) {
            console.log("init_tooltip(): tooltip-placement attribute is missing! Using \"top\" placement.");
            placement = "top";
        }

        // Placement must be one of "left", "right", "top", "bottom".
        const placement_options = ["left", "right", "top", "bottom"];
        if ( !$.inArray(placement, placement_options) ) {
            console.error("init_tooltip(): The specified placement was not one of: ", placement_options);
            console.error("init_tooltip(): Skipping tooltip initialization for: ", obj);
            return;
        }


        // Type definitions are currently incorrect (side, delay, ...)
        // Therefore using an <any> cast.
        $(obj).tooltipster(<any>{
            content: title,
            contentAsHTML: true,
            theme: "tooltipster-borderless",
            side: placement,
            animation: "fade",
            interactive: true,
            delay: [0, 100],
            functionReady(instance: any, helper: any) {
                $(helper.tooltip).addClass(Crud_Config.instance_wrapper_identifier.substring(1));
            }
        });

        // A click on a tooltip (which is needs to open the tooltip on a mobile device) should not trigger any
        // "on click" handlers of parent elements.
        // Example: A click on a tooltip in a table-header cell would otherwise reload the crud instance (new sorting).
        $(obj).click(function(event) {
            //event.stopPropagation();
        });
    }
    
}