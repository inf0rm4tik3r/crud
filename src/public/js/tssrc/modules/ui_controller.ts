import {Crud_Tools} from "../crud_tools";
import {Crud_Instance} from "../crud_instance";

export class UI_Controller {

    private instance: Crud_Instance;

    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }

    /**
     * Shows / activates the tab with the specified tab id.
     * @param tab_id The id (string) of the tab which should be shown / activated.
     * @param track_change Boolean which specifies if the tab change should be tracked. This would update the
     *      list of currently active tabs stored in the local storage.
     */
    show_tab(tab_id: string, track_change: boolean | undefined = true) {
        const active_link_class_name = "crud-nav-link-active";
        const active_pane_class_name = "crud-tab-pane-active";

        const target_pane = $("[data-tab-id='" + tab_id + "']");
        const active_pane = $(target_pane).siblings("." + active_pane_class_name);

        const target_link = $("[data-tab-ref='" + tab_id + "']");
        const active_link = $(target_link).closest("[data-crud-nav-tabs]").find("." + active_link_class_name);

        const active_tab_id = $(active_link).data("tab-ref");

        // Activate the new tab pane.
        // Remove first, add last, for the case that active and target are equal.
        $(active_pane).removeClass(active_pane_class_name);
        $(target_pane).addClass(active_pane_class_name);

        // Activate the clicked link.
        // Remove first, add last, for the case that active and target are equal.
        $(active_link).removeClass(active_link_class_name);
        $(target_link).addClass(active_link_class_name);

        // Remove the previously active tab id from the local storage and add the new id to it.
        if ( track_change === true ) {
            this.remove_active_tab_id(active_tab_id);
            this.add_active_tab_id(tab_id);
        }
    }

    /**
     * Access the local storage to find all tabs which should be active and show / activate them.
     */
    show_stored_tabs(): void {
        // The local storage must be available for the 'active tabs' feature.
        if ( !this.instance.get_state().has_local_storage() ) {
            return;
        }

        // Show / activate each tab which is currently stored in the local storage.
        $.each(this.get_active_tab_ids(), function (this: UI_Controller, i: number, tab_id: string) {
            this.show_tab(tab_id, false);
        }.bind(this));
    }

    /**
     * Retrieves the list of currently active tabs from the local storage.
     * @returns [string]
     */
    get_active_tab_ids(): String[] {
        // The local storage must be available for the 'active tabs' feature.
        if ( !this.instance.get_state().has_local_storage() ) {
            return [];
        }

        const storage_access = this.instance.get_state().instance_name() + ".active_tab_ids";
        const active_tab_ids_string = String( localStorage.getItem(storage_access) );

        // The stored string should be a JSON encoded array...

        return Crud_Tools.enforce_array( Crud_Tools.try_parse_json(active_tab_ids_string) );
    }

    /**
     * Stores the provided array of currently active tabs (their ids) in the local storage.
     * @param active_tab_ids Array of ids (as strings).
     */
    store_active_tab_ids(active_tab_ids: String[]): void {
        // The local storage must be available for the 'active tabs' feature.
        if ( !this.instance.get_state().has_local_storage() ) {
            return;
        }

        const storage_access = this.instance.get_state().instance_name() + ".active_tab_ids";

        // The local storage can only store strings! The array must therefore be converted to a string.
        localStorage.setItem(storage_access, JSON.stringify(active_tab_ids));
    }

    /**
     * Removes the specified tab_id from the 'active_tab_ids' array stored in the local storage.
     * @param tab_id The tab id to remove.
     */
    remove_active_tab_id(tab_id: String): void {
        // The local storage must be available for the 'active tabs' feature.
        if ( !this.instance.get_state().has_local_storage() ) {
            return;
        }

        const active_tab_ids = this.get_active_tab_ids();
        //console.log("remove_active_tab_id (before): ", active_tab_ids);

        // Remove all instances of tab_id in our array of active tab ids.
        let index;
        while ( (index = $.inArray(tab_id, active_tab_ids, 0)) !== -1 ) {
            active_tab_ids.splice(index, 1);
        }

        //console.log("remove_active_tab_id (after): ", active_tab_ids);
        this.store_active_tab_ids(active_tab_ids);
    }

    /**
     * Adds the specified tab_id to the 'active_tab_ids' array stored in the local storage.
     * @param tab_id The tab id to remove.
     */
    add_active_tab_id(tab_id: String): void {
        // The local storage must be available for the 'active tabs' feature.
        if ( !this.instance.get_state().has_local_storage() ) {
            return;
        }

        const active_tab_ids = this.get_active_tab_ids();
        //console.log("store_active_tab_id (before): ", active_tab_ids);

        // Add the specified tab_id to our array of active tab ids, if it is not already in there.
        if ( $.inArray(tab_id, active_tab_ids, 0) === -1 ) {
            active_tab_ids.push(tab_id);
        }

        //console.log("store_active_tab_id (after): ", active_tab_ids);
        this.store_active_tab_ids(active_tab_ids);
    }
    
}