import {Crud_Types} from "../../crud_types";
import {Crud_Instance} from "../../crud_instance";

export class Modal {

    private instance: Crud_Instance;

    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }

    public init(data: Crud_Types.Crud_Modal_Data, modal_callback: Function, append_to: JQuery | HTMLElement | string = "body"): void {
        if (modal_callback === null || typeof modal_callback !== "function") {
            console.error("init_modal(): " + modal_callback + " is not a function!");
        }

        if (append_to === null || typeof append_to === typeof undefined) {
            console.log("init_modal(): append_to variable was not set. Using \"body\".");
            append_to = "body";
        }

        let modal_template = this.instance.find(".crud-modal-template");

        // Clone the modal template to the body element.
        let clone = $(modal_template).clone();
        $(clone).attr("id", "crud-modal-js-copy");
        $(clone).removeClass("crud-modal-template");
        $(clone).addClass("crud-modal");
        $(clone).appendTo(append_to);
        let modal_copy: HTMLElement = this.get_modal();

        // Configure the clone.
        this.set_data(data, modal_copy);

        let active_elem: HTMLElement | null = null;

        // Generate the modal.
        $(modal_copy).iziModal({
            appendTo: append_to,
            appendToOverlay: append_to,
            onClosing() {
                // It is allowed to return false in the callback function.
                // This will cancel the close attempt.
                // TODO: implement

                // For some reason, IE only knows the "activeElement" during the execution of this function.
                // We therefore need to store it here to later use it in the onClosed function.
                // ...
                // Acquire the element which led to the disappearance of the modal.
                // The user will most likely click on an element to close the modal.
                // The "active_element" will be exactly this element. We pass it to the callback for easy processing.
                active_elem = $(document.activeElement)[0];
            },
            onClosed() {
                // TODO: second callback
                let result = modal_callback(active_elem);

                // Delete the cloned modal after the hide animation finished.
                $(modal_copy).iziModal('destroy');
                $(modal_copy).remove();
            }
        });

        // Open the modal.
        $(modal_copy).iziModal('open');
    }

    public get_modal(): HTMLElement {
        return $("#crud-modal-js-copy")[0];
    }

    public set_data(data: Crud_Types.Crud_Modal_Data, modal_copy?: HTMLElement): void {
        if ( !modal_copy ) {
            modal_copy = this.get_modal();
        }

        if (data.header) {
            $(modal_copy).find(".crud-modal-header").html(<string>data.header); // <string>: All data. types are viable!
        }
        if (data.body) {
            $(modal_copy).find(".crud-modal-body").html(<string>data.body);
        }
        if (data.footer) {
            $(modal_copy).find(".crud-modal-footer").html(<string>data.footer);
        }
    }

    public get_header(modal_copy?: HTMLElement): HTMLElement {
        if ( !modal_copy ) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-header")[0];
    }

    public get_body(modal_copy?: HTMLElement): HTMLElement {
        if ( !modal_copy ) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-body")[0];
    }

    public get_footer(modal_copy?: HTMLElement): HTMLElement {
        if ( !modal_copy ) {
            modal_copy = this.get_modal();
        }
        return $(modal_copy).find(".crud-modal-footer")[0];
    }

}