import {Modal} from "./modal";
import {Input_Manager} from "../inputs/input_manager";
import {Crud_Instance} from "../../crud_instance";
import {Action_Manager} from "../action_manager";

export class Inlineedit_Modal extends Modal {

    private input_manager: Input_Manager;
    private action_manager: Action_Manager;

    constructor(instance: Crud_Instance) {
        super(instance);

        // Additional initialization:
        this.input_manager = new Input_Manager(instance);
        this.action_manager = new Action_Manager(instance);
    }

    public init_content(): void {
        this.input_manager.init_inputs(this.get_body());
        this.action_manager.init_actions(this.get_body());
    }

    public destroy_content(): void {
        this.input_manager.destroy_inputs();
    }

    public get_input_manager(): Input_Manager {
        return this.input_manager;
    }

    public get_action_manager(): Action_Manager {
        return this.action_manager;
    }

}