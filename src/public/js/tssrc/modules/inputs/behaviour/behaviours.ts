import {Crud_Tools} from "../../../crud_tools";
import {Behaviour} from "./behaviour";
import {Crud_Condition} from "../../../crud_condition";
import {Input} from "../input";

export class Behaviours {

    /**
     * Reference to the Input to which this object belongs.
     */
    private input: Input;

    private behaviours: Behaviour[] | null = null;

    constructor(input: Input) {
        this.input = input;
    }

    /**
     *
     */
    public init(): void {
        // The field behaviours must be present.
        if ( this.behaviours === null ) {
            return;
        }

        // Each behaviour must get initialized.
        $.each(this.behaviours, function(key: Number, behaviour: Behaviour) {
            behaviour.init();
        });
    }

    /**
     *
     */
    public load(): void {
        this.behaviours = this.load_behaviours();
    }

    /**
     * Loads the "data-behaviour" attribute from the specified input and returns all specified behaviours properly
     * parsed in a "ready-to-use" array of Field_Behaviour instances.
     * This method will always return null if it encounters an error.
     * @return {Behaviour} The fully parsed attribute.
     */
    private load_behaviours(): Behaviour[] | null {
        const data_behaviour = $(this.input.get_elem()).attr("data-behaviours");

        // Abort if the "data-behaviour" attribute does not exist or did not hold any data.
        if ( !Crud_Tools.is_set(data_behaviour) ) {
            console.error("Behaviour_Controller: load_behaviours(): data-behaviour attribute missing!", this.input);
            return null;
        }

        // The behaviour is a complex array, provided as a json string.
        const data_behaviours = Crud_Tools.try_parse_json(<string>data_behaviour);

        // Abort if the behaviours could not be parsed.
        if ( data_behaviours === false )
        {
            console.error("Behaviour_Controller: load_behaviours(): " +
                "\"data-behaviour\" attribute was not json-parsable!", data_behaviour);
            return null;
        }

        // The parsed behaviours must be an array.
        if ( !Crud_Tools.is_array(data_behaviours) ) {
            console.error("Behaviour_Controller: load_behaviours(): " +
                "\"data-behaviour\" attribute was not a json encoded array!", this.input);
            return null;
        }

        // The parsed array must not be empty.
        if ( Crud_Tools.is_empty(data_behaviours) ) {
            // An empty array means that no behaviour got defined. This is not an error, but initialization still
            // needs to stop at this point.
            return null;
        }

        /* The behaviours (outer array) got properly loaded. */

        const parsed_behaviours: Behaviour[] = [];
        let error = false;

        // Each behaviour gets provided as a json encoded string and therefor needs to be processed.
        $.each(data_behaviours, function(this: Behaviours, key: number, value: string) {
            const data_behaviour = Crud_Tools.try_parse_json(value);

            // Abort if the behaviour could not be parsed.
            if ( data_behaviour === false )
            {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was not json-parsable!", value);
                error = true;
                return false; // Terminates .each().
            }

            // The parsed behaviour must be an object.
            if ( !Crud_Tools.is_object(data_behaviour) ) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was not an object!", this.input, value);
                error = true;
                return false; // Terminates .each().
            }

            // The parsed object must not be empty.
            if ( Crud_Tools.is_empty(data_behaviour) ) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour was an empty object!", this.input, value);
                error = true;
                return false; // Terminates .each().
            }

            /* The behaviour itself got properly loaded. */

            // Each behaviour contains a condition, which also gets provided as a json string. It must be processed.
            data_behaviour.condition = Crud_Tools.try_parse_json(String( data_behaviour.condition ));

            // Abort if the behaviour condition could not be parsed.
            if ( data_behaviour.condition === false ) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour condition was not json-parsable!");
                error = true;
                return false; // Terminates .each().
            }

            // The parsed behaviour condition must be an array.
            if ( !Crud_Tools.is_array(data_behaviour.condition) ) {
                console.error("Behaviour_Controller: load_behaviours(): " +
                    "Behaviour condition was not an array!", this.input);
                error = true;
                return false; // Terminates .each().
            }

            /* The behaviours condition got properly loaded. */

            const action = data_behaviour.action;
            const condition = new Crud_Condition().from_array(data_behaviour.condition);

            const behaviour = new Behaviour(this.input);
            behaviour.set_action(String(action));
            behaviour.set_condition(condition);

            parsed_behaviours.push(behaviour);
        }.bind(this));

        if ( error ) {
            return null;
        }
        return parsed_behaviours;
    }

}