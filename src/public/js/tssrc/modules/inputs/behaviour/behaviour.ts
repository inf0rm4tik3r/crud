import {Crud_Condition} from "../../../crud_condition";
import {Input_Manager} from "../input_manager";
import {Input} from "../input";
import {Crud_Config} from "../../../crud_config";

export class Behaviour {

    /**
     * Reference to the Input on which this behaviour is defined.
     */
    private input: Input;

    /**
     * What to do if the condition evaluated to true or false.
     */
    private action: string;

    /**
     * Condition, determining when to execute the specified action.
     */
    private condition: Crud_Condition;


    constructor(input: Input) {
        this.input = input;
    }


    /**
     * Each input, mentioned in condition of this behaviour, must trigger the process() function of this behaviour
     * if its value changes.
     *
     * Therefore:
     * 1. Get all inputs mentioned in this behaviours condition.
     * 2. Initialize a callback on them ( $.on() ) which will listen for changes.
     * 3. Every time an input changes, process this behaviour.
     */
    public init(): void {
        // The input behaviour needs to be processed once to bring the input in its correct state.
        this.process();

        // Process the input behaviour at any time one of the involved inputs changes.
        $.each(this.get_involved_inputs(), function (this: Behaviour, key: Number, involved_input: Input) {

            const activators = this.acquire_behaviour_check_triggers(involved_input);

            $(involved_input.get_elem()).on(activators, function (this: Behaviour) {
                this.process();
            }.bind(this));

        }.bind(this));
    }

    /**
     *
     * @param {Input} input
     * @return {string}
     */
    private acquire_behaviour_check_triggers(input: Input): string {
        // TODO: Let the returned triggers be dependant on the inputs type.

        const test = "varchar";
        const triggers: string[] = Crud_Config.behaviour_check_triggers[test];
        let trigger_string: string = "";

        for ( let i = 0; i < triggers.length; i++ ) {
            trigger_string += triggers[i];
            if ( i !== triggers.length - 1 ) {
                trigger_string += " ";
            }
        }

        return trigger_string;
    }

    /**
     *
     */
    public process(): void {
        const eval_result = this.condition.evaluate( this.input.get_instance() );

        switch ( this.action ) {
            case "4": // CHANGE_VISIBILITY
                if ( eval_result === true ) {
                    this.input.show();
                }
                else {
                    this.input.hide();
                }
                break;

            case "7": // CHANGE_EDITABILITY
                if ( eval_result === true ) {
                    this.input.enable();
                }
                else {
                    this.input.disable();
                }
                break;

            default:
                console.error("process(): Unknown behaviour action \"" + this.action + "\". Doing nothing...");
                break;
        }
    }

    /**
     *
     * @return {string[]}
     */
    public get_involved_input_ids(): string[] {
        return this.condition.get_involved();
    }

    /**
     *
     * @return {HTMLElement[]}
     */
    public get_involved_inputs(): Input[] {
        const input_ids = this.get_involved_input_ids();
        const inputs: Input[] = [];

        $.each(input_ids, function(this: Behaviour, key: number, input_id: string) {
            const input = this.input.get_instance().get_input_manager().get_input( input_id );
            if (input) {
                inputs.push(input);
            }
            else {
                console.error("get_involved_inputs(): Input for: \"" + input_id + "\" could not be found!");
            }
        }.bind(this));

        return inputs;
    }

    // GETTER AND SETTER

    public set_input(input: Input) {
        this.input = input;
    }

    public get_input(): Input {
        return this.input;
    }

    public set_action(action: string) {
        this.action = action;
    }

    public get_action(): string {
        return this.action;
    }

    public set_condition(condition: Crud_Condition) {
        this.condition = condition;
    }

    public get_condition(): Crud_Condition {
        return this.condition;
    }

}