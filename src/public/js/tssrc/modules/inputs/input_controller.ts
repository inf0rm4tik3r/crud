import {Crud_Types} from "../../crud_types";
import {Crud_Instance} from "../../crud_instance";
import {Input} from "./input";

export abstract class Input_Controller {

    protected instance: Crud_Instance;

    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;
    }

    abstract init(input: Input, ...other: any[]): void;
    abstract get_data(input: Input, options?: Crud_Types.Acquire_Input_Options, ...other: any[]): any;
    abstract destroy(input: Input, ...other: any[]): void;

}