import {Crud_Types} from "../../crud_types";
import {Crud_Instance} from "../../crud_instance";
import {Behaviours} from "./behaviour/behaviours";
import {Crud_Tools} from "../../crud_tools";
import {Validation} from "./validation/validation";

export class Input {

    private instance: Crud_Instance;
    private elem: HTMLElement;

    private id: string;
    private group: string;
    private html_type: string | undefined;
    private db_type: string;
    private internal_type: string;
    private display_type: string;
    private editable: boolean | undefined;
    private inlined: boolean | undefined;

    private validation: Validation;
    private behaviours: Behaviours;

    constructor(instance: Crud_Instance, elem: HTMLElement) {
        this.instance = instance;
        this.elem = elem;
    }

    /**
     * Loads all necessary data-attributes
     * @return self
     */
    public load(): Input {

        const id = $(this.elem).attr("id");
        if ( id ) {
            this.id = id;
        } else {
            console.error("load(): Unable to load the the \"data-inline\" attribute for: ", this.elem);
        }

        // The "data-group" attribute determines the group in which this input should reside.
        // Typically "entry" or "search"...
        const group = $(this.elem).attr("data-group");
        if ( group ) {
            this.group = group;
        } else {
            console.error("load(): Unable to load the the \"data-group\" attribute for: ", this.elem);
        }

        const html_type = $(this.elem).attr("type");
        if ( html_type ) {
            this.html_type = html_type;
        } else {
            this.html_type = undefined;
            //console.error("load(): Unable to load the the \"type\" attribute for: ", this.elem);
        }

        const db_type = $(this.elem).attr("data-db-type");
        if ( db_type ) {
            this.db_type = db_type;
        } else {
            console.error("load(): Unable to load the the \"data-db-type\" attribute for: ", this.elem);
        }

        const internal_type = $(this.elem).attr("data-internal-type");
        if ( internal_type ) {
            this.internal_type = internal_type;
        } else {
            console.error("load(): Unable to load the the \"data-internal-type\" attribute for: ", this.elem);
        }

        const display_type = $(this.elem).attr("data-display-type");
        if ( display_type ) {
            this.display_type = display_type;
        } else {
            console.error("load(): Unable to load the the \"data-display-type\" attribute for: ", this.elem);
        }

        const editable = $(this.elem).attr("data-editable");
        if ( editable ) {
            this.editable = editable !== "false";
        } else {
            this.editable = undefined;
            console.error("load(): Unable to load the the \"data-editable\" attribute for: ", this.elem);
        }

        const inlined = $(this.elem).attr("data-inline");
        if ( inlined ) {
            this.inlined = inlined !== "false";
        } else {
            this.inlined = undefined;
            //console.error("load(): Unable to load the the \"data-inline\" attribute for: ", this.elem);
        }

        return this;
    }

    /**
     * This method should only be called when the input is fully loaded!
     * => Accessing the current value through Crud.acquire_input_data_single() is guaranteed then to work.
     *
     * @param {Crud_Types.Input_Activators} activators
     */
    public after_init(activators?: Crud_Types.Input_Activators): void {
        // Initialize the validation for this input.
        this.validation = new Validation(this);
        this.validation.load();
        this.validation.init(activators);

        // Load and initialize the behaviours for this input.
        // Other inputs then the ones from the "entry" group should not "behave".
        if ( this.group === "entry" ) {
            this.behaviours = new Behaviours(this);
            this.behaviours.load();
            this.behaviours.init();
        }
    }

    public show(): void {
        $(this.elem).closest("[data-field-container]").css("display", "block");
    }

    public hide(): void {
        $(this.elem).closest("[data-field-container]").css("display", "none");
    }

    public enable(): void {
        $(this.elem).attr("disabled", null);
    }

    public disable(): void {
        $(this.elem).attr("disabled", "true");
    }

    public acquire_elements_for_class_assignment(): HTMLElement[] {
        let objects: HTMLElement[] = [];

        // The input object itself needs to get the desired classes.
        objects.push(this.get_elem());

        // If a select2 field is displayed instead of the default input, the container needs the classes as well.
        $(this.get_elem()).siblings(".select2.select2-container").each(function(i: number, obj: HTMLElement) {
            objects.push(obj);
        });

        // If the flatpickr plugin created its input field, it needs the classes as well.
        $(this.get_elem()).siblings(".flatpickr-input").each(function(i: number, obj: HTMLElement) {
            objects.push(obj);
        });

        return objects;
    }

    public add_class(class_names: string | string[]): void {
        $.each(this.acquire_elements_for_class_assignment(), function(key: number, elem: HTMLElement) {
            if ( Crud_Tools.is_array(class_names) ) {
                $.each(class_names, function (key: number, class_name: string) {
                    $(elem).addClass(class_name);
                });
            } else {
                $(elem).addClass(<string>class_names);
            }
        });
    }

    public remove_class(class_names: string | string[]): void {
        $.each(this.acquire_elements_for_class_assignment(), function(key: number, elem: HTMLElement) {
            if ( Crud_Tools.is_array(class_names) ) {
                $.each(class_names, function (key: number, class_name: string) {
                    $(elem).removeClass(class_name);
                });
            } else {
                $(elem).removeClass(<string>class_names);
            }
        });
    }


    public get_instance(): Crud_Instance {
        return this.instance;
    }

    public get_elem(): HTMLElement {
        return this.elem;
    }

    public get_id(): string {
        return this.id;
    }

    public get_group(): string {
        return this.group;
    }

    public get_html_type(): string | undefined {
        return this.html_type;
    }

    public get_db_type(): string {
        return this.db_type;
    }

    public get_internal_type(): string {
        return this.internal_type;
    }

    public get_display_type(): string {
        return this.display_type;
    }

    public is_editable(): boolean | undefined {
        return this.editable;
    }

    public is_inlined(): boolean | undefined {
        return this.inlined;
    }

    public get_validation(): Validation {
        return this.validation;
    }

    public get_behaviours(): Behaviours {
        return this.behaviours;
    }

}