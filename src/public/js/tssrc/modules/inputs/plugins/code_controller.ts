import {Input_Controller} from "../input_controller";
import {Crud_Tools} from "../../../crud_tools";
import {Input} from "../input";
import * as hljs from "highlight.js";

export class Code_Controller extends Input_Controller {

    /**
     * Initializes a 'code' input. One layer for the highlighted view / one for the input.
     * @param input
     */
    init(input: Input): void {
        let code_edit_id = input.get_id();
        let code_view_id = code_edit_id + "-view";

        let code_edit = input.get_elem();
        let code_view = $("#" + code_view_id)[0];

        // Initialize the code highlighting.
        hljs.highlightBlock(code_view);

        if ( input.is_editable() ) {
            let show_view_btn = $("button[data-show='" + code_view_id + "'][data-hide='" + code_edit_id +"']");

            // Hide the 'show-view' button.
            $(show_view_btn).css("display", "none");

            // Hide the edit-view.
            $(code_edit).css("display", "none");
        }
    }

    get_data(input: Input): string {
        return String( $(input.get_elem()).val() );
    }

    destroy(input: Input): void {
        // ...
    }

    edit(obj: HTMLElement): void {
        let id_to_show = $(obj).attr("data-show");
        let id_to_hide = $(obj).attr("data-hide");

        let to_show = $("#" + id_to_show);
        let to_hide = $("#" + id_to_hide);

        let other_btn = $("button[data-show=" + id_to_hide +"][data-hide=" + id_to_show +"]");

        // Hide self.
        $(obj).css("display", "none");

        // Show other_button.
        $(other_btn).css("display", "block");

        // Hide to_hide.
        $(to_hide).css("display", "none");

        // Show to_show.
        $(to_show).css("display", "block");
    }

    view(obj: HTMLElement): void {
        let id_to_show = $(obj).attr("data-show");
        let id_to_hide = $(obj).attr("data-hide");

        let to_show = $("#" + id_to_show);
        let to_hide = $("#" + id_to_hide);

        let other_btn = $("button[data-show=" + id_to_hide +"][data-hide=" + id_to_show +"]");

        // Hide self.
        $(obj).css("display", "none");

        // Show other_button.
        $(other_btn).css("display", "block");

        // Hide to_hide.
        $(to_hide).css("display", "none");

        // Copy the current state of the input field back to the view element.
        $(to_show).html( Crud_Tools.escape_html( String( $(to_hide).val() ) ) );

        // And reinitialize the code highlighting.
        hljs.highlightBlock( $(to_show)[0] );

        // Show to_show.
        $(to_show).css("display", "block");
    }

}