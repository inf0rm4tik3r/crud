import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Limit_Controller extends Input_Controller {

    /**
     * Initializes the limit display element for the specified input.
     * @param {HTMLElement} input
     */
    init(input: Input): void {
        // Find the limit display element.
        const limit_obj_jquery: JQuery = $("#" + input.get_id() + "_limit");
        if ( limit_obj_jquery.length === 0 ) {
            // IF there is no "_limit" DOM element, initialization can be aborted.
            //console.error("init(): Limit display element could not be found!");
            return;
        }
        const limit_obj = limit_obj_jquery[0];

        // Check if no limit calculation should take place.
        if ($(limit_obj).hasClass("crud_limit_infinite")) { // TODO: change _ to - in class name.
            $(limit_obj).html("&infin;");
            return;
        }

        // Find the maximum input length.
        const data_length: string | undefined = $(input.get_elem()).attr("data-length");
        if ( !data_length ) {
            console.error("data-length attribute does not exist on: ", input);
            return;
        }
        const max_length = parseInt( data_length );

        // Initial setup.
        this.update_limit(input, max_length, limit_obj);

        // Update the indicator after the textarea changed.
        $(input.get_elem()).on('change keydown paste', function (this: Limit_Controller) {
            this.update_limit(input, max_length, limit_obj);
        }.bind(this));
    }

    update_limit(input: Input, max_length: number, limit_elem: HTMLElement): void {
        const current_length = String( $(input.get_elem()).val() ).length;
        const characters_left = max_length - current_length;

        // We let the indicator flash if the character limit is reached.
        if (characters_left <= 0) {
            // Force display to 0.
            $(limit_elem).text(0);

            // Flash color.
            $(limit_elem).css("color", "#d9281b");
            $(limit_elem).css("font-weight", "bold");

            //Revert style after short delay.
            setTimeout(function () {
                $(limit_elem).css("color", "lightgrey");
                $(limit_elem).css("font-weight", "normal");
            }, 250);
        }
        // Otherwise just set the remaining characters.
        else {
            $(limit_elem).text(characters_left);
        }
    }

    /**
     * Not implemented!
     * @param {HTMLElement} input
     * @return {any}
     */
    get_data(input: Input): any {
        return undefined;
    }

    /**
     * Not implemented!
     * @param {HTMLElement} input
     */
    destroy(input: Input): void {
    }

}