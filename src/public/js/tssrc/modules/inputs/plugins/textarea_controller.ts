import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Textarea_Controller extends Input_Controller {

    init(input: Input): void {
        // Automatic height adjustment.
        /*
        $(input).on('keyup', function () {
            this.style.overflow = 'hidden';
            this.style.height = "0";
            this.style.height = this.scrollHeight + 'px';
        });
        */
    }

    get_data(input: Input): string {
        return String( $(input.get_elem()).val() );
    }

    /**
     * Not implemented!
     * @param {HTMLElement} input
     */
    destroy(input: Input): void {
    }

}