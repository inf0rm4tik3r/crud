import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Signature_Pad_Controller extends Input_Controller {

    private signature_pads: {[key: string]: SignaturePad} = {};

    store(input: Input, signature_pad: SignaturePad): void {
        this.signature_pads[ input.get_id() ] = signature_pad;
    }

    retrieve(input: Input): SignaturePad {
        return this.signature_pads[ input.get_id() ];
    }

    init(input: Input): void {
        const sig_pad = new SignaturePad(input.get_elem());
        // Store the object reference.
        this.store(input, sig_pad);

        // Load old data.
        const data = String( $(input.get_elem()).attr("data-data") );
        const sig_pad_data: SignaturePad.Point[][] = JSON.parse(data);

        if ( sig_pad_data.length !== 0 ) {
            sig_pad.fromData(sig_pad_data);
        }

        // The signature pad needs to be redrawn whenever the size / dimension of the window changes.
        window.addEventListener("resize", function(this: Signature_Pad_Controller) {
            this.draw_canvas(input);
        }.bind(this));

        // Initial canvas rendering:
        // INFO: This might not work properly (results in a blank canvas).
        // But if if works, we immediately see a sharp rendering of our signature. Only using the delayed rendering
        // from the function below would leave us with a blurry signature for .5 seconds.
        this.draw_canvas(input);

        // Safety feature:
        // No matter if the initial rendering worked or not, we redraw the canvas after half a second
        // to guarantee functionality. This is unnoticeable and might not be necessary in a future update of
        // the utilized plugin.
        setTimeout(function (this: Signature_Pad_Controller) {
            this.draw_canvas(input);
        }.bind(this), 500);
    }

    draw_canvas(input: Input): void {
        const sig_pad: SignaturePad = this.retrieve(input);
        const sig_pad_data = sig_pad.toData();

        this.resize_canvas(sig_pad._canvas);

        if (sig_pad_data.length !== 0) {
            sig_pad.fromData(sig_pad_data);
        }
    }

    resize_canvas(canvas: HTMLCanvasElement): void {
        const ratio =  Math.max(window.devicePixelRatio || 1, 1);
        const canvas_rendering_context: CanvasRenderingContext2D = <CanvasRenderingContext2D>canvas.getContext("2d");

        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas_rendering_context.scale(ratio, ratio);
    }

    get_controls(input: Input): JQuery {
        return $(input.get_elem()).closest(".crud-signature-pad").children(".crud-signature-pad-controls");
    }

    unlock(input: Input): void {
        $(input.get_elem()).siblings(".crud-signature-pad-canvas-overlay").css("display", "none");

        const button_wrapper: HTMLElement = this.get_controls(input).children(".crud-btn-wrapper")[0];
        console.log(this.get_controls(input));
        $(button_wrapper).children("[data-unlock-btn]").css("display", "none");
        $(button_wrapper).children("[data-lock-btn]").css("display", "block");
        $(button_wrapper).children("[data-clear-btn]").css("display", "block");

        // The signature pad will not get drawn correctly if not visible upon initialization.
        // We will therefore redraw it now to guarantee a proper display of the current signature.
        this.draw_canvas(input);
    }

    lock(input: Input): void {
        $(input.get_elem()).siblings(".crud-signature-pad-canvas-overlay").css("display", "block");

        const button_wrapper: HTMLElement = this.get_controls(input).children(".crud-btn-wrapper")[0];
        $(button_wrapper).children("[data-unlock-btn]").css("display", "block");
        $(button_wrapper).children("[data-lock-btn]").css("display", "none");
        $(button_wrapper).children("[data-clear-btn]").css("display", "none");

        // The signature pad will not get drawn correctly if not visible upon initialization.
        // We will therefore redraw it now to guarantee a proper display of the current signature.
        this.draw_canvas(input);
    }

    enter_draw_mode(input: Input): void {
        const ctx = this.retrieve(input)._canvas.getContext("2d");
        if (ctx) {
            ctx.globalCompositeOperation = "source-over"; // This is the default value.
        } else {
            console.warn("enter_draw_mode(): No canvas.");
        }
    }

    enter_erase_mode(input: Input): void {
        const ctx = this.retrieve(input)._canvas.getContext("2d");
        if (ctx) {
            ctx.globalCompositeOperation = "destination-out";
        } else {
            console.warn("enter_erase_mode(): No canvas.");
        }
    }

    undo(input: Input): void {
        const ctx = this.retrieve(input)._canvas.getContext("2d");
        let gco_backup: string = "";
        if (ctx) {
            gco_backup = ctx.globalCompositeOperation;
            ctx.globalCompositeOperation = "source-over";
        }

        const data: SignaturePad.Point[][] = this.get_data(input);
        if (data) {
            data.pop(); // Removes the last dot or line.
            this.restore_from_data(input, data);
        }

        if (ctx) {
            ctx.globalCompositeOperation = gco_backup;
        }
    }

    clear(input: Input): void {
        this.retrieve(input).clear();
        // Trigger a fake click, so that the validation plugin runs again.
        $(input.get_elem()).trigger("click");
    }

    get_data(input: Input): SignaturePad.Point[][] {
        return this.retrieve(input).toData();
    }

    get_data_url(input: Input): string {
        return this.retrieve(input).toDataURL();
    }

    restore_from_data(input: Input, data: SignaturePad.Point[][]): void {
        let sig_pad = this.retrieve(input);
        sig_pad.fromData(data);
        sig_pad._data = data; // BUG WORKAROUND!
    }

    restore_from_data_url(input: Input, data: string): void {
        this.retrieve(input).fromDataURL(data);
    }

    destroy(input: Input): void {
        this.retrieve(input).off();
    }

}