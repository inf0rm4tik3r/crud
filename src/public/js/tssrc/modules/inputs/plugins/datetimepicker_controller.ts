import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Datetimepicker_Controller extends Input_Controller {

    /**
     * Initializes a datetimepicker with the provided settings.
     * This method does nothing if parameter obj is null.
     *
     * @param input The Input object to initialize the datetimepicker on.
     * @param show_datepicker boolean: Should the datepicker be displayed? Default: true
     * @param show_timepicker boolean: Should the timepicker be displayed? Default: true
     */
    init(input: Input,
         show_datepicker: boolean = true,
         show_timepicker: boolean = true
    ): void {
        // Determine the date and time formats which should be used by the datetimepicker.
        let format = "";

        if ( show_datepicker && show_timepicker ) {
            format = this.instance.get_state().date_format() +
                     this.instance.get_state().date_separator() +
                     this.instance.get_state().time_format();
        }
        else if ( show_datepicker && !show_timepicker ) {
            format = this.instance.get_state().date_format();
        }
        else if ( !show_datepicker && show_timepicker ) {
            format = this.instance.get_state().time_format();
        }
        else {
            console.error("init_datetimepicker(): At least one of showDatepicker and showTimepicker must be true!");
        }

        let alt_format = format;
        //Flatpickr.l10de.default.firstDayOfWeek = 1; // Monday

        $(input.get_elem()).flatpickr({
            // Major:
            //appendTo: $(obj).parent(),
            inline: input.is_inlined(),

            // Formats.
            dateFormat: format,
            altInput: true,
            //altFormat: alt_format,
            altFormat: "d. F Y, H:i", // TODO: make dynamic
            //locale: crud_state.language_short, // language file needs to be present on site!!
            locale: "de", // language file needs to be present on site!!
            /*
            locale: {
                locale: "de",
                firstDayOfWeek: 1
            },
            */

            /* Date */
            noCalendar: !show_datepicker,
            weekNumbers: false,

            /* Time */
            enableTime: show_timepicker,
            enableSeconds: false,
            time_24hr: true,
            // Initial time values.
            defaultHour: 12,
            defaultMinute: 0,

            /* Functions */
            onReady(selectedDates: any, dateStr: any, instance: any) {
                // The main systems wrapper class is needed to apply styling.
                $(instance.calendarContainer).addClass("crud-instance-wrapper");
                $(instance.input).siblings(".flatpickr-input").removeClass("form-control");
            },
        });
    }

    get_data(input: Input): string {
        return String( $(input.get_elem()).val() );
    }

    destroy(input: Input): void {
        $(input.get_elem()).flatpickr().destroy();
    }

}