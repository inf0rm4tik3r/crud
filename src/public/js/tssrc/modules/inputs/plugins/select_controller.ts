import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Select_Controller extends Input_Controller {

    init(input: Input, use_multiselect: boolean): void {
        switch ( this.instance.get_state().select_driver() ) {
            case "select2":
                const select2 = $(input.get_elem()).select2({
                    placeholder: use_multiselect ? "Optionen wählen..." : '', // TODO: translate
                    //placeholder: {
                    //    id: '0', // the value of the option
                    //    text: '- keine -'
                    //},
                    allowClear: use_multiselect,
                    multiple: use_multiselect,
                    closeOnSelect: !use_multiselect,
                    language: this.instance.get_state().language_short(),
                    dropdownParent: $(input.get_elem()).parent()
                });

                // Select all options in the select2 element that have the selected attribute set.
                const selected: string[] = [];
                const all_options = $(select2).find("option");
                all_options.each(function (i: number, element: HTMLElement) {
                    const obj = element as HTMLOptionElement;
                    if (obj.defaultSelected === true) {
                        selected.push( String( $(this).val() ) );
                    }
                });
                if (selected.length === 0 && all_options.length > 0 && !use_multiselect) {
                    selected.push( String( $(all_options[0]).val() ) );
                }

                select2.val(selected).trigger('change');
                break;

            default:
                console.error("init(): Unknown select driver specified: ", this.instance.get_state().select_driver());
                break;
        }
    }

    get_data(input: Input): string | number | string[] | undefined {
        return $(input.get_elem()).val();
    }

    destroy(input: Input): void {
        switch ( this.instance.get_state().select_driver() ) {
            case "select2":
                $(input.get_elem()).select2("destroy");
                break;

            default:
                console.error("destroy(): Unknown select driver specified: ", this.instance.get_state().select_driver());
                break;
        }
    }

}