import {Crud_Types} from "../../../crud_types";
import {Input_Controller} from "../input_controller";
import {Input} from "../input";

declare const tinymce: any;

export class Texteditor_Controller extends Input_Controller {

    public init(input: Input): void {
        let self = this;

        switch ( this.instance.get_state().texteditor_driver() ) {
            case "tinymce":
                tinymce.init({
                    selector: "#" + input.get_id(),
                    height: 200,
                    theme: 'modern',
                    branding: false,
                    plugins: [
                        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                        'searchreplace wordcount visualblocks visualchars code fullscreen',
                        'insertdatetime media nonbreaking save table contextmenu directionality',
                        'utf8_emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
                    ],
                    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: 'print preview media | forecolor backcolor utf8_emoticons | codesample help',
                    image_advtab: true,
                    templates: [
                        { title: 'Test template 1', content: 'Test 1' },
                        { title: 'Test template 2', content: 'Test 2' }
                    ],
                    content_css: [
                        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                        '//www.tinymce.com/css/codepen.min.css'
                    ],
                    setup(editor: any) { // TODO: what type???
                        editor.on("init", function(args: any) {
                            // Validation needs to be triggered on an editor change.
                            input.after_init([
                                {"element": editor, "triggers": "keyup"}
                            ]);
                        });

                        // Pass through any editor events to jquery.
                        editor.on("keyup", function(args: any) {
                            $(editor).trigger("keyup");
                        });
                    }
                });
                break;

            case "ckeditor":
                CKEDITOR.replace(input.get_id());
                break;

            default:
                console.error(
                    "init_texteditor(): Unknown texteditor driver specified: ",
                    this.instance.get_state().texteditor_driver()
                );
                break;
        }
    }

    public get_data(input: Input, options?: Crud_Types.Acquire_Input_Options): string {
        let format: string = "html";
        if ( options && options.hasOwnProperty("texteditor_format") ) {
            format = options["texteditor_format"];
        }

        switch ( this.instance.get_state().texteditor_driver() )
        {
            case "tinymce":
                return tinymce.get(input.get_id()).getContent({format: format});

            case "ckeditor":
                let ckeditor_instance = CKEDITOR.instances[ (<any>input.get_id()) ];

                // Try to read the editor-data.
                if (ckeditor_instance === null) {
                    console.error("get_ckeditor_data(): " + input.get_id() + " is not an instance!", true);
                    return "error";
                }
                else {
                    return CKEDITOR.instances[ (<any>input.get_id()) ].getData();
                }

            default:
                console.error("get_texteditor_data(): Unknown texteditor driver specified: ", this.instance.get_state().texteditor_driver());
                return "error";
        }
    }

    public destroy(input: Input): void {

        switch ( this.instance.get_state().texteditor_driver() ) {
            case "tinymce":
                tinymce.remove("#" + input.get_id());
                break;

            case "ckeditor":
                let ckeditor_instance = CKEDITOR.instances[ (<any>input.get_id()) ];

                // Try to destroy the editor-instance.
                if (ckeditor_instance === null || ckeditor_instance === undefined) {
                    console.error("destroy_ckeditor(): " + input.get_id() + " is not an instance!", true);
                }
                else {
                    CKEDITOR.instances[ (<any>input.get_id()) ].destroy();
                }
                break;

            default:
                console.error(
                    "destroy_texteditor(): Unknown texteditor driver specified: ",
                    this.instance.get_state().texteditor_driver()
                );
                break;
        }
    }

}