import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class Boolean_Controller extends Input_Controller {

    init(input: Input): void {
        // ...
    }

    get_data(input: Input): string {
        return $(input.get_elem()).is(":checked") ? "1" : "0"; // TODO: true / false instead?
    }

    destroy(input: Input): void {
        // ...
    }

}