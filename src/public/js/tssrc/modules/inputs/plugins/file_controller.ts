import {Input_Controller} from "../input_controller";
import {Input} from "../input";

export class File_Controller extends Input_Controller {

    init(input: Input): void {
        const elem = input.get_elem();

        $(elem).on("change", function(this: HTMLElement) {
            const input_elem: HTMLInputElement = <HTMLInputElement>elem;
            const num_files = input_elem.files ? input_elem.files.length : 0; // TODO: changed from 1 to 0: was that incorrect ???
            const label = String( $(input_elem).val() ).replace(/\\/g, '/').replace(/.*\//, '');

            $(elem).trigger('fileselect', [num_files, label]);
        });

        $(elem).on('fileselect', function(this: File_Controller, event: JQuery.Event, num_files: number, label: string) {
            const text_elem = $(elem).parents('.input-group').find(':text');

            if ( num_files > 1 ) {
                $(text_elem).val(num_files + ' ' + this.instance.get_state().translations().files_selected);
            } else {
                $(text_elem).val(label);
            }
        }.bind(this));
    }

    get_data(input: Input): string | File {
        const input_elem: HTMLInputElement = <HTMLInputElement>input.get_elem();
        const file_list: FileList | null = input_elem.files;

        if ( !file_list ) {
            console.error("get_data(): No file selected...");
            return "";
        } else {
            return file_list[0];
        }
    }

    destroy(input: Input): void {
        // ...
    }

}