import {Crud_Instance} from "../../crud_instance";
import {Crud_Types} from "../../crud_types";
import {Boolean_Controller} from "./plugins/boolean_controller";
import {Code_Controller} from "./plugins/code_controller";
import {Colorpicker_Controller} from "./plugins/colorpicker_controller";
import {File_Controller} from "./plugins/file_controller";
import {Datetimepicker_Controller} from "./plugins/datetimepicker_controller";
import {Limit_Controller} from "./plugins/limit_controller";
import {Select_Controller} from "./plugins/select_controller";
import {Signature_Pad_Controller} from "./plugins/signature_pad_controller";
import {Textarea_Controller} from "./plugins/textarea_controller";
import {Texteditor_Controller} from "./plugins/texteditor_controller";
import {Input} from "./input";
import {Crud_Tools} from "../../crud_tools";

export class Input_Manager {

    private instance: Crud_Instance;

    private boolean_controller: Boolean_Controller;
    private code_controller: Code_Controller;
    private colorpicker_controller: Colorpicker_Controller;
    private file_controller: File_Controller;
    private datetimepicker_controller: Datetimepicker_Controller;
    private limit_controller: Limit_Controller;
    private select_controller: Select_Controller;
    private signature_pad_controller: Signature_Pad_Controller;
    private textarea_controller: Textarea_Controller;
    private texteditor_controller: Texteditor_Controller;

    private inputs: {[key: string]: Input};

    constructor(crud_instance: Crud_Instance) {
        this.instance = crud_instance;

        this.boolean_controller = new Boolean_Controller(this.instance);
        this.code_controller = new Code_Controller(this.instance);
        this.colorpicker_controller = new Colorpicker_Controller(this.instance);
        this.file_controller = new File_Controller(this.instance);
        this.datetimepicker_controller = new Datetimepicker_Controller(this.instance);
        this.limit_controller = new Limit_Controller(this.instance);
        this.select_controller = new Select_Controller(this.instance);
        this.signature_pad_controller = new Signature_Pad_Controller(this.instance);
        this.textarea_controller = new Textarea_Controller(this.instance);
        this.texteditor_controller = new Texteditor_Controller(this.instance);

        this.inputs = {};
    }

    public get_boolean_controller(): Boolean_Controller { return this.boolean_controller }
    public get_code_controller(): Code_Controller { return this.code_controller }
    public get_colorpicker_controller(): Colorpicker_Controller { return this.colorpicker_controller }
    public get_file_controller(): File_Controller { return this.file_controller }
    public get_datetimepicker_controller(): Datetimepicker_Controller { return this.datetimepicker_controller }
    public get_select_controller(): Select_Controller { return this.select_controller }
    public get_signature_pad_controller(): Signature_Pad_Controller { return this.signature_pad_controller }
    public get_texteditor_controller(): Texteditor_Controller { return this.texteditor_controller }

    /**
     * Finds all inputs (DOM elements which have a "data-input" attribute)
     */
    public init_inputs(in_obj?: HTMLElement): void {
        // Load inputs only from the specified HTMLElement if such an element got defined.
        let inputs: JQuery;
        if ( Crud_Tools.is_set(in_obj) ) {
            inputs = $(in_obj).find("[data-crud-input]");
        } else {
            inputs = this.instance.find("[data-crud-input]")
        }

        /*
         * The initialization of "normal" (entry) inputs and inputs for the search feature are divided,
         * so that we can access them individually and have the ability to alternate their initialization
         * process if necessary.
         */
        $(inputs).each(function (this: Input_Manager, key: number, elem: HTMLElement) {

            const input = new Input(this.instance, elem);
            input.load();
            this.init_input(input);

            this.inputs[input.get_id()] = input;

        }.bind(this));
    }

    /**
     * Initializes an input element.
     * @param {Input} input The input to initialize.
     */
    public init_input(input: Input): void {
        const elem = input.get_elem();
        let finish_init_directly = true;

        // Initialize the input fields based on their type.
        switch ( input.get_html_type() ) {
            case "number":
                $(input).on('change keyup paste', function (this: Input_Manager) {
                    this.instance.limit_input_length(elem);
                }.bind(this));
                break;
        }

        switch ( input.get_db_type() ) {
            case "default":
                //...
                break;

            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "bigint":
                switch ( input.get_display_type() )
                {
                    case "default":
                        //...
                        break;

                    case "select":
                        this.select_controller.init(input, false);
                        break;

                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;

            case "varchar":
                switch ( input.get_display_type() )
                {
                    case "default":
                        //...
                        break;

                    case "password":
                        //...
                        break;

                    case "textarea":
                        this.textarea_controller.init(input);
                        break;

                    case "texteditor":
                        this.texteditor_controller.init(input);
                        finish_init_directly = false;
                        break;

                    case "code":
                        this.code_controller.init(input);
                        break;

                    case "color":
                        this.colorpicker_controller.init(input);
                        break;

                    case "select":
                        this.select_controller.init(input, false);
                        break;

                    case "multiselect":
                        this.select_controller.init(input, true);
                        break;

                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }

                // Always initialize the limit display.
                this.limit_controller.init(input);
                break;

            case "enum":
                switch ( input.get_display_type() )
                {
                    default:
                        this.select_controller.init(input, false);
                        break;
                }
                break;

            case 'timestamp':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.init(input, true, true);
                        break;
                }
                break;

            case 'datetime':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.init(input, true, true);
                        break;
                }
                break;

            case 'date':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.init(input, true, false);
                        break;
                }
                break;

            case 'time':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.init(input, false, true);
                        break;
                }
                break;

            case 'tinytext':
            case 'mediumtext':
            case 'text':
            case 'longtext':
                switch ( input.get_display_type() )
                {
                    case "default":
                    case "textarea":
                        this.textarea_controller.init(input);
                        break;

                    case "texteditor":
                        this.texteditor_controller.init(input);
                        finish_init_directly = false;
                        break;

                    case "signature":
                        this.signature_pad_controller.init(input);
                        break;

                    case "code":
                        this.code_controller.init(input);
                        break;

                    case "color":
                        this.colorpicker_controller.init(input);
                        break;

                    case "select":
                        this.select_controller.init(input, false);
                        break;

                    case "multiselect":
                        this.select_controller.init(input, true);
                        break;

                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;

            case 'file':
            case 'image':
                switch ( input.get_display_type() )
                {
                    default:
                        this.file_controller.init(input);
                        break;
                }
                break;

            default:
                console.error("init_input(): Unknown html data-db-type (" + input.get_db_type() + ")", input);
        }

        /*
         * Problem:
         *
         * Libraries / Plugins can use a deferred initialization, but the final initialization steps
         * ( initializing the validation for example ) need access to the inputs current value!

         * If that should be the case, a call to Crud.acquire_input_data_single() may leeds to false
         * values or even exceptions / errors.
         * The final steps for initializing an input are therefore outsourced to the finish_input_initialization()
         * method, which should be called individually by anny Library / Plugin which loaded an input
         * in a deferred way.
         */

        if ( finish_init_directly === true ) {
            input.after_init();
        }
    }

    public destroy_inputs(): void {
        $.each(this.inputs, function(this: Input_Manager, key: number, input: Input) {
            this.destroy_input(input);
        }.bind(this));
    }

    /**
     * Destroys / deinitializes / restores default of - the specified input DOM element. (Does not necessarily have
     * to be of <input>!)
     * @param {HTMLElement} input The DOM object to destroy / reset.
     */
    public destroy_input(input: Input): void {
        // Destroy the input field based on its data type.
        switch ( input.get_db_type() ) {
            case "default":
                //...
                break;

            case "tinyint":
            case "smallint":
            case "mediumint":
            case "int":
            case "bigint":
                switch ( input.get_display_type() )
                {
                    case "default":
                        //...
                        break;

                    case "select":
                        this.select_controller.destroy(input);
                        break;

                    default:
                        console.error("destroy_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;

            case "varchar":
                switch ( input.get_display_type() )
                {
                    case "default":
                        //...
                        break;

                    case "password":
                        //...
                        break;

                    case "textarea":
                        //...
                        break;

                    case "texteditor":
                        this.texteditor_controller.destroy(input);
                        break;

                    case "code":
                        //...
                        break;

                    case "color":
                        this.colorpicker_controller.destroy(input);
                        break;

                    case "select":
                        this.select_controller.destroy(input);
                        break;

                    case "multiselect":
                        this.select_controller.destroy(input);
                        break;

                    default:
                        console.error("destroy_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;

            case "enum":
                switch ( input.get_display_type() )
                {
                    default:
                        this.select_controller.destroy(input);
                        break;
                }
                break;

            case 'timestamp':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;

            case 'datetime':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;

            case 'date':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;

            case 'time':
                switch ( input.get_display_type() )
                {
                    default:
                        this.datetimepicker_controller.destroy(input);
                        break;
                }
                break;

            case 'tinytext':
            case 'mediumtext':
            case 'text':
            case 'longtext':
                switch ( input.get_display_type() )
                {
                    case "default":
                    case "textarea":
                        //...
                        break;

                    case "texteditor":
                        this.texteditor_controller.destroy(input);
                        break;

                    case "signature":
                        this.signature_pad_controller.destroy(input);
                        break;

                    case "code":
                        //...
                        break;

                    case "color":
                        this.colorpicker_controller.destroy(input);
                        break;

                    case "select":
                        this.select_controller.destroy(input);
                        break;

                    case "multiselect":
                        this.select_controller.destroy(input);
                        break;

                    default:
                        console.error("init_input(): Unknown display_type (" + input.get_display_type() + ")", input);
                        break;
                }
                break;

            case 'file':
            case 'image':
                switch ( input.get_display_type() )
                {
                    default:
                        //...
                        break;
                }
                break;

            default:
                console.error("destroy_input(): Unknown data-db-type (" + input.get_db_type() + ")", input);
        }

        // Remove the input from the "inputs" object.
        delete this.inputs[input.get_id()];
    }


    public acquire_input_data(group: string, options?: Crud_Types.Acquire_Input_Options): Crud_Types.Input_Data[] {
        // This array will hold all of our field data objects.
        const data_array: Crud_Types.Input_Data[] = [];

        // Iterate over every input inside this instance and...
        $.each(this.inputs, function (this: Input_Manager, key: number, input: Input) {

            // load the current data/state of the input regarding its type and possibly active plugins.
            if ( input.get_group() === group ) {
                const data = this.acquire_input_data_single(input, options);
                if ( data ) {
                    data_array.push( data );
                }
            }

        }.bind(this));

        return data_array;
    }

    /**
     * Loads the current value of the specified input.
     * This function take into account which type the input is for and which plugins are active on this input.
     * Returns an array, containing the id of the input, and the loaded value.
     *
     * @param input
     * @param options
     * @returns
     *     {
     *       id: string,
     *       value: any
     *     }
     */
    // TODO: Move to Input class. Where should the manager objects go? Should they all be static?
    public acquire_input_data_single(input: Input, options?: Crud_Types.Acquire_Input_Options): Crud_Types.Input_Data | undefined {
        if ( !options ) {
            options = {};
        }

        const length_attr = $(input.get_elem()).attr('data-length');
        const length = length_attr ? parseInt( length_attr ) : undefined;

        let value = undefined; // TODO: null instead?

        switch ( input.get_db_type() ) {
            case "varchar":
            case "tinytext":
            case "mediumtext":
            case "text":
            case "longtext":
                switch ( input.get_internal_type() )
                {
                    case "default":
                    case "code":
                    case "password":
                    case "relation":
                        value = String( $(input.get_elem()).val() );
                        break;

                    case "textarea":
                        value = this.textarea_controller.get_data(input);
                        break;

                    case "texteditor":
                        value = this.texteditor_controller.get_data(input, options);
                        break;

                    case "color":
                        value = this.colorpicker_controller.get_data(input, options);
                        break;

                    case "signature":
                        value = JSON.stringify({
                            'data': this.signature_pad_controller.get_data(input),
                            'data_url': this.signature_pad_controller.get_data_url(input)
                        });
                        break;

                    default:
                        value = undefined;
                        console.error("acquire_input_data_single(): Unknown data-internal-type (" + input.get_internal_type() + ")", input);
                        break;
                }
                break;

            case "tinyint":
                if (length && length === 1) {
                    value = this.boolean_controller.get_data(input);
                }
                break;

            case "file":
            case "image":
                value = this.file_controller.get_data(input);
                break;

            default:
                value = String( $(input.get_elem()).val() );
                break;
        }

        switch ( input.get_display_type() ) {
            case "multiselect":
                if ( (<string>value).length === 0 )
                {
                    value = "";
                }
                break;
        }

        return {
            id: input.get_id(),
            value: value
        };
    }


    public get_inputs(): {[key: string]: Input} {
        return this.inputs;
    }

    public get_input(id: string): Input | null {
        return this.inputs.hasOwnProperty(id) ? this.inputs[id] : null;
    }



}