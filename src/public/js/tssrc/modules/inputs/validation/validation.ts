import {Input} from "../input";
import {Crud_Tools} from "../../../crud_tools";
import {Crud_Config} from "../../../crud_config";
import {Crud_Types} from "../../../crud_types";
import Input_Activator = Crud_Types.Input_Activator;
import Input_Activators = Crud_Types.Input_Activators;
import Validation_Constraints = Crud_Types.Validation_Constraints;
import {Validation_Results} from "./validation_results";
import {Validator_Results} from "./validator_results";
import Validation_Constraint = Crud_Types.Validation_Constraint;

declare const validate: any;
declare const moment: any;

export class Validation {

    /**
     * Reference to the input for which this validation is.
     */
    private input: Input;

    /**
     * The constraints used to validate the input.
     */
    private constraints: Validation_Constraints | null;

    /**
     * Constructor.
     * @param {Input} input The input
     */
    constructor(input: Input) {
        this.input = input;
    }

    public load(): void {
        this.constraints = this.load_constraints();
    }

    public init(activators?: Input_Activators) {
        // At least the input itself should trigger the input validation:
        if (typeof activators === typeof undefined) {
            const main_activator: Input_Activator = {
                'element': this.input,
                'triggers': this.acquire_validation_triggers()
            };
            activators = [];
            activators.push(main_activator);
        }

        // There is no need for any initialization if no constraints were specified.
        if ( !Crud_Tools.is_set(this.constraints) || Crud_Tools.is_empty(this.constraints) ) {
            return;
        }

        // Validate this input right away.
        this.validate();

        // Validate the input at any time one of the specified events get triggered.
        $.each(activators, function (this: Validation, key: number, activator: Input_Activator) {

            $(activator['element'].get_elem()).on(activator['triggers'], function (this: Validation) {
                this.validate();
            }.bind(this));

        }.bind(this));
    }

    /**
     * Validates the bound input.
     */
    public validate(): void {
        //TODO : It should be possible to pass options which are then to be used when field_data needs to be acquired.
        const options = {
            texteditor_format: "text"
        };

        // Acquire the value which should be validated.
        const input_data = this.input.get_instance().get_input_manager().acquire_input_data_single(this.input, options);
        if ( !input_data ) {
            console.error("validate_input(): Unable to load data from input: ", this.input, "Skipping validation...");
            return;
        }

        let value = input_data.value;

        // A "signature" can/should only be validated against the "presence" validator.
        // We therefore calculate a simple string, which specifies if the signature is empty or set.
        if ( this.input.get_internal_type() === "signature" ) {
            // 'value' must be a string then...
            value = JSON.parse(<string>value).data.length === 0 ? '' : 'set';
        }

        const validation_results = new Validation_Results();

        $.each(this.constraints, function (validator_name: string, constraint: Validation_Constraint) {
            // Constructing the "constraints" object for the current validator.
            const validator_constraints: { [elem: string]: {} } = {};
            validator_constraints[validator_name] = constraint;

            // Validating the value with the current validator and storing the result with a reference to the current
            // validator.
            validation_results.add( validator_name, new Validator_Results( validate.single(value, validator_constraints) ) );
        });

        // Update the DOM of that input.
        this.update_validation_state(validation_results);
    }

    private update_validation_state(validation_results: Validation_Results): void {
        const input_val_msg_output: HTMLElement = $("#" + this.input.get_id() + "-validation-msg-output")[0];

        let validation_succeeded = true;
        let msg = "";

        // As soon as we see a validator, which did not return 'undefined', we know that the validation did not pass.
        $.each(validation_results.get(), function(validator_name: string, validator_results: Validator_Results) {
            if ( typeof validator_results.get() !== typeof undefined ) {
                validation_succeeded = false;
            }
        });

        // Change to the "crud-validation-success" class and clear the error output.
        if ( validation_succeeded ) {
            this.input.remove_class("crud-validation-error");
            this.input.add_class("crud-validation-success");

            msg = "";
        }
        // Change to the "crud-validation-error" class and set the error output.
        else {
            this.input.remove_class("crud-validation-success");
            this.input.add_class("crud-validation-error");

            msg = "<ul>";
            $.each(validation_results.get(), function(validator_name: string, validator_results: Validator_Results) {
                $.each(validator_results.get(), function(key: Number, line: string) {
                    msg +=
                        '<li data-validator="' + validator_name + '" data-validator-result-id="' + key + '">' +
                        line +
                        "</li>";
                });
            });
            msg += "</ul>";
        }

        $(input_val_msg_output).html(msg);
    }

    private load_constraints(): Validation_Constraints | null {
        const constraints_json = $(this.input.get_elem()).attr("data-validation-constraints");

        // The attribute which specifies the constraint must exist.
        if ( !constraints_json ) {
            console.error("load_constraints(): \"data-validation-constraints\" attribute missing on:", this.input);
            return null;
        }

        // There could be an empty string. We interpret that as an empty object.
        if ( constraints_json.length === 0 ) {
            return {};
        }

        const constraints = Crud_Tools.try_parse_json(constraints_json);

        if ( !constraints ) {
            console.error(
                "load_constraints(): \"data-validation-constraints\" attribute was not JSON-parsable!", this.input
            );
            return null;
        }

        return constraints;
    }

    /* HELPER */

    public acquire_validation_triggers(): string {
        // TODO: Let the returned triggers be dependant on the inputs type.

        const test = "varchar";
        const triggers: string[] = Crud_Config.validation_triggers[test];
        let trigger_string: string = "";

        for ( let i = 0; i < triggers.length; i++ ) {
            trigger_string += triggers[i];
            if ( i !== triggers.length - 1 ) {
                trigger_string += " ";
            }
        }

        return trigger_string;
    }

}