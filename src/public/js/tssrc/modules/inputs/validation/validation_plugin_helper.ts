import {Crud_Instance} from "../../../crud_instance";
import {Crud_Tools} from "../../../crud_tools";
import {Crud_Types} from "../../../crud_types";
import Custom_Validator = Crud_Types.Custom_Validator;

declare const validate: any;
declare const moment: any;

export class Validation_Plugin_Helper {

    public static readonly STANDARD_VALIDATORS = [
        'date',
        'datetime',
        'email',
        'equality',
        'exclusion',
        'format',
        'inclusion',
        'length',
        'numericality',
        'presence',
        'url'
    ];

    public static init(instance: Crud_Instance) {
        // Before using it we must add the parse and format functions
        // Here is a sample implementation using moment.js
        validate.extend(validate.validators.datetime, {

            // The value is guaranteed not to be null or undefined but otherwise it could be anything.
            // A unix timestamp must be returned.
            parse(value: any, options: any) {
                let format = undefined;
                if ( options.dateOnly ) {
                    format = instance.get_state().moment_date_format();
                } else {
                    format =
                        instance.get_state().moment_date_format() +
                        instance.get_state().moment_date_separator() +
                        instance.get_state().moment_time_format();
                }

                const moment_obj = moment.utc(value, format);

                if ( moment_obj.isValid() ) {
                    return moment_obj.unix();
                } else {
                    return NaN;
                }
            },

            // Input is a unix timestamp.
            // The formatted date must be returned.
            format(value: any, options: any) {
                let format = "";
                if ( options.dateOnly ) {
                    format = instance.get_state().moment_date_format();
                } else {
                    format =
                        instance.get_state().moment_date_format() +
                        instance.get_state().moment_date_separator() +
                        instance.get_state().moment_time_format();
                }

                return moment
                    .utc(value)
                    .locale(instance.get_state().language_short())
                    .format(format);
            }

        });

        Validation_Plugin_Helper.clean_validators(instance);
        Validation_Plugin_Helper.init_custom_validators(instance);
        Validation_Plugin_Helper.init_custom_async_validators(instance);
    }

    public static clean_validators(instance: Crud_Instance): void {
        // Remove every non-standard function from the validators object.
        $.each(validate.validators, function (validator_name: string, validator: any) {
            if ( !Crud_Tools.in_array(validator_name, this.STANDARD_VALIDATORS) ) {
                delete validate.validators[validator_name];
            }
        });
    }

    public static init_custom_validators(instance: Crud_Instance): void {
        // Add custom validator functions.
        $.each(
            instance.get_state().custom_validators(),
            function (this: Validation_Plugin_Helper, name: string, validator: Custom_Validator) {

                validate.validators[name] = function(
                    this: Validation_Plugin_Helper, value: any, options: any, key: any, attributes: any
                ) {
                    return Crud_Tools.execute_function_by_name(
                        validator.js_function_name, window, value, options, key, attributes
                    );
                }.bind(this);

            }.bind(this)
        );
    }

    public static init_custom_async_validators(instance: Crud_Instance): void {
        // TODO: design and implement
    }


}