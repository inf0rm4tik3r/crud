import {Validator_Results} from "./validator_results";

type Validation_Results_Object = {
    [key: string]: Validator_Results
}

export class Validation_Results {

    private results: Validation_Results_Object = {};

    constructor() {
    }

    public add(validator_name: string, validator_results: Validator_Results): void {
        this.results[validator_name] = validator_results;
    }

    public get(): Validation_Results_Object {
        return this.results;
    }

}