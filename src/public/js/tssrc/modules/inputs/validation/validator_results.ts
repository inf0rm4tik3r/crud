type Validator_Results_Object = string[] | undefined;

export class Validator_Results {

    private results: Validator_Results_Object = [];

    constructor(results?: Validator_Results_Object) {
        this.set(results);
    }

    public set(results: Validator_Results_Object): void {
        this.results = results;
    }

    public get(): Validator_Results_Object {
        return this.results;
    }

}