<?php

declare(strict_types = 1);

use Illuminate\Support\Facades\Route;

Route::group(
    [
        //'prefix' => 'crud',
        'middleware' => 'web',
        //'namespace' => 'Lukaspotthast\Crud\Frameworks\Laravel'
    ],
    function () {
        Route::post('/crud/{route_name}', 'Lukaspotthast\Crud\Frameworks\Laravel\Crud_Controller@handle');
    }
);