<?php

/*
 |--------------------------------------------------------------------------
 | CRUD Settings
 |--------------------------------------------------------------------------
 |
 | These settings specify the default configuration of a crud instance.
 | Most options can be individually specified for each instance.
 |
 */

return [

    /*
     |--------------------------------------------------------------------------
     | Instance compression
     |--------------------------------------------------------------------------
     |
     | The system can compress an instance object to binary code and store this
     | data into the session. The instance will then later be uncompressed
     | to be accessible. This will reduce the space taken up in the session
     | of about factor 5!
     | Use values [1, ... , 9] for the "compression_level" configuration.
     | Where 1 represents the lowest and 9 the highest compression level.
     |
     | Supported: (boolean) true / false
     |            (int) [1, ..., 9]
     | Default: true, 6
     |
     */

    'use_compression' => true,
    'compression_level' => 3,

    /*
     |--------------------------------------------------------------------------
     | Lode assets...
     |--------------------------------------------------------------------------
     |
     | Specify which assets should be loaded by the crud system.
     | Set each plugin/library to false, which already gets loaded by your site.
     |
     | The crud system always tries to be compatible with the latest version of
     | any library/plugin, and ship that latest version.
     |
     | You always have the ability to query if an asset should be autoloaded by
     | calling Asset::should_be_autoloaded('some_identifier').
     |
     | You can execute calls like: Asset::get('crud.css') in your main
     | template header to get the html inclusion statements for the specified
     | element. If done so, do not forget to deactivate the corresponding
     | boolean value in the array below!!
     |
     | Supported: (array(string => boolean))
     | Default: false for all
     |
     */

    'load_asset' => [
        /*
         * Config values cannot contain dots, but calling for example:
         * Asset::should_be_autoloaded('crud.css') will do just fine.
         */
        'crud'           => false,
        'crud_minified'  => false,

        'bootstrap3'     => false,
        'bootstrap4'     => false,
        'ckeditor'       => false,
        'copybtn'        => false,
        'datetimepicker' => false,
        'fontawesome'    => false,
        'highlight_js'   => false,
        'jcrop'          => false,
        'jquery'         => false,
        'jqueryui'       => false,
        'moment_js'      => false,
        'popper_js'      => false,
        'select2'        => false,
        'signature-pad'  => false,
        'spectrum'       => false,
        'tinymce'        => false,
        'validate_js'    => false,
    ],

    /*
     |--------------------------------------------------------------------------
     | Custom wrapper classes
     |--------------------------------------------------------------------------
     |
     | Each crud instance gets wrapped in a div-element.
     | This div always gets the 'crud-instance-wrapper' class, which will apply
     | all styling to the instance.
     | If you want your own class (or classes) to be attached to these wrappers,
     | maybe to add your own styling, feel free to add them here.
     |
     | Supported: (array(string))
     | Example: ['my-class', 'my-second-class']
     | Default: []
     |
     */

    'custom_wrapper_classes' => [],

    /*
     |--------------------------------------------------------------------------
     | Color blindness
     |--------------------------------------------------------------------------
     |
     | Determine whether you want the color-blindness corrections to be applied.
     |
     | Supported: (boolean) true / false
     | Default: true
     |
     */

    'enable_color_blindness_corrections' => false,

    /*
     |--------------------------------------------------------------------------
     | Primary auto incrementing column name
     |--------------------------------------------------------------------------
     |
     | Specify which name the primary ai columns of your tables have.
     | Use any non empty string.
     | Use the set_id_column_name(string) function to set the column name
     | for a specific instance.
     |
     | Supported: (string)
     | Default: 'id'
     |
     */

    'id_column_name' => 'id',

    /*
     |--------------------------------------------------------------------------
     | Asynchronous loading
     |--------------------------------------------------------------------------
     |
     | All crud instances can get "lazy loaded". This means that an instance
     | does not get fully rendered (which includes accessing the database,
     | generating multiple views, etc.) until the browser completely processed
     | the page (DOM) in which the instance is located.
     |
     | If you activate this option, each call to an instances render() function
     | will just return a "loader". After the web-page got fully computed by
     | the browser, an ajax call will fetch the final rendering from the server.
     | You do not need to do everything for this option to work!
     |
     | Activating this feature will speed up the general loading time of your
     | pages, and is a viable option for slower servers, really complex instances
     | or pages in which many crud instances must be rendered.
     | But in doing so, "display-latency" (how much time does it take until the
     | user can access the crud instance) increases and you will visually see
     | the crud instances pop up on your page.
     | But most important: the cost of the additional request!
     | Is it worth the request overhead to deffer that ~ 20ms - 50ms rendering?
     |
     | Nested instance typically do not need to be accessed that fast, and
     | lazy loading them increases the overall responsiveness for the user when
     | going into the 'view' or 'edit' mode.
     |
     | Supported: (boolean) true / false
     | Default: false, true
     |
     */

    'use_asynchronous_loading' => false,
    'use_asynchronous_loading_for_nested_instances' => true,

    /*
     |--------------------------------------------------------------------------
     | Permissions
     |--------------------------------------------------------------------------
     |
     | Specify what should be allowed (true) or forbidden (false) in each
     | instance by default.
     |
     | Supported: (array(string => boolean))
     | Default: true for all
     |
     */

    'permissions' => [
        'list'       => true,
        'create'     => true,
        'view'       => true,
        'edit'       => true,
        'save'       => true,
        'delete'     => true,
        'structure'  => true,
        'print'      => true,
        'export'     => true,
        'search'     => true,
        'order_by'   => true
    ],

    /*
     |--------------------------------------------------------------------------
     | Visibility
     |--------------------------------------------------------------------------
     |
     | ...
     |
     | Supported: (array(string => boolean))
     | Default: true for all
     |
     */

    'visibility' => [
        'title'               => true,
        'entry_info'          => true,
        'save_btn'            => true,
        'save_and_return_btn' => true,
        'save_and_new_btn'    => true,
        'delete_btn'          => true,
        'list_btn'            => true,
        'limits'              => true,
        'pagination'          => true
    ],

    /*
     |--------------------------------------------------------------------------
     | Order by mode
     |--------------------------------------------------------------------------
     |
     | Specify how the user will be able to interact with columns to sort by
     | them:
     |   'single':
     |       Instances only allow sorting by one column.
     |   'multi':
     |       Instances allow sorting by multiple columns.
     |       Each click on a column header changes the sorting for that column.
     |       Possible sort states: not sorted, ascending, descending.
     |   'multi_with_cmd':
     |       Same as 'multi', but sorting by an additional column only applies
     |       when holding down the 'Strg' modifier while clicking the column.
     |
     | Supported: (string) 'single', 'multi', 'multi_with_cmd'
     | Default: 'multi_with_cmd'
     |
     */

    'order_by_mode' => 'multi_with_cmd',

    /*
     |--------------------------------------------------------------------------
     | Allowed order by columns
     |--------------------------------------------------------------------------
     |
     | Specify column names in an array, for which ordering by them should
     | generally be allowed / not allowed (based on the boolean you specified
     | for each column name).
     |
     | Supported: (array)
     | Example: ['name' => false] // Disables ordering for any 'name' column!
     | Default: []
     |
     */

    'allowed_order_by_columns' => [],

    /*
     |--------------------------------------------------------------------------
     | Soft delete
     |--------------------------------------------------------------------------
     |
     | Specify if the soft-delete functionality should be active by default.
     | Use true / false.
     | Call the use_soft_delete(bool) function to set the active state of soft-
     | delete for a specific instance.
     | When using the soft-delete mechanism, entries can not get deleted from
     | the database. A 'deleted' flag for an entry will be set to true instead.
     | This will then allow the user to 'restore' previously deleted entries
     | with a single click.
     | Please check the other configuration values regarding the soft delete
     | functionality ('soft_delete_column' and 'show_deleted_entries').
     |
     | Supported: (boolean) true / false
     | Default: false
     |
     */

    'use_soft_delete' => false,

    /*
     |--------------------------------------------------------------------------
     | Soft delete column
     |--------------------------------------------------------------------------
     |
     | The name of your table columns which will represent the deleted status of
     | an entry. Use any non empty string.
     | Use set_soft_delete_column(string) on an instance to specify this option
     | for a specific instance.
     |
     | Supported: (string)
     | Default: 'deleted'
     |
     */

    'soft_delete_column' => 'deleted',

    /*
     |--------------------------------------------------------------------------
     | Show deleted entries
     |--------------------------------------------------------------------------
     |
     | Determines if soft deleted entries will be shown in the list view.
     | Use show_deleted_entries(bool) on an instance to specify this option
     | for a specific instance.
     |
     | Supported: (boolean) true / false
     | Default: true
     |
     */

    'show_deleted_entries' => true,

    /*
     |--------------------------------------------------------------------------
     | Texteditor driver
     |--------------------------------------------------------------------------
     |
     | The default texteditor which should be used in a crud instance.
     | Please check that you have the editors resources loaded in your site
     | through one of:
     |     <?= Asset::get_if_not_autoloaded('tinymce') ?>
     |     <?= Asset::get_if_not_autoloaded('ckeditor') ?>
     |
     | Supported: (string) 'tinymce', 'ckeditor'
     | Default: 'tinymce'
     |
     */

    'texteditor_driver' => 'tinymce',

    /*
     |--------------------------------------------------------------------------
     | Texteditor driver options
     |--------------------------------------------------------------------------
     |
     | Currently unused...
     |
     */

    'texteditor_driver_options' => [
        'tinymce' => [],
        'ckeditor' => []
    ],

    /*
     |--------------------------------------------------------------------------
     | Select driver
     |--------------------------------------------------------------------------
     |
     | The default colorpicker plugin which should be used in a crud instance.
     |
     | Supported: (string) 'select2'
     | Default: 'select2'
     |
     */

    'select_driver' => 'select2',

    /*
     |--------------------------------------------------------------------------
     | Colorpicker driver
     |--------------------------------------------------------------------------
     |
     | The default colorpicker plugin which should be used in a crud instance.
     |
     | Supported: (string) 'spectrum'
     | Default: 'spectrum'
     |
     */

    'colorpicker_driver' => 'spectrum',

    /*
     |--------------------------------------------------------------------------
     | Success message after save
     |--------------------------------------------------------------------------
     |
     | Determine whether you want a success message to be shown after an entry
     | got successfully saved to the database.
     |
     | Supported: (boolean) true / false
     | Default: true
     |
     */

    'render_success_message_after_save' => true,

];